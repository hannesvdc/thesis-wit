%\documentclass{beamer}
\documentclass[kul]{kulakbeamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{framed}
\usepackage{listings} 
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{algorithm,algpseudocode}
\usepackage{pgfpages}
\usepackage{sidecap}
\usepackage{appendixnumberbeamer}
\usepackage[backend=biber]{biblatex}
\bibliography{december.bib}
\addtobeamertemplate{footnote}{}{\vspace{2ex}}

\captionsetup{font=scriptsize,labelfont=scriptsize}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\title[Micro-macro acceleration]{Efficiency of micro-macro acceleration for scale-separated SDEs}
\author{Hannes Vandecasteele, Przemys{\l}aw Zieli{\'n}ski, Giovanni Samaey}
\institute[Kulak]{KU Leuven}
\date{September 6th, 2018}

 \setbeamertemplate{navigation symbols}
{ \hspace{1em}  
	\usebeamerfont{footline}
	\insertframenumber/\inserttotalframenumber }
\setcounter{page}{1} 
\pagenumbering{arabic} 

\begin{document}
%\begin{titleframe}
%	\titlepage
%\end{titleframe}
\begin{titleframe}
\maketitle
\end{titleframe}

\begin{frame}
	\frametitle{Importance of Stochastic Differential Equations}
	\begin{minipage}{0.7\textwidth}
%		\begin{center}
			Many processes (nuclear fusion, molecular dynamics, \dots)  are modelled using SDEs
%		\end{center}
    \end{minipage}%
	\hfill%
	\begin{minipage}{0.25\textwidth}
		\begin{figure}
			\includegraphics[width=\textwidth]{kinetic_gas_model}
		\end{figure}
	\end{minipage}%
	
	\[
	dX(t) = a(t, X(t))dt + b(t, X(t))dW(t).
	\]
	In practice, we are interested in expectations of the process, $\mathbb{E}[R_l(X(t))]$. A closed model for $\mathbb{E}[R_l(X(t))]$ is often not available.
\begin{figure}
	\begin{subfigure}[b]{0.38\textwidth}
		\centering
		\input{particlemovement.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.1\textwidth}
		\centering
		\includegraphics[width=\textwidth]{arrow}
		
		\vspace{13mm}
	\end{subfigure}%
	\begin{subfigure}[b]{0.38\textwidth}
		\centering
		\input{varfigurecopy.tex}
	\end{subfigure}
\end{figure}

\vspace{4mm}
\end{frame}

\begin{frame}
\frametitle{Many practical problems are stiff}

\begin{itemize}
\item Due to an inherent time-scale separation with $\epsilon < 1$
\begin{align*}
dX(t) &= (Y(t)-X(t))dt + dW_X(t) \\
dY(t) &= -\frac{1}{\epsilon}Y(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_Y(t)
\end{align*}

\item or due to strict boundary conditions
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
\begin{align*}
dX &= -F(X)dt+dW, \ F(x) &= \frac{bx}{b-x^2}
\end{align*}

\vspace{2mm}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\centering
	\includegraphics[width=0.3\textwidth]{spring}
\end{subfigure}
\end{figure}
\end{itemize}

We present micro-macro acceleration to overcome the problem of stiffness and show its efficiency.
\note{Other methods: implicit methods, approximate models, extended stability domains, ...}
\end{frame}

\begin{frame}
\frametitle{General four-step coarse acceleration algorithm}
Coarse projective integration:\footnotemark
\vspace{0.5mm}
\hline
\footnotetext[1]{Kevrekidis, Samaey, 2009}

\vspace{2mm}
\begin{algorithmic}
		\caption*{Coarse projective integration}
		\noindent
		\textbf{(i) Burst:}  Perform microscopic simulation with step $\delta t$ and ensemble $\mathcal{X}^{n,k}$ at time $t=n\Delta t+k\delta t$, $k = 1,\dots K$. \newline
		\textbf{(ii) Restriction:} Record moments of interest
		\[
			m^{n,k}=\mathbb{E}[R_l(\mathcal{X}^{n,k})], \ l= 1 \dots L
		\]
		\textbf{(iii) Extrapolation:} Extrapolate these moments over $\Delta t$
		\[
		m_l^{n+1} = m_l^n+\frac{\Delta t}{\delta t}(m_l^{n,K}-m_l^n)
		\]
		\textbf{(iv) Lifting: } Find a new micro state consistent with $m_l^{n+1}, \ l=1,\dots,L$ and go back to (i).
\end{algorithmic}
\end{frame}

\begin{frame}
\frametitle{Lifting has some disadvantages}
Coarse projective integration with \textit{lifting} converges to the exact solution when separation $\epsilon$ tends to zero.

\vspace{2mm}

\begin{itemize}
	\item Modelling error with finite scale separation \note{convergence when $\epsilon$ tends to zero}
	\item Finding microscopic state or distribution can be costly \note{example: bistable system}
	\item Extra computational steps needed for constrained simulation for better numerical results \footnotemark \note{if far from conditional equilibrium}
	\footnotetext{Samaey, Leli{\`e}vre, Legat, 2010}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Replace lifting by \textit{matching}}
Find the distribution that is consistent with the extrapolated moments $m^{n+1}$ but deviates the least from the prior distribution at time $n\Delta t+K \delta t$. \footnotemark
\footnotetext[3]{Debrabant, Zieli{\'n}ski, Samaey, 2017}
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{acceleration}
	\label{fig:acceleration}
\end{figure}

\end{frame}

\begin{frame}
	\frametitle{We use matching in relative entropy}
	Minimize the Kullback-Leibler divergence with prior $\pi(x)$:
	\[
	\varphi = \underset{\varphi \in \mathcal{R}_L^{-1}(m)}{\text{arg min}}\int_G \ln{\left(\frac{\varphi(x)}{\pi(x)}\right)} \varphi(x) dx \ \text{s.t.} \ \ \mathbb{E}[R_l(\varphi)] = m_l
	\]
	\begin{itemize}
		\item Simple matching formula: $\varphi = \exp \left(\sum_{l=0}^L \lambda_l R_l\right) \pi$
		\item Lagrange multipliers: $\int_G R_l \exp \left(\sum_{k=0}^L \lambda_k R_k\right) \pi  dx = m_l$
		\item Reweighing: $w_j^{n+1} = \exp \left(\sum_{k=0}^L \lambda_k R_k\right)  w_j^n$
		\item Analytical formulas for matching with Gaussian prior
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{We focus on stiff systems with time-scale separation}
	A thorough \textit{efficiency analysis} of a general slow-fast system
	\begin{equation*}
	\begin{aligned}
	dX &= f(X,Y)dt + A(X,Y)dW_X \\
	dY &= \frac{1}{\varepsilon}g(X,Y)dt+\frac{1}{\sqrt{\varepsilon}}B(X,Y)dW_Y \\
	\end{aligned}
	\end{equation*}
	consists of
	\begin{itemize}
		\item proving convergence when $\delta t, \ \Delta t \to 0$ and $L \to \infty$ already shown
		\item a new convergence theorem and stability for linear systems
		\item adequate a priori macroscopic state selection
		\item a study of the accuracy of the extrapolation step $\Delta t$
	\end{itemize}
\end{frame}


% Overzicht bij het begin van elk hoofdstuk 
\AtBeginSection[]{\only<beamer>{\addtocounter{framenumber}{-1}
		\begin{outlineframe}[Overview]
			\tableofcontents[currentsection]
	\end{outlineframe}}
}

%\section{A new convergence result}
%\begin{frame}
%	\frametitle{Convergence with slow-mean extrapolation and Gaussian initial}
%	Works for linear systems of the form $dX = AXdt + BdW$
%	\begin{theorem} \label{thm:linearslowconvergence}
%		Given a Gaussian initial and extrapolation contains only the slow-mean. Fix the end time $T > 0$. Denote by $\mathbf{X}^t$ the exact solution of the SDE at time $t \in [0,T]$, and by $\mathbf{X}^{n_{\Delta t}(t)}$ the solution obtained by the micro-macro acceleration scheme at that same time, where $n_{\Delta t}(t) = t/\Delta t$. Then the micro-macro acceleration distribution converges uniformly to the exact distribution as $\delta t$, and $\Delta t$ decrease to 0.
%	\end{theorem}
%	\begin{itemize}
%		\item The proof relies on formulas of relative-entropy matching with Gaussian prior
%		\item A generalisation would be of great importance
%	\end{itemize}
%\end{frame}	
%
%\section{Influence of moment selection}
%\begin{frame}
%	\frametitle{Two slow moments are adequate for linear SDEs}
%		The state variables have a significant impact on the accuracy of the micro-macro acceleration scheme. 
%		\begin{align*}
%		dX(t) &= (Y(t)-X(t))dt + dW_x(t) \\
%		dY(t) &= -\frac{1}{\epsilon}Y(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_y(t) 
%		\end{align*}
%		For a linear system two slow moments are enough:
%		\begin{itemize}
%			\item One slow moment gives inaccurate variance
%			\item More than two moments can give non-Gaussian results
%			\item Fast moments settle to conditional equilibrium
%			\item Fast moments give stability concerns
%		\end{itemize}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Two slow moments are accurate}
%	\begin{figure}
%	\begin{subfigure}[b]{0.5\textwidth}
%		\centering
%		\input{slowmeanLx.tex}
%		\caption{Slow mean}
%	\end{subfigure}%
%	\begin{subfigure}[b]{0.5\textwidth}
%		\centering
%		\input{slowvarianceLx.tex}
%		\caption{Slow variance}
%	\end{subfigure}
%	\end{figure}
%		\begin{itemize}
%	\item One slow moment gives inaccurate variance
%	\item More than two moments can give non-Gaussian results
%	\item Fast moments settle to conditional equilibrium
%	\item Fast moments give stability concerns
%\end{itemize}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Adequate state selection for FENE-dumbbells}
%
%	A priori macroscopic state selection for non-linear problems is harder. Here we work with FENE-dumbbells:
%	\[
%	dX(t) = (\kappa(t)X(t) - \frac{1}{2W_e}\frac{b}{b-\norm{X(t)}^2}X(t))dt + \frac{1}{\sqrt{W_e}}dW(t)
%	\]
%	where the quantity of interest is the stress tensor
%	\[
%	\tau = \frac{1}{W_{\text{e}}} \left( \mathbb{E}[X F(X)] - 1\right).
%	\]
%\end{frame}
%
%\begin{frame}
%	\frametitle{Hysteresis curve with time-dependent $\kappa$}
%	When $\kappa(t) = 100t(1-t)e^{-t}$, the $\tau-M_1$ curve is \footnotemark
%	\footnotetext[4]{Samaey, Leli{\`e}vre, Legat, 2011}
%	\begin{figure}
%		\centering
%		\input{phasediagramhysteresis.tex}
%	\end{figure}
%	where $M_1 = \mathbb{E}[X^2]$. We will look at three different state hierarchies.
%\end{frame}
%\begin{frame}
%	\frametitle{Moments as state variables}
%	Consider as moment functions $R_l(x) = x^{2l}, l = 1, \dots, L$.
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.8\linewidth]{strategy1factor5}
%		\label{fig:strategy1factor5}
%	\end{figure}
%	
%\end{frame}
%
%\begin{frame}
%	\frametitle{Adding the stress tensor improves the fit}
%	Now take $R_l(x) = x^{2l}, l = 1, \dots, L-1$ and $R_L(x) = \tau(x)$
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.8\linewidth]{strategy2factor5}
%		\label{fig:strategy2factor5}
%	\end{figure}
%	
%\end{frame}
%
%\begin{frame}
%	\frametitle{The terms in the Taylor expansion give the best fit}
%	Take as states the terms in the evolution equation of the stress $\tau$, by It{\^o}'s law.
%	Here $R_1(x)=x^2, \ R_2(x)=\frac{x^2}{1-x^2/b}-1, R_3(x)=\frac{x^2}{(1-x^2/b)^2}, R_4(x)=\frac{x^4}{(1-x^2/b)^3}$
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.7\linewidth]{strategy3factor5}
%		\label{fig:strategy3factor5}
%	\end{figure}
%\end{frame}

\section{A periodically driven linear system}
\begin{frame}
	\frametitle{A first example: linear driven system}
	We now focus on a simple linear driven SDE
	\begin{equation*} \label{eq:lineardrivensde}
	\begin{cases}
	dX = -2(X+Y)dt + 10\sin(2\pi t)dt + dW_x \\
	dY = \frac{1}{\varepsilon}(X-Y)dt + \frac{1}{\sqrt{\varepsilon}}dW_y,
	\end{cases}
	\end{equation*}
	where, by averaging, the process
	\begin{equation*}
	d\overset{\_}{X} = -4\overset{\_}{X}dt + 10\sin(2\pi t)dt + dW
	\end{equation*}
	approximates the slow variable well when $\varepsilon$ is small.
	
	\vspace{2mm}
	We now look at the accuracy of micro-macro acceleration for a varying $\varepsilon$ and $\Delta t$, measured in $L_2$ norm over 1 period.
\end{frame}

\begin{frame}
	\frametitle{General theorem for the macroscopic model}
	\footnotetext[4]{Pavliotis, Stuart, 2008}
	\begin{theorem}
	Given a general system
	\begin{equation*} \label{eq:ch6sdesystem}
	\begin{aligned}
	dX &= f_1(X,Y)dt + \alpha(X,Y)dW_x(t), \ \ X(0) = X_0 \\
	dY &= \frac{1}{\varepsilon} g(X,Y)dt + \frac{1}{\sqrt{\varepsilon}} \beta(X,Y) dW_y(t), \ \ Y(0) = Y_0
	\end{aligned}
	\end{equation*}
	
	\vspace{-2mm}
	The solution of the approximate macroscopic SDE
	\begin{equation*} \label{eq:ch6macro}
	d\overset{\_}{X} = F(\overset{\_}{X})dt + A(\overset{\_}{X})dW, \ \ \overset{\_}{X}(0) = X_0 
	\end{equation*}
	
	\vspace{-2mm}
	converges weakly to  the exact solution as $\varepsilon$ decreases to $0$.
\end{theorem}
The theorem makes a modelling error when $\varepsilon$ is finite.
\end{frame}

\begin{frame}
\frametitle{The approximate model fits better with small $\varepsilon$}
The unknowns $F(x)$ and $A(x)$ are defined by
\begin{equation*}
\begin{aligned}
F(x) &= \int_Y f_1(x,y) \rho^{\infty}(y;x) dy \\
A(x)A(x)^T &= \int_Y \alpha(x,y) \alpha(x,y)^T \rho^{\infty}(y;x)dy.
\end{aligned}
\end{equation*}
For the linear driven system, the error decreases linearly in $\varepsilon$:

\vspace{-6mm}

\begin{figure}
	\centering
	\hspace*{-0.5cm}
	\begin{subfigure}[b]{0.37\textwidth}
		\centering
		\input{meanseps05.tex}
		\caption{$\varepsilon=0.5$}
	\end{subfigure}%
	\begin{subfigure}[b]{0.37\textwidth}
		\centering
		\input{meanseps005.tex}
		\caption{$\varepsilon=0.05$}
	\end{subfigure}%
	\begin{subfigure}[b]{0.37\textwidth}
		\centering
		\input{macroerror.tex}
		\caption{      }
	\end{subfigure}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Micro-macro with mean beats the approximate model}
	When $\delta t$ decreases linearly with $\varepsilon$ due to stability, the maximal extrapolation step $\Delta t = M \ \delta t = M \mathcal{O}(\varepsilon)$ decreases slower!
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{crossovercurves.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{maximalDt.tex}
		\end{subfigure}
	\end{figure}
	The micro-macro acceleration algorithm can hence be more accurate than the approximate macroscopic model, while taking larger time steps than a microscopic simulation.
\end{frame}

\section{Bimodal problem}
\begin{frame}
	\frametitle{The double-well model problem}
	\begin{equation*} \label{eq:ch6doublewellsde}
	\begin{aligned}
	dX &= -(2X+Y)dt + AdW_x \\
	dY &= \frac{1}{\varepsilon}(Y-Y^3)dt + \frac{1}{\sqrt{\varepsilon}} dW_y = -\frac{1}{\varepsilon}\nabla V(Y)dt + \frac{1}{\sqrt{\varepsilon}} dW_y.
	\end{aligned}
	\end{equation*}
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{potential.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{density.tex}
		\end{subfigure}
	\end{figure}
	This system fits in the framework of the previous theorem, so that the approximate macroscopic model reads
	\[
	d\overset{\_}{X} = -2\overset{\_}{X}dt + AdW_x.
	\]
	
	\vspace{4mm}                        	
\end{frame}

\begin{frame}
	\frametitle{The approximate model is inaccurate for moderate $\varepsilon$}
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\input{particleeps0.1text.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\input{particleeps0.01text.tex}
		\end{subfigure}
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\input{varfigureeps01A01.tex}
		\end{subfigure}
		\begin{subfigure}[b]{0.4\textwidth}
			\centering
			\input{histogramfigureeps01A01.tex}
		\end{subfigure}
	\end{figure}
	
	\vspace{2mm}
\end{frame}

\begin{frame}
	\frametitle{The micro-macro scheme removes the modelling error}
	Use the first and second moments of $X$ as states. The transient error however increases with $\Delta t$.
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{varfigure.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{histogramfigure.tex}
		\end{subfigure}
	\end{figure}
\end{frame}

%\begin{frame}
%	\frametitle{Second example: a bimodal model}
%	\begin{equation*} 
%	\begin{aligned}
%	dX &= -(2X+Y)dt + AdW_x \\
%	dY &= \frac{1}{\varepsilon}(Y-Y^3)dt + \frac{1}{\sqrt{\varepsilon}} dW_y.
%	\end{aligned}
%	\end{equation*}
%	with approximate macroscopic model for the slow variable
%	\[
%	dX = -2Xdt+AdW
%	\]
%	\begin{figure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\input{varfigure.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\input{histogramfigure.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}

\section{The tri-atom molecule}
\begin{frame}
	\frametitle{Molecular dynamics: a tri-atom molecule}
\begin{minipage}{0.65\linewidth}
	\begin{align*}
	dx_a = -\frac{\partial V}{\partial x_a} dt + \sqrt{2 \beta^{-1}}dW_{x_a} \\
	dx_c = -\frac{\partial V}{\partial x_c} dt + \sqrt{2 \beta^{-1}} dW_{x_c} \\
	dy_c = -\frac{\partial V}{\partial y_c} dt + \sqrt{2 \beta^{-1}} dW_{y_c}
	\end{align*}
\end{minipage}%
\hfill%
\begin{minipage}{0.30\linewidth}
	\begin{center}
		\begin{tikzpicture}
		\draw[fill=yellow!15] (0cm,0cm) circle(0.3cm);
		\draw[fill=yellow!15] (2cm,0cm) circle(0.3cm);
		\draw[fill=yellow!15] (1cm,2cm) circle(0.3cm);
		\draw (0.3,0) -- (1.7,0);
		\draw (0.13,0.26) -- (0.87,1.74);
		\node[text width=1cm] at (0.35,0) 
		{B};
		\node[text width=1cm] at (2.35,0) 
		{A};
		\node[text width=1cm] at (1.35,2) 
		{C};
		\node[text width=1cm] at (2.35,-0.5) 
		{$x_a$};
		\node[text width=1cm] at (1.9,2) {$(x_c,y_c)$};
		\draw [domain=0:62] plot ({1.2*cos(\x)}, {1.2*sin(\x)});
		\node[text width=1cm] at (1.7,1) {$\theta$};
		\end{tikzpicture}
	\end{center}
\end{minipage}%

\vspace{2mm}
	The potential energy $V(x_a, x_c, y_c)$ is given by
	\begin{equation*} \label{eq:triatompotential}
	V(x_a, x_c, y_c) = \frac{1}{2\varepsilon}(x_a-l_{\text{eq}})^2 + \frac{1}{2\varepsilon}(\sqrt{x_c^2+y_c^2}-l_{\text{eq}})^2 + W(\theta),
	\end{equation*}
	with $\beta = 1/T$ the inverse temperature, and
	\begin{equation*}
	W(\theta) = \frac{k}{2}((\theta - \theta_{\text{saddle}})^2 - \delta \theta^2).
	\end{equation*}
	\footnotetext[4]{Legoll, Leli{\`e}vre, 2010}
\end{frame}

\begin{frame}
	\frametitle{Many possible approximate macroscopic models}
	For overdamped Langevin dynamics: define a reaction coordinate $\xi(x_a,x_c,y_c)$, the approximate model then is \footnotemark
		\footnotetext[4]{Legoll, Leli{\`e}vre, 2010}
	\[d\xi = b(\xi)dt + \sqrt{2\beta^{-1}} \sigma(\xi)dW \]
	where 
	\begin{equation*}
	\begin{aligned}
	b(z) &= \int(-\nabla V(\mathbf{x}) \cdot \nabla(\mathbf{x}) \xi+ \beta^{-1} \triangle \xi(\mathbf{x}) ) \Psi_{\infty}(\mathbf{x}) \delta_{\xi(\mathbf{x})=z}  d\mathbf{x} \\ \sigma^2(z) &= \int |\nabla \xi(\mathbf{x})|^2 \Psi_{\infty}(\mathbf{x})\delta_{\xi(\mathbf{x})=z} d\mathbf{x},
	\end{aligned}
	\end{equation*}
	where $\Psi_{\infty}(x_a,x_c,y_c)$ is the invariant distribution. The approximate macroscopic model converges path-wise to the exact dynamics. Now take $\xi_1=\theta$ and $\xi_2=\norm{A-C}^2$.
\end{frame}

\begin{frame}
	\frametitle{Micro-macro acceleration removes the error in $\xi_2$}
	Micro-macro acceleration makes no modelling error for large $\Delta t$, while the approximate macroscopic model with reaction coordinate $\xi_2$ does.
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{thetameanmmangle.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{distancemeanmmangle.tex}
		\end{subfigure}%
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Extrapolating $\xi_2$ makes no modelling error}
	Micro-macro acceleration also removes the modelling error in $\xi_2$ when extrapolating this reaction coordinate.
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{thetameanmmdistance.tex}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\centering
			\input{distancemeanmmdistance.tex}
		\end{subfigure}
	\end{figure}
	The micro-macro acceleration scheme takes larger time steps than the microscopic model, with  a smaller error than the approximate macroscopic model.
\end{frame}

%\begin{frame}
%	\frametitle{Extrapolating the first two moments of $x_c$ and $y_c$}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetamean022.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetahistogram022.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancemean022.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancehistogram022.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}

\section{Conclusion}
\begin{frame}
	\frametitle{Conclusion}
%	\begin{itemize}
%		\item a new convergence result of micro-macro for linear SDE
%		\item reproduction of stability on linear systems with additive noise
%		\item an adequate macroscopic state selection for both a linear and non-linear case
%		\item that the micro-macro acceleration scheme can take larger time steps and still be more accurate than approximate macroscopic models
%		\item a similar conclusion on two practical examples
%	\end{itemize}
\begin{itemize}
	\item Discussion of micro-macro acceleration for scale-separated SDEs
	\item Micro-macro acceleration allows for larger time steps while attaining a lower error than the approximate macroscopic model
	\item For the bimodal and tri-atom model, the steady-state error is removed with larger time steps than the microscopic simulation
\end{itemize}
\end{frame}

%\begin{frame}
%	\frametitle{Possible further research directions}
%	\begin{itemize}
%		\item Is there convergence to slow dynamics with only slow moments?
%		\item Quantify the statistical error of a finite number of particles
%		\item Devise an adaptive state and time step selection, based on accuracy
%		\item Expand the domain of applications
%	\end{itemize}
%\end{frame}

\begin{frame}[c]
\begin{center}
	\Huge Questions?
\end{center}
\end{frame}










\appendix

%\begin{frame}
%	\frametitle{Many possible optimization criteria}
%	\note{Optimization with equality constraints}
%	Find matched density $\phi$ that is consistent with extrapolated macroscopic moments $m_l$ and deviates the least from the prior microscopic ensemble.
%	
%	\begin{itemize}
%		\item $L_2$- norm: \ $\text{arg min} \ \ \frac{1}{2} \norm{\phi - \pi}_2^2  \ s.t. \ \ \mathcal{R}_L(\phi) = m$
%		
%		\item $f$-divergence: \  $\text{arg min}_{\phi} \mathcal{I}_f(\phi|\pi) \ \ s.t. \ \ \mathcal{R}_L(\phi) = m$
%	\end{itemize}
%	\vspace{2mm}
%	with $\mathcal{I}(\phi|\pi) = \int_G f\left(\frac{\phi(x)}{\pi(x)}\right) \pi(x) dx$.
%	\note{f-divergence has many applications in information theory and machine learning.}
%	
%	\vspace{8mm}
%	Special cases:
%	\begin{itemize}
%		\item Kullback-Leibler divergence (KLD): \ $f(t) = t \ln(t) - t + 1$
%		\item L2D matching: \ $f(t) = (t-1)^2$.
%	\end{itemize}
%\end{frame}
%
%\begin{frame}
%	\frametitle{KLD matching is superior}
%	This thesis focusses on KLD-matching
%	
%	\[
%	\text{arg min}_{\phi}\int_G \ln{\left(\frac{\phi(x)}{\pi(x)}\right)} \pi(x) dx
%	\]
%	
%	\begin{itemize}
%		\item Simple matching formula: $\phi = \exp \left(\sum_{l=0}^L \lambda_l R_l\right) \pi$
%		
%		\item Lagrange multipliers: $e^{\lambda_0} \int_G R_l \exp \left(\sum_{k=0}^L \lambda_k R_k\right) \pi  dx = m_l$
%		\item Reweighing: $w(j) = \exp \left(\sum_{k=0}^L \lambda_k R_k\right)  w(j)$
%		\item Analytical results for matching with a Gaussian prior
%	\end{itemize}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Illustration: FENE Dumbbells}
%	Model for a dilute polymer solution. Polymers experience a spring force $F(x)$ and undergo Brownian motion
%	\[
%	F: B(\sqrt{b}) \to \mathbb{R}^d, x \mapsto F(x) = \frac{b}{b-\norm{x}^2}x
%	\]
%	Stochastic model:
%	\[
%	dX(t) = (\kappa(t)X(t) - \frac{1}{2W_e}F(X(t)))dt + \frac{1}{\sqrt{W_e}}dW(t)
%	\]
%	with a velocity field $\kappa(t) = 2$.
%	
%	Look at different matching strategies with prior after 1 second and target distribution at 1.1 second.
%\end{frame}
%
%\begin{frame}
%	\frametitle{$f-$divergence is superior}
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.9\linewidth]{../../Source/Python/samples/fene/plots/Strategies}
%		\label{fig:Strategies}
%	\end{figure}
%	
%\end{frame}
%
%
%\begin{frame}
%	\frametitle{Linear models are an important to study the acceleration algorithm}
%	\note{Linear problems for stability analysis, moment selection, here with scale separation}
%	Consider a linear slow-fast system with scale separation $\epsilon$
%	\begin{align*}
%	dX(t) &= -\frac{1}{\epsilon}X(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_x(t) \\
%	dY(t) &= (X(t)-Y(t))dt + dW_y(t)
%	\end{align*}
%	with $X(t)$ the fast variable and $Y(t)$ the slow or coarse one.
%	
%	\vspace{3mm}
%	\begin{itemize}
%		\item Allows to study stability
%		\item Gives insight in how many moments are needed \note{to represent the coarse variables}
%		\item Analytic results on matching with only coarse moments
%	\end{itemize}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Explicit formula for matching with coarse mean}
%	\begin{theorem} \label{thm:linearmatchingmean}
%		Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$ where the subscript $d$ indicates the fast variables and $m$ the slow. Then the distribution $Q^*$ that minimizes the  Kullback-Leibler divergence, constrained with $\mathbb{E}[Q]_m = \mu_m^*$ is a normal distribution $\mathcal{N}(\mu^*, \Sigma)$ with $\mu^* = [\mu^*_d, \mu^*_m]$ and $\mu_d^* = \mu_d + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m)$.
%	\end{theorem}
%\end{frame}

%\begin{frame}
%	\frametitle{Numerical confirmation of the theorem}
%	Initial condition: Gaussian distribution at $[1,2]$ with unit covariance matrix.
%	
%	\begin{figure}
%		\centering
%		%	\includegraphics[width=0.8\linewidth]{../../Source/Python/samples/linear/plots/Mean_reference_Ly1_tend1_gaussianinitial}
%		\label{fig:meanreferencely1tend1gaussianinitial}
%	\end{figure}
%	
%\end{frame}
%
%\begin{frame}
%	\frametitle{Variance is not well approximated}
%	\begin{figure}
%		\centering
%		%	\includegraphics[width=1\linewidth]{../../Source/Python/samples/linear/plots/Variance_reference_Ly1_tend1_gaussianinitial}
%		\label{fig:variancereferencely1tend1gaussianinitial}
%	\end{figure}
%	
%\end{frame}

%\begin{frame}
%	\frametitle{More complicated formulas for extrapolated mean and variance}
%	\begin{theorem} \label{thm:linearmatchingmeanvariance}
%		Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$ where the subscript $d$ indicates the fast variables and $m$ the slow. Also suppose that the restriction operator $\mathcal{R}$ consists of the coarse mean $\mathcal{R}(Q) = \mu_m^*$ and variance $\Sigma_m^*$. Then the distribution $Q$ that minimizes the KLD-divergence, constrained with $\mathbb{E}[Q]_m = \mu_m^*$ and $Var[Q]=\Sigma_m^*$ is a normal distribution $\mathcal{N}(\mu^*, \Sigma^*)$ with $\mu^* = [\mu^*_d, \mu^*_m]$ and $\mu_d^* = \mu_d + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m)$.
%	\end{theorem}
%\end{frame}

%\begin{frame}
%	\frametitle{Variance is now a lot better}
%	\begin{figure}
%		\centering
%		%	\includegraphics[width=1\linewidth]{../../Source/Python/samples/linear/plots/Variance_reference_Ly2_tend1_gaussianinitial}
%		\label{fig:variancereferencely2tend1gaussianinitial}
%	\end{figure}
%	
%\end{frame}

%\begin{frame}
%	\frametitle{How many slow and fast moments describe the linear system accurately?}
%	\begin{itemize}
%		\item We are only interested in the slow variables
%		\item Normal distribution is completely determined by mean and variance
%		\item Fast variables will reach equilibrium fast
%		\item So far, two coarse moments seemed to give decent approximations for mean and variance.
%	\end{itemize}
%	\vspace{3mm}
%	Conjecture: Two coarse moments give the best approximation results for the linear test system.
%\end{frame}

%\begin{frame}
%	\frametitle{Going beyond two slow moments}
%	\begin{figure}
%		\centering
%		%		\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/MeanVariance_Lx0_Ly0123_guassianinitial_tend1}
%		\label{fig:meanvariancelx0ly0123guassianinitialtend1}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Adding moments of the fast variable}
%	\begin{figure}
%		\centering
%		%	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/MeanVariance_Lx0123_Ly2_gaussianinitial_tend1}
%		\label{fig:meanvariancelx0123ly2gaussianinitialtend1}
%	\end{figure}
%	
%\end{frame}


%\begin{frame}
%	\frametitle{The double-well model problem}
%	\begin{equation*} \label{eq:ch6doublewellsde}
%	\begin{aligned}
%	dX &= -(2X+Y)dt + AdW_x \\
%	dY &= \frac{1}{\varepsilon}(Y-Y^3)dt + \frac{1}{\sqrt{\varepsilon}} dW_y = -\frac{1}{\varepsilon}\nabla V(Y)dt + \frac{1}{\sqrt{\varepsilon}} dW_y.
%	\end{aligned}
%	\end{equation*}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{potential.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{density.tex}
%		\end{subfigure}
%	\end{figure}
%	This system fits in the framework of the previous theorem, so that the approximate macroscopic model reads
%	\[
%	d\overset{\_}{X} = -2\overset{\_}{X}dt + AdW_x.
%	\]
%	
%	\vspace{4mm}                        	
%\end{frame}
%
%\begin{frame}
%	\frametitle{The approximate model is inaccurate for moderate $\varepsilon$}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.4\textwidth}
%			\centering
%			\input{particleeps0.1text.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.4\textwidth}
%			\centering
%			\input{particleeps0.01text.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.4\textwidth}
%			\centering
%			\input{varfigureeps01A01.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.4\textwidth}
%			\centering
%			\input{histogramfigureeps01A01.tex}
%		\end{subfigure}
%	\end{figure}
%	
%	\vspace{2mm}
%\end{frame}
%
%\begin{frame}
%	\frametitle{The micro-macro scheme removes the modelling error}
%	Use the first and second moments of $X$ as states. The transient error however increases with $\Delta t$.
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{varfigure.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{histogramfigure.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	\frametitle{A tri-atom molecule}
%	\begin{figure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%				\begin{equation*}
%			\begin{cases}
%			dx_a = -\frac{\partial V}{\partial x_a} dt + \sqrt{2 \beta^{-1}}dW_{x_a} \\
%			dx_c = -\frac{\partial V}{\partial x_c} dt + \sqrt{2 \beta^{-1}} dW_{x_c} \\
%			dy_c = -\frac{\partial V}{\partial y_c} dt + \sqrt{2 \beta^{-1}} dW_{y_c}.
%			\end{cases}
%			\end{equation*}
%			
%					\vspace{5mm}
%		\end{subfigure}%
%	    \begin{subfigure}[b]{0.5\textwidth}
%	    		\centering
%	    		\includegraphics[width=0.7\linewidth]{"../complete text/triatom"}
%	    \end{subfigure}
%    \end{figure}
%	The potential energy $V(x_a, x_c, y_c)$ is given by
%	\begin{equation*} \label{eq:triatompotential}
%	V(x_a, x_c, y_c) = \frac{1}{2\varepsilon}(x_a-l_{\text{eq}})^2 + \frac{1}{2\varepsilon}(\sqrt{x_c^2+y_c^2}-l_{\text{eq}})^2 + W(\theta),
%	\end{equation*}
%	with $\beta = 1/T$ the inverse temperature.
%	\begin{equation*}
%	W(\theta) = \frac{k}{2}((\theta - \theta_{\text{saddle}})^2 - \delta \theta^2).
%	\end{equation*}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Many possible approximate macroscopic models}
%	Define a reaction coordinate $\xi(X,Y,Z)$, the approximate model
%	\[d\xi = b(\xi)dt + \sqrt{2\beta^{-1}} \sigma(\xi)dW \]
%	where 
%	\begin{equation*}
%	\begin{aligned}
%	b(z) &= \int(-\nabla V(\mathbf{x}) \cdot \nabla(\mathbf{x}) \xi+ \beta^{-1} \triangle \xi(\mathbf{x}) ) \Psi_{\infty}(\mathbf{x}) \delta_{\xi(\mathbf{x})=z}  d\mathbf{x} \\ \sigma^2(z) &= \int |\nabla \xi(\mathbf{x})|^2 \Psi_{\infty}(\mathbf{x})\delta_{\xi(\mathbf{x})=z} d\mathbf{x},
%	\end{aligned}
%	\end{equation*}
%	where $\Psi_{\infty}(x_a,x_c,y_c)$ is the invariant distribution. Now take $\xi_1=\theta$ and $\xi_2=\norm{A-C}^2$.
%\end{frame}
%
%\begin{frame}
%	\frametitle{Micro-macro acceleration removes the error with $\xi_1$}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetameanmmangle.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetahistogrammmangle.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancemeanmmangle.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancehistogrammmangle.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}
%
%
%\begin{frame}
%	\frametitle{Extrapolating $\xi_2$}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetameanmmdistance.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetahistogrammmdistance.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancemeanmmdistance.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancehistogrammmdistance.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}
%
%\begin{frame}
%	\frametitle{Extrapolating the first two moments of $x_c$ and $y_c$}
%	\begin{figure}
%		\centering
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetamean022.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/thetahistogram022.tex}
%		\end{subfigure}
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancemean022.tex}
%		\end{subfigure}%
%		\begin{subfigure}[b]{0.5\textwidth}
%			\centering
%			\input{../../Source/Python/Samples/triatom/plots/distancehistogram022.tex}
%		\end{subfigure}
%	\end{figure}
%\end{frame}
\end{document}