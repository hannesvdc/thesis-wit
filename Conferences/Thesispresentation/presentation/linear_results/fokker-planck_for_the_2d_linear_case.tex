\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\begin{document}
Consider a general linear two-dimensional SDE with additive noise:
\begin{equation*}
\begin{aligned}
dX &= (aX+bY)dt + D_xdW_x \\
dY &= (cX+dY)dt + D_ydW_y
\end{aligned}
\end{equation*}
where $W_x(t)$ and $W_y(t)$ are standard Brownian motion processes. The Fokker-Planck equation describing the joint density $\rho(x, y, t)$ is then given by 
\[
\partial_t \rho + \partial_x((ax+by) \rho) + \partial_y((cx+dy)\rho) = \frac{1}{2}(D_x \partial_{xx}\rho + D_y \partial_{yy}\rho)
\]
We also want that the total mass is conserved, being equal to one. Therefore we will use a finite-volume method to discretize the drift term, while using a finite difference scheme for the second order diffusion term. The following section contains a derivation of a first order scheme for the Fokker-Planck with a general drift term.
\[
\partial_t \rho + \partial_xF(\rho) + \partial_yG(\rho) = \frac{1}{2}(D_x \partial_{xx}\rho + D_y \partial_{yy}\rho)
\]

\section{Derivation of a finite volume scheme}
Suppose for simplicity that we are using a rectangular grid ${C_{i,j}}$ with box dimensions $(\Delta x, \Delta y)$, and points $(x_i, y_j)=((i-\frac{1}{2})\Delta x, (j-\frac{1}{2})\Delta y)$ in the middle of each rectangle as in Figure \ref{fig:fvgrid}. 
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\linewidth]{FVgrid}
	\caption{The two-dimensional finite volume grid. This figure was taken from Ward Melis' PhD text.}
	\label{fig:fvgrid}
\end{figure}
Finite volume methods are based on the integral form of the PDE. Integrating the Fokker-Planck equation over a small rectangle $C_{i,j}$ yields
\[
\frac{d}{dt} \int_{C_{i,j}} \rho(x,y,t) dx dy + \int_{C_{i,j}} \nabla \cdot (F(\rho), G(\rho)) dxdy = \frac{1}{2} \int_{C_{i,j}} D_x \partial_{xx}\rho(x,y,t) + D_y \partial_{yy} \rho(x,y,t) dxdy.
\]
By Green's theorem, the surface integral of the divergence of $(F(\rho), G(\rho))$ equals the line integral around $C_{i,j}$, counter clockwise, of the vector field $(F(\rho), G(\rho))$ itself. Filling this in yields
\begin{multline}\label{eq:discreteFP}
\frac{d}{dt} \int_{C_{i,j}} \rho(x,y,t) dx dy + \int_{y_{j-\frac{1}{2}}}^{y_{j+\frac{1}{2}}} F(\rho(x_{i+\frac{1}{2}}, y, t))-F(\rho(x_{i-\frac{1}{2}}, y, t))dy+\\ \int_{x_{i-\frac{1}{2}}}^{x_{i+\frac{1}{2}}} G(\rho(x, y_{j+\frac{1}{2}}, t))-G(\rho(x, y_{j-\frac{1}{2}}, t))dx =\frac{1}{2} \int_{C_{i,j}} D_x \partial_{xx}\rho(x,y,t) + D_y \partial_{yy} \rho(x,y,t) dxdy.
\end{multline}
Before discretizing the integrals, let's first introduce the cell average of $\rho(x,y,t)$ over a cell $C_{i,j}$ as
\[
\bar{\rho}_{i,j}(t) = \frac{1}{\Delta x \Delta y} \int_{C_{i,j}} \rho(x, y, t) dx dy.
\]
Now we can approximate each of different terms occurring in \ref{eq:discreteFP}. For the diffusion we use standard finite differences, reading
\[
\partial_{xx} \bar{\rho}_{i,j} \approxeq  \frac{\bar{\rho}_{i-1,j}-2\bar{\rho}_{i,j}+\bar{\rho}_{i+1,j}}{\Delta x^2}
\]
and similarly 
\[
\partial_{yy} \bar{\rho}_{i,j} \approxeq  \frac{\bar{\rho}_{i,j-1}-2\bar{\rho}_{i,j}+\bar{\rho}_{i,j-1}}{\Delta y^2}
\]
while for the drift terms we introduce 
\begin{equation*}
\begin{aligned}
F_{i\pm \frac{1}{2},j}(t) &\approxeq \frac{1}{\Delta y}  \int_{y_{j-\frac{1}{2}}}^{y_{j+\frac{1}{2}}} F(\rho(x_{i\pm\frac{1}{2}}, y, t))dy \\
G_{i,j\pm \frac{1}{2}}(t) &\approxeq \frac{1}{\Delta x}  \int_{x_{i-\frac{1}{2}}}^{x_{i+\frac{1}{2}}} G(\rho(x, y_{j\pm\frac{1}{2}}, t))dx
\end{aligned}
\end{equation*}
where a implicit midpoint rule suffices for an accurate computation of the flux integrals
\begin{equation*}
\begin{aligned}
F_{i\pm \frac{1}{2},j}(t) &= F(\rho(x_{i\pm\frac{1}{2}}, y_j, t)) \\
G_{i,j\pm \frac{1}{2}}(t) &= G(\rho(x_i, y_{j\pm\frac{1}{2}}, t))
\end{aligned}
\end{equation*}


\noindent Putting it all together, the finite volume approximation of the Fokker-Planck equation becomes
\[
\frac{d}{dt} \bar{\rho}_{i,j} = -\frac{1}{\Delta x}(F_{i+\frac{1}{2},j}-F_{i-\frac{1}{2},j}) - \frac{1}{\Delta y}(G_{i,j+\frac{1}{2}}-F_{i,j-\frac{1}{2}}) + D_x \frac{\bar{\rho}_{i-1,j}-2\bar{\rho}_{i,j}+\bar{\rho}_{i+1,j}}{\Delta x^2} + D_y  \frac{\bar{\rho}_{i,j-1}-2\bar{\rho}_{i,j}+\bar{\rho}_{i,j-1}}{\Delta y^2}
\]

\vspace{2mm}
\noindent
Right now the Fokker-Planck PDE is reduced to a system of ODE's that can be integrated over time using any standard technique for ODE's like Runge-Kutta methods or alike, but a simple forward Euler will work nicely too. The complete discrete finite volume approximation thus reads
\[
\bar{\rho}_{i,j}^{n+1} = \bar{\rho}_{i,j}^{n}-\frac{\Delta t}{\Delta x}(F_{i+\frac{1}{2},j}-F_{i-\frac{1}{2},j}) - \frac{\Delta t}{\Delta y}(G_{i,j+\frac{1}{2}}-F_{i,j-\frac{1}{2}}) + \frac{D_x \Delta t}{\Delta x^2}(\bar{\rho}_{i-1,j}-2\bar{\rho}_{i,j}+\bar{\rho}_{i+1,j}) + \frac{D_y \Delta t}{\Delta y^2}  (\bar{\rho}_{i,j-1}-2\bar{\rho}_{i,j}+\bar{\rho}_{i,j-1}).
\]

\section{Boundary conditions}
In principle, the invariant distribution of a linear model is a two-dimensional Gaussian that stretches out all over the plane $\mathbb{R}^2$. The bell-curve decays very rapidly so that we can approximate the plane $\mathbb{R}^2$ by a square grid $[x_1, x_2] \times [y_1, y_2]$ so that the transient and invariant distributions are very close to zero on the boundary of the rectangular grid. A numerical computation on this finite grid also requires boundary conditions. Mass conservation is very important in the context of probability distributions so that no-flux boundary conditions are needed. No flux on the boundaries demands that the numerical fluxes $F_{i\pm \frac{1}{2},j}$ and $G_{i,j\pm \frac{1}{2}}$ are zero on the boundary. Also for second order derivatives near the boundary, the no flux condition implies that $G_{i,j\pm \frac{1}{2}}=0$ on the lower and upper boundary and $F_{i\pm \frac{1}{2},j}=0$  on the left and right boundary.

\section{Order test to verify a correct implementation}
One way to verify the correctness of the finite volume implementation is by computing the error between the numerical solution and analytical solution for different grid sizes and temporal steps with the same end time and see if the error decreases with a certain order. We will use the standard forward Euler method so the error should decrease linearly in time and also quadratically in space due to the central implicit midpoint scheme to approximate the fluxes and central differences for the second order derivatives. Both approximations are second order in space. Figures \ref{fig:convergencefptime} and \ref{fig:convergencespacefp} show the error as a function of the temporal and spatial step for a Gaussian initial condition with mean $\mu_x, \mu_y= 1,2$ and covariances $\Sigma_x, \Sigma_y, \Sigma_{x,y}=1,1,0$ and final simulation time 0.5 seconds. The time step $dt$ scales quadratically with the spatial grid size $dx$ to keep the numerical scheme stable. Figure \ref{fig:convergencespacefp} clearly shows better than second order convergence in space, it even is third order, and similarly the convergence in time is better than first order. The higher-order convergence is called \textit{hyper-convergence} but more research is needed on why hyper-convergence manifests on a linear PDE. Both figures confirm a correct implementation of the finite volume approximation of the Fokker-Planck equation.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Convergence_plots/convergence_fp_time}
	\caption{Convergence of the error as a function of the temporal step size for a fixed end time for the Fokker-Planck equation. The figure shows first order convergence in time.}
	\label{fig:convergencefptime}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Convergence_plots/convergence_space_fp}
	\caption{Convergence of the error as a function of the spatial step size for the Fokker-Planck equation. The figure indicates second order convergence in space as expected.}
	\label{fig:convergencespacefp}
\end{figure}

\end{document}