\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\begin{document}
\title{Derivation of closure relations for linear SDE's with additive noise}
\author{Hannes Vandecasteele}
\maketitle

\section{Derivation of the closure relations}
Consider again an $n$-dimensional linear SDE with additive noise. We can write this as 
\[
dX = AXdt + DdW
\]
where $X \in \mathbb{R}^n, A \in \mathbb{R}^{n \times n}$, $D = \text{diag}(D_1, \dots, D_n)$ and $W(t)$ an $n-$dimensional Wiener process. Instead of simulating the SDE directly, it is possible to derive a system of ODE's describing the propagation of means $\mathbb{E}[X]$ and (co)variances $\mathbb{E}[(X-\mathbb{E}(X))(X-\mathbb{E}(X))^T]$ of the components of the SDE. In the two dimensional case for example, the SDE reduces to a system of five ODE's because the covariance matrix is symmetric. The above system is also an Ohrnstein-Uhlenbeck process so the invariant distribution is a multidimensional Gaussian distribution. In this setting, the invariant mean and covariance describe the invariant distribution completely.

For the mean, we can proceed by taking the expectation of both sides
\[
d\mathbb{E}[X] = A \mathbb{E}[X]dt + D\mathbb{E}[dW].
\]
By the martingale property in Ito calculus the final term is zero and we get the first two relations as $\frac{d\mathbb{E}[X]}{dt} = A \mathbb{E}[X]$.

For the covariance, a similar derivation is possible, but Ito's lemma is required. The derivative of the covariance matrix using Ito's lemma reads
\[
d(X-\mathbb{E}[X])(X-\mathbb{E}[X])^T = dX (X-\mathbb{E}[X])^T + (X-\mathbb{E}[X]) dX^T + \frac{1}{2}D^2dt.
\]
Writing the former expression out further gives us
\[
d(X-\mathbb{E}[X])(X-\mathbb{E}[X])^T = \left(AX (X-\mathbb{E}[X])^T + (X-\mathbb{E}[X]) X^TA^T + \\
\frac{1}{2}D^2 \right)dt + dW (X-\mathbb{E}[X])^T + (X-\mathbb{E}[X]) dW
\]
Again taking expectations from both sides, the terms with Brownian motion will disappear due to the martingale property, so we end up with
\[
d\text{Cov[X]} = \left(A\mathbb{E}[X (X-\mathbb{E}[X])^T] + \mathbb{E}[](X-\mathbb{E}[X]) X^T]A^T + \frac{1}{2}D^2 \right)dt.
\]
The first two expectation-terms equal the covariance matrix, so the system of ODE's describing the covariance matrix is
\[
\frac{d\text{Cov(t)}}{dt} = A\text{Cov(t)} + \text{Cov(t)}A^T + \frac{1}{2}D^2.
\]
In general no higher order terms pop up in the equations describing the covariance matrix. For non-linear SDE's this is usually the case so that in principle an infinite number of equations are required to describe the intermediate distributions completely.
For example, the the two dimensional linear case where 
\[
A = \begin{pmatrix}
a & b \\ c & d
\end{pmatrix} \ \ D = \begin{pmatrix}
D_x & 0 \\ 0 & D_y
\end{pmatrix}
\]
the equations for means and covariance reduce to
\[
\frac{d}{dt} \begin{pmatrix} \mu_X \\ \Sigma_X \\ \mu_Y \\ \Sigma_Y \\ \text{Cov}(X,Y) \end{pmatrix} = \begin{pmatrix} a & 0 & b & 0 & 0 \\ 0 & 2a & 0 & 0 & 2b \\ c & 0 & d & 0 & 0 \\  0 & 0 & 0 & 2d & 2c \\ 0 & c & 0 & b & a+d \end{pmatrix} \begin{pmatrix} \mu_X \\ \Sigma_X \\ \mu_Y \\ \Sigma_Y \\ \text{Cov}(X,Y) \end{pmatrix} + \begin{pmatrix} 0 \\ D_x^2 \\ 0 \\ D_y^2 \\ 0 \end{pmatrix}
\]
which can be solved directly or simulated using any standard ODE solver.

\section{Analytical solution of the closure relations}
The system of ODE's from above has a simple analytical solution. Denote the system as
\begin{equation}
\frac{dx(t)}{dt} = Mx + b, \ \ x(0) = x_0.
\end{equation}
Then the steady-state solution is given by $x_{\infty} = -M^{-1}b$ and if we denote $y(t)=x(t) - x_{\infty}$, the evolution law for $y(t)$ is simply
\begin{equation}
\frac{dy(t)}{dt} = Ay(t)
\end{equation}
such that $y(t) = e^{At}y_0$ and hence
\begin{equation}\label{eq:closureanalytic}
x(t) = x_{\infty} + e^{At}(x_0-x_{\infty}).
\end{equation}

\section{Convergence test for the forward Euler implementation}
Chapter 4 uses a forward Euler implementation of the two dimensional closure model to compare the accuracy of the matching algorithms with the true solution. Therefore it is necessary to be sure that the implementation is correct by carrying out an order test. The following test compares the analytical solution given by equation \ref{eq:closureanalytic} at time $t=0.5$ seconds with initial condition $[\mu_x, \Sigma_x, \mu_y, \Sigma_y, \Sigma_{x,y}] = [1., 1., 2., 1., 0.]$ with the numerical solution at the same time as a function of the temporal step size. Figure \ref{fig:convergencemeanclosuretend05} shows the error of the mean of the fast and slow variable relative to the analytic solution as a function of the step size. Figure \ref{fig:convergencevarianceclosuretend05} show a similar plot for the error of the variance. Both plots show first order convergence which is exactly the order of the forward Euler method. This is sufficient proof that the numerical implementation is trustworthy and if the step size is small enough, like $10^{-3}$ then the closure model is close enough to the exact solution.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/convergence_plots/convergence_mean_closure_tend05}
	\caption{First order convergence in time for the error of the mean of the slow and fast variables.}
	\label{fig:convergencemeanclosuretend05}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/convergence_plots/convergence_variance_closure_tend05}
	\caption{First order convergence in time for the error of the variance of the slow and fast variables.}
	\label{fig:convergencevarianceclosuretend05}
\end{figure}

\end{document}