\documentclass{kulakreport}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}
\usepackage{mathtools}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\theoremstyle{proposition}
\newtheorem*{proposition}{Proposition}
\theoremstyle{lemma}
\newtheorem*{lemma}{Lemma}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\theoremstyle{theorem}
\newtheorem*{theorem}{Theorem}

\begin{document}
\subsection{An iterative formula for slow mean-only extrapolation}
The convergence proof relies on an iterative formula that defines how the complete mean and variance propagate through one full step of the micro-macro scheme. The formula is also fundamental to the stability bound with slow mean extrapolation in section 4 of this chapter. The derivation assumes only one Euler-Maruyama inner step of size $\delta t$ for simplicity, but it can easily be extended to $K$ inner steps. The derivation of the iterative formula was first done in [cite stability] and changed here for consistency of notation.

\vspace{2mm}
\noindent
Suppose at time $t^n = n \Delta t$,  the mean vector is $\mu^n = [\mu_s^n , \mu_f^n]$ and the covariance matrix $\Sigma = \begin{bmatrix} \Sigma_s^n & C^n \\ C^{n T} & \Sigma_f^n \end{bmatrix}$.  Also write the matrix $A$ in block-diagonal form $A = \begin{bmatrix} A_s & 0 \\ 0 &A_f \end{bmatrix}$. The subscript 's' again has the meaning of 'slow' mode.  After one Euler-Maruyama step, a particle $X^n$ propagates as
\[
x^{n,1} = (I+A\delta t)X^n + \sqrt{\delta t}B \xi^n, \ \ \xi^n \sim \mathcal{N}(0,1),
\]
so that, by taking expectations and employing the martingale property,
\[
\mu^{n,1} = (I+A\delta t)\mu^n \\
\Sigma^{n,1} = (I+\delta tA)\Sigma^n(I+\delta tA)^T + \delta t BB^T
\]
Extrapolating the slow mean yields
\[
\mu_s^{n+1} = \mu_s^n + \frac{\Delta t}{\delta t}(\mu_s^{n,1}-\mu_s^n) = (I+\Delta t A)\mu_s^n.
\]
By a theorem on matching in the linear case, we know that the covariance matrix $\Sigma^n$ stays constant during matching, and that the fast mean is given by
\begin{equation*}
\mu_f^{n+1} = (I_f + \delta t A_f)\mu_f^n + (\Delta t - \delta t) C^{n,1 T}(\Sigma^{n,1})^{-1} A_s \mu_s^n. 
\end{equation*}
Bundling the propagation of the complete mean vector in one equation, finally reads
\begin{equation} \label{eq:meanpropagation}
\mu^{n+1} = \left(I + \begin{bmatrix}  \Delta t A_s  & 0 \\ (\Delta t - \delta t)C_{n,1}^T(\Sigma^{n,1})^{-1}A_s & \delta t A_f \end{bmatrix}\right)\mu^n 
\end{equation}

\subsection{Convergence Theorem}
The following theorem shows convergence of the micro-macro scheme on a linear SDE with only slow mean extrapolation.

\begin{theorem} \label{thm:linearslowconvergence}
	Given a linear SDE with a normal initial distribution, and consider the micro-macro acceleration algorithm with relative-entropy matching and only slow-mean extrapolation. Also fix an end time $T > 0$. Denote by $X^t$ the exact distribution of the linear SDE at time $t \in [0,T]$, and by $X^{n_{\Delta t}(t)}$ the distribution obtained by the micro-macro scheme, where $n_{\Delta t}(t) = t/\Delta t$. Then the Kullback-Leibler divergence $H(X^{n_{\Delta t}(t)} | X^t)$ converges uniformly to zero over the interval $[0,T]$ as $\delta t$ goes to zero and $\Delta t$ goes to $\delta t$,
	\begin{equation} \label{eq:proofobjective}
	\underset{\delta t \to 0}{\lim}	\ \underset{\Delta t \to \delta t}{\lim} \	\underset{t \in [0,T] }{\max} \mathcal{H}(X^{n_{\Delta t}(t)}|X^t) = 0.
	\end{equation}
	 As a consequence, micro-macro distribution also converges uniformly to the exact distribution in the same limits.
\end{theorem}
\begin{proof}
	If the initial condition of a linear equation is Gaussian, all intermediate distributions of the exact solution, the Euler-Maruyama method and the micro-macro acceleration algorithm are Gaussian too. By a well known expression, the Kullback-Leibler between the micro-macro approximation and the exact distribution then reads
	\begin{equation*}
	\mathcal{H}(X^{n_{\Delta t}(t)}|X^t) = \frac{1}{2} \left( \ln \left( \frac{\Sigma^t}{\Sigma^{n_{\Delta t}(t)}} \right) - s-f + \text{trace}((\Sigma^t)^{-1} \Sigma^{n_{\Delta t}(t)})  + (\mu^t - \mu^{n_{\Delta t}(t)})^T (\Sigma^t)^{-1}(\mu^t - \mu^{n_{\Delta t}(t)}) \right),
	\end{equation*}
	where $\mu^t$ and $\Sigma^t$ are the mean and variance of the exact solution at time $t$. Identically, $\mu^{n_{\Delta t}(t)}$ and $\Sigma^{n_{\Delta t}(t)}$ are the mean and variance of the micro-macro approximation at time $T$. If we first take  $\delta t$ and $\Delta t$ fixed, we can expand iteration \ref{eq:meanpropagation} as
	\begin{equation} \label{eq:iterativewrittenout}
	\begin{aligned}
	\mu_s^{n_{\Delta t}(t)} &= (I_s+A_s\Delta t)^{n_{\Delta t}(t)} \mu_s^0 \\
	\mu_f^{n_{\Delta t}(t)} &= (I_f + \delta t A_f)\mu_f^{n_{\Delta t}(t)-1} + (\Delta t - \delta t) (C^{n_{\Delta t}(t)-1,1 })^T(\Sigma^{n_{\Delta t}(t)-1,1})^{-1} A_s \mu_s^{n_{\Delta t}(t)-1} \\
	&= (I_f+A_f\delta t)^{n_{\Delta t}(t)}\mu_f^0 + (\Delta t - \delta t)\sum_{k=0}^{n_{\Delta t}(t)-1} (I_f + A_f\delta t)^{n_{\Delta t}(t)-k-1} (C^{k,1})^T (\Sigma^{k})^{-1} A_s \mu_s^{k}. \\
	\end{aligned}
	\end{equation}
	As $\Delta t$ decreases to $\delta t$, $n_{\Delta t}(t)$ will increase to $n_{\delta t}(t)$ and the number of terms in the sum will remain finite. The contribution of the large sum then becomes zero. As a result, the mean vector $\mu^{n_{\Delta t}(t)}$ uniformly approaches his respective mean $\mu^{n_{\delta t}(t)}$ of the  Euler-Maruyama scheme. Similarly, the variance stays constant during matching, thus as $n_{\Delta t}(t)$ goes to $n_{\delta t}(t)$, the micro-macro variances converges uniformly to the variance $\Sigma^{n_{\delta t}(t)}$ of the Euler-Maruyama scheme. The expression in \eqref{eq:proofobjective} hence reduces to 
	\begin{equation}
    \underset{\delta t \to 0} {\lim}	\ \underset{t \in [0,T] }{\max} = \frac{1}{2} \left( \ln \left( \frac{\Sigma^t}{\Sigma^{n_{\delta t}(t)}} \right) - s-f + \text{trace}((\Sigma^t)^{-1} \Sigma^{n_{\delta t}(t)})  + (\mu^t - \mu^{n_{\delta t}(t)})^T (\Sigma^t)^{-1}(\mu^t - \mu^{n_{\delta t}(t)}) \right),
	\end{equation}
	which is the Kullback-Leibler divergence between the Euler-Maruyama scheme and the exact solution. The latter expression converges to zero because the mean and variance of the Euler-Maruyama method converges to their respective exact values, as $\delta t$ goes to zero. Hereby concluding the proof.
\end{proof}

\end{document}