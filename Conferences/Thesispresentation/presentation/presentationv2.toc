\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\beamer@sectionintoc {1}{A new convergence result}{9}{0}{1}
\beamer@sectionintoc {2}{Influence of moment selection}{11}{0}{2}
\beamer@sectionintoc {3}{Effect of the extrapolation step}{19}{0}{3}
\beamer@sectionintoc {4}{Conclusion and outlook}{24}{0}{4}
