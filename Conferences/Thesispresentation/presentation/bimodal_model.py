import numpy as np

from timeintegrators.euler_maryama import *

class BimodalModel:
    def __init__(self, eps, A):
        self.a = lambda t, x: np.array([-2.*x[0]-x[1], -1./eps*(x[1]**3-x[1])])
        self.b = lambda t, x, dw: np.array([A, 1./np.sqrt(eps)])*dw
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None, storeparticles=False):
        timestepper = EulerMaryamaIntegrator(self.a, self.b)
            
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        if storeparticles:
            particles = [np.copy(X[0,:])]
            
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt
            
            if storeparticles:
                particles.append(np.copy(X[0,:]))

        if not storeparticles:
            return X, np.asarray(cbtimes), np.asarray(cbvalues)
        else:
            return X, np.asarray(cbtimes), np.asarray(cbvalues), np.array(particles)