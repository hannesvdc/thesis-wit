\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Linear SDEs: Convergence \& Stability}{6}{0}{2}
\beamer@sectionintoc {3}{Influence of moment selection}{12}{0}{3}
\beamer@sectionintoc {4}{Effect of the extrapolation step}{18}{0}{4}
\beamer@sectionintoc {5}{A practical example}{23}{0}{5}
\beamer@sectionintoc {6}{Conclusion and outlook}{27}{0}{6}
