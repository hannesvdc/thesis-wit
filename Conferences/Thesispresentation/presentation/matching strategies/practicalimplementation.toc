\select@language {english}
\contentsline {chapter}{\numberline {1}Implementation of the acceleration algorithm}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Practical Implementation Issues}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Newton-Raphson iterations}{2}{subsection.1.1.1}
\contentsline {paragraph}{KLD Matching}{3}{section*.2}
\contentsline {paragraph}{L2D Matching}{3}{section*.3}
\contentsline {subsection}{\numberline {1.1.2}Towards a practical acceleration algorithm}{4}{subsection.1.1.2}
\contentsline {paragraph}{Adaptivity}{4}{section*.4}
\contentsline {paragraph}{Particle reweighing}{4}{section*.5}
\contentsline {paragraph}{Particle resampling}{5}{section*.6}
\contentsline {section}{\numberline {1.2}Object-oriented structure of the code}{6}{section.1.2}
\contentsline {paragraph}{Matching Strategies}{6}{section*.7}
\contentsline {paragraph}{Iterative Solvers}{7}{section*.8}
\contentsline {paragraph}{Microscopic Timesteppers}{7}{section*.9}
