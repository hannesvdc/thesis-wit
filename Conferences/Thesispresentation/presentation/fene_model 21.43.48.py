from math import *
import time

import numpy as np
import numpy.random as rd

from timeintegrators.accept_reject_em import *
from tools.plots import *

class Fene:
    def __init__(self, We, gamma, k):
        self.We = We
        self.gamma = gamma
        self.k = k
        self.F = lambda x: (gamma**2)*x/(gamma**2 - x**2)
        self.a = lambda t, x: k(t)*x - 0.5/We*self.F(x)
        self.b = lambda t, x: 1/sqrt(We) 
        
    def sampleInitialDistribution(self, J):
        ga = self.gamma
        
        dist = lambda r: exp(self.We*ga**2/2.*log(1-r*r/ga**2))
        a = lambda x: -0.5/self.We*ga**2*x/(ga**2-x*x)
        b = lambda x: 1./sqrt(self.We)
        
        from scipy.integrate import quad
        Z = quad(lambda t: (
                 np.sqrt(1 - abs(t / ga)**2)**(ga**2)),
                 -ga, ga)[0]
        x = 0.
        print("Z = ", Z)
        samples = []
        
        ntrial = 0
        nb_samples = 0
        rd.seed(int(round(time.time() * 1000))%(2**31))
        # Use standard accept-reject to sample the initial distribution
        while nb_samples < J:
            ntrial = ntrial + 1
            x = rd.uniform(-ga, ga)
            y = rd.uniform(0, 1);
            
            if y < dist(x):
                nb_samples = nb_samples + 1
                samples.append(x)
            
        print("Trials: ",  ntrial, " Accepted: ", nb_samples, " Acceptance ratio: ", nb_samples/ntrial, ".")
        return np.asarray(samples)
        
    def fullScaleMicroSimulation(self, points, tbegin, tend, dt):
        timestepper = AcceptRejectEulerMaryama(self.a, self.b, lambda x: abs(x) < (1.-sqrt(dt))*self.gamma)
        points = timestepper.integrate(points, tbegin, tend, dt)

        return points
        
    def computeStress(self, X, weights=None):
        if weights is None:
            weights = np.ones(X.size)/X.size
        weights = weights/np.sum(weights)
        
        stress = 0.
        for k in range(X.size):
            stress = stress + X[k]*self.F(X[k])*weights[k]
        stress = stress - 1
        
        return stress
