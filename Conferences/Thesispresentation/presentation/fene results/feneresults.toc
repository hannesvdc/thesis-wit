\select@language {english}
\contentsline {section}{\numberline {0.1}Introduction}{1}{section.0.1}
\contentsline {section}{\numberline {0.2}Numerical results of matching}{1}{section.0.2}
\contentsline {subsection}{\numberline {0.2.1}Sampling from the initial distribution}{1}{subsection.0.2.1}
\contentsline {subsection}{\numberline {0.2.2}Full micro simulation}{1}{subsection.0.2.2}
\contentsline {subsection}{\numberline {0.2.3}Empirical matching pdf's}{2}{subsection.0.2.3}
\contentsline {subsection}{\numberline {0.2.4}Errors in the computed moments}{2}{subsection.0.2.4}
\contentsline {subsection}{\numberline {0.2.5}Matching error as a function of the time step}{2}{subsection.0.2.5}
\contentsline {subsection}{\numberline {0.2.6}On the number of NR steps}{2}{subsection.0.2.6}
