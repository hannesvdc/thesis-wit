# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 09:59:21 2015

@author: PrzemekZ
"""
import numpy as np
import os


class Model(object):

    # This class implements a 1D polymer as two beads whose state is described
    # by the end-to-end vector X. The beads are connected by a Finetely
    # Extensible Nonlineary Elastic (FENE) spring with force given by
    #
    #    F(X) = X / (1 - |X / ga|^2),
    #
    # where ga=gamma>0 is the non-dimensional parameter related to the maximal
    # polymer length.
    #
    # The dynamics of polymers in solvent with constant velocity gradient ka =
    # kappa is governed by the Fokker-Planck (or Smoluchowski) equation for the
    # density of probability distribution of X
    #
    #    p_t = div_x{[1/(2*We)*F(x) - ka*x]*p} + 1/(2*We)*lap_x(p),
    #
    # where p=p(x,t) is the pdf of distribution of X and We is the Weissenberg
    # number.
    #
    # This class implements also the (direct) sampling of the equilibrum
    # (invariant) pdf of the dynamics given by
    #
    #    eq_dens(x) = (1/Z)*(1 - |x/g|^2)^(g^2/2) * exp(We*ka*|x|^2),
    #
    # where Z is normalising constant.

    def __init__(self, param=None):
        # default parameters of the model
        self.param = {'kappa': 2.0,
                      'We': 1.0,
                      'gamma': 7.0}

        # check and update default parameters with
        # values from param
        if param is not None:
            if not all(k in self.param for k in param):
                raise KeyError('Wrong model parameters')
            self.param.update(param)

        # normalizing constant for equilibrium density
        # only if velocity gradient is constant
        if isfloat(self.param['kappa']):

            # conversion of kappa
            self.param['kappa'] = float(self.param['kappa'])

            ga = self.param['gamma']
            We = self.param['We']
            ka = self.param['kappa']

            self.kappa_name = str(ka)

            from scipy.integrate import quad
            self.eq_dens_const = quad(lambda t: (
                                      np.sqrt(1 - abs(t / ga)**2)**(ga**2) *
                                      np.exp(We * ka * abs(t)**2)),
                                      -ga, ga)[0]
        else:
            self.eq_dens_const = None
            try:
                self.param['kappa'].__call__
                self.kappa_name = self.param['kappa'].__name__
            except AttributeError:
                raise TypeError('Velocity gradient is neither float nor '
                                'function')

    # FENE force with barrier at gamma
    def F(self, X):

        ga = self.get_gamma()

        if np.asarray(X).ndim == 0:
            return X/(1 - abs(X/ga)**2)
        else:
            Y = np.zeros(len(X))
            for j in range(0, len(X)):
                Y[j] = 1/(1 - abs(X[j]/ga)**2)  # add condition for X<=0
            return X*Y

    # equilibrium distribution for the polymer configuration
    def eq_dens(self, X):

        ga, we, ka = self.get_param()
        Z = self.eq_dens_const

        absX = np.absolute(X)
        B = np.less(absX, ga)

        if np.asarray(X).ndim == 0:
            if B:
                return (
                        (1/Z) * np.sqrt(1 - (absX / ga)**2)**(ga**2) *
                        np.exp(we * ka * absX**2))
            else:
                return 0
        else:
            Y = np.zeros_like(absX, dtype=np.float)
            for j in range(0, len(absX)):
                if B[j]:
                    Y[j] = (
                            (1/Z) * np.sqrt(1 - (absX[j] / ga)**2)**(ga**2) *
                            np.exp(we * ka * absX[j]**2))
                else:
                    Y[j] = 0
        return Y

    # generates samples from equilibrium distribution
    # using acceptance-rejection method
    def sample_eq_dens(self, nb_samples):

        if self.eq_dens_const is None:
            raise TypeError('This model does not have equilibrum density.')

        ga, We, ka = self.get_param()
        Z = self.eq_dens_const

        # maximal value of eq_dens
        if ka == 0.0:
            y_max = Z**(-1)
        else:
            y_max = (
                max(1, (2 * We * ka)**(-ga**2 / 2) *
                    np.exp(ga**2 * (We * ka - 0.5))) / Z)

        # counters
        naccept = 0
        ntrial = 0

        # Keeps generating numbers until we achieve the desired nb_samples
        ran = []  # output list of random numbers
        while naccept < nb_samples:
            x = np.random.uniform(-ga, ga)  # x'
            y = np.random.uniform(0, y_max)  # y'

            if y < self.eq_dens(x):
                ran.append(x)
                naccept = naccept + 1
            ntrial = ntrial + 1

        ran = np.asarray(ran)

        return ran

    def vel_grad(self, t):
        ka = self.get_kappa()
        if isfloat(ka):
            return ka
        else:
            return ka(t)

    def check_lengths(self, sample):
        ga = self.get_gamma()

        if np.any(np.absolute(sample) > ga):
            raise ValueError('Samples are out of range for model with'
                             'ga = {0:.3g}'.format(ga))

    # get methods
    def get_param(self):
        return self.param['gamma'], self.param['We'], self.param['kappa']

    def get_gamma(self):
        return self.param['gamma']

    def get_we(self):
        return self.param['We']

    def get_kappa(self):
        return self.param['kappa']

    def get_directory(self):
        ga = self.get_gamma()
        we = self.get_we()

        return 'ga=' + str(ga) + '_we=' + str(we) + '_ka=' + self.kappa_name

    def get_raw_moment(self, l, sample, weights=None):
            # l - moment index
            return np.average(sample**l, weights=weights)

    def get_norm_raw_moment(self, l, sample, weights=None):
        # l - moment index

        ga = self.param['gamma']
        return np.average((sample / ga)**l, weights=weights)

    def get_stress(self, sample, weights=None):

        we = self.param['We']
        fene_sample = self.F(sample)
        if weights is None:
            weights = np.ones_like(sample)

        return (1 / we) * (np.average(sample*fene_sample, weights=weights) - 1)

    # kernel density estimation
    def get_kde(self, sample, x_grid, bw=None, safe=False):

        if safe is False:
            self.check_lengths(sample)

        if bw is None:
            ga = self.param['gamma']
            bw = ga * 10**(-2)  # bandwith (how to choose?)

        return x_grid, kde(sample, x_grid, bw)


class Sim(object):

    def __init__(self, init_sample=None, model=None, param=None):

        # default simulation parameters
        self.param = {'dt': 10**(-2),
                      'T': 2.0,
                      'method': 'ar',
                      'samples': 10**5,
                      'seed': 100,
                      'T_store': 0.0}

        # setting model
        if model is None:
            model = Model()
        self.model = model

        # check and update default parameters with
        # values from param
        if param is not None:
            if not all(k in self.param for k in param):
                raise KeyError('Wrong model parameters')
            self.param.update(param)

        # available methods for SDE discretization schemes
        self.methods = {'ar': Sim.ar_step}
        # check if method is good
        if not self.param['method'] in self.methods:
            raise NotImplementedError('The method is not implemented')

        # if initial sample is not given, we use sample from equilibrum
        # density corresponding to zero velocity gradient
        if init_sample is None:
            model_zero = Model({'kappa': 0,
                                'We': self.model.param['We'],
                                'gamma': self.model.param['gamma']})
            init_sample = model_zero.sample_eq_dens(self.param['samples'])
        else:  # else we check if all samples are within -gamma and gamma
            self.model.check_lengths(init_sample)
            # update number of samples
            self.param['samples'] = len(init_sample)
        self.init_sample = init_sample

        # dictionary for storing data
        self.data = {}

        # model parameters
        ga, we, ka = self.model.get_param()

        # simulation parameters
        dt = self.param['dt']
        T = self.param['T']
        method = self.param['method']

        # path & filename construction
        model_dir = self.model.get_directory()
        sim_dir = method + '_' + str(T) + 'T_' + str(dt) + 'dt'

        # directory for data storing
        self.data_dir = os.path.join('data', model_dir, sim_dir)
        self.fname = '{0}_{1:.1e}'.format(self.param['seed'],
                                          self.param['samples'])
        self.data_fname = self.fname + '.npy'
        self.add_data_fname = self.fname + '.npz'
        self.param_fname = self.fname + '.dat'

    # SIMULATION
    ################################################################
    # Euler-Maruyama step
    def em_step(self, X, dt, ga, we, ka):

        # implements one step of Euler-Maruyama method for SDE
        #
        #   dX = (ka*X - 1/(2*We)*F(X))*dt + 1/sqrt(We)*dW
        #
        # where ka = kappa, We are model parameters
        # and F is a FENE force with barrier at ga = gamma.
        #
        # Discretization scheme follows:
        # D.J.Higham, "An algorithmic introduction to numerical
        # simulation of Stochastic Differential Equations",
        # SIAM Review, Vol 43, No. 3, pp. 525-546
        #
        # X  - input state (nparray) in [-ga,ga]
        # dt - time step (float)

        # brownian increment
        dW = np.sqrt(dt)*self.sim_rng.randn(len(X))
        F_X = self.model.F(X)

        return X + (ka*X - (1/(2*we)) * F_X)*dt + (1/np.sqrt(we))*dW

    # accept-reject step
    def ar_step(self, prev_sample, dt, ga, we, ka, max_length):

        # j - step number
        # dt - step length
        #
        # first performs em_step on (j-1)th ensamble with step length dt
        # and repeats same em_step for realizations with absolute value
        # larger than (1 - sqrt(dt)) * gamma until there's none of them

        next_sample = np.zeros(self.param['samples'])

        # rejected indices
        rejects = np.arange(len(prev_sample))

        # total number of rejections made in one step
        nb_rejects = 0

        while len(rejects) > 0:

            next_sample[rejects] = self.em_step(prev_sample[rejects],
                                                dt, ga, we, ka)
            rejects = rejects[abs(next_sample[rejects]) > max_length]
            nb_rejects = nb_rejects + len(rejects)

        return next_sample, nb_rejects

    # simulation
    def perform(self):
        from time import clock

        method = self.param['method']

        # seed setting
        self.sim_rng = np.random.RandomState(self.param['seed'])
        self.sim_rng.randint(10**5, size=10**3)  # warm up of RNG

        # set the number of time steps
        T = self.param['T']
        Ts = self.param['T_store']
        dt = self.param['dt']

        nb_steps = int(np.ceil(T / dt))
        s_step = int(np.ceil(Ts / dt))

        # set the array for timing
        self.data['step_time'] = np.zeros(nb_steps + 1)

        # model parameters
        ga, we, ka = self.model.get_param()

        if isfloat(ka):
            ka = ka * np.ones(nb_steps)
        else:
            ka = np.array([self.model.vel_grad(i*dt) for i in range(nb_steps)])

        # initial sample
        init_sample = self.init_sample

        # check truncation condition
        if method == 'ar':

            max_length = np.sqrt(1 - np.sqrt(dt)) * ga
            if any(abs(init_sample) > max_length):
                raise ValueError('Accept-reject method: samples out of bound'
                                 'with absolute value bigger than {0:.4g}'
                                 .format(max_length))
            # add array to store the number of rejections
            # at each step
            self.data['nb_rejects'] = np.zeros(nb_steps + 1)

        # simulation data is stored in nb_samples times nb_steps+1 matrix
        # rows of the matrix are the trajectories of simulation
        # columns of the matrix are the ensembles approx. distributions
        self.data['samples'] = np.zeros((self.param['samples'],
                                        nb_steps - s_step + 1))
        if s_step == 0:
            self.data['samples'][:, 0] = init_sample
        old_sample = init_sample
        for j in range(1, nb_steps + 1):
            start = clock()
            sample, nb_rejects = self.methods[method](self, old_sample,
                                                      dt, ga, we, ka[j-1],
                                                      max_length)
            if j >= s_step:
                self.data['samples'][:, j - s_step] = sample
            self.data['nb_rejects'] = nb_rejects
            old_sample = sample
            end = clock()
            self.data['step_time'][j] = end - start

    # ADDITIONAL FUNCTIONS
    #########################################################################

    # get methods

    def get_all_param(self):
        return self.param.values()  # ??

    def get_all_samples(self):
        return self.data['samples']

    def get_nb_traj(self):
        return self.data['samples'].shape[0]

    def get_traj(self, nb_traj):
        if nb_traj <= self.get_nb_traj():
            return self.data['samples'][nb_traj, :]
        else:
            return None

    def get_nb_steps(self):  # total number of steps in simulation
        return self.data['samples'].shape[1] - 1

    def get_sample(self, t):

        T = self.param['T']
        Ts = self.param['T_store']
        dt = self.param['dt']

        if t > T or t < Ts:
            raise ValueError('No sample for time {0:3g}'.format(t))

        sample_nb = int(np.ceil(t / dt)) - int(np.ceil(Ts / dt))
        return self.data['samples'][:, sample_nb]

    def get_total_time(self):
        return self.data['step_time'].sum()

    def get_directory(self):
        return self.data_dir

    def get_fname(self):
        return self.fname

    def get_stress(self, t):

        sample = self.get_sample(t)

        return self.model.get_stress(sample)

    # histogram density estimation
    def get_hde(self, t):

        sample = self.get_sample(t)

        # use Rice rule for the number of bins
        nb_bins = int(np.ceil(2 * len(sample)**(1/3)))

        hist, bins = np.histogram(np.absolute(sample),
                                  bins=nb_bins,
                                  density=True)

        # normalize so that it is the positive part
        # of symmetric density
        hist = 0.5 * hist

        # add 0 as the last value
        hist = np.r_[hist, 0]

        return bins, hist

    # kernel density estimation
    def get_kde(self, t, x_grid, bw=None):

        sample = self.get_sample(t)

        return self.model.get_kde(sample, x_grid, bw)

    # saving and reading data
    def dump(self):
        import pickle
        # high_prot = pickle.HIGHEST_PROTOCOL

        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)
        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)

        f = open(param_fname, 'wb')
        pickle.dump(self.model.param, f)
        pickle.dump(self.param, f)
        f.close()

        np.save(data_fname, self.data['samples'])
        data_keys = self.data.keys()
        np.savez(add_data_fname,
                 **{k: self.data[k] for k in data_keys if k != 'samples'})

    def load(self):
        import pickle

        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)
        if not os.path.exists(param_fname):
            raise OSError('File not found')

        f = open(param_fname, 'rb')
        model_param_tmp = pickle.load(f)
        if not self.model.param == model_param_tmp:
            raise ValueError('Model parameters disagree')

        sim_param_tmp = pickle.load(f)
        if not self.param == sim_param_tmp:
            raise ValueError('Simulation parameters disagree')
        f.close()

        self.data['samples'] = np.load(data_fname, mmap_mode='r')
        add_data = dict(np.load(add_data_fname))
        self.data.update(add_data)

    # moments
    def get_raw_moment(self, l, t):
        # l - moment index
        # t - time

        sample = self.get_sample(t)
        return self.model.get_raw_moment(l, sample)

    def get_norm_raw_moment(self, l, t):
        # l - moment index
        # t - time

        sample = self.get_sample(t)

        return self.model.get_norm_raw_moment(l, sample)


class CPI(object):

    def __init__(self, init_ens=None, model=None, param=None):

        # default simulation parameters
        self.param = {'Dt': 10**(-3),  # macroscopic time step
                      'T': 2.0,  # total time
                      'nb_mom': 3,  # info used from micro distr.
                      'newt_tol': 10**(-9),  # tolerance for Newton optim
                      'max_newt_steps': 10,
                      'out_method': 'PFE',  # method for outer integrator
                      'match_type': 'KLD',
                      'dt': 10**(-4),  # microscopic time step
                      'K': 5,  # number of inner steps
                      'inn_method': 'ar',  # method for inner integrator
                      'samples': 10**4,  # number of samples for inner int.
                      'seed': 1001,
                      'max_vg': 4.0}

        # setting model
        if model is None:
            model = Model()
        self.model = model

        # check and update default parameters with
        # values from param
        if param is not None:
            if not all(k in self.param for k in param):
                raise KeyError('Wrong model parameters')
            self.param.update(param)

        # if initial sample is not given, we use sample from equilibrum
        # density corresponding to zero velocity gradient
        if init_ens is None:
            model_zero = Model({'kappa': 0,
                                'We': self.model.param['We'],
                                'gamma': self.model.param['gamma']})
            init_ens = model_zero.sample_eq_dens(self.param['samples'])
        else:  # else we check if all samples are within -gamma and gamma
            self.model.check_lengths(init_ens)
            # update the number of samples
            self.param['samples'] = len(init_ens)
        self.init_ens = init_ens

        # dictionary for storing data
        self.data = {}

        # model parameters
        ga, we, ka = self.model.get_param()

        # coarse simulation parameters
        Dt = self.param['Dt']
        dt = self.param['dt']
        T = self.param['T']
        nb_mom = self.param['nb_mom']

        # path & filename construction
        model_dir = self.model.get_directory()
        cpi_dir = 'cpi_{0:.3g}T_{1:.3g}Dt_{2:.3g}dt_{3:d}mom'.format(T, Dt,
                                                         dt, nb_mom)

        # directory for data storing
        self.data_dir = os.path.join('data', model_dir, cpi_dir)
        self.fname = '{0}_{1:.1e}'.format(self.param['seed'],
                                          self.param['samples'])
        self.data_fname = self.fname + '.npy'
        self.add_data_fname = self.fname + '.npz'
        self.param_fname = self.fname + '.dat'

    # simulation
    def perform(self):
        from time import clock
        import match

        # seed setting
        cpi_rng = np.random.RandomState(self.param['seed'])
        cpi_rng.randint(10**5, size=10**3)  # warm up of RNG

        # set the number of time steps
        T = self.param['T']
        Kdt = self.param['K'] * self.param['dt']
        Dt = self.param['Dt']
        nb_steps = int(np.ceil(T / Dt))
        max_nb_run = Dt / Kdt

        # set the array for timing
        self.data['step_time'] = np.zeros(nb_steps + 1)
        
        # set array for counting extrapolation time domain
        self.data['extr_dom'] = np.zeros(nb_steps + 1)

        # set the arrays for weighted ensembles
        self.data['ensembles'] = np.zeros((self.param['samples'], nb_steps+1))
        self.data['weights'] = np.zeros((self.param['samples'], nb_steps+1))

        # initial condition
        ens = self.init_ens
        weights = np.ones_like(ens)

        self.data['ensembles'][:,0] = ens
        self.data['weights'][:,0] = weights
        self.data['nb_res'] = 0.0
        self.data['match_fails'] = 0

        nb_run = max_nb_run - 1
        T_inner = 0
        # maximal entropy of weights
        if self.param['match_type'] == 'KLD':
            max_ent = np.log(len(weights))
        if self.param['match_type'] == 'L2D':
            max_ent = (len(weights) - 1)**2 / len(weights)

        for n in range(1, nb_steps+1):
            start = clock()
            
            conv = False
            ind = 0  # changes to 1 if second run needed of mic sim
            while True:

                # microscopic simulation
                t_init = (n-1) * Dt + ind * T_inner
                def loc_drag(t):
                    return self.model.vel_grad(t_init + t)

                if loc_drag(0.0) <= self.param['max_vg']:
                    T_inner = nb_run * Kdt
                else:
                    T_inner = Dt

                # details for inner integrator
                mic_param = {'dt': self.param['dt'],
                             'T': T_inner + 0.5*self.param['dt'],
                             'samples': self.param['samples'],
                             'seed': cpi_rng.randint(10**5)}
                # details for inner model
                loc_model = Model({'gamma': self.model.param['gamma'],
                                   'kappa': loc_drag,
                                   'We': self.model.param['We']})
                inner = Sim(ens, loc_model, mic_param)
                inner.perform()
                
                if ind == 1 or T_inner == Dt:
#                    print('No extrapolation between {0:.3f} and {1:.3f}'.format(
#                        (n-1) * Dt, n * Dt))
                    ens = inner.get_sample(T_inner)
                    nb_run = max_nb_run - 1 * (1-ind) 
                    break
#                if ind * T_inner > Dt:
#                    print('!!!')

                # extrapolation & matching
                mom0 = np.array([self.model.get_norm_raw_moment(2*(k + 1),
                                                       inner.get_sample(T_inner - Kdt),
                                                       weights = weights)
                        for k in range(self.param['nb_mom'])])
                mom1 = np.array([self.model.get_norm_raw_moment(2*(k + 1),
                                                       inner.get_sample(T_inner),
                                                       weights = weights)
                        for k in range(self.param['nb_mom'])])
                targ_mom = self.pfe(mom0, mom1, Kdt, Dt - T_inner + Kdt)

                ma = match.Match(pr_sample=ens,
                                 weights=weights,
                                 targ_mom=targ_mom,
                                 match_type=self.param['match_type'],
                                 tol=self.param['newt_tol'],
                                 max_steps=self.param['max_newt_steps'],
                                 scale_fact=self.model.param['gamma'])
                ma.perform()
                conv = ma.data['conv']

                if conv == True:
                    weights = ma.get_weights()
                    nb_run = max(nb_run - 1, 1)
                    self.data['extr_dom'][n] = Dt - T_inner
                    break
                else: 
                    ens = inner.get_sample(T_inner)
                    nb_run = max_nb_run - nb_run
                    ind = 1

            # resampling
            if n % 10 == 0 and ent(weights, self.param['match_type']) > max_ent / 10:
                ens = resample(ens, weights, cpi_rng.randint(10**5))
                weights = np.ones_like(ens)
                self.data['nb_res'] += 1

            self.data['ensembles'][:, n] = ens
            self.data['weights'][:, n] = weights
            end = clock()
            self.data['step_time'][n] = end - start

    # projective forward Euler step
    def pfe(self, x0, x1, dt, Dt):
        return x0 + (x1 - x0) * Dt / dt

    def get_ens(self, t):
        Dt = self.param['Dt']

        ens_nb = int(np.ceil(t / Dt))
        return self.data['ensembles'][:, ens_nb], self.data['weights'][:, ens_nb]

    def get_norm_raw_moment(self, l, t):
        # l - moment index
        # t - time

        ens, weights = self.get_ens(t)

        return self.model.get_norm_raw_moment(l, ens, weights)

    def get_stress(self, t):
        
        ens, weights = self.get_ens(t)

        return self.model.get_stress(ens, weights)

    def get_total_time(self):
        return self.data['step_time'].sum()

    def get_total_extr_dom(self):
        return self.data['extr_dom'].sum()

    # saving and reading data
    def dump(self):
        import pickle
        # high_prot = pickle.HIGHEST_PROTOCOL

        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)
        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)

        f = open(param_fname, 'wb')
        pickle.dump(self.model.param, f)
        pickle.dump(self.param, f)
        f.close()

        ens_data = np.dstack((self.data['ensembles'], self.data['weights']))
        np.save(data_fname, ens_data)
        data_keys = self.data.keys()
        np.savez(add_data_fname,
                 **{k: self.data[k] for k in data_keys
                     if k not in ['ensembles', 'weights']})

    def load(self):
        import pickle

        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)
        if not os.path.exists(param_fname):
            raise OSError('File not found')

        f = open(param_fname, 'rb')
        model_param_tmp = pickle.load(f)
        if not self.model.param == model_param_tmp:
            raise ValueError('Model parameters disagree')

        sim_param_tmp = pickle.load(f)
        if not self.param == sim_param_tmp:
            raise ValueError('Simulation parameters disagree')
        f.close()

        ens_data = np.load(data_fname, mmap_mode='r')
        [particles, weights] = np.dsplit(ens_data, 2)
        self.data['ensembles'] = np.squeeze(particles)
        self.data['weights'] = np.squeeze(weights)
        # print(self.data['ensembles'].shape, self.data['weights'].shape)
        add_data = dict(np.load(add_data_fname))
        self.data.update(add_data)


class CPI2(object):

    def __init__(self, init_ens=None, model=None, param=None):

        # default simulation parameters
        self.param = {'Dt': 10**(-3),  # max macroscopic time-step
                      'T': 2.0,  # total time of simulation
                      'nb_mom': 3,  # info used from micro distr.
                      'newt_tol': 10**(-9),  # tolerance for Newton optim
                      'max_newt_steps': 10,
                      'out_method': 'PFE',  # method for outer integrator
                      'match_type': 'KLD',
                      'dt': 10**(-4),  # microscopic time step
                      'K': 5,  # number of inner steps
                      'inn_method': 'ar',  # method for inner integrator
                      'samples': 10**4,  # number of samples for inner int.
                      'seed': 1001,
                      'fact': [0.5, 1.2]}  # adaptive time-stepping

        # setting model
        if model is None:
            model = Model()
        self.model = model

        # check and update default parameters with
        # values from param
        if param is not None:
            if not all(k in self.param for k in param):
                raise KeyError('Wrong model parameters')
            self.param.update(param)

        # if initial sample is not given, we use sample from equilibrum
        # density corresponding to zero velocity gradient
        if init_ens is None:
            model_zero = Model({'kappa': 0,
                                'We': self.model.param['We'],
                                'gamma': self.model.param['gamma']})
            init_ens = model_zero.sample_eq_dens(self.param['samples'])
        else:  # else we check if all samples are within -gamma and gamma
            self.model.check_lengths(init_ens)
            # update the number of samples
            self.param['samples'] = len(init_ens)
        self.init_ens = init_ens

        # dictionary for storing data
        self.data = {}

        # model parameters
        ga, we, ka = self.model.get_param()

        # coarse simulation parameters
        Dt = self.param['Dt']
        dt = self.param['dt']
        T = self.param['T']
        nb_mom = self.param['nb_mom']

        # path & filename construction
        model_dir = self.model.get_directory()
        cpi_dir = 'cpi2_{0:.3g}T_{1:.3g}Dt_{2:.3g}dt_{3:d}mom'.format(T, Dt,
                                                         dt, nb_mom)

        # directory for data storing
        self.data_dir = os.path.join('data', model_dir, cpi_dir)
        self.fname = '{0}_{1:.1e}'.format(self.param['seed'],
                                          self.param['samples'])
        self.data_fname = self.fname + '.npy'
        self.add_data_fname = self.fname + '.npz'
        self.param_fname = self.fname + '.dat'

    # simulation
    def perform(self):
        from time import clock
        import match

        # seed setting
        cpi2_rng = np.random.RandomState(self.param['seed'])
        cpi2_rng.randint(10**5, size=10**3)  # warm up of RNG

        # parameters
        T = self.param['T']
        Kdt = self.param['K'] * self.param['dt']
        max_Dt = self.param['Dt']
        dec = self.param['fact'][0]  # decreasing factor
        inc = self.param['fact'][1]  # increasing factor
        m_type = self.param['match_type']

        self.data['tmesh'] = [0.0]  # time-mesh
        self.data['step_time'] = []  # timing
        self.data['extr_dom'] = []  # extrapolation domain

        # arrays for weighted ensembles
        nb_steps = int(np.ceil(T / (dec * max_Dt)))  # provisional
        nb_samples = self.param['samples']  # fixed
        self.data['ensembles'] = np.zeros((nb_samples, nb_steps))
        self.data['weights'] = np.zeros((nb_samples, nb_steps))

        # initial condition
        time = self.data['tmesh'][0]  # current time in the simulation
        step = 0
        Dt = max_Dt  # macroscopic (extrapolation) step
        ens = self.init_ens
        weights = np.ones_like(ens)

        self.data['ensembles'][:, step] = ens
        self.data['weights'][:, step] = weights
        self.data['nb_res'] = 0

        # maximal entropy of weights
        if m_type == 'KLD':
            max_ent = np.log(len(weights))
        if m_type == 'L2D':
            max_ent = (len(weights) - 1)**2 / len(weights)

        while time < T:
            start = clock()

            # microscopic simulation
            def loc_drag(t):  # shift the time in drag
                return self.model.vel_grad(time + t)

            # details for inner model
            loc_model = Model({'gamma': self.model.param['gamma'],
                               'kappa': loc_drag,
                               'We': self.model.param['We']})
            # details for inner integrator
            mic_param = {'dt': self.param['dt'],
                         'T': Kdt + 0.5*self.param['dt'],
                         'samples': nb_samples,
                         'seed': cpi2_rng.randint(10**5)}
            inner = Sim(ens, loc_model, mic_param)
            inner.perform()
            ens = inner.get_sample(Kdt)

            while True:

                # extrapolation
                mom0 = np.array([self.model.get_norm_raw_moment(2*(k + 1),
                                                       inner.get_sample(0.0),
                                                       weights = weights)
                        for k in range(self.param['nb_mom'])])
                momKdt = np.array([self.model.get_norm_raw_moment(2*(k + 1),
                                                       inner.get_sample(Kdt),
                                                       weights = weights)
                        for k in range(self.param['nb_mom'])])
                targ_mom = self.pfe(mom0, momKdt, Kdt, Dt)

                # matching
                ma = match.Match(pr_sample=ens,
                                 weights=weights,
                                 targ_mom=targ_mom,
                                 match_type=m_type,
                                 tol=self.param['newt_tol'],
                                 max_steps=self.param['max_newt_steps'],
                                 scale_fact=self.model.param['gamma'])
                ma.perform()
                conv = ma.data['conv']

                if conv:
                    weights = ma.get_weights()
                    self.data['extr_dom'].append(Dt - Kdt)
                    time = time + Dt
                    self.data['tmesh'].append(time)
                    step += 1
                    Dt = min(inc * Dt, max_Dt)
                    break

                Dt = max(dec * Dt, Kdt)

            # resampling
            if step % 10 == 0 and ent(weights, m_type) > max_ent / 10:
                ens = resample(ens, weights, cpi2_rng.randint(10**5))
                weights = np.ones_like(ens)
                self.data['nb_res'] += 1

            # append data
            try:
                self.data['ensembles'][:, step] = ens
                self.data['weights'][:, step] = weights
            except IndexError:  # resizing
                self.data['ensembles'] = np.c_[self.data['ensembles'],
                                               np.zeros((nb_samples, nb_steps))
                                               ]
                self.data['weights'] = np.c_[self.data['weights'],
                                             np.zeros((nb_samples, nb_steps))
                                             ]
                self.data['ensembles'][:, step] = ens
                self.data['weights'][:, step] = weights
            end = clock()
            self.data['step_time'].append(end - start)

        # trim data
        self.data['ensembles'] = self.data['ensembles'][:, 0:step + 1]
        self.data['weights'] = self.data['weights'][:, 0:step + 1]

    # projective forward Euler step
    def pfe(self, x0, x1, dt, Dt):
        return x0 + Dt * (x1 - x0) / dt

    def get_ens(self, t):
        T = self.param['T']
        if t > T or t < 0:
            raise ValueError('Time out of simulation bounds')

        times = self.data['tmesh']
        try:
            ind = next(times.index(tval) - 1 for tval in times if tval > t)
        except StopIteration:
            ind = len(times) - 1

        return self.data['ensembles'][:, ind], self.data['weights'][:, ind]

    def get_norm_raw_moment(self, l, t):
        # l - moment index
        # t - time

        ens, weights = self.get_ens(t)

        return self.model.get_norm_raw_moment(l, ens, weights)

    def get_stress(self, t):
        
        ens, weights = self.get_ens(t)

        return self.model.get_stress(ens, weights)

    def get_total_time(self):
        return sum(self.data['step_time'])

    def get_total_extr_dom(self):
        return sum(self.data['extr_dom'])

    # saving and reading data
    def dump(self):
        import pickle
        # high_prot = pickle.HIGHEST_PROTOCOL

        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)
        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)

        f = open(param_fname, 'wb')
        pickle.dump(self.model.param, f)
        pickle.dump(self.param, f)
        f.close()

        ens_data = np.dstack((self.data['ensembles'], self.data['weights']))
        np.save(data_fname, ens_data)
        data_keys = self.data.keys()
        np.savez(add_data_fname,
                 **{k: self.data[k] for k in data_keys
                     if k not in ['ensembles', 'weights']})

    def load(self):
        import pickle

        param_fname = os.path.join(self.data_dir, self.param_fname)
        data_fname = os.path.join(self.data_dir, self.data_fname)
        add_data_fname = os.path.join(self.data_dir, self.add_data_fname)
        if not os.path.exists(param_fname):
            raise OSError('File not found')

        f = open(param_fname, 'rb')
        model_param_tmp = pickle.load(f)
        if not self.model.param == model_param_tmp:
            raise ValueError('Model parameters disagree')

        sim_param_tmp = pickle.load(f)
        if not self.param == sim_param_tmp:
            raise ValueError('Simulation parameters disagree')
        f.close()

        ens_data = np.load(data_fname, mmap_mode='r')
        [particles, weights] = np.dsplit(ens_data, 2)
        self.data['ensembles'] = np.squeeze(particles)
        self.data['weights'] = np.squeeze(weights)
        # print(self.data['ensembles'].shape, self.data['weights'].shape)
        add_data = dict(np.load(add_data_fname))
        self.data.update(add_data)


#################################################################
# Additional methods

# time-dependent velocity gradients
def instantWe(t):
    if t < 1:
        return 100 * t * (1-t) * np.exp(-4*t)
    else:
        return 0

def perInstantWe(t):
    t_per = t % 2
    return instantWe(t_per)
#    if t_per < 1:
#        return 100 * t_per * (1-t_per) * np.exp(-4*t_per)
#    else:
#        return 0

def sinusoidal(t):
	return 2.0*(1.1 + np.sin(np.pi*t))

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
    except TypeError:
        return False

def raw_moment(sample, ind, weights = None):
    return np.average(sample**ind, weights = weights)


def hde(sample, nbins = None):
    if nbins == None:
        # use Rice rule for the number of bins
        nbins = int(np.ceil(2 * len(sample)**(1/3)))

    hist, bin_edges = np.histogram(sample,
                              bins = nbins,
                              density = True)
    grid = (bin_edges[:-1] + bin_edges[1:]) / 2.0

    return grid, hist

# kernel density estimation
def kde(sample, grid, bw):
    import sklearn as skl
    from sklearn.neighbors.kde import KernelDensity
    from sklearn.grid_search import GridSearchCV
    
    # denisty computation
    kde = KernelDensity(kernel='gaussian', bandwidth=bw, rtol=1E-5).fit(sample[:, np.newaxis])
    log_pdf = kde.score_samples(grid[:, np.newaxis])
    
    #dens = sm.nonparametric.KDEUnivariate(self.traj[:,step_nr])
    #dens.fit(bw = "silverman", adjust = g*10**(-2))#kernel = "triw", 
    #bw = "silverman", fft = False, cut = self.model.param['gamma'])
    return np.exp(log_pdf)


def ent(weights, match_type):
    nweights = weights / np.sum(weights)
    if match_type == 'KLD':
        return np.sum(nweights * np.log(len(nweights)*nweights))
    if match_type == 'L2D':
        return np.sum((len(nweights)*nweights - 1)**2) / len(nweights)


def resample(sample, weigths, seed):
    # resampling (stratified)

    # seed setting
    res_rng = np.random.RandomState(seed)
    res_rng.randint(10**5, size=10**3)  # warm up of RNG

    nb_sam = len(sample)
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + res_rng.rand(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros(nb_sam)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample


