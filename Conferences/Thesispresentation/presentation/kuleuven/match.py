# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 09:59:21 2015

@author: PrzemekZ
"""
import numpy as np
import scipy as sp
from scipy.linalg import hankel
from numpy.polynomial.polynomial import polyval as pval
# for c=[c_0,...,c_k], pval(x,c)=c_0 + c_1*x +...+ c_k*x**k


class Match(object):

    def __init__(self, pr_sample=None, weights=None, targ_mom=None,
                 match_type='KLD', tol=10**(-9), max_steps=100,
                 scale_fact=None):

        self.input = {}
        self.data = {}

        # checking prior sample
        if pr_sample is None:
            raise ValueError('Specify prior sample!')
        self.input['prior'] = pr_sample

        if weights is None:
            weights = np.ones_like(pr_sample)
        self.input['weights'] = weights

        # checking moments
        if targ_mom is None:
            raise ValueError('Specify moments of target distribution!')
        self.input['moments'] = targ_mom

        # available matching types
        self.match_types = {'KLD': Match.matchKLD,
                            'KLDv2': Match.matchKLDv2,
                            'KLDv3': Match.matchKLDv3,
                            'KLDv4': Match.matchKLDv4,
                            'L2D': Match.matchL2D,
                            'L2N': Match.matchL2N}

        if match_type in self.match_types:
            self.input['match_type'] = match_type
        else:
            raise KeyError('No such matching type is implemented')

        if scale_fact is None:
            self.input['scale_fact'] = 1
        else:
            self.input['scale_fact'] = scale_fact

        self.input['tol'] = tol
        self.input['max_steps'] = max_steps  # max number of Newton iterations

        self.data['lm_time'] = 0.0  # timing of Lagrange mult. computation
        self.data['conv'] = False  # does method converged

    def perform(self):
        import time

        match_type = self.input['match_type']
        prior = self.input['prior']
        targ_mom = self.input['moments']

        # stored values
        self.data['of'] = []  # values of Obj. Func.
        self.data['grad'] = []  # gradients of OF
        self.data['hess'] = []  # Hessians of OF
        self.data['lm'] = [np.zeros(len(targ_mom) + 1)]  # Lagrange multipliers

        start = time.clock()
        self.match_types[match_type](self, prior=prior,
                                     weights=self.input['weights'],
                                     targ_mom=targ_mom)
        self.data['lm_time'] = time.clock() - start

    # MATCHING IMPLEMENTATIONS
    ########################################################

    # matching with Kulback-Leibler Divergence (log relative entropy)
    def matchKLD(self, prior, weights, targ_mom):
        # prior, weights - weighted prior ensamble
        # targ_mom - positive even moments of target ensamble

        nb_lm = len(targ_mom) + 1  # number of Lagrange mult.
        ext_targ_mom = np.r_[1, targ_mom]  # with 0th = 1

        # initial values of Lagrange multipliers
        # correspond to prior sample
        lm = self.data['lm'][0]

        tol = self.input['tol'] * ext_targ_mom
        sf = self.input['scale_fact']

        #######################################
        # Newton-Raphson iteration to determine
        # the Lagrange multipliers
        step = 1
        max_steps = self.input['max_steps']
        np.seterr(over='raise')
        while True:

            # likelihood ratios corr. to curr. lm
            try:
                lr = np.exp(pval((prior / sf)**2, lm))
            except FloatingPointError:
                print('Matching stopped: too big lr')
                break
            # self.data['lr'].append(lr)

            # value of obj. funct.
            self.data['of'].append(np.dot(ext_targ_mom, lm) -
                                   np.average(lr, weights=weights) + 1)

            # raw moments of weighted prior
            raw_mom = np.zeros(2 * nb_lm - 1)
            for k in range(2 * nb_lm - 1):
                raw_mom[k] = np.average((prior / sf)**(2*k) * lr,
                                        weights=weights)

            # gradient of objective function
            grad = ext_targ_mom - raw_mom[:nb_lm]
            self.data['grad'].append(grad)
            if all(np.absolute(grad) < tol) or step == max_steps:
                if step < max_steps:
                    self.data['conv'] = True
                else:
                    print('Matching stopped: no convergence')
                break

            # hessian of objective function
            hess = -hankel(raw_mom[:nb_lm], raw_mom[nb_lm - 1:])
            self.data['hess'].append(hess)

            # Newton step
            try:
                lm -= sp.linalg.solve(hess, grad)
            except np.linalg.LinAlgError:
                print('Matching stopped: singular hessian')
                break
            self.data['lm'].append(lm)

            step += 1
            
    # second method that uses the partition function
    # instead of the additional zeroth Lagrange mult.
    def matchKLDv2(self, prior, weights, targ_mom):
        from numpy.polynomial.polynomial import polyval as pval
        # for c=[c_0,...,c_k], pval(x,c)=c_0 + c_1*x +...+ c_k*x**k

        nb_mom = len(targ_mom)
        tol = self.input['tol'] * np.array(targ_mom)
        sf = self.input['scale_fact']
        lm = self.data['lm'][0][1:]

        mom_diff = np.zeros((nb_mom, len(prior)))
        for k in range(nb_mom):
            mom_diff[k] = (prior / sf)**(2*(k+1)) - targ_mom[k]

        #######################################
        # Newton-Raphson iteration to determine
        # the Lagrange multipliers
        step = 1
        max_steps = self.input['max_steps']
        np.seterr(over='raise')
        while True:

            # likelihood ratios corr. to curr. lm
            try:
                lr = np.exp(np.dot(lm, mom_diff)) * weights
            except FloatingPointError:
                print('Matching stopped: too big lr')
                break
            # self.data['lr'].append(lr)

            integr = np.average(lr)

            # partition function
            part = -np.log(integr)
            # value of obj. funct.
            self.data['of'].append(1 - integr)

            # gradient of objective function
            grad = -np.average(mom_diff * lr, axis=1)
            self.data['grad'].append(grad)
            if all(np.absolute(grad) < tol) or step == max_steps:
                if step < max_steps:
                    self.data['conv'] = True
                else:
                    print('Matching stopped: no convergence')
                break

            # hessian of objective function
            hess = np.zeros((nb_mom, nb_mom))
            for k in range(nb_mom):
                for j in range(k, nb_mom):
                    hess[k, j] = -np.average(mom_diff[k] * mom_diff[j] * lr)
                    hess[j,k] = hess[k,j]
            self.data['hess'].append(hess)

            # Newton step
            try:
                lm -= sp.linalg.solve(hess, grad)
            except np.linalg.LinAlgError:
                print('Matching stopped: singular hessian')
                break
            self.data['lm'].append(np.r_[part, lm])

            step += 1

    # third method that computes te Newton step with
    # pseudo-inverse of appropriate matrix
    def matchKLDv3(self, prior, weights, targ_mom):
        # prior, weights - weighted prior ensamble
        # targ_mom - positive even moments of target ensamble

        nb_mom = len(targ_mom)
        nb_lm = nb_mom + 1  # number of Lagrange mult.
        ext_targ_mom = np.r_[1, targ_mom]  # with 0th = 1

        # initial values of Lagrange multipliers
        # correspond to prior sample
        lm = self.data['lm'][0]

        tol = self.input['tol'] * ext_targ_mom
        sf = self.input['scale_fact']
        
        mom_diff = np.zeros((nb_lm, len(prior)))
        for k in range(nb_lm):
            mom_diff[k] = (prior / sf)**(2*k) - ext_targ_mom[k]
        
        wgh_root = np.sqrt(weights)

        #######################################
        # Newton-Raphson iteration to determine
        # the Lagrange multipliers
        step = 1
        max_steps = self.input['max_steps']
        np.seterr(over='raise')
        while True:

            # likelihood ratios corr. to curr. lm
            try:
                lr_root = np.exp(0.5 * np.dot(lm, mom_diff)) * wgh_root
            except FloatingPointError:
                print('Matching stopped: too big lr')
                break
            # self.data['lr'].append(lr)
            mat = mom_diff * lr_root

            # value of obj. funct.
            self.data['of'].append(1 - np.average(lr_root**2))

            # raw moments of weighted prior
            #raw_mom = np.zeros(2 * nb_lm - 1)
            #for k in range(2 * nb_lm - 1):
            #    raw_mom[k] = np.average((prior / sf)**(2*k) * lr,
            #                            weights=weights)

            # gradient of objective function
            grad = -np.dot(mat,lr_root)
            self.data['grad'].append(grad)
            if all(np.absolute(grad) < tol) or step == max_steps:
                if step < max_steps:
                    self.data['conv'] = True
                else:
                    print('Matching stopped: no convergence')
                break

            # hessian of objective function
            #hess = -np.dot(mat, np.transpose(mat))
            #self.data['hess'].append(hess)

            # Newton step
            try:
                lm -= np.dot(lr_root, sp.linalg.pinv(mat))
            except np.linalg.LinAlgError:
                print('Matching stopped: singular hessian')
                break
            self.data['lm'].append(lm)

            step += 1

    # fourth method that uses the Robbins-Monro algorithm
    def matchKLDv4(self, prior, weights, targ_mom):
        #from numpy.polynomial.polynomial import polyval as pval
        # for c=[c_0,...,c_k], pval(x,c)=c_0 + c_1*x +...+ c_k*x**k

        nb_mom = len(targ_mom)
        tol = self.input['tol'] * np.array(targ_mom)
        sf = self.input['scale_fact']
        lm = self.data['lm'][0][1:]

        mom_diff = np.zeros((nb_mom, len(prior)))
        for k in range(nb_mom):
            mom_diff[k] = (prior / sf)**(2*(k+1)) - targ_mom[k]

        #######################################
        # iteration to determine
        # the Lagrange multipliers
        step = 1
        max_steps = self.input['max_steps']
        nb_subsam = 10
        gamma = 0.6
        step_seq = 0.1
        np.seterr(over='raise')
        while True:

            # sub-sampling prior ensemble
            ind = np.random.randint(len(prior), size=nb_subsam)
            step_seq = step_seq * step**(-gamma)

            # likelihood ratios corr. to curr. lm
            try:
                lr = np.exp(np.dot(lm, mom_diff)) * weights
            except FloatingPointError:
                print('Matching stopped: too big lr')
                break
            # self.data['lr'].append(lr)

            integr = np.average(lr)

            # partition function
            part = -np.log(integr)
            # value of obj. funct.
            self.data['of'].append(1 - integr)

            # gradient of objective function
            grad = -np.average(mom_diff * lr, axis=1)
            self.data['grad'].append(grad)
            if all(np.absolute(grad) < tol) or step == max_steps:
                if step < max_steps:
                    self.data['conv'] = True
                else:
                    print('Matching stopped: no convergence')
                break

            # hessian of objective function
            hess = np.identity(nb_mom)
            #for k in range(nb_mom):
            #    for j in range(k, nb_mom):
            #        hess[k, j] = -np.average(mom_diff[k] * mom_diff[j] * lr)
            #        hess[j,k] = hess[k,j]
            self.data['hess'].append(hess)

            # Robbins-Monro step
            try:
                lm -= step_seq * np.average(mom_diff[:, ind] * lr[ind], axis=1)
            except np.linalg.LinAlgError:
                print('Matching stopped: singular hessian')
                break
            self.data['lm'].append(np.r_[part, lm])

            step += 1


    # matching with L^2 divergence (quadratic relative entropy)
    def matchL2D(self, prior, weights, targ_mom):
        # prior, weights - weighted prior ensamble
        # targ_mom - positive even moments of target sample

        nb_lm = len(targ_mom) + 1  # number of Lagrange mult.
        ext_targ_mom = np.r_[1, targ_mom]  # with 0th moment = 1
        tol = self.input['tol'] * ext_targ_mom
        sf = self.input['scale_fact']

        # initial values of Lagrange multipliers
        # correspond to prior sample
        lm = self.data['lm'][0]

        #######################################
        # Newton-Raphson iteration to determine
        # the Lagrange multipliers
        step = 1
        max_steps = self.input['max_steps']
        np.seterr(over='raise')
        while True:

            # likelihood ratios corr. to curr. lm
            try:
                lr = np.maximum(0, 1 + pval((prior / sf)**2, lm))
            except FloatingPointError:
                print('Matching stopped: too big weights')
                break

            # raw moments of trimmed prior
            raw_mom = np.zeros(2 * nb_lm - 1)
            for k in range(2 * nb_lm - 1):
                raw_mom[k] = np.average((prior / sf)**(2*k) * np.sign(lr),
                                        weights=weights)

            # hessian of objective function
            hess = -hankel(raw_mom[:nb_lm], raw_mom[nb_lm - 1:])

            # objective function
            self.data['of'].append(np.dot(ext_targ_mom - raw_mom[:nb_lm], lm) +
                                   0.5 * np.dot(lm, np.dot(hess, lm)) +
                                   0.5 * (1 - raw_mom[0]))

            # gradient of objective function
            grad = ext_targ_mom + np.dot(hess, lm) - raw_mom[:nb_lm]
            self.data['grad'].append(grad)

            if all(np.absolute(grad) < tol) or step == max_steps:
                if step < max_steps:
                    self.data['conv'] = True
                else:
                    print('Matching stopped: no convergence')
                break

            self.data['hess'].append(hess)

            # Newton step
            try:
                lm = sp.linalg.solve(hess, raw_mom[:nb_lm] - ext_targ_mom)
            except np.linalg.LinAlgError:
                print('Matching stopped: singular hessian')
                break
            self.data['lm'].append(lm)

            step += 1

    # signed matching with L2 norm
    def matchL2N(self, prior, weights, targ_mom):
        # prior, weights - weighted prior ensamble
        # targ_mom - positive even moments of target sample

        nb_lm = len(targ_mom) + 1  # number of Lagrange mult.
        ext_targ_mom = np.r_[1, targ_mom]  # with 0th moment = 1
        max_prior = max(abs(prior))
        sf = self.input['scale_fact']

        # initial values of Lagrange multipliers
        # correspond to prior sample
        lm = self.data['lm'][0]
        self.data['of'].append(0.0)

        # raw moments of prior
        raw_mom = np.zeros(nb_lm)
        for k in range(nb_lm):
            raw_mom[k] = np.average((prior / sf)**(2*k), weights=weights)

        # Hilber-like matrix
        hilb = 2.0 * sf * hankel(
            [(max_prior / sf)**(2*k+1) / (2*k + 1) for k in range(nb_lm)],
            [(max_prior / sf)**(2*k+1) / (2*k + 1)
             for k in range(nb_lm - 1, 2*nb_lm - 1)])
        self.data['grad'].append(ext_targ_mom - raw_mom)
        self.data['hess'].append(-hilb)

        try:
            lm = sp.linalg.solve(hilb, ext_targ_mom - raw_mom)
        except np.linalg.LinAlgError:
            print('Matching stopped: singular hessian')
        self.data['lm'].append(lm)
        self.data['of'].append(np.dot(ext_targ_mom - raw_mom, lm) -
                               0.5*np.dot(lm, np.dot(hilb, lm)))
        self.data['conv'] = True


    #############################################################
    # Additional methods

    def get_lm(self):

        return self.data['lm'][-1]

    def get_weights(self):

        lm = self.get_lm()
        prior = self.input['prior']
        weights = self.input['weights']
        sf = self.input['scale_fact']

        max_prior = max(self.input['prior'])
        bw = max_prior * 10**(-2)  # bandwidth (how to choose?)

        options = {'KLD': lambda x: np.exp(pval(x**2, lm)) * weights,
                   'KLDv2': lambda x: np.exp(pval(x**2, lm)) * weights,
                   'KLDv3': lambda x: np.exp(pval(x**2, lm)) * weights,
                   'KLDv4': lambda x: np.exp(pval(x**2, lm)) * weights,
                   'L2D': lambda x: np.maximum(0, pval(x**2, lm) + 1) *
                                    weights,
                   'L2N': lambda x: (pval(x**2, lm) /
                                    kde(prior[:len(prior)/10], x, bw) + 1)
                   }

        return options[self.input['match_type']](prior / sf)

    def get_sample(self):
        old_sample = self.input['prior']
        weights = self.get_weights()
        seed = np.random.random_integers(1000, 10000)

        return self.resample(old_sample, weights, seed)

    def get_hde(self):

        sample = self.get_sample()

        bin_edges, hist = hde(abs(sample))
        hist = 0.5 * hist

        return bin_edges, hist

    # kernel density estimation
    def get_kde(self, x_grid, bw=None):

        if bw is None:
            # ga = self.model.param['gamma']
            max_prior = max(self.input['prior'])
            bw = max_prior * 10**(-2)  # bandwith (how to choose?)

        if False:
            from numpy.polynomial.polynomial import polyval as pval
            # for c=[c_0,...,c_k], pval(x,c)=c_0 + c_1*x +...+ c_k*x**k

            lm = self.get_lm()
            prior = self.input['prior']

            return x_grid, kde(prior, x_grid, bw) + pval(x_grid**2, lm)
        else:
            sample = self.get_sample()

            return x_grid, kde(sample, x_grid, bw)

    def resample(self, sample, weigths, seed):
        # resampling (stratified)

        # seed setting
        np.random.seed(seed)
        np.random.randn(10**3)  # warm up of RNG

        nb_sam = len(sample)
        # stratified random numbers in [0,1)
        strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

        # normalized cumulative sum of weights
        ncs = np.cumsum(weigths) / np.sum(weigths)

        rsample = np.zeros(nb_sam)  # resampling
        # DC = np.zeros(nb_sam) # duplication count

        shelf = 0
        ind = 0
        while max(shelf, ind) < nb_sam:
            count = 0
            while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
                rsample[ind] = sample[shelf]  # duplication of samples
                count += 1
                ind += 1
            # DC[shelf] = count # duplication count update
            shelf += 1

        return rsample


# histogram density
def hde(sample, nbins=None):
    if nbins is None:
        # use Rice rule for the number of bins
        nbins = int(np.ceil(2 * len(sample)**(1/3)))

    hist, bin_edges = np.histogram(sample,
                                   bins=nbins,
                                   density=True)
    grid = (bin_edges[:-1] + bin_edges[1:]) / 2.0

    return grid, hist


# kernel density estimation
def kde(sample, grid, bw):
    # import sklearn as skl
    from sklearn.neighbors.kde import KernelDensity
    # from sklearn.grid_search import GridSearchCV

    # denisty computation
    kde = KernelDensity(kernel='gaussian',
                        bandwidth=bw,
                        rtol=1E-5).fit(sample[:, np.newaxis])
    log_pdf = kde.score_samples(grid[:, np.newaxis])

    return np.exp(log_pdf)
