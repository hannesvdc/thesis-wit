import numpy as np

from timeintegrators.noflux_euler_maruyama import *

class Bruna:
    def __init__(self, k, eps, tauy):
        # Fetch parameters
        k1 = k[0]
        k2 = k[1]
        k3 = k[2]
        k4 = k[3]
        k5 = k[4]
        k6 = k[5]
        self.eps = eps
        self.tauy = tauy
        
        # define SDE
        v = lambda x: k1 - k2*x + k3*x*(x-1) - k4*x*(x-1)*(x-2)
        d = lambda x: 0.5*(k1 + k2*x + k3*x*(x-1) + k4*x*(x-1)*(x-2))
        V = lambda x, y: k5*x - k6*y
        D = lambda x, y: 0.5*(k5*x + k6*y)
        self.a = lambda t, x: np.transpose(np.asarray([v(x[0])/eps, V(x[0], x[1])/tauy]))
        self.b = lambda t, x: np.transpose(np.asarray([np.sqrt(2*d(x[0])/eps), np.sqrt(2*D(x[0],x[1])/tauy)]))
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None):
        timestepper = NoFluxEulerMaryama(self.a, self.b)
        
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt

        return X, np.asarray(cbtimes), np.asarray(cbvalues)
        
