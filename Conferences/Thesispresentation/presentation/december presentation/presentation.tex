\documentclass[kul]{kulakbeamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{framed}
\usepackage{listings} 
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{algorithm,algpseudocode}
\usepackage{pgfpages}
\usepackage[backend=biber]{biblatex}
\bibliography{december.bib}
\addtobeamertemplate{footnote}{}{\vspace{2ex}}

\captionsetup{font=scriptsize,labelfont=scriptsize}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\title[Micro-macro acceleration]{Analysis of efficient simulation methods for stochastic processes}
\author{Hannes Vandecasteele \\ \\
			Promotor:  Giovanni Samaey \\
			Supervisor: Przemyslaw Zielinski}
\institute[Kulak]{KU Leuven}
\date{Academic year 2017 - 2018}

\begin{document}
\begin{titleframe}
	\titlepage
\end{titleframe}

\section{Introduction}

\begin{frame}
	\frametitle{Importance of Stochastic Differential Equations}
	General form
	\[
	dX(t) = a(t, X(t))dt + b(t, X(t))dW(t)
	\]
	with
	\begin{itemize}
		\item $a(t, X(t))$ a deterministic \textit{drift} term
		\item $b(t, X(t))$ a \textit{diffusion} term
		\item $W(t)$ Brownian motion: $W(t+\Delta t) - W(t) \sim \mathcal{N}(0, \Delta t)$
	\end{itemize}

	\vspace{2mm}
	In practice interested in expectations of the process $\mathbb{E}[R_l(X(t))]$
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{cutmypic}
	\label{fig:cutmypic}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Many practical problems are stiff}

\begin{itemize}
\item Coarse-grained model for $\mathbb{E}[R_l(X(t))]$ often not available
\item Full Monte Carlo simulation needed
\item Stiffness due to time scale separation with $\epsilon < 1$
\begin{align*}
dX(t) &= -\frac{1}{\epsilon}X(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_x(t) \\
dY(t) &= (X(t)-Y(t))dt + dW_y(t)
\end{align*}

or due to strict boundary conditions
\[
F(x) = \frac{b}{b-x^2}
\]

\end{itemize}
\note{Other methods: implicit methods, approximate models, extended stability domains, ...}
\end{frame}

\begin{frame}
\frametitle{General four-step coarse acceleration algorithm}
Projective integration:\footnotemark
\vspace{0.5mm}
\hline
\footnotetext[1]{Kevrekidis, Samaey, 2009}

\vspace{2mm}
\begin{algorithmic}
		\caption*{Projective Integration}
		\noindent
		\textbf{(i) Burst:}  Perform microscopic simulation with step $\delta t$ and ensemble $(X_j)_{j=1}^J$. \newline
		\textbf{(ii) Restriction:} Record moments of interest
		\[
			\mathbb{E}[R_l(X)], \ l= 1 \dots L
		\]
		of interest. \newline
		\textbf{(iii) Extrapolation:} Extrapolate these quantities over $\Delta t$
		\[
		m_l = \mathcal{E}(R_l(X), \delta t, \Delta t)
		\]
		\textbf{(iv) Lifting: } Find a new micro state consistent with $m_l$ and restart the algorithm.		
\end{algorithmic}
\end{frame}

\begin{frame}
\frametitle{Lifting has some disadvantages}
Converges to the exact solution when separation $\epsilon$ tends to zero.

\vspace{3mm}

\begin{itemize}
	\item Modelling error with finite scale separation \note{convergence when $\epsilon$ tends to zero}
	\item Finding microscopic state or distribution can be costly \note{example: bistable system}
	\item Extra microscopic steps needed for constrained simulation \footnotemark \note{if far from conditional equilibrium}
	\footnotetext{Samaey, Lelievre, Legat, 2010}
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Replace lifting by \textit{matching}}
Find the distribution that is consistent with the extrapolated moments $m_l$ but deviates the least from the prior microscopic state. \footnotemark
\footnotetext[3]{Debrabant, Zielinski, Samaey, 2017}
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{acceleration}
	\label{fig:acceleration}
\end{figure}

\end{frame}

% Overzicht bij het begin van elk hoofdstuk 
\AtBeginSection[]{\only<beamer>{\addtocounter{framenumber}{-1}
		\begin{outlineframe}[Overview]
			\tableofcontents[currentsection]
	\end{outlineframe}}
}
\section{Matching Strategies}

\begin{frame}
\frametitle{Many possible optimization criteria}
\note{Optimization with equality constraints}
Find matched density $\phi$ that is consistent with extrapolated macroscopic moments $m_l$ and deviates the least from the prior microscopic ensemble.

\begin{itemize}
	\item $L_2$- norm: \ $\text{arg min} \ \ \frac{1}{2} \norm{\phi - \pi}_2^2  \ s.t. \ \ \mathcal{R}_L(\phi) = m$
	
	\item $f$-divergence: \  $\text{arg min}_{\phi} \mathcal{I}_f(\phi|\pi) \ \ s.t. \ \ \mathcal{R}_L(\phi) = m$
\end{itemize}
\vspace{2mm}
with $\mathcal{I}(\phi|\pi) = \int_G f\left(\frac{\phi(x)}{\pi(x)}\right) \pi(x) dx$.
\note{f-divergence has many applications in information theory and machine learning.}

\vspace{8mm}
Special cases:
\begin{itemize}
	\item Kullback-Leibler divergence (KLD): \ $f(t) = t \ln(t) - t + 1$
	\item L2D matching: \ $f(t) = (t-1)^2$.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{KLD matching is superior}
This thesis focusses on KLD-matching

\[
\text{arg min}_{\phi}\int_G \ln{\left(\frac{\phi(x)}{\pi(x)}\right)} \pi(x) dx
\]

\begin{itemize}
	\item Simple matching formula: $\phi = \exp \left(\sum_{l=0}^L \lambda_l R_l\right) \pi$
	
	\item Lagrange multipliers: $e^{\lambda_0} \int_G R_l \exp \left(\sum_{k=0}^L \lambda_k R_k\right) \pi  dx = m_l$
	\item Reweighing: $w(j) = \exp \left(\sum_{k=0}^L \lambda_k R_k\right)  w(j)$
	\item Analytical results for matching with a Gaussian prior
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration: FENE Dumbbells}
Model for a dilute polymer solution. Polymers experience a spring force $F(x)$ and undergo Brownian motion
\[
F: B(\sqrt{b}) \to \mathbb{R}^d, x \mapsto F(x) = \frac{b}{b-\norm{x}^2}x
\]
Stochastic model:
\[
dX(t) = (\kappa(t)X(t) - \frac{1}{2W_e}F(X(t)))dt + \frac{1}{\sqrt{W_e}}dW(t)
\]
with a velocity field $\kappa(t) = 2$.

Look at different matching strategies with prior after 1 second and target distribution at 1.1 second.
\end{frame}

\begin{frame}
\frametitle{$f-$divergence is superior}
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{../../Source/Python/samples/fene/plots/Strategies}
	\label{fig:Strategies}
\end{figure}

\end{frame}

\section{Matching on a linear test SDE}
\begin{frame}
\frametitle{Linear models are an important to study the acceleration algorithm}
\note{Linear problems for stability analysis, moment selection, here with scale separation}
Consider a linear slow-fast system with scale separation $\epsilon$
\begin{align*}
dX(t) &= -\frac{1}{\epsilon}X(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_x(t) \\
dY(t) &= (X(t)-Y(t))dt + dW_y(t)
\end{align*}
with $X(t)$ the fast variable and $Y(t)$ the slow or coarse one.

\vspace{3mm}
\begin{itemize}
		\item Allows to study stability
	\item Gives insight in how many moments are needed \note{to represent the coarse variables}
	\item Analytic results on matching with only coarse moments
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Explicit formula for matching with coarse mean}
\begin{theorem} \label{thm:linearmatchingmean}
	Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$ where the subscript $d$ indicates the fast variables and $m$ the slow. Then the distribution $Q^*$ that minimizes the  Kullback-Leibler divergence, constrained with $\mathbb{E}[Q]_m = \mu_m^*$ is a normal distribution $\mathcal{N}(\mu^*, \Sigma)$ with $\mu^* = [\mu^*_d, \mu^*_m]$ and $\mu_d^* = \mu_d + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m)$.
\end{theorem}
\end{frame}

\begin{frame}
\frametitle{Numerical confirmation of the theorem}
Initial condition: Gaussian distribution at $[1,2]$ with unit covariance matrix.

\begin{figure}
	\centering
%	\includegraphics[width=0.8\linewidth]{../../Source/Python/samples/linear/plots/Mean_reference_Ly1_tend1_gaussianinitial}
	\label{fig:meanreferencely1tend1gaussianinitial}
\end{figure}

\end{frame}

\begin{frame}
	\frametitle{Variance is not well approximated}
\begin{figure}
	\centering
%	\includegraphics[width=1\linewidth]{../../Source/Python/samples/linear/plots/Variance_reference_Ly1_tend1_gaussianinitial}
	\label{fig:variancereferencely1tend1gaussianinitial}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{More complicated formulas for extrapolated mean and variance}
\begin{theorem} \label{thm:linearmatchingmeanvariance}
	Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$ where the subscript $d$ indicates the fast variables and $m$ the slow. Also suppose that the restriction operator $\mathcal{R}$ consists of the coarse mean $\mathcal{R}(Q) = \mu_m^*$ and variance $\Sigma_m^*$. Then the distribution $Q$ that minimizes the KLD-divergence, constrained with $\mathbb{E}[Q]_m = \mu_m^*$ and $Var[Q]=\Sigma_m^*$ is a normal distribution $\mathcal{N}(\mu^*, \Sigma^*)$ with $\mu^* = [\mu^*_d, \mu^*_m]$ and $\mu_d^* = \mu_d + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m)$.
\end{theorem}
\end{frame}

\begin{frame}
	\frametitle{Variance is now a lot better}
\begin{figure}
	\centering
%	\includegraphics[width=1\linewidth]{../../Source/Python/samples/linear/plots/Variance_reference_Ly2_tend1_gaussianinitial}
	\label{fig:variancereferencely2tend1gaussianinitial}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{How many slow and fast moments describe the linear system accurately?}
\begin{itemize}
	\item We are only interested in the slow variables
	\item Normal distribution is completely determined by mean and variance
	\item Fast variables will reach equilibrium fast
	\item So far, two coarse moments seemed to give decent approximations for mean and variance.
\end{itemize}
\vspace{3mm}
Conjecture: Two coarse moments give the best approximation results for the linear test system.
\end{frame}

\begin{frame}
	\frametitle{Going beyond two slow moments}
	\begin{figure}
		\centering
%		\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/MeanVariance_Lx0_Ly0123_guassianinitial_tend1}
		\label{fig:meanvariancelx0ly0123guassianinitialtend1}
	\end{figure}
\end{frame}

\begin{frame}
\frametitle{Adding moments of the fast variable}
\begin{figure}
	\centering
%	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/MeanVariance_Lx0123_Ly2_gaussianinitial_tend1}
	\label{fig:meanvariancelx0123ly2gaussianinitialtend1}
\end{figure}

\end{frame}


\section{Outlook}
\begin{frame}
\frametitle{Outlook}
This semester:
\begin{itemize}
	\item Reproduce matching results for FENE dumbbels
	\item Investigate moment selection on linear problems
	\item Reproduce stability results
\end{itemize}
Next semester:
\begin{itemize}
	\item Look at number of moments for non-linear problems
	\item Adaptive moment selection
	\item Extension to the kinetic equations
\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Concept of stability}
	We again look at a linear test SDE of the form
	\[
	dX(t) = AX(t)dt + BdW(t)
	\]
	with Gaussian initial $X_0$. A Numerical method is stable if all paths tend to zero or to an invariant distribution.
	
	\vspace{3mm}
	For Euler-Maruyama:
	\begin{align*}
	\mathbb{E}[X_{\delta t}^n] &= (I + \delta t A)^n \mathbb{E}[X_0] \\
	\mathbb{V}[X_{\delta t}^n] &= (I+\delta t A)^n \mathbb{V}[X_0](I+\delta t A^T)^n + \sum_{j=0}^{n-1} (I+\delta t A)^j B B^T (I+\delta t A^T)^j
	\end{align*}
\end{frame}

\begin{frame}
	\frametitle{Stability for Euler-Maruyama is related to the fast variable}
\begin{align*}
	\mathbb{E}[X_{\delta t}^{\infty}] &= 0 \\
	\mathbb{V}[X_{\delta t}^{\infty}] &= \sum_{j=0}^{\infty} (I+\delta t A)^j B B^T (I+\delta t A^T)^j
	\end{align*}
	when $\text{spec}(I+\delta tA) \in B(0,1)$. Example: take $\epsilon=0.1$ in the slow-fast system 
	\begin{align*}
	dX(t) &= -\frac{1}{\epsilon}X(t)dt  + \frac{1}{\sqrt{\epsilon}} dW_x(t) \\
	dY(t) &= (X(t)-Y(t))dt + dW_y(t)
	\end{align*}
	then $\text{spec}(I+\delta tA) = \{10\delta t, \delta t\}$ so $\delta t \leq 0.2$.	
	
	
	
\end{frame}

\begin{frame}
	\frametitle{Breaking stability of Euler-Maruyama}
	\centering
	Stable iff  $\delta t \leq 0.2$
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
			\includegraphics[width=1.1\linewidth]{../../Source/Python/samples/linear/plots/stability/dt0197}
			\caption{$\delta t=0.197$}
			\label{fig:dt0197}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
			\includegraphics[width=1.1\linewidth]{../../Source/Python/samples/linear/plots/stability/dt0203}
			\caption{$\delta t = 0.203$}
			\label{fig:dt0203}
		\end{subfigure}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Stability of the acceleration algorithm is related to the slow modes}
	Consider coarse mean extrapolation with step $\Delta t$, then the acceleration algorithm is stable iff $\text{spec}(1+\Delta t A_m) \in B(0,1)$. For the slow-fast system $A_m=-1$ so $\Delta t \leq 2$.
	
	\begin{figure}
		\centering
		\begin{subfigure}[b]{0.5\textwidth}
%			\includegraphics[width=1.1\linewidth]{../../Source/Python/samples/linear/plots/stability/Dt195}
			\caption{$\Delta t=1.95$}
			\label{fig:Dt195}
		\end{subfigure}%
		\begin{subfigure}[b]{0.5\textwidth}
%			\includegraphics[width=1.1\linewidth]{../../Source/Python/samples/linear/plots/stability/Dt21}
			\caption{$\Delta t = 2.1$}
			\label{fig:Dt21}
		\end{subfigure}
	\end{figure}
\end{frame}
\end{document}