\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Matching Strategies}{7}{0}{2}
\beamer@sectionintoc {3}{Matching on a linear test SDE}{12}{0}{3}
\beamer@sectionintoc {4}{Outlook}{22}{0}{4}
