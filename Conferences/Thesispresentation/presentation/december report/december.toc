\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Background of this thesis}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Techniques to overcome stiffness}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Goal of this thesis}{4}{section.1.3}
\contentsline {chapter}{\numberline {2}Idea Of Matching}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}A General Micro-Macro Acceleration Algorithm}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Restriction and Matching Operators}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Definition of the restriction operator}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Definition of the matching operator}{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Consistency between the restriction and matching operator}{8}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Extrapolation Operator}{9}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Convergence Theorem}{9}{section.2.3}
