\documentclass{kulakreport}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}
\usepackage{cite}
\usepackage{algorithm}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}
\newtheorem{property}{Property}
\theoremstyle{assumption}
\newtheorem*{assumption}{Assumption}
\begin{document}
	\title{Masters Thesis: December report}
	\author{Hannes Vandecasteele}
	\tableofcontents
\chapter{Introduction}
\section{Background of this thesis}
Many systems in nature can be described by a system of differential equations (ordinary, partial or stochastic) in which one variable evolves a lot faster than the others. For example, individual particles in a nuclear fusion reactor move on a much faster time scale than the plasma itself, or individual atoms in a polymer network vibrate much faster than the movement of the longer polymer molecules. The difference between evolution speeds makes simulating these systems very costly since we need to capture the fast dynamics accurately as they influence the slower time scales, but on the other hand are we only interested in the dynamics of some slow functions of interest. We hence need to use small time steps to capture the fast dynamics but also compute a lot of these time steps to simulate the slow dynamics over long enough time intervals. This is an inherent problem with stiff systems of equations.

Moreover, the fast variables are often modelled using a stochastic process due to inherent stochastic behaviour , or because the dimensionality is too high. In the latter case a deterministic simulation would be too costly since refining the grid with a factor of two in each dimension results in an exponential increase of total degrees of freedom. The deterministic numerical scheme is not suitable in most situations because the simulations would soon become intractable, let alone storing all the data points in a computer. As a result Monte Carlo techniques soon become very attractive to solve inherent stochastic or high dimensional problems with a reasonable accuracy.

\section{Techniques to overcome stiffness}
\noindent There are many techniques that deal with the inherent multiscale behaviour in both deterministic and stochastic problems and are able to overcome the problem of stiffness. We mention here the use of implicit methods, explicit methods with a larger stability domain, techniques that make use of approximate coarse models, equation-free techniques and a new slow-fast acceleration method recently developed at KU Leuven on which this thesis will focus.

\vspace{2mm}
\noindent Implicit time integration methods have proven to be very successful in the context of stiff ODE's where they allow to take much larger time steps than their explicit counterparts as these implicit methods have a much larger stability domain. The computational cost of an implicit method is however higher than for explicit methods since in every step a system of equations needs to be solved but implicit methods still are a major improvement over the only small steps explicit methods can take. A problem arises however when employing implicit methods, like the implicit Euler method or the trapezoidal rule, to stochastic differential equations. The authors of \cite{li2008effectiveness} have shown that these implicit methods are unable to capture the probability distribution of fast variables and hence are not able to converge to the exact solution of the coarse variable, unless the time steps are very small. In other words the big advantage of implicit methods, e.g. larger time steps, is exactly what prevents them from working for stochastic differential equations. For SDE's there is no reason anymore to choose implicit methods over the simpler and more accurate explicit methods. 
\newline
\noindent
Another possibility instead of a full implicit simulation of both fast and slow variables consists of an implicit method for the fast variables alone and an explicit method for the coarse variables. In literature these methods are known as IMEX methods and have found applications in the kinetic equations \cite{dimarco2014numerical}. We will not go into more detail on these methods.

\vspace{2mm}
\noindent It is also possible to artificially increase the stability domain of explicit solvers by so-called S-ROCK(Stochastic Orthogonal Runge-Kutta Chebyshev) \cite{abdulle2008s}. S-ROCK increases the stability domain by using a Runge-Kutta like stage-wise scheme with Chebyshev polynomials. The method can be useful since a small stability domain is one of the main drawbacks of explicit methods for slow-fast stochastic systems. We also will not go deeper into this technique.

\vspace{2mm}
\noindent In specific situations like the kinetic equations, it is possible to derive an approximate coarse model in the limit when the time scale separation between slow and fast goes to infinity. Many techniques that use an approximate coarse model, such as the chemical master equation and averaging are explained in the review paper \cite{givon2004extracting}.  Using only a coarse model one can take much larger time steps to simulate the variables of interest, while the microscopic dynamics are not important anymore as these are averaged out. The construction of coarse approximate models is however not the focus of this thesis.

\vspace{2mm}
\noindent In many other applications however, an approximate coarse model is not available in closed form, making it impossible to construct a coarse time stepper directly. Methods for this kind of problems are called equation-free techniques since there is no direct coarse model to simulate. Usually in an equation-free context one is only interested in a few macroscopic quantities or moments that describe the macroscopic system accurately enough. A general three-way framework then exists to simulate this unavailable coarse model over larger time intervals, explained in much detail in \cite{kevrekidis2009equation}. First, a microscopic state or distribution is built from available macroscopic quantities in the \textit{lifting} step. Then follows a short simulation of the full microscopic stiff system to let the fast dynamics settle to a conditional equilibrium after which the macroscopic quantities of interest are computed from this new microscopic distribution in the \textit{restriction} step. The obtained macroscopic quantities can then be extrapolated over a larger time step so that everything is in place for lifting again with these extrapolated macro moments. The latter four-step algorithm constructs an approximate coarse stepper for the unavailable macroscopic model using so-called \textit{coarse projective integration} and it is widely used in practice. There are however two major problems with this method. First, it is possible to prove convergence of coarse projective integration when the scale separation tends to infinity. In this limit the fast variables settle almost immediately to their conditional invariant distribution making larger extrapolation steps possible. If however the scale separation is not that big (order 10, 100), then a modelling error is introduced since the microscopic quantities are not modelled accurately enough. As the fast variables also influence the coarse variables, a lack of accuracy in the former variables induces an inaccurate approximation of the latter. The second problem is that the lifting step usually initializes the micro variables with fixed values or a certain family of parametric distributions (consistent with the macro quantities). In the deterministic case the fixed initial value may be far from conditional equilibrium such that the micro time stepper needs more steps to relax the fast variables In the stochastic case it can take a long time to sample the probability distributions accurately using a MCMC scheme for example.

\vspace{2mm}
\noindent Recently a new micro-macro acceleration technique was developed at KU Leuven by prof. Samaey and his research group \cite{debrabant2015micro} that tries to alleviate the two problems from the equation-free context, specifically for stochastic slow-fast systems. They propose to replace the lifting procedure for equation free methods by a new matching algorithm that picks the probability density that is closest to a prior distribution and is of course consistent with the extrapolated macroscopic moments. The new density is determined uniquely by this prior distribution and the few extrapolated moments from the slow variable. The improvement over equation free modelling is twofold. First, by considering the minimal deviation from a prior distribution, the matched distribution is unique in sharp contrast to equation free modelling where there can be an infinite number of densities consistent with the extrapolated moments if there exists one. With matching the computations are faster since we only have to update the prior distribution instead of sampling a completely new distribution. Second, the authors are able to prove convergence of the Monte Carlo scheme to the exact joint distribution between slow and fast components even if there is a finite time scale separation, in contrast to equation-free methods where there is only convergence when the separation tends to infinity. This should make the matching algorithm both faster and more accurate than lifting.

\section{Goal of this thesis}
The new matching algorithm has already been successfully applied with only a few macroscopic moments to a stiff stochastic differential equation describing FENE-dumbbels \cite{debrabant2015micro} and a few theoretical properties of the acceleration algorithm are known too. The number of macroscopic moments however has a big impact on the accuracy of the acceleration algorithm and there is in general not much known about how many moments are needed to simulate a given problem. The goal of the thesis is to gain more insight in the problem, both for linear and the more difficult non-linear problems.




\chapter{Idea Of Matching}
As mentioned in the introduction, equation free methods construct a coarse time stepper for an unavailable macroscopic model in a three way scheme. The first step is called \textit{lifting} and initializes the microscopic variables so that they are consistent with a macroscopic state of interest. The microscopic state can either be stochastic or deterministic and in the former case a probability density is sampled that is consistent with the choice of moments. The second step consists of a few microscopic time steps with the full model to let the microscopic quantities settle to a conditionally invariant distribution. These steps are necessary to minimize the modelling error introduced by initializing the microscopic variables. Third, while performing the micro steps, the macroscopic values or moments are computed from this microscopic data because usually in practical situations only the coarse variables are truly of interest. The third step is also called \textit{restriction}. Afterwards, a \textit{coarse projective integration} step extrapolates these macroscopic quantities so that they are available for the next lifting stage. Projective integration tries to construct an approximate derivative for the unknown coarse model and causes the acceleration in simulating stiff systems.

\vspace{2mm}
\noindent The new algorithm described in \cite{debrabant2015micro} replaces the \textit{lifting}  by a new \textit{matching} algorithm, specifically in the context of stiff stochastic differential equations. The idea is that instead of choosing a new microscopic state in the lifting step that is only consistent with the coarse quantities, it is better to choose the micro state that also deviates the least from the  distribution obtained by the previous micro steps, or prior distribution, and is of course still consistent the macroscopic moments of interest. The new criterion makes the matched distribution unique in contrast to lifting and allows convergence to the joint microscopic density even if there is only a finite scale separation.

\vspace{2mm}
\noindent This chapter first describes the general problem statement and matching algorithm, after which several conditions on the restriction and matching operators are discussed. The third section introduces the coarse projective extrapolation operator and discuss the fundamental convergence theorem. The next chapter will introduce specific matching algorithms that can be used in practice.

\section{A General Micro-Macro Acceleration Algorithm}
This thesis focusses on processes modelled by general stochastic differential equations in Ito sense 
\begin{equation} \label{eq:generalsde}
dX(t) = a(t, X(t))dt + b(t, X(t)) dW(t), \ \ \ X(t) \in G
\end{equation}
on the time interval $[0, T]$, where $a(t, x) \in [0,T] \times G$ is a drift vector, $b(t,x) \in [0,T] \times G$ the diffusion matrix and $W$ is an $n$-dimensional Wiener process. The domain $G$ of the state vectors is an open, measurable set and the initial distribution $X(0)$ is also independent from the Brownian motion $W$. The process above is of course stochastic, but in practice only certain expectations $\mathbb{E}[g(X(t))]$ of a given function $g$ of the stochastic process are of interest. These will be the moment functions of the restriction operator defined below. 

\vspace{2mm}
\noindent The general matching algorithm \ref{algo:accelerationalgorithm} is a four step procedure and the next sections describe each of these steps in much more detail. Let us first introduce the necessary notation. An ensemble of $J$ microscopic independent particles $\mathcal{X}=(X_j)_{j=1}^J$ will be used to simulate \ref{eq:generalsde} for a few microscopic steps using a standard Monte Carlo procedure. On the macroscopic level the algorithm computes $L$  moments $m = (m_1, \dots, m_L)$ after each microscopic step with step size $\delta t$ using the restriction operator $\mathcal{R}_L$. The macroscopic moments are computed by averaging certain problem-specific functions $R_i$ for $i=1 \dots L$, which results in the following notation for the restriction operator
\begin{equation} \label{eq:finiterestriction}
\mathcal{R}_L(\mathcal{X}) = \left(\mathbb{\hat{E}}_J(R_1(X)), \dots, \mathbb{\hat{E}}_J(R_L(X))\right)
\end{equation}
where $\mathbb{\hat{E}}_J$ denotes the finite expected value over the microscopic ensemble $\mathcal{X} = (X_j)_{j=1}^J$. After restriction, the extrapolation operator  $\mathcal{E}$ extrapolates these macroscopic quantities over a much larger time step $\Delta t$ so that the matching operator $\mathcal{P}(m, \pi)$ can compute the new microscopic distribution that is both consistent with the extrapolated moments $m$ and deviates the least from the prior distribution $\pi$.  Figure \ref{fig:acceleration} also shows these four steps and how they all relate to each other.

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{"../december presentation/acceleration"}
	\caption{Visual representation of acceleration algorithm \ref{algo:accelerationalgorithm}. This figure was taken from \cite{debrabant2015micro}.}
	\label{fig:acceleration}
\end{figure}

\vspace{2mm}
\noindent The abstract micro-macro acceleration algorithm then consists of many iterations of these four steps until a certain end time.  The notation and phrasing is very similar to \cite{debrabant2015micro}.

\begin{algorithm}  
	Given a microscopic ensemble $X^n$ at time $t^n$, a microscopic step size $\delta t$, a macroscopic step size $\Delta t$ and a number of microscopic steps $K$ such that $K \delta t \leq \Delta t$. The algorithm produces the microscopic ensemble $X^{n+1}$ in four steps:
	
	(i) Monte Carlo simulation: Simulate the microscopic ensemble $X^n$ over $K$ small inner steps of size $\delta t$ by using an inner microscopic discrete time stepper 
	\[
	X^{n, k+1} = S^{n,k}(X^{n,k}, \delta t)
	\]
	
	(ii) Restriction: compute the macroscopic states corresponding to these microscopic ensembles
	\[
		m^{n,k} = \mathcal{R}_L(X^{n,k}).
	\]
	
	(iii) Extrapolate these macroscopic moments over a time interval $\Delta t$ using the extrapolation operator
	\[
		m^{n+1} = \mathcal{E}((m^{n,k})_{k=0}^K, \delta t, \Delta t)
	\]
	
	(iv) Matching: compute a new ensemble consistent with these extrapolated moments using the matching operator with prior distribution $X^{n,K}$
	\[
	X^{n+1} = \mathcal{P}_L(m^{n+1}, X^{n,K})
	\]
	and advance time with $\Delta t$ until the end time $T$ is reached.
\caption{The general micro-macro acceleration algorithm for stiff SDE's with the matching procedure.}
\label{algo:accelerationalgorithm}
\end{algorithm}	

\section{Restriction and Matching Operators}
The restriction and corresponding matching operators need to have certain properties in order to prove convergence of the acceleration algorithm \ref{algo:accelerationalgorithm}. Moreover they also have to be consistent with each other. This will give rise to the definition of a restriction-matching pair.

\subsection{Definition of the restriction operator}
The restriction operator $\mathcal{R}_L$ is defined in terms of so-called moments $m_l(\mu), \ l=1 \dots L$ over a probability distribution $\mu \in \mathcal{M}_1(G)$. The notation $\mathcal{M}_1(G)$ denotes the set of all probability density functions on the measurable set $G$. Suppose there is a sequence of moment functions $R_l : \mathbb{R}^n \to \mathbb{R}$, the moments of these functions relative to a probability density $\mu$ are defined as
\begin{equation} \label{eq:momentintegral}
m_l(\mu) = \mathbb{E}_{\mu}(R_l) = \int_{\mathbb{R}^n} R_l(x) d\mu(x).
\end{equation}
The functions $R_l$ should have a finite integral over the measure $\mu$ and are problem specific. The general restriction operator then is a grouping of such moments in a large vector, written as 
\begin{equation} \label{eq:restrictionoperator}
\mathcal{R}_L \mu = (m_1(\mu), m_2(\mu), \dots, m_L(\mu))
\end{equation}
so that \ref{eq:finiterestriction} is a finite approximation over an ensemble of particles of the restriction operator.
The number of moments $L$ is also problem dependent but in practice however it is important that every moment function $R_l$ adds new information about the density $\mu$. Otherwise there is no point in considering this function as it can be written as a linear combination of the other moment functions. The following statements combines these assumptions on the moment functions. These conditions are roughly the same as in \cite{debrabant2015micro}.
\begin{assumption} \label{def:momentfunctions}
	The moment functions $R_l, l \geq 1$ need to satisfy the following conditions:
	\begin{enumerate}
		\item The moment functions $(R_l)_{l=1}^{L}$ are linearly independent on every non-null subset of G
		
		\item There is a one-to-one correspondence between the infinite sequence of moments $(m_l(\mu))_{l=1}^{\infty}$ and the distribution $\mu$.
	\end{enumerate}
\end{assumption}

\noindent
The second condition is important since there only is convergence of the acceleration algorithm to the microscopic density when the number of moments $L$ goes to infinity. In the limit there needs to be a one-to-one correspondence between the moment functions and the distribution they represent.

\subsection{Definition of the matching operator}
Contrary to the restriction operator, the matching operator $\mathcal{P}_L(m, \pi)$ takes a vector of moments $m = (m_1, m_2, \dots, m_L)$ and a prior density $\pi$ and computes a new probability density that is consistent with the moments $m$ and is the minimal deviation from $\pi$. A functional $d$ represents the deviation between the prior $\pi$ and the matched density $mathcal{P}_L(m, \pi)$ . Mathematically the minimization reads
\[
\mathcal{P}_L(m, \pi) = \underset{\nu \in \mathcal{R}_L^{-1}(m)}{\text{arg min}}  d(\nu, \pi).
\]
The function $d$ to minimize is also problem-dependent but the following chapter discusses several matching techniques such as Kullback-Leibler divergence or matching in $L_2$ norm. As mentioned in the introduction, matching makes the density consistent with the extrapolated moments unique compared to lifting where there can be an infinite amount of densities consistent with the moments, if such a distribution exists. As a result the lifted density can lie far from the exact microscopic distribution and introduce a modelling error. In practice it happens that a constrained simulation follows that keeps the lifted density consistent with the moments but tries to minimize $d$ with respect to the prior. Matching does exactly this so it has the potential of also being faster than lifting too. 

\subsection{Consistency between the restriction and matching operator}
The restriction $\mathcal{R}_L(\pi)$ and matching $\mathcal{P}_L(m, \pi)$ operator work closely together and need to have certain properties for consistency, otherwise there would be no convergence at all. The first condition is that applying the restriction operator after matching should give exactly the same moments as started with, and the other condition is that applying the matching operator on a set of moments computed by the restriction operator on a distribution has to return exactly this distribution. The matching-restriction pair acts like they are inverse operators of each other. Putting it more mathematically, $\mathcal{R}_L$ and $\mathcal{P}_L$  need to follow the next assumption. The assumption was taken almost literally from \cite{debrabant2015micro}.

\begin{assumption} \label{def:restrictionmatchingpair} Restriction-Matching pair. Suppose $\mathcal{R}_L$ and $\mathcal{P}_L$ satisfy
	\begin{itemize}
		\item $\mathcal{R}_L(\mathcal{P}_L(m, \pi)) = m$ for all $(m, \pi) \in \text{dom}(\mathcal{P}_L)$
		\item $\mathcal{P}_L(\mathcal{R}_L(\mu), \mu) = \mu$ for all $(\mathcal{R}_L(\mu), \mu) \in \text{dom}(\mathcal{P}_L)$
	\end{itemize}
	then the pair $(\mathcal{R}_L, \mathcal{P}_L)$ is called a Restriction-Matching pair.
\end{assumption}
\noindent
A few remarks are in order here. The first condition implicitly states that $\text{dom}(\mathcal{P}_L) \subset \text{Im}(\mathcal{R}_L) \times \mathcal{M}(G)$, or equivalently the moment vector $m$ should always be attainable from some density by the restriction operator. In practice however it sometimes happens that an extrapolated moment vector $m$ falls outside the domain of the matching operator and no density can correspond with these moments. So this condition is not always met in practice. The second condition on the other hand states that the function $\mathcal{P}_L(m, .)$ is a projection. Indeed, let $\mathcal{P}_L(m, \pi) = \mu$, then $\mathcal{P}_L^2(m, \pi) = \mathcal{P}_L(m, \mu) = \mu = \mathcal{P}_L(m, \pi)$ since $\mu$ is consistent with $m$.

\subsection{Extrapolation Operator}
The extrapolation operator $\mathcal{E}((m^{n,k})_{k=0}^K, \delta t, \Delta t)$ in the most general form takes a sequence of macroscopic moment vectors $(m^{n,k})_{k=0}^K$ at times $t_n + k\delta t, \ k = 0 \dots K$ as input and produces a new moment vector $m^{n+1}$ at time $t_n + \Delta t$. The moments $m^{n,k}$ correspond to the restriction of the microscopic state using $\mathcal{R}_L$ after $k$ steps of the microscopic integrator, and the computed extrapolation $m^{n+1}$ serves as an input to the matching operator afterwards. 

\noindent
Any extrapolation method is acceptable for the micro-macro acceleration algorithm but this thesis only considers the simplest case: linear extrapolation. The formula for $\mathcal{E}$ then simply becomes
\begin{equation} \label{eq:linearextrapolation}
m^{n+1} = \mathcal{E}((m^{n,k})_{k=0}^K, \delta t, \Delta t) = m^{n,0} + \frac{\Delta t}{K \delta t}(m^{n,K}-m^{n,0})
\end{equation}
where $m^{n,0} = m^n$. Only the macroscopic moments at time $t_n$ and $t_n+K\delta t$ are needed in the extrapolation formula so it is not necessary to apply the restriction operator after every microscopic state but only after the last one (since $m^n$ is already available from the previous extrapolation step).

\section{Convergence Theorem}
All elements are now in place to state the convergence theorem for the micro-macro acceleration algorithm \ref{algo:accelerationalgorithm}. The theorem is stated in terms of more abstract restriction and matching operators but assumes linear extrapolation. The proof also requires some very technical requirements that are explained in \cite{debrabant2015micro} but are omitted in this thesis for clarity and because it is out of scope. The proof itself is also very technical and not given here. It can be found in full detail in \cite{debrabant2015micro}. The statement of the theorem is also copied from \cite{debrabant2015micro}.

\begin{theorem}
Consider an SDE of the form \ref{eq:generalsde}  with initial condition $X(0) = X_0$. Let $R_l l \geq 1$, be a sequence of moment functions, fulfilling Assumption \ref{def:momentfunctions}, that generate restriction operators $\mathcal{R}_L, L \geq 1$, by \ref{eq:restrictionoperator} and let ${(R_L,P_L)}_{L=1}^{\infty}$ be a sequence of restriction-matching pairs fulfilling two technical conditions in \cite{debrabant2015micro}. Furthermore, consider a microscopic time discretization scheme of order $p_S \geq 1$ with time step $\delta t$. Finally, let $\mathcal{E}$ denote linear extrapolation \ref{eq:linearextrapolation} with step size $\Delta t$ and let $K \in \mathbb{N}$ be a number of microscopic steps with $K \delta t \leq \Delta t$.
If we denote the solution of Algorithm \ref{algo:accelerationalgorithm} with $L$ macroscopic state variables at time $T$ as $X_L^N$, then for any smooth enough function $g$ we have
\begin{equation} \label{eq:convergenceresult}
 \mathbb{E}(| g(X^N_L)-g(X(T))|)  \leq C_L + \tilde{C}_L (\delta t)^{p_S} + \Delta t ,
 \end{equation}
in which $C_L$ and $\tilde{C}_L$ are constants that depend also on $T,g$ and $X_0$, with $C_L \to  0$ as $L \to \infty$.
\end{theorem}
\noindent
Loosely speaking, the theorem says that acceleration algorithm \ref{algo:accelerationalgorithm} convergences to the exact solution when both the microscopic and macroscopic step size go to zero and the number of moments $L$ goes to infinity. One of the downsides of the theorem is that it gives no precise formula for $C_L$ that contains information of how many moments are needed to keep this part of the error small. The number of moments is application specific and part of the thesis is to discover what a good number of moments is, or how fast this term in the error decreases to zero.

\vspace{2mm}
\noindent
A few remarks about the theorem statement. First, the theorem omits some technical conditions that the SDE and restriction-matching pair should have but these are nicely explained in \cite{debrabant2015micro}. Also the function $g$ needs to belong to a certain class of function but the exact requirements are out of scope for this thesis and every example in text obeys these conditions. Second, the theorem states that the method convergences even when there is only a finite time scale separation, in contrast to other multiscale techniques, like equation-free modelling, that usually converge when the scale separation goes to infinity. This is an asset compared to other multiscale approaches. Third, this is only a convergence theorem and does not say anything on the maximal extrapolation step $\Delta t$ or ratio $\Delta t / \delta t$ possible to keep the solution finite. This is related to stability and chapter 4 goes much deeper into this problem.

\mbox{}
\bibliography{december}{}
\bibliographystyle{plain}
\end{document}