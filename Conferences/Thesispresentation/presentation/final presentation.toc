\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Linear SDEs: Convergence \& Stability}{5}{0}{2}
\beamer@sectionintoc {3}{Influence of moment selection}{11}{0}{3}
\beamer@sectionintoc {4}{Effect of the extrapolation step}{17}{0}{4}
\beamer@sectionintoc {5}{A practical example}{22}{0}{5}
\beamer@sectionintoc {6}{Conclusion and outlook}{26}{0}{6}
