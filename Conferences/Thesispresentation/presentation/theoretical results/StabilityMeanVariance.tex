\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\theoremstyle{proposition}
\newtheorem*{proposition}{Proposition}
\theoremstyle{lemma}
\newtheorem*{lemma}{Lemma}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\begin{document}
	\title{Stability with extrapolated coarse mean and variance}
	\author{Hannes Vandecasteele}
	\maketitle
\noindent	
Take the SDE
\begin{equation}
dX_t  = DX_t dt + BdW_t
\end{equation}
with $D=\text{diag}(D_m, D_r)$ and $D_m \in \mathbb{R}^{m \times m}, \ D_r \in \mathbb{R}^{r \times r}$. The variables corresponding to $D_m$ are the slow modes and the other the fast. We are interested in investigating stability of the micro-macro algorithm with extrapolated coarse mean and variance.

\paragraph{Micro Time Stepper}
\noindent 
At time step $n$ the mean and variances are  $\mu_n$ and $\Sigma_n$. One step with size $\delta t$ of the micro Euler-Maruyama stepper then gives
\begin{align} \label{eq:emmean}
\mu_{n,1} &= (1+\delta t D) \mu_n \\ 
\Sigma_{n,1} &= (I+\delta t D) \Sigma_n(I+\delta t D)^T + \delta t B B^T
\end{align}

\paragraph{Extrapolation of coarse mean and variance}
Extrapolating mean and variance over a time step $\Delta t$ gives
\begin{align} \label{eq:coarsemean}
\begin{cases}
(\mu_{n+1}^*)_m &= (1-\frac{\Delta t}{\delta t})(\mu_n)_m + \frac{\Delta t}{\delta t}(\mu_{n,1})_m \\
&= (1-\frac{\Delta t}{\delta t})(\mu_n)_m + \frac{\Delta t}{\delta t}(1+\delta t D_m)(\mu_n)_m \\
&= (1+\Delta t D_m)(\mu_n)_m \\
\end{cases}
\end{align}
\begin{align*}
(\Sigma_{n+1}^*)_m &= (1-\frac{\Delta t}{\delta t})(\Sigma_n)_m + \frac{\Delta t}{\delta t}(\Sigma_{n,1})_m \\
&= (1-\frac{\Delta t}{\delta t})(\Sigma_n)_m + \frac{\Delta t}{\delta t}(1+\delta t D_m)(\Sigma_n)_m(1+\delta t D_m)^T + \frac{\Delta t}{\delta t} \delta t (B B^T)_m \\
&= (\Sigma_n)_m + \Delta t D_m (\Sigma_n)_m + \Delta t (\Sigma_n)_m D_m^T + \Delta t \delta t D_m (\Sigma_n)_m D_m^T + \Delta t (B B^T)_m
\end{align*}

\paragraph{Matching with extrapolated mean and variance}
Based on the document "Matching with slow mean and variance" we know that that matched distribution with prior Gaussian $\mathcal{N}(\mu, \Sigma)$ and extrapolated mean $\mu_m^*$ and variance $\Sigma_m^*$ the matched distribution is also Gaussian $\mathcal{N}(\mu^*, \Sigma^*)$ with

\begin{align}
\begin{cases}
\mu_r^* &= \mu_r + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m) \\
C^{*T} &= C^T \Sigma_m^{-1}\Sigma_m^* \\
\Sigma_r^* &= \Sigma_r - C^T\Sigma_m^{-1}(\Sigma_m-\Sigma_m^*)\Sigma_m^{-1}C
\end{cases}
\end{align}
Applied to our framework this yields
\begin{align} \label{eq:matching}
\begin{cases}
(\mu_{n+1}^*)_r &= (\mu_{n,1}^*)_r + C_{n,1}^T \Sigma_{n,1}^{-1}((\mu_{n+1}^*)_m - (\mu_{n,1})_m) \\
C_{n+1}^{*T} &= C_{n,1}^T \Sigma_{n,1}^{-1}(\Sigma_{n+1}^*)_m \\
(\Sigma_{n+1}^*)_r &= (\Sigma_{n,1})_r - C_{n,1}^T(\Sigma_{n,1})_m^{-1}((\Sigma_{n,1})_m-(\Sigma_{n+1}^*)_m)(\Sigma_{n,1})_m^{-1} C
\end{cases}
\end{align}

The formulas for the mean are very similar to the case with only extrapolated coarse mean. Filling in \ref{eq:emmean} in the previous equation gives
\begin{align}
\begin{cases}
(\mu_{n+1}^*)_r &= (\mu_{n,1}^*)_r + C_{n,1}^T \Sigma_{n,1}^{-1}((\mu_{n+1}^*)_m - (\mu_{n,1})_m) \\
&= (1+\delta t D_r)(\mu_n)_r + C_{n,1}^T \Sigma_{n,1}^{-1}(\Delta t - \delta t)D_m(\mu_n)_m
\end{cases}
\end{align}
and combining this with \ref{eq:coarsemean} results in the following matrix notation
\begin{equation}
\mu_{n+1}^* = \begin{bmatrix}
(1+\Delta t D_m) & 0 \\ 
C_{n,1}^T \Sigma_{n,1}^{-1}(\Delta t - \delta t)D_m & (1+\delta t D_r) \\
\end{bmatrix} \begin{bmatrix}
(\mu_n)_m \\
(\mu_n)_r
\end{bmatrix}
\end{equation}
and hence stability for the mean is determined by the joint spectral radius of all matrices of the form $\begin{bmatrix}
(1+\Delta t D_m) & 0 \\ 
\star & (1+\delta t D_r) \\
\end{bmatrix} $ and thus basically by the eigenvalues of the coarse matrix $D_m$ if the micro stepper is stable.

\vspace{2mm}
\noindent
The formula for the fine scale variance are a lot harder. For example for $C_{n+1}^{*T}$ we can deduce that 
\[
C_{n,1} = (1+\delta t D_m)C(1+\delta t D_r)^T
\]
but the main problem is working with the inverses of $(\Sigma_{n,1})_m$ and $(\Sigma_n)_m$ in formula \ref{eq:matching}. I also tried writing 
\[
\Sigma_{n+1}^* = K \Sigma_n K^T + L
\]
but this is also not really possible if we look at the coarse variance alone in due to the term with $\Delta t \delta t$. The formula for how the complete covariance matrix behaves during matching will however be important to deduce a stability criterion because the criterion on the mean will probably not be strong enough. How did you go about this?
\end{document}