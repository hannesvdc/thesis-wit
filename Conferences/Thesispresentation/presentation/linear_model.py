import numpy as np

from timeintegrators.gridnoflux_eulermaruyama import *
from timeintegrators.euler_maryama import *

class LinearModel:
    def __init__(self, A, D, grid=None):
        self.grid = grid
        self.A = A
        self.D = D
        
        # define SDE
        self.a = lambda t, x: A.dot(x)
        self.b = lambda t, x, dw: D.dot(dw)
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None):
        if self.grid is not None:
            timestepper = GridNoFluxEulerMaruyama(self.a, self.b, self.grid)
        else :
            timestepper = EulerMaryamaIntegrator(self.a, self.b)
            
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt

        return X, np.asarray(cbtimes), np.asarray(cbvalues)

    # Get the invariant distribution using an Euler-Maruyama scheme with a given 
    # step size and tolerance for the variance.
    def invariantMeasure(self, dt, tol):
        B = np.diag(self.D)
        v, P = np.linalg.eig(self.A)
        t = np.transpose(np.dot(np.linalg.inv(P), B))
        d = np.diag(np.power((np.ones_like(v)+dt*v), 0))
        variance = np.dot(d, np.dot(np.linalg.inv(P), np.dot(B, np.dot(t, d))))
        j = 1
        
        while True:
            t = np.transpose(np.dot(np.linalg.inv(P), B))
            d = np.diag(np.power((np.ones_like(v)+dt*v), j))
            term = np.dot(d, np.dot(np.linalg.inv(P), np.dot(B, np.dot(t, d))))
    
            if np.linalg.norm(term) < tol:
                break
            
            variance = variance + term
            j = j+1
            
        return np.zeros(self.A.shape[0]), dt*np.dot(P, np.dot(variance, np.transpose(P)))