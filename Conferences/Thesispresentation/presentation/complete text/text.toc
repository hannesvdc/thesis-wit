\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {english}
\contentsline {chapter}{Preface}{i}{chapter*.1}
\contentsline {chapter}{Abstract}{iv}{chapter*.2}
\contentsline {chapter}{Abstract}{v}{chapter*.3}
\contentsline {chapter}{List of Figures}{vi}{section*.4}
\contentsline {chapter}{List of Symbols}{ix}{chapter*.5}
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Techniques to overcome stiffness}{1}{section.1.2}
\contentsline {paragraph}{Implicit time steppers}{2}{section*.6}
\contentsline {paragraph}{S-ROCK}{2}{section*.7}
\contentsline {paragraph}{Approximate macroscopic models}{2}{section*.8}
\contentsline {paragraph}{Equation-free techniques}{2}{section*.9}
\contentsline {section}{\numberline {1.3}A micro-macro acceleration method}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Goal and contributions}{4}{section.1.4}
\contentsline {chapter}{\chapternumberline {2}The micro-macro acceleration method}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}A General Micro-Macro Acceleration Algorithm}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Properties of the restriction and matching operators}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Convergence Theorem}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Matching in $f$-divergence}{12}{section.2.4}
\contentsline {paragraph}{Relative-entropy matching}{13}{section*.11}
\contentsline {paragraph}{Other matching strategies}{13}{section*.12}
\contentsline {chapter}{\chapternumberline {3}Implementation of the micro-macro acceleration algorithm}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Practical Implementation Issues}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Newton-Raphson for relative-entropy matching}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Particle reweighting and resampling}{16}{subsection.3.1.2}
\contentsline {paragraph}{Implementing matching by particle reweighting}{16}{section*.13}
\contentsline {paragraph}{Particle resampling}{17}{section*.14}
\contentsline {subsection}{\numberline {3.1.3}Resolving matching failures by extrapolation time step adaptivity}{17}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Object-oriented structure of the code}{18}{section.3.2}
\contentsline {paragraph}{Matching Strategies}{19}{section*.15}
\contentsline {paragraph}{Iterative Solvers}{19}{section*.16}
\contentsline {paragraph}{Microscopic Time steppers}{19}{section*.17}
\contentsline {section}{\numberline {3.3}Numerical Verification: FENE Dumbbells}{21}{section.3.3}
\contentsline {paragraph}{Euler-Maruyama simulation}{21}{section*.19}
\contentsline {paragraph}{Comparison of matching strategies}{22}{section*.21}
\contentsline {paragraph}{Moment accuracy for a varying number of state variables}{23}{section*.23}
\contentsline {paragraph}{Accuracy dependency on the extrapolation step}{24}{section*.25}
\contentsline {chapter}{\chapternumberline {4}Linear slow-fast SDEs: Convergence \& Stability}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}A decomposition for matching with only slow state variables}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Analytic results for matching with a Gaussian initial distribution}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Minimizing relative entropy}{28}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Matching with extrapolated slow mean}{29}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Matching with extrapolated slow mean and variance}{30}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Numerical experiments}{31}{subsection.4.2.4}
\contentsline {subsubsection}{Propagation of mean and variance with the Euler-Maruyama scheme}{31}{section*.27}
\contentsline {subsubsection}{Experiment 1: extrapolating the slow mean}{32}{section*.28}
\contentsline {subsubsection}{Experiment 2: extrapolating slow mean and variance}{34}{section*.30}
\contentsline {section}{\numberline {4.3}Convergence of the micro-macro acceleration method with slow mean extrapolation}{34}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}An iterative formula for slow mean-only extrapolation}{34}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Convergence Theorem}{36}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Stability of micro-macro acceleration}{37}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Stability of the Euler-Maruyama scheme with additive noise}{38}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Stability of micro-macro acceleration with mean extrapolation}{38}{subsection.4.4.2}
\contentsline {paragraph}{Crossing the stability boundary}{40}{section*.33}
\contentsline {paragraph}{Independence of $\varepsilon $}{40}{section*.35}
\contentsline {subsection}{\numberline {4.4.3}Stability with extrapolated mean and variance}{40}{subsection.4.4.3}
\contentsline {paragraph}{Numerical Experiment}{42}{section*.37}
\contentsline {paragraph}{Conclusion}{42}{section*.39}
\contentsline {chapter}{\chapternumberline {5}Effect of the choice of macroscopic state variables}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}The Fokker-Planck equation and closure relations}{45}{section.5.1}
\contentsline {paragraph}{The Fokker-Planck equation}{46}{section*.40}
\contentsline {paragraph}{Closure relations}{46}{section*.41}
\contentsline {paragraph}{Comparison of both methods}{46}{section*.42}
\contentsline {section}{\numberline {5.2}Estimating the number of moments for linear SDEs}{47}{section.5.2}
\contentsline {paragraph}{Effect of the number of slow moments}{48}{section*.44}
\contentsline {paragraph}{Adding fast moments to the extrapolation operator}{48}{section*.46}
\contentsline {paragraph}{A uniform initial distribution}{48}{section*.48}
\contentsline {section}{\numberline {5.3}A non-linear case: FENE-dumbbells}{49}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}FENE macroscopic state hierarchies}{50}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Numerical experiments}{50}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Conclusion}{52}{subsection.5.3.3}
\contentsline {chapter}{\chapternumberline {6}Accuracy of the extrapolation step size}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}A natural averaging strategy}{55}{section.6.1}
\contentsline {section}{\numberline {6.2}A linear driven process}{57}{section.6.2}
\contentsline {section}{\numberline {6.3}Efficiency and accuracy of the micro-macro scheme}{58}{section.6.3}
\contentsline {paragraph}{Conclusion}{62}{section*.60}
\contentsline {chapter}{\chapternumberline {7}Applications}{63}{chapter.7}
\contentsline {section}{\numberline {7.1}A bistable system}{63}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}The modelling error in the approximate macroscopic model}{64}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Improvements by the micro-macro acceleration scheme}{66}{subsection.7.1.2}
\contentsline {paragraph}{Conclusion}{66}{section*.66}
\contentsline {section}{\numberline {7.2}A tri-atom molecule}{68}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Approximate macroscopic models for over-damped Langevin dynamics}{69}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Improvements by micro-macro acceleration}{71}{subsection.7.2.2}
\contentsline {paragraph}{Extrapolating the reaction coordinates}{72}{section*.71}
\contentsline {paragraph}{Extrapolating moments of $x_c$ and $y_c$}{73}{section*.74}
\contentsline {paragraph}{Conclusion}{74}{section*.76}
\contentsline {chapter}{\chapternumberline {8}Conclusion and Outlook}{75}{chapter.8}
\contentsline {section}{\numberline {8.1}Results}{75}{section.8.1}
\contentsline {section}{\numberline {8.2}Outlook to future work}{76}{section.8.2}
\contentsline {appendix}{\chapternumberline {A}Finite volume method for the Fokker-Planck equation}{79}{appendix.A}
\contentsline {section}{\numberline {A.1}Derivation of a finite volume scheme}{79}{section.A.1}
\contentsline {section}{\numberline {A.2}Boundary conditions}{81}{section.A.2}
\contentsline {section}{\numberline {A.3}Order test to verify a correct implementation}{81}{section.A.3}
\contentsline {appendix}{\chapternumberline {B}Derivation of a closure model for linear SDEs}{83}{appendix.B}
\contentsline {section}{\numberline {B.1}Derivation of the closure relations}{83}{section.B.1}
\contentsline {section}{\numberline {B.2}Analytical solution of the closure relations}{84}{section.B.2}
\contentsline {section}{\numberline {B.3}Convergence test for the forward Euler implementation}{85}{section.B.3}
\contentsline {appendix}{\chapternumberline {C}Poster}{87}{appendix.C}
\contentsline {chapter}{Bibliography}{89}{section*.82}
\select@language {dutch}
\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {dutch}
\select@language {english}
\select@language {dutch}
\select@language {english}
