import sys
sys.path.append("../../")

from library.models.linear_model import *
from linear_fokker_planck import *

from library.matching.kld import *
from library.timeintegrators.micromacro_cpi import *
from library.timeintegrators.gridnoflux_eulermaruyama import *
from library.tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib2tikz import save as tikz_save

import numpy.random as rd

# Closure implementation for the linear system of SDE's
def closurelinear(A, D, dt, K, initial):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    
    A = np.array([[a11, 0, a12, 0, 0],
                  [0, 2.*a11, 0, 0, 2*a12], 
                  [1, 0, a22, 0, 0],
                  [0, 0, 0, 2*a22, 2*a21],
                  [0, a21, 0, a12, a11+a22]])
    b = np.array([0, Dx**2, 0, Dy**2, 0])
    
    stats = [initial]
    times = [0.]
    sol = np.copy(initial)
    for k in range(K):
        sol = sol + dt*(A.dot(sol) + b)
        stats.append(sol)
        times.append((k+1)*dt)
        
    return times, np.matrix(stats)
    
# Full microscale simulation of the linear system of SDE's
def microsimulation(A, D, a, b, c, d, particles, dt, tend):
    model = LinearModel(A, D, [a, b, c, d])
    particles, times, cb = model.fullScaleMicroSimulation(particles, 0., tend, dt, MCStatistics)
    
    return particles, times, cb

# Define the moment functions
def momentFunctions(Lx, Ly, cov=False):
    RLX = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    
    if cov:
        RXY = lambda X: np.asarray([X[0]*X[1]])
        Rcov = lambda X: np.append(R(X), RXY(X))
        return Rcov
        
    return R

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result
    
# Matching acceleration algorithm
def matching(A, D, a, b, c, d, tbegin, tend, dt, Lx, Ly, particles, dtmin, dtmax, alfainc, alfadec):
    # And perform matching
    N = particles.shape[0]
    R = momentFunctions(Lx, Ly)
    Rplot = momentFunctions(2, 2, cov=True)
    model = LinearModel(A, D, [a, b, c, d])
    stepper = GridNoFluxEulerMaruyama(model.a, model.b, [a, b, c, d])
    kld = KLDMatchingStrategy(R)
    params = {'K': 1, 'Dtmin': dtmin, 'Dtmax': dtmax, 'alfadecrease': alfadec, 'alfaincrease': alfainc}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    
    callback = lambda X, w: computeMoments(points=X, R=Rplot, weights=w) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, callback)
    particles = cpiresult['points']
    weights = cpiresult['weights']
    cbtimes = cpiresult['cbtimes']
    cbvalues = cpiresult['cbvalues']
    
    return particles, weights, cbtimes, cbvalues
    
# Compute the five statistics form an MC distribution   
def MCStatistics(particles, weights=None):
    if weights is None:
        weights = np.ones(particles.shape[0])
        
    X = particles[:,0]
    Y = particles[:,1]
    N = particles.shape[0]
    
    meanX = np.average(X, weights=weights)
    meanY = np.average(Y, weights=weights)
    varX = N/(N-1)*np.average((X-meanX)**2, weights=weights)
    varY = N/(N-1)*np.average((Y-meanY)**2, weights=weights)
    
    normw = weights/np.sum(weights)
    V2 = np.sum(normw*normw)
    covXY = np.average((X-meanX)*(Y-meanY), weights=normw)/(1-V2)
    
    return np.array([meanX, varX, meanY, varY, covXY])

# Compute the five statistics form an MC distribution   
def MCStatistics1D(particles, weights=None):
    if weights is None:
        weights = np.ones(particles.shape[0])
        
    N = particles.shape[0]
    
    meanX = np.average(particles, weights=weights)
    varX = N/(N-1)*np.average((particles-meanX)**2, weights=weights)

    return np.array([meanX, varX])

# Compute the statistics from a density given in a grid
def computeStatistics(data, X, Y, dx, dy):
    # Compute marginal densities
    margX = dy*np.sum(data, axis=0)
    margY = dx*np.sum(data, axis=1)

    # Compute statistics
    meanX = dx*margX.dot(X)
    meanY = dy*margY.dot(Y)
    varX = dx*margX.dot((X-meanX)**2)
    varY = dy*margY.dot((Y-meanY)**2)
    X, Y = np.meshgrid(X, Y)
    covXY = dx*dy*np.sum((X-meanX)*(Y-meanY)*data)
    
    # Return the results as a vector
    return [meanX, varX, meanY, varY, covXY]

# Compute the marginal distribution from a given joint distribution
def computeMarginals(data, dx, dy):
    margX = np.sum(data, axis=1)*dy
    margY = np.sum(data, axis=0)*dx
    
    return margX, margY
    
# Several different models
def slowfastmodel(eps):
    A = np.matrix([[-1./eps, 0],[1, -1]])
    D = np.asarray([1./np.sqrt(eps), 1])
    return A, D
    
# Just a random system
def slowfastmodelindependent(eps):
    A = np.matrix([[-1./eps, 0],[0, -1]])
    D = np.asarray([1./np.sqrt(eps), 1])
    return A, D

# Sample a uniform distribution
def sampleUniformDistribution(a, b, c, d, Nx, Ny, N):
    initial = np.ones([int(Nx), int(Ny)])/((b-a)*(d-c))
    particles = np.zeros([N,2])
    particles[:,0] = rd.uniform(0., b, N)
    particles[:,1] = rd.uniform(0., d, N)
    
    return initial, particles
   
# Gaussian initial condition   
def sampleGaussianDistribution(a, b, c, d, Nx, Ny, N, meanX=1., meanY=2., varX=1., varY=1.):
    Nx = int(Nx)
    Ny = int(Ny)
    
    gauss = lambda x, y: 1./(2.*pi)*np.exp(-((x-meanX)**2/(2.*varX) + (y-meanY)**2/(2.*varY)))
    initial = np.zeros([Nx, Ny])
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    
    for i in range(Nx):
        for j in range(Ny):
            initial[i,j] = gauss(a+0.5*dx+i*dx, c+0.5*dy+j*dy)
    
    particles = np.zeros([N,2])
    particles[:,0] = rd.normal(meanX, varX, N)
    particles[:,1] = rd.normal(meanY, varY, N)
    
    return initial, particles

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,3]
    moment2Y = vector[:,4]
    vector[:,4] = moment2Y - meanY*meanY
    vector[:,5] = vector[:,5] - meanX*meanY
    
    return vector

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats
    
if __name__ == '__main__':
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -6.
    b = 6.
    c = -6.
    d = 6.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**5
    dt = 0.01
    K = 100
    tend = 5.
    
    # Define the model and initial condition
    eps = 0.1
    A, D = slowfastmodel(eps)
    initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N)
    initialmatchparticles = np.copy(particles)
    initialstats = computeStatistics(np.transpose(initial), X, Y, dx, dy)
    
    # Compute the numerical results
    solution, times, stats = FPLinear(A, D, a, b, c, d, dx, dy, dt/5., tend, initial)
    closuretimes, closurestatistics = closurelinear(A, D, dt, int(tend/dt), initialstats)
#    mcparticles, mctimes, mcstatistics = microsimulation(A, D, a, b, c, d, np.copy(particles), dt, tend)
    experiments = [[0,2, 0.01, 0.05, 1, 1.2,0.5],[1,2, 0.01, 0.05, 1, 1.2,0.5], [2,2, 0.01, 0.05, 1, 1.2, 0.5], [3,2, 0.01, 0.05, 1, 1.2, 0.5]]
    Ls = []
    dtmaxs = []
    matchparticles = []
    matchweights = []
    matchtimes=[]
    matchstatistics=[]
    for experiment in experiments:
       Lx = experiment[0]
       Ly = experiment[1]
       dtmin = experiment[2]
       dtmax = experiment[3]
       Kintern = experiment[4]
       p, w, t, st = matching(A, D, a, b, c, d, 0.0, tend, dt, Lx, Ly, np.copy(initialmatchparticles), dtmin, dtmax, experiment[5], experiment[6])
       Ls.append([Lx, Ly])
       dtmaxs.append(dtmax)
       matchparticles.append(p)
       matchweights.append(w)
       matchtimes.append(t)
       matchstatistics.append(getMeanAndVariance(st))
    
    # Create plot of the marginal densities
#     margX, margY = computeMarginals(solution, dx, dy)
#     closureMoments = np.asarray(closurestatistics[-1,:])[0]
#     closurePdfX = lambda x: 1./(np.sqrt(2.*pi*closureMoments[1]))*np.exp(-0.5*((x-closureMoments[0])**2/closureMoments[1]))
#     closurePdfY = lambda y: 1./(np.sqrt(2.*pi*closureMoments[3]))*np.exp(-0.5*((y-closureMoments[2])**2/closureMoments[3]))
#     mcpointsX = mcparticles[:,0]
#     mcpointsY = mcparticles[:,1]
#     figmarg = plt.figure()
#     ax1 = figmarg.add_subplot(211)
#     ax1.set_xlabel("x")
#     ax1.plot(X, margX, label="FP X")
#     ax1.hist(mcpointsX, bins=40, histtype='step', normed=True, label="Micro X")
#     ax1.plot(X, closurePdfX(X), label="Closure pdf X")
#     plt.legend()
#     ax2 = figmarg.add_subplot(212)
#     ax2.set_xlabel("y")
#     ax2.plot(Y, margY, label="FP Marginal Y")
#     ax2.hist(mcpointsY, bins=40, histtype='step', normed=True, label="Micro Y")
#     ax2.plot(Y, closurePdfY(Y), label="Closure pdf Y")
#     for i in range(len(Ls)):
#         L = Ls[i]
#         Lx = L[0]
#         Ly = L[1]
#         dtmax = dtmaxs[i]
#         p = matchparticles[i]
#         pX = p[:,0]
#         pY = p[:,1]
#         w = matchweights[i]
#        ax1.hist(pX, weights=w, histtype='step', bins=40, normed=True, label="Dt = "+str(dtmax)) #"Matching Lx="+str(Lx)+", Ly="+str(Ly)+" Dtmax="+str(dtmax))
#        ax2.hist(pY, weights=w, histtype='step', bins=40, normed=True, label="Dt = "+str(dtmax)) #"Matching Lx="+str(Lx)+", Ly="+str(Ly))
#     plt.legend()
    
    # Make a plot of the closure model
#     fig6 = plt.figure()
#     plt.plot(closuretimes, closurestatistics[:, 0], label="Closure fast")
#     plt.plot(times, stats[:, 0], label="Fokker-Planck fast")
#     plt.plot(mctimes, mcstatistics[:, 0], label="Microscale Monte Carlo fast")
#     plt.plot(closuretimes, closurestatistics[:, 2], label="Closure slow")
#     plt.plot(times, stats[:, 2], label="Fokker-Planck slow")
#     plt.plot(mctimes, mcstatistics[:, 2], label="Microscale Monte Carlo slow")
#     plt.xlabel("Time[s]")
#     plt.title("Mean of the slow and fast variables")
#     plt.legend()
#     fig2 = plt.figure()
#     plt.plot(closuretimes, closurestatistics[:, 1], label="Closure fast")
#     plt.plot(times, stats[:, 1], label="Fokker-Planck fast")
#     plt.plot(mctimes, mcstatistics[:, 1], label="Microscale Monte Carlo fast")
#     plt.plot(closuretimes, closurestatistics[:, 3], label="Closure slow")
#     plt.plot(times, stats[:, 3], label="Fokker-Planck slow")
#     plt.plot(mctimes, mcstatistics[:, 3], label="Microscale Monte Carlo slow")
#     plt.xlabel("Time[s]")
#     plt.title("Variance of the slow and fast variables")
#     plt.legend()
#     fig4 = plt.figure()
#     plt.plot(closuretimes, closurestatistics[:, 4], label="Closure")
#     plt.plot(times, stats[:, 4], label="Fokker-Planck")
#     plt.plot(mctimes, mcstatistics[:, 4], label="Microscale Monte Carlo")
#     plt.xlabel("Time[s]")
#     plt.title("Covariance between the slow and fast variables")
#     plt.legend()
    
    # Make a plot for the variance of matching strategies
    fig3 = plt.figure()
    ax1 = fig3.add_subplot(111)
    ax1.plot(times, stats[:, 1], label="Fokker-Planck")
    for i in range(len(Ls)):
        L = Ls[i]
        Lx = L[0]
        Ly = L[1]
        dtmax = dtmaxs[i]
        t, cbvalues = getRealPoints(matchtimes[i], matchstatistics[i])
        ax1.plot(t, cbvalues[:,2], label=r"Micro-macro $L_x="+str(Ly)+",\ L_y="+str(Lx)+"$")
    plt.title("Variance of the fast variable")
    plt.legend()
    plt.xlabel("Time[s]")
    fig8 = plt.figure()
    ax2 = fig8.add_subplot(111)
    ax2.plot(times, stats[:, 3], label="Fokker-Planck")
    for i in range(len(Ls)):
        L = Ls[i]
        Lx = L[0]
        Ly = L[1]
        dtmax = dtmaxs[i]
        t, cbvalues = getRealPoints(matchtimes[i], matchstatistics[i])
        ax2.plot(t, cbvalues[:,4], label=r"Micro-macro $L_x="+str(Ly)+",\ L_y="+str(Lx)+"$")
    plt.title("Variance of the slow variable")
    plt.legend()
    plt.xlabel("Time[s]")
    
    # Make a plot for the means of matching strategies
    fig7 = plt.figure()
    ax3 = fig7.add_subplot(111)
    ax3.plot(times, stats[:, 0], label="Fokker-Planck")
    for i in range(len(Ls)):
        L = Ls[i]
        Lx = L[0]
        Ly = L[1]
        dtmax = dtmaxs[i]
        t, cbvalues = getRealPoints(matchtimes[i], matchstatistics[i])
        ax3.plot(t, cbvalues[:,1], label=r"Micro-macro $L_x="+str(Ly)+",\ L_y="+str(Lx)+"$")
    plt.title("Mean of the fast variable")
    plt.legend()
    fig9 = plt.figure()
    ax4 = fig9.add_subplot(111)
    ax4.plot(times, stats[:, 2], label="Fokker-Planck")
    for i in range(len(Ls)):
        L = Ls[i]
        Lx = L[0]
        Ly = L[1]
        dtmax = dtmaxs[i]
        t, cbvalues = getRealPoints(matchtimes[i], matchstatistics[i])
        ax4.plot(t, cbvalues[:,3], label=r"Micro-macro $L_x="+str(Ly)+",\ L_y="+str(Lx)+"$")
    plt.title("Mean of the slow variable")
    plt.legend()
    
    plt.figure(fig8.number)
    tikz_save('plots/Agreement_simulations/slowvarianceLy.tex')
    plt.figure(fig9.number)
#    tikz_save('plots/Agreement_simulations/slowmeanLx.tex')
    # Show all plots
    plt.show()
