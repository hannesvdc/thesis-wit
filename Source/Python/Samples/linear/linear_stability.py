import sys
sys.path.append("../../")

from library.models.linear_model import *
from linear import *

from library.matching.kld import *
from library.timeintegrators.micromacro_cpi import *
from library.timeintegrators.euler_maryama import *
from library.tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib2tikz import save as tikz_save

import numpy as np
import numpy.random as rd
import argparse

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "micro", "mean" and "meanvariance".')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float,
                        help='Choose the time scale separation. Default is 0.1.')
    parser.add_argument('--dt', metavar='dt', nargs='?', dest='dt', type=float,
                        help='Choose the (maximal) time step. Exact meaning depends on the experiment.')
                        
    args = parser.parse_args()
    return args

# Compute one term in the variance expression
def computeVarianceTerm(v, P, B, dt, j):
    t = np.transpose(np.dot(np.linalg.inv(P), B))
    d = np.diag(np.power((np.ones_like(v)+dt*v), j))
    
    return np.dot(d, np.dot(np.linalg.inv(P), np.dot(B, np.dot(t, d))))
    
# Compute the analytical variance up to a given tolerance
def analyticVariance(A, B, dt, tol):
    v, P = np.linalg.eig(A)
    variance = computeVarianceTerm(v, P, B, dt, 0)
    j = 1
    
    while True:
        term = computeVarianceTerm(v, P, B, dt, j)

        if np.linalg.norm(term) < tol:
            break
        
        variance = variance + term
        j = j+1
        
    return dt*np.dot(P, np.dot(variance, np.transpose(P)))
    

# Full microscale simulation of the linear system of SDE's
def microsimulation(A, D, a, b, c, d, particles, dt, tend):
    model = LinearModel(A, D, [a, b, c, d])
    em = EulerMaryamaIntegrator(model.a, model.b)
    particles = em.integrate(particles, 0., tend, dt)
    
    return particles

# micro stability tests
def microStability():
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -4.
    b = 4.
    c = -4.
    d = 4.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**5
    dt = 0.01
    K = 100
    tend = 30.
    var = 0.545454
    xas = np.linspace(-6,6,100)
    
    # Define the model and initial condition
    eps = 0.1
    A, D = slowfastmodel(eps)
    B = np.diag(D)
    initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N)
    initialmatchparticles = np.copy(particles)
    initialstats = computeStatistics(np.transpose(initial), X, Y, dx, dy)
    
    # Compute the numerical results
    mcparticles4 = microsimulation(A, D, a, b, c, d, np.copy(particles), 0.210, tend)
    
    mcpointsY4 = mcparticles4[:,1]
    fig4 = plt.figure()
    plt.hist(mcpointsY4, bins=500000, normed=True, label="Euler-Maruyama", color='r')
    plt.title("dt = 0.21")
    plt.legend()
    plt.xlabel("y")
    plt.xlim(-4,4)
    tikz_save('plots/stability/dt021.tex')
    
    # Show all plots
    plt.show()

def macroMeanStability():
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -4.
    b = 4.
    c = -4.
    d = 4.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**5
#    dt = 0.01
    tend = 290.
    dtmax = 1.95
    
    # Define the model and initial condition
    eps = 0.1
    dt = 0.9*eps
    dtmin = dt
#    A, D = slowfastmodel(eps)
    A = np.array([[-10.,0],[0.,-1]])
    B = 1./9.*np.array([[20./9., np.sqrt(10.)],[np.sqrt(10.), 9.]])
#    B = np.diag(D)
    initial, particles = initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N, 1., 1., 1., 1)
    
    # Compute the micro steady state solution
    xas = np.linspace(-4,4,100)
    V = analyticVariance(A, B, dt, 10**-8)
    print(V)
    rho = lambda x: 1./np.sqrt(2*pi*V[1,1])*np.exp(-x*x/(2*V[1,1]))
    density = rho(xas)
    
    # And perform matching
    N = particles.shape[0]
    R = momentFunctions(0, 1)
    model = LinearModel(A, B, [a, b, c, d])
    stepper = EulerMaruyamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(R, 'NR', 50)
    params = {'K': 1, 'Dtmin': dtmin, 'Dtmax': dtmax, 'Dt0': dtmax, 'alfadecrease': 1., 'alfaincrease': 1.}
    cpi = MicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, callback=None, adaptive=True)
    particles2 = cpiresult['points']
    weights = cpiresult['weights']
    matchfails = cpiresult['matchfails']
    meanDt = cpiresult['meanDt']
    varDt = cpiresult['varDt']
    
    np.savetxt('data/meanstability'+str(dtmax)+'.data', np.vstack((particles2[:,0],particles2[:,1],weights)))
    fig2 = plt.figure()
#    plt.rcParams['patch.edgecolor'] = 'red'
    plt.hist(particles2[:,1], weights=weights, bins=160, normed=True, label="Euler-Maruyama", color='r')
    plt.plot(xas, density, label="Invariant Distribution", color='b', linewidth=2.)
    print("Time step variance", varDt)
    plt.title("Dt = "+str(dtmax) + "\n Mean "+"{:10.4f}".format(meanDt)+" Std = "+"{:10.4f}".format(np.sqrt(varDt)))
    plt.legend()
    plt.xlabel("y")
    plt.xlim(-4,4)
#    tikz_save('plots/stability/Dt'+(str(dtmax).replace(".", ""))+'eps'+str(eps)+'_fails.tex')
    
    # Show all plots
    plt.show()

def macroMeanVarianceStability():
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -4.
    b = 4.
    c = -4.
    d = 4.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**5
    dt = 0.05
    tend = 210.
    dtmin = dt
    dtmax = 1.1
    
    # Define the model and initial condition
    eps = 0.1
    A, D = slowfastmodel(eps)
    B = np.diag(D)
    A = np.array([[-10.,0],[0.,-1]])
    B = 1./9.*np.array([[20./9., np.sqrt(10.)],[np.sqrt(10.), 9.]])
    V = analyticVariance(A, B, dt, 10**-6)
    initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N, 0., 0., 1., V[1,1])
    
    # Compute the micro steady state solution
    xas = np.linspace(-4,4,100)
    print(V)
    rho = lambda x: 1./np.sqrt(2*pi*V[1,1])*np.exp(-x*x/(2*V[1,1]))
    density = rho(xas)
    
    # And perform matching
    N = particles.shape[0]
    R = momentFunctions(0, 2)
    model = LinearModel(A, D, [a, b, c, d])
    stepper = EulerMaruyamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(R, 'NR', 10)
    params = {'K': 1, 'Dt0': dtmax, 'Dtmax': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, adaptive=True)
    particles2 = cpiresult['points']
    weights = cpiresult['weights']
    matchfails = cpiresult['matchfails']
    meanDt = cpiresult['meanDt']
    varDt = cpiresult['varDt']
    
    print(matchfails, ' matchfails')
    np.savetxt('data/meanvariancestability'+str(dtmax)+'.data', np.vstack((particles2[:,0],particles2[:,0],weights)))
    fig2 = plt.figure()
    plt.rcParams['patch.edgecolor'] = 'red'
    plt.hist(particles2[:,1], weights=weights, bins=160, normed=True, label="Euler-Maruyama", color='r')
    plt.plot(xas, density, label="Invariant Distribution", color='b')
    plt.title("Dt = "+str(dtmax)+"\n Mean "+"{:10.4f}".format(meanDt)+" Std = "+"{:10.4f}".format(np.sqrt(varDt)))
    plt.legend()
    plt.xlabel("y")
    plt.xlim(-4,4)
    tikz_save('plots/stability/meanvarianceDt'+(str(dtmax).replace(".", ""))+'_statistics.tex')
    
    # Show all plots
    plt.show()

def resample(sample, weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = len(sample)
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros(nb_sam)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample
    
if __name__ == "__main__":
    args = parseArguments()

#    macroMeanVarianceStability()    
    macroMeanStability()
