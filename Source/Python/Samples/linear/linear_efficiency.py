import sys
sys.path.append("../../")

from linear_model import *
from linear import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.euler_maryama import *
from tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import numpy.linalg as lin
import numpy.random as rd
import argparse

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--eps', metavar='e', nargs='?', dest='eps', default=0.1,
                        help='Select the time scale separation epsilon for the problem.')
    parser.add_argument('--K', metavar='K', nargs='?', dest='K', default=1,
                        help='Select the number of microscopic steps.')
    parser.add_argument('--experiment', nargs='?', dest='experiment', help='Select the mean or variance extrapolation.')                    
    args = parser.parse_args()
    return args

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Perform matching with the simple linear model with given epsilon and endtime.
def match(eps, X, Y, tend, dt, dtmax, Lx, Ly, K, kld=False):
    N = X.shape[0]
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    D = np.asarray([1./np.sqrt(eps), 1.])
    model = LinearModel(A, D, grid=None)
    muinv, varinv = model.invariantMeasure(dt, 10.**-4)
    Yinv = np.random.normal(muinv[1], np.sqrt(varinv[1,1]), N)
    print("Invariant slow variance ", varinv)
    priorY = np.copy(Y)
    Rcbpartial = lambda x, w: computeMoments(points=x, R=momentFunctions(2, 2, cov=True), weights=w)
    if kld is True:
        Rcb = lambda x, w: np.append(Rcbpartial(x, w), np.array([KLDMatchingStrategy.computeDistance(x[:,1], priorY, weights=w), KLDMatchingStrategy.computeDistance(x[:,1], Yinv, weights=w)]))
    else:
        Rcb = Rcbpartial
    
    Rmatchcb = lambda x, w, neww: np.array([KLDMatchingStrategy.computeDistance(x, x, priorweights=w, weights=neww),KLDMatchingStrategy.computeDistance(x[:,1], x[:,1], priorweights=w, weights=neww)])
    # And perform matching
    particles = np.zeros([N, 2])
    particles[:,0] = X
    particles[:,1] = Y
    R = momentFunctions(Lx, Ly)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(particles, R, computeMoments(particles, R), 'NR', 7)
    params = {'K': K, 'Dtmax': dtmax, 'Dt0': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, [0,1], adaptive=True, callback = Rcb, matchcallback=Rmatchcb)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues'], cpiresult['matchcbtimes'], cpiresult['matchcbvalues']

# Average the fast variables so that we become a single SDE for the slow variable.
def averageFast(Y, tend, dt):
    model = LinearModel(np.matrix([-2.]), np.diag(np.asarray([1.])), grid=None)
    particles, times, cb = model.fullScaleMicroSimulation(particles, 0., tend, dt, MCStatistics)
    
    return particles, times, cb

# Perform all experiments in the given config
def performEfficiencyExperiments(configs, eps):
    # Fixed parameters for the experiments
    tend = 1.
    Lx = 0
    Ly = 2
    N = 10**5
    
    # Setup the plot
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    plt.title("Mean Fast Variables, epsilon = "+str(eps))
    ax2 = fig1.add_subplot(212)
    plt.title("Mean Slow Variables")
    plt.xlabel("Time[s]")
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title("Variance Fast Variables, epsilon = "+str(eps))
    ax4 = fig2.add_subplot(212)
    plt.title("Variance Slow Variables")
    plt.xlabel("Time[s]")
    fig5 = plt.figure()
    ax5 = fig5.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    fig6 = plt.figure()
    ax6 = fig6.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    
    # Setup models
    fullModel = LinearModel(np.matrix([[-1./eps, 1./eps],[-1., -1.]]), np.asarray([1./np.sqrt(eps), 1.]), grid=None)
    avgModel = LinearModel(np.matrix([-2.]), np.asarray([1.]))
    
    # Perform all given experiments
    for config in configs:
        # Initial Condition
        X0 = rd.normal(10., 10., N)
        Y0 = rd.normal(1., 1., N)
        initial = np.zeros([N,2])
        initial[:,0] = np.copy(X0)
        initial[:,1] = np.copy(Y0)
        
        if config['type'] == 'micro':
            fullParticles, fullTimes, fullCb = fullModel.fullScaleMicroSimulation(initial, 0., tend, config['dt'], MCStatistics)
            ax1.plot(fullTimes, fullCb[:,0], color='b', label="Full Micro Model")
            ax1.plot(fullTimes, fullCb[:,2], color='g', label="Mean Slow Variables", marker="x")
            ax2.plot(fullTimes, fullCb[:,2], color='b', label="Full Micro Model")
            ax3.plot(fullTimes, fullCb[:,1], color='b', label="Full Micro Model")
            ax4.plot(fullTimes, fullCb[:,3], color='b', label="Full Micro Model")
        elif config['type'] == 'mM':
            mmParticles, mmWeights, mmCbtimes, mmCbvalues, mmMatchtimes, mmMatchvalues = match(eps, np.copy(X0), np.copy(Y0), tend, config['dt'], config['dtmax'], Lx, Ly, config['K'], kld=True)
            mmCbtimes, mmCbvalues = getRealPoints(mmCbtimes, mmCbvalues)
            mmRelativeEntropy = mmCbvalues[:,6]
            mmRelativeEntropyInvariant = mmCbvalues[:,7]
            mmCbvalues = getMeanAndVariance(mmCbvalues)
            ax1.plot(mmCbtimes, mmCbvalues[:,1], label="mM Acceleration {0:.4f}".format(config['dtmax']))
            ax2.plot(mmCbtimes, mmCbvalues[:,3], label="mM Acceleration {0:.4f}".format(config['dtmax']))
            ax3.plot(mmCbtimes, mmCbvalues[:,2], label="mM Acceleration {0:.4f}".format(config['dtmax']))
            ax4.plot(mmCbtimes, mmCbvalues[:,4], label="mM Acceleration {0:.4f}".format(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropy, label='Entropy to initial condition, dtmax={0:.4f}'.format(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropyInvariant, label='Entropy to invariant measure, dtmax={0:.4f}'.format(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,0], label="Full relative entropy, dtmax={0:.4f}".format(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,1], label="Coarse relative entropy, dtmax={0:.4f}".format(config['dtmax']))
        elif config['type'] == 'macro':
            avgParticles, avgTimes, avgCb = avgModel.fullScaleMicroSimulation(np.copy(Y0), 0., tend, config['Dt'], MCStatistics1D)
            ax2.plot(avgTimes, avgCb[:,0], color = 'r', label="Averaged Model")
            ax4.plot(avgTimes, avgCb[:,1], color = 'r', label="Averaged Model")

    # Finalise the plots and show them.
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    plt.show()

def meanvarExtrapolationExperiments(configs, eps):
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    plt.title("Mean Fast Variables, epsilon = "+str(eps))
    ax2 = fig1.add_subplot(212)
    plt.title("Mean Slow Variables")
    plt.xlabel("Time[s]")
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title("Variance Fast Variables, epsilon = "+str(eps))
    ax4 = fig2.add_subplot(212)
    plt.title("Variance Slow Variables")
    plt.xlabel("Time[s]")
    fig5 = plt.figure()
    ax5 = fig5.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    fig6 = plt.figure()
    ax6 = fig6.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    
    # Setup models
    fullModel = LinearModel(np.matrix([[-1./eps, 1./eps],[-1., -1.]]), np.asarray([1./np.sqrt(eps), 1.]), grid=None)
    avgModel = LinearModel(np.matrix([-2.]), np.asarray([1.]))
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    B = np.asmatrix(np.diag(np.asarray([1./np.sqrt(eps), 1.])))
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = B[0,0]
    Dy = B[1,1]
    tend = 1.
    Lx = 0
    Ly = 2
    N = 10**5
    fastmean = 10.
    fastscale = 10.
    integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm(-u*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm(-u*np.transpose(A))))[i,j]
    for config in configs:
        if config['type'] == 'analytic':
            dt = config['dt']
            Dt = config['Dt']
            mean = np.array([fastmean, 1.])
            var = np.matrix([[fastscale**2, 0.],[0., 1.]])
            means = [mean]
            variances = [var]
            entropies = []
            slowvars= [var[1,1]]
            fastvars = [var[0,0]]
            times = [0.]
            matchtimes = []
            
            # Time stepping
            t = 0.0
            while t < tend:
                # EM step
                times.append(t+dt)
                mean = np.squeeze(np.asarray((np.eye(2)+dt*A).dot(mean)))
                var = (np.eye(2)+dt*A)*var*(np.eye(2)+dt*np.transpose(A)) + dt*(B*np.transpose(B))
                print('micro mean', mean)
                print('micro var', var)
                means.append(mean)
                variances.append(var)
                slowvars.append(var[1,1])
                fastvars.append(var[0,0])
                
                # Exact extrapolation over time Dt
                extremean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm((Dt-dt)*A)).dot(mean)))
                intfastvar = scipy.integrate.quad(integral, 0, Dt-dt, args=(0,0))[0]
                intcovar = scipy.integrate.quad(integral, 0, Dt-dt, args=(1,0))[0]
                intslowvar = scipy.integrate.quad(integral, 0, Dt-dt, args=(1,1))[0]
                extrvar = np.asmatrix(scipy.linalg.expm((Dt-dt)*A))*(var + np.matrix([[intfastvar, intcovar],[intcovar,intslowvar]]))*np.asmatrix(scipy.linalg.expm((Dt-dt)*np.transpose(A)))
                means.append(extremean)
                times.append(t+Dt)
                variances.append(extrvar)
                fastvars.append(extrvar[0,0])
                slowvars.append(extrvar[1,1])
                print('extrapolated')
                print(extremean)
                print(extrvar)
                
                # Perform matching with these new moments using the analytical formulas
                newmean = np.array([mean[0] + var[0,1]*(extremean[1]-mean[1])/var[1,1], extremean[1]])
                newcovar = var[1,0]*extrvar[1,1]/var[1,1]
                newvar = np.matrix([[var[0,0] - var[1,0]**2*(var[1,1]-extrvar[1,1])/(var[1,1]**2),newcovar],[newcovar,extrvar[1,1]]])
                means.append(newmean)
                divergence = 0.5*(log(np.abs(lin.det(var)/lin.det(newvar)))-2.+np.trace(lin.inv(var)*newvar)+np.dot(mean-newmean, np.squeeze(np.asarray((lin.inv(var)).dot(mean-newmean)))))
                entropies.append(divergence)
                matchtimes.append(t+Dt)
                variances.append(newvar)
                fastvars.append(newvar[0,0])
                slowvars.append(newvar[1,1])
                mean = newmean
                var = newvar
                print('new mean', mean)
                print('new var', var)
                t += Dt
                times.append(t)
            
            times = np.array(times)
            means = np.array(means)
            print(times.shape)
            print(means[:,0].shape)
            slowvars = np.array(slowvars)
            fastvars = np.array(fastvars)    
            ax1.plot(times, means[:,0], label="Analytic Dt={0:.4f}".format(Dt))
            ax2.plot(times, means[:,1], label="Analytic Dt={0:.4f}".format(Dt))                  
            ax3.plot(times, fastvars, label="Analytic Dt={0:.4f}".format(Dt))    
            ax4.plot(times, slowvars, label="Analytic Dt={0:.4f}".format(Dt)) 
            ax6.semilogy(matchtimes, entropies, label='Analytic entropy, Dt={0:.4f}'.format(Dt))      
        elif config['type'] == 'micro':
            X0 = rd.normal(fastmean, fastscale, N)
            Y0 = rd.normal(1., 1., N)
            initial = np.zeros([N,2])
            initial[:,0] = np.copy(X0)
            initial[:,1] = np.copy(Y0)
            fullParticles, fullTimes, fullCb = fullModel.fullScaleMicroSimulation(initial, 0., tend, config['dt'], MCStatistics)
            ax1.plot(fullTimes, fullCb[:,0], color='b', label="Full Micro Model")
            ax1.plot(fullTimes, fullCb[:,2], color='g', label="Mean Slow Variables", marker="x")
            ax2.plot(fullTimes, fullCb[:,2], color='b', label="Full Micro Model")
            ax3.plot(fullTimes, fullCb[:,1], color='b', label="Full Micro Model")
            ax4.plot(fullTimes, fullCb[:,3], color='b', label="Full Micro Model")
        elif config['type'] == 'mM':        
            X0 = rd.normal(fastmean, fastscale, N)
            Y0 = rd.normal(1., 1., N)
            initial = np.zeros([N,2])
            initial[:,0] = np.copy(X0)
            initial[:,1] = np.copy(Y0)
            mmParticles, mmWeights, mmCbtimes, mmCbvalues, mmMatchtimes, mmMatchvalues = match(eps, np.copy(X0), np.copy(Y0), tend, config['dt'], config['dtmax'], Lx, Ly, config['K'], kld=True)
            mmCbtimes, mmCbvalues = getRealPoints(mmCbtimes, mmCbvalues)
            mmRelativeEntropy = mmCbvalues[:,6]
            mmRelativeEntropyInvariant = mmCbvalues[:,7]
            mmCbvalues = getMeanAndVariance(mmCbvalues)
            ax1.plot(mmCbtimes, mmCbvalues[:,1], label="mM Acceleration Dt = {0:.4f}".format(config['dtmax']))
            ax2.plot(mmCbtimes, mmCbvalues[:,3], label="mM Acceleration Dt = {0:.4f}".format(config['dtmax']))
            ax3.plot(mmCbtimes, mmCbvalues[:,2], label="mM Acceleration Dt = {0:.4f}".format(config['dtmax']))
            ax4.plot(mmCbtimes, mmCbvalues[:,4], label="mM Acceleration Dt = {0:.4f}".format(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropy, label='Entropy to initial condition, dtmax={0:.4f}'.format(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropyInvariant, label='Entropy to invariant measure, dtmax={0:.4f}'.format(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,0], label="Full relative entropy, dtmax{0:.4f}".format(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,1], label="Coarse relative entropy, dtmax{0:.4f}".format(config['dtmax']))
        elif config['type'] == 'macro':
            Y0 = rd.normal(1., 1., N)
            avgParticles, avgTimes, avgCb = avgModel.fullScaleMicroSimulation(Y0, 0., tend, config['Dt'], MCStatistics1D)
            ax2.plot(avgTimes, avgCb[:,0], color = 'r', label="Averaged Model")
            ax4.plot(avgTimes, avgCb[:,1], color = 'r', label="Averaged Model")
        elif config['type'] == 'correctness':
            Dt = config['Dt']
            t = 0.
            mean = np.array([fastmean, 1.])
            var = np.matrix([[fastscale**2, 0.],[0., 1.]])
            fastvars = [var[0,0]]
            slowvars = [var[1,1]]
            means = [mean]
            integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm(-u*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm(-u*np.transpose(A))))[i,j]
            entropies = []
            times = [0.]
            while t < tend:
                newmean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm((Dt)*A)).dot(mean)))
                newfastvar = scipy.integrate.quad(integral, 0, Dt, args=(0,0))[0]
                newcovar = scipy.integrate.quad(integral, 0, Dt, args=(1,0))[0]
                newslowvar = scipy.integrate.quad(integral, 0, Dt, args=(1,1))[0]
                print('var ', var)
                newvar = np.asmatrix(scipy.linalg.expm((Dt)*A))*(var + np.matrix([[newfastvar, newcovar],[newcovar,newslowvar]]))*np.asmatrix(scipy.linalg.expm((Dt)*np.transpose(A)))
                means.append(newmean)
                fastvars.append(newvar[0,0])
                slowvars.append(newvar[1,1])
                print('new mean', newmean)
                print('new var', newvar)
                divergence = 0.5*(log(np.abs(lin.det(var)/lin.det(newvar)))-2.+np.trace(lin.inv(var)*newvar)+np.dot(mean-newmean, np.squeeze(np.asarray((lin.inv(var)).dot(mean-newmean)))))
                entropies.append(divergence)
                times.append(t+Dt)
                
                mean = newmean
                var = newvar
                t += Dt
            
            means = np.asarray(means)
            ax1.plot(times, means[:,0], label="Exact mean")
            ax2.plot(times, means[:,1], label="Exact mean")
            ax3.plot(times, fastvars, label="Exact variance")
            ax4.plot(times, slowvars, label="Exact variance")
            ax6.semilogy(times[1:], entropies, label="Exact solution")
            
    # Finalise the plots and show them.
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    plt.show()                   

def meanExtrapolationExperiments(configs, eps):
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    plt.title("Mean Fast Variables, epsilon = "+str(eps))
    ax2 = fig1.add_subplot(212)
    plt.title("Mean Slow Variables")
    plt.xlabel("Time[s]")
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title("Variance Fast Variables, epsilon = "+str(eps))
    ax4 = fig2.add_subplot(212)
    plt.title("Variance Slow Variables")
    plt.xlabel("Time[s]")
    fig5 = plt.figure()
    ax5 = fig5.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    fig6 = plt.figure()
    ax6 = fig6.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    
    # Setup models
    fullModel = LinearModel(np.matrix([[-1./eps, 1./eps],[-1., -1.]]), np.asarray([1./np.sqrt(eps), 1.]), grid=None)
    avgModel = LinearModel(np.matrix([-2.]), np.asarray([1.]))
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    B = np.asmatrix(np.diag(np.asarray([1./np.sqrt(eps), 1.])))
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = B[0,0]
    Dy = B[1,1]
    tend = 1.
    Lx = 0
    Ly = 1
    N = 10**5
    fastmean = 10.
    fastscale = 10.
    integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm(-u*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm(-u*np.transpose(A))))[i,j]
    for config in configs:
        if config['type'] == 'analytic':
            dt = config['dt']
            Dt = config['Dt']
            mean = np.array([fastmean, 1.])
            var = np.matrix([[fastscale**2, 0.],[0., 1.]])
            means = [mean]
            variances = [var]
            slowvars= [var[1,1]]
            fastvars = [var[0,0]]
            times = [0.]
            entropies = []
            matchtimes = []
            
            # Time stepping
            t = 0.0
            while t < tend:
                # EM step
                t += dt
                times.append(t)
                mean = np.squeeze(np.asarray((np.eye(2)+dt*A).dot(mean)))
                var = (np.eye(2)+dt*A)*var*(np.eye(2)+dt*np.transpose(A)) + dt*(B*np.transpose(B))
                print('micro mean', mean)
                print('micro var', var)
                means.append(mean)
                variances.append(var)
                slowvars.append(var[1,1])
                fastvars.append(var[0,0])
                
                # Exact extrapolation over time Dt
                extremean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm(Dt*A)).dot(mean)))
                means.append(extremean)
                times.append(t+Dt)
                print('extrapolated')
                print(extremean)
                
                # Perform matching with these new moments using the analytical formulas
                newmean = np.array([mean[0] + var[0,1]*(extremean[1]-mean[1])/var[1,1], extremean[1]])
                means.append(newmean)
                variances.append(var)
                fastvars.append(var[0,0])
                slowvars.append(var[1,1])
                divergence = 0.5*(-1.+np.trace(lin.inv(var)*var)+np.dot(mean-newmean, np.squeeze(np.asarray((lin.inv(var)).dot(mean-newmean)))))
                entropies.append(divergence)
                matchtimes.append(t+Dt)
                mean = newmean
                print('new mean', mean)
                print('new var', var)
                t += Dt
                times.append(t)
            
            times = np.array(times)
            means = np.array(means)
            print(times.shape)
            print(means[:,0].shape)
            slowvars = np.array(slowvars)
            fastvars = np.array(fastvars)    
            ax1.plot(times, means[:,0], label="Analytic Dt="+str(Dt))
            ax2.plot(times, means[:,1], label="Analytic Dt="+str(Dt))                  
            ax3.plot(np.delete(times, np.s_[2::3]), fastvars, label="Analytic Dt="+str(Dt))    
            ax4.plot(np.delete(times, np.s_[2::3]), slowvars, label="Analytic Dt="+str(Dt))
            ax6.semilogy(matchtimes, entropies, label='Analytic entropy, Dt='+str(Dt)) 
        elif config['type'] == 'micro':
            X0 = rd.normal(fastmean, fastscale, N)
            Y0 = rd.normal(1., 1., N)
            initial = np.zeros([N,2])
            initial[:,0] = np.copy(X0)
            initial[:,1] = np.copy(Y0)
            fullParticles, fullTimes, fullCb = fullModel.fullScaleMicroSimulation(initial, 0., tend, config['dt'], MCStatistics)
            ax1.plot(fullTimes, fullCb[:,0], color='b', label="Full Micro Model")
            ax1.plot(fullTimes, fullCb[:,2], color='g', label="Mean Slow Variables", marker="x")
            ax2.plot(fullTimes, fullCb[:,2], color='b', label="Full Micro Model")
            ax3.plot(fullTimes, fullCb[:,1], color='b', label="Full Micro Model")
            ax4.plot(fullTimes, fullCb[:,3], color='b', label="Full Micro Model")
        elif config['type'] == 'mM':        
            X0 = rd.normal(fastmean, fastscale, N)
            Y0 = rd.normal(1., 1., N)
            initial = np.zeros([N,2])
            initial[:,0] = np.copy(X0)
            initial[:,1] = np.copy(Y0)
            mmParticles, mmWeights, mmCbtimes, mmCbvalues, mmMatchtimes, mmMatchvalues = match(eps, np.copy(X0), np.copy(Y0), tend, config['dt'], config['dtmax'], Lx, Ly, config['K'], kld=True)
            mmCbtimes, mmCbvalues = getRealPoints(mmCbtimes, mmCbvalues)
            mmRelativeEntropy = mmCbvalues[:,6]
            mmRelativeEntropyInvariant = mmCbvalues[:,7]
            mmCbvalues = getMeanAndVariance(mmCbvalues)
            ax1.plot(mmCbtimes, mmCbvalues[:,1], label="mM Acceleration"+str(config['dtmax']))
            ax2.plot(mmCbtimes, mmCbvalues[:,3], label="mM Acceleration"+str(config['dtmax']))
            ax3.plot(mmCbtimes, mmCbvalues[:,2], label="mM Acceleration"+str(config['dtmax']))
            ax4.plot(mmCbtimes, mmCbvalues[:,4], label="mM Acceleration"+str(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropy, label='Entropy to initial condition, dtmax='+str(config['dtmax']))
            ax5.plot(mmCbtimes, mmRelativeEntropyInvariant, label='Entropy to invariant measure, dtmax='+str(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,0], label="Full relative entropy, dtmax"+str(config['dtmax']))
            ax6.semilogy(mmMatchtimes, mmMatchvalues[:,1], label="Coarse relative entropy, dtmax"+str(config['dtmax']))
        elif config['type'] == 'macro':
            Y0 = rd.normal(1., 1., N)
            avgParticles, avgTimes, avgCb = avgModel.fullScaleMicroSimulation(Y0, 0., tend, config['Dt'], MCStatistics1D)
            ax2.plot(avgTimes, avgCb[:,0], color = 'r', label="Averaged Model")
            ax4.plot(avgTimes, avgCb[:,1], color = 'r', label="Averaged Model")
        elif config['type'] == 'correctness':
            Dt = config['Dt']
            t = 0.
            mean = np.array([fastmean, 1.])
            var = np.matrix([[fastscale**2, 0.],[0., 1.]])
            fastvars = [var[0,0]]
            slowvars = [var[1,1]]
            means = [mean]
            integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm(-u*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm(-u*np.transpose(A))))[i,j]
            entropies = []
            times = [0.]
            while t < tend:
                newmean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm((Dt)*A)).dot(mean)))
                newfastvar = scipy.integrate.quad(integral, 0, Dt, args=(0,0))[0]
                newcovar = scipy.integrate.quad(integral, 0, Dt, args=(1,0))[0]
                newslowvar = scipy.integrate.quad(integral, 0, Dt, args=(1,1))[0]
                print('var ', var)
                newvar = np.asmatrix(scipy.linalg.expm((Dt)*A))*(var + np.matrix([[newfastvar, newcovar],[newcovar,newslowvar]]))*np.asmatrix(scipy.linalg.expm((Dt)*np.transpose(A)))
                means.append(newmean)
                fastvars.append(newvar[0,0])
                slowvars.append(newvar[1,1])
                print('new mean', newmean)
                print('new var', newvar)
                divergence = 0.5*(log(np.abs(lin.det(var)/lin.det(newvar)))-2.+np.trace(lin.inv(var)*newvar)+np.dot(mean-newmean, np.squeeze(np.asarray((lin.inv(var)).dot(mean-newmean)))))
                entropies.append(divergence)
                times.append(t+Dt)
                
                mean = newmean
                var = newvar
                t += Dt
            
            means = np.asarray(means)
            ax1.plot(times, means[:,0], label="Exact mean")
            ax2.plot(times, means[:,1], label="Exact mean")
            ax3.plot(times, fastvars, label="Exact variance")
            ax4.plot(times, slowvars, label="Exact variance")
            ax6.semilogy(times[1:], entropies, label="Exact solution")
            
    # Finalise the plots and show them.
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    plt.show()                 

def entropyOrderExperiment(configs, Dts):
    N = 10**5
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    plt.title("Linear")
    plt.xlabel('Extrapolation factor')
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    plt.title("LogLog")
    plt.xlabel('Extrapolation factor')
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    plt.title("Semilogx")
    plt.xlabel('Extrapolation factor')
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111)
    plt.title("Semilogy")
    plt.xlabel('Extrapolation factor')
    
    # Perform all the experiments
    for config in configs:
        if config['type'] != 'Hplot': 
            continue
        
        Ly = config['Ly']
        eps = config['eps']
        dt = config.get('dt', eps/10.)
        
        if config['initial'] == 'good':
            mean = np.array([1., 1.])
            var = np.matrix([[1., 0.],[0., 1.]])
        elif config['initial'] == 'bad':
            mean = np.array([10., 1.])
            var = np.matrix([[100., 0.],[0., 1.]])
        print('eps', eps)
        A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
        D = np.asarray([1./np.sqrt(eps), 1.])
        B = np.diag(D)
        model = LinearModel(A, D, grid=None)
        print(Ly)
        R = momentFunctions(0, Ly)
        starttime = config['starttime']
        
        integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm((starttime-u)*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm((starttime-u)*np.transpose(A))))[i,j]
        mean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm((starttime)*A)).dot(mean)))
        fastvar = scipy.integrate.quad(integral, 0, starttime, args=(0,0))[0]
        covar = scipy.integrate.quad(integral, 0, starttime, args=(1,0))[0]
        slowvar = scipy.integrate.quad(integral, 0, starttime, args=(1,1))[0]
        var = np.asmatrix(scipy.linalg.expm((starttime)*A))*var*np.asmatrix(scipy.linalg.expm((starttime)*np.transpose(A))) + np.matrix([[fastvar, covar],[covar, slowvar]])

        entropies = []
        for factor in Dts:
            Dt = factor*dt
            print("Dt = ", Dt)
            particles = np.zeros([N,2])
            np.random.seed(4038)
            particles[:,0] = np.random.normal(mean[0], np.sqrt(var[0,0]), N)
            np.random.seed(3302)
            particles[:,1] = np.random.normal(mean[1], np.sqrt(var[1,1]), N)
            priorweights = np.ones(N)
            kld = KLDMatchingStrategy(particles, R, computeMoments(particles, R), 'NR', 7)
            moments = computeMoments(particles, R)
            newparticles = model.fullScaleMicroSimulation(np.copy(particles), 0., dt, dt)
            newmoments = computeMoments(newparticles, R)
            extrapolatedmoments = moments + Dt/dt*(newmoments-moments)
            print(newmoments, extrapolatedmoments)
            
            # Match
            kld.setMoments(extrapolatedmoments)
            kld.setX(newparticles)
            conv, lambdas = kld.match(weights=priorweights)
            newweights = kld.weights(lambdas, weights=priorweights)
            print(np.sum(priorweights))
            print(np.sum(newweights))
            # Compute entropy
            entropy = kld.computeDistance(newparticles, newparticles, priorweights=priorweights, weights=newweights)
            entropies.append(entropy)      
        
        ax1.plot(Dts, np.asarray(entropies)+10.**-15, label="t = "+str(starttime)+" eps="+str(eps)+" dt="+str(dt)+" Ly="+str(Ly)+" "+config['initial']+" initial", marker='x')
        ax2.loglog(Dts, np.asarray(entropies)+10.**-15, label="t = "+str(starttime)+" eps="+str(eps)+" dt="+str(dt)+" Ly="+str(Ly)+" "+config['initial']+" initial", marker='x')
        ax3.semilogx(Dts, np.asarray(entropies)+10.**-15, label="t = "+str(starttime)+" eps="+str(eps)+" dt="+str(dt)+" Ly="+str(Ly)+" "+config['initial']+" initial", marker='x')
        ax4.semilogy(Dts, np.asarray(entropies)+10.**-15, label="t = "+str(starttime)+" eps="+str(eps)+" dt="+str(dt)+" Ly="+str(Ly)+" "+config['initial']+" initial", marker='x')
        
    print(entropies)
    
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    plt.show()
    
def main():
    # Parse the input arguments
    args = parseArguments()
    eps = float(args.eps)
    K = int(args.K)
    
    # Simulation parameters
    dt = eps/10.
    Dt = 0.05
    Dtmax = min(5.*dt, 0.05)
    
    configs = []
    configs.append({'type':'macro', 'Dt': Dt})
    configs.append({'type': 'micro', 'dt': dt})
#    configs.append({'type': 'mM', 'dt': dt, 'dtmax': 3.*dt, 'K': K})
#    configs.append({'type': 'mM', 'dt': dt, 'dtmax': 2.*dt, 'K': K})
#    configs.append({'type': 'mM', 'dt': dt, 'dtmax': 1.1*dt, 'K': K})
    configs.append({'type': 'analytic', 'dt': dt, 'Dt': 3*dt})
    configs.append({'type': 'analytic', 'dt': dt, 'Dt': 2*dt})
#     configs.append({'type': 'analytic', 'dt': dt, 'Dt': 1.5*dt})
    configs.append({'type': 'analytic', 'dt': dt, 'Dt': 1.1*dt})
#    configs.append({'type': 'analytic', 'dt': dt, 'Dt': dt})
#    configs.append({'type': 'correctness'})
#    performEfficiencyExperiments(configs, eps)
    configs.append({'type': 'Hplot', 'starttime': 0., 'eps': 0.1, 'dt': 0.01, 'Ly': 2, 'initial': 'good'})
    configs.append({'type': 'Hplot', 'starttime': 0., 'eps': 0.1, 'dt': 0.05, 'Ly': 2, 'initial': 'good'})
    configs.append({'type': 'Hplot', 'starttime': 0., 'eps': 0.01, 'dt': 0.001, 'Ly': 2, 'initial': 'bad'})
    configs.append({'type': 'Hplot', 'starttime': 0., 'eps': 0.01, 'dt': 0.005, 'Ly': 2, 'initial': 'bad'})

    if args.experiment == 'mean':
        meanExtrapolationExperiments(configs, eps)
    elif args.experiment == 'meanvariance':
        meanvarExtrapolationExperiments(configs, eps) 
    elif args.experiment == "match":
        entropyOrderExperiment(configs, [1., 1.012, 1.05, 1.1, 1.2, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10])  
    else:
        print('The experiment is not provided, choose another one.')

    
if __name__ == '__main__':
    main()
