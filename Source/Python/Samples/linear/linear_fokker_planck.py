import sys
sys.path.append("../../")

import numpy as np

# General FP implementation with finite volumes for a two-dimensional 
# linear system of SDE's.
def FPLinear(A, D, a, b, c, d, dx, dy, dt, tend, initial):
    solution = np.copy(initial)
    Nx = solution.shape[0]
    Ny = solution.shape[1]
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    
    t = dt
    k = 1
    times = [0.0]
    stats = [computeGridStatistics(np.transpose(initial), X, Y, dx, dy)]
    while k <= int(tend/dt):
        print("t = ", t)
        nextstep = np.zeros_like(solution)
        for i in range(Nx):
            for j in range(Ny):
                # Get fluxes at the boundaries of each square
                if i == 0:
                    Fl = 0.
                else:
                    pavg = 0.5*(solution[i-1,j] + solution[i,j])
                    Fl = (a11*(a+i*dx) + a12*(c+(j+0.5)*dy))*pavg
                    
                if i == Nx-1:
                    Fr = 0.
                else:
                    pavg = 0.5*(solution[i,j] + solution[i+1,j])
                    Fr = (a11*(a+(i+1)*dx)+a12*(c+(j+0.5)*dy))*pavg
                    
                if j == 0:
                    Fd = 0.
                else:
                    pavg = 0.5*(solution[i,j-1] + solution[i,j])
                    Fd = (a21*(a+(i+0.5)*dx)+ a22*(c+j*dy))*pavg
                    
                if j == Ny-1:
                    Fu = 0.
                else:
                    pavg = 0.5*(solution[i,j] + solution[i,j+1])
                    Fu = (a21*(a+(i+0.5)*dx) + a22*(c+(j+1)*dy))*pavg
                    
                # Compute second derivatives
                if i == 0:
                    dxx = (solution[i+1,j]-solution[i,j])/(dx**2)
                elif i == Nx-1:
                    dxx = (solution[i-1,j]-solution[i,j])/(dx**2)
                else:
                    dxx = (solution[i-1,j]-2*solution[i,j]+solution[i+1,j])/(dx**2)
                  
                if j == 0:
                    dyy = (solution[i,j+1]-solution[i,j])/(dy**2)
                elif j == Ny-1:
                    dyy = (solution[i,j-1]-solution[i,j])/(dy**2)
                else:
                    dyy = (solution[i,j-1]-2*solution[i,j]+solution[i,j+1])/(dy**2)
                
                # And compute the forward euler approximation
                rhs = (Fl-Fr)/dx + (Fd-Fu)/dy + 0.5*(Dx**2*dxx + Dy**2*dyy)
                nextstep[i,j] = solution[i,j] + dt*rhs
             
        solution = nextstep
        print("mass = ", np.sum(solution)*dx*dy)
        k += 1
        t += dt
        times.append(t)
        stats.append(computeGridStatistics(np.transpose(solution), X, Y, dx, dy))
        
    return solution, times, np.asmatrix(stats)

# Compute the statistics from a density given in a grid
def computeGridStatistics(data, X, Y, dx, dy):
    # Compute marginal densities
    margX = dy*np.sum(data, axis=0)
    margY = dx*np.sum(data, axis=1)

    # Compute statistics
    meanX = dx*margX.dot(X)
    meanY = dy*margY.dot(Y)
    varX  = dx*margX.dot((X-meanX)**2)
    varY  = dy*margY.dot((Y-meanY)**2)
    X, Y  = np.meshgrid(X, Y)
    covXY = dx*dy*np.sum((X-meanX)*(Y-meanY)*data)
    
    # Return the results as a vector
    return [meanX, varX, meanY, varY, covXY]
