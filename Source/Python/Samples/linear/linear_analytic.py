import sys
sys.path.append("../../")

import argparse

from linear_model import *
from linear import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.noflux_euler_maruyama import *
from tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy.random as rd

# Select the input the user wants to perform
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the fene model')
    parser.add_argument('--Ly', metavar='e', nargs='?', dest='Ly',
                        help='Select which experiment you want to perform.')

    args = parser.parse_args()
    return args
    
# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Compute the analytical results for coarse mean matching
def meanExtrapolatedAnalytic(times, initial, A, D):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    
    vector = np.copy(initial)
    statistics = [vector]
    index = 0
    while index+2 < len(times):
        # Set one monte carlo step
        dt = times[index+1]-times[index]
        mc = np.zeros(5)
        mc[0] = (1+a11*dt)*vector[0] + a12*dt*vector[2]
        mc[1] = (1+a11*dt)**2*vector[1] + (a12*dt)**2*vector[3] + Dx**2*dt + 2*a12*dt*(1+a11*dt)*vector[4]
        mc[2] = a21*dt*vector[0] + (1+a22*dt)*vector[2]
        mc[3] = (a21*dt)**2*vector[1] + (1+a22*dt)**2*vector[3] + Dy**2*dt + 2*a21*dt*(1+a22*dt)*vector[4]
        mc[4] = a21*dt*(1+a11*dt)*vector[1] + a12*dt*(1+a22*dt)*vector[3] + ((1+a11*dt)*(1+a22*dt)+a21*a12*dt**2)*vector[4]
        statistics.append(mc)
        
        # Perform the extrapolation
        Dt = times[index+2]-times[index]
        newmean = (1.-Dt/(dt))*vector[2] + Dt/(dt)*mc[2]
        
        # And finally compute the matching means and variances
        match = np.zeros(5)
        match[0] = mc[0] + mc[4]*(newmean-mc[2])/mc[3]
        match[1] = mc[1]
        match[2] = newmean
        match[3] = mc[3]
        match[4] = mc[4]
        statistics.append(match)
        
        vector = np.copy(match)
        index = index+2
        
    return times, np.asarray(statistics)

# Create the mean and covariance plots for coarse mean extrapolation
def meanExtrapolatedPlot(A, D, a, b, c, d, tend, dt, dtmax, initialmatchparticles, initialstats):
    # Compute the reference solution using the closure model
    closuretimes, closurestatistics = closurelinear(A, D, dt, int(tend/dt), initialstats)
    
    # Compute the result using matching with Lx = 0 and Ly = 1
    N = initialmatchparticles.shape[0]
    p1, w1, t1, st1 = matching(A, D, a, b, c, d, 0.0, tend, dt, 0, 1, np.copy(initialmatchparticles), dt, dtmax, 1.2, 0.5)
    st1 = getMeanAndVariance(st1)
    realt1, realst1 = getRealPoints(t1, st1)
    analytictimes, analyticstats = meanExtrapolatedAnalytic(realt1, initialstats, A, D)
    
    # And plot the graphs for this experiment
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(closuretimes, closurestatistics[:, 0], label="Closure model")
    ax1.plot(t1, st1[:,1], label="Matching Lx=0, Ly=1")
    ax1.plot(realt1, realst1[:,1], 'X')
    ax1.plot(analytictimes, analyticstats[:,0], label="Analytical reference")
    plt.title("Mean of the fast variable with "+str(N)+" particles.")
    plt.legend()
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(closuretimes, closurestatistics[:, 2], label="Closure model")
    ax2.plot(t1, st1[:,3], label="Matching Lx=0, Ly=1")
    ax2.plot(realt1, realst1[:,3],'X')
    ax2.plot(analytictimes, analyticstats[:,2], label="Analytical reference")
    plt.title("Mean of the slow variable with "+str(N)+" particles.")
    plt.legend()
    
    # And plot the graphs for the variance of this experiment
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(closuretimes, closurestatistics[:, 1], label="Closure model")
    ax3.plot(t1, st1[:,2], label="Matching Lx=0, Ly=1")
    ax3.plot(realt1, realst1[:,2], 'X')
    ax3.plot(analytictimes, analyticstats[:,1], label="Analytical reference")
    plt.title("Variance of the fast variable with "+str(N)+" particles.")
    plt.legend()
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111)
    ax4.plot(closuretimes, closurestatistics[:, 3], label="Closure model")
    ax4.plot(t1, st1[:,4], label="Matching Lx=0, Ly=1")
    ax4.plot(realt1, realst1[:,4],'X')
    ax4.plot(analytictimes, analyticstats[:,3], label="Analytical reference")
    plt.title("Variance of the slow variable with "+str(N)+" particles.")
    plt.legend()
    
    # And plot the graphs for this experiment
    fig5 = plt.figure()
    plt.plot(closuretimes, closurestatistics[:, 4], label="Closure model")
    plt.plot(t1, st1[:,5], label="Matching Lx=0, Ly=1")
    plt.plot(realt1, realst1[:,5], 'X')
    plt.plot(analytictimes, analyticstats[:,4], label="Analytical reference")
    plt.title("Covariance of the fast and slow variables with "+str(N)+" particles.")
    plt.legend()
    
    
    # And finally show the plots
    plt.figure(fig1.number)
    tikz_save('plots/Analytical_Reference/fastmeanmeanextrapolated.tex')
    plt.figure(fig2.number)
    tikz_save('plots/Analytical_Reference/slowmeanmeanextrapolated.tex')
    plt.figure(fig3.number)
    tikz_save('plots/Analytical_Reference/fastvariancemeanextrapolated.tex')
    plt.figure(fig4.number)
    tikz_save('plots/Analytical_Reference/slowvariancemeanextrapolated.tex')
    plt.show()
    
    
# Compute the analytical results for coarse mean matching
def meanAndVarianceExtrapolatedAnalytic(times, initial, A, D):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    
    vector = np.copy(initial)
    secondmoment = initial[3] + initial[2]**2
    statistics = [vector]
    index = 0
    while index+2 < len(times):
        # Set one monte carlo step
        dt = times[index+1]-times[index]
        mc = np.zeros(5)
        mc[0] = (1+a11*dt)*vector[0] + a12*dt*vector[2]
        mc[1] = (1+a11*dt)**2*vector[1] + (a12*dt)**2*vector[3] + Dx**2*dt + 2*a12*dt*(1+a11*dt)*vector[4]
        mc[2] = a21*dt*vector[0] + (1+a22*dt)*vector[2]
        mc[3] = (a21*dt)**2*vector[1] + (1+a22*dt)**2*vector[3] + Dy**2*dt + 2*a21*dt*(1+a22*dt)*vector[4]
        mc[4] = a21*dt*(1+a11*dt)*vector[1] + a12*dt*(1+a22*dt)*vector[3] + ((1+a11*dt)*(1+a22*dt)+a21*a12*dt**2)*vector[4]
        statistics.append(mc)
        mcsecondmoment = mc[3] + mc[2]**2
        
        # Perform the extrapolation
        Dt = times[index+2]-times[index]
        newmean = (1.-Dt/(dt))*vector[2] + Dt/(dt)*mc[2]
        newsecondmoment = (1.-Dt/(dt))*secondmoment + Dt/(dt)*mcsecondmoment
        newvariance = newsecondmoment - newmean**2
        
        # And finally compute the matching means and variances
        match = np.zeros(5)
        match[0] = mc[0] + mc[4]*(newmean-mc[2])/mc[3]
        match[1] = mc[1] - mc[4]**2*(mc[3]-newvariance)/(mc[3]**2)
        match[2] = newmean
        match[3] = newvariance
        match[4] = mc[4]*newvariance/mc[3]
        statistics.append(match)
        
        secondmoment = newsecondmoment
        vector = np.copy(match)
        index = index+2
        
    return times, np.asarray(statistics)

# Create the mean and covariance plots for coarse mean and variance extrapolation
def meanAndVarianceExtrapolatedPlot(A, D, a, b, c, d, tend, dt, dtmax, initialmatchparticles, initialstats):
    # Compute the reference solution using the closure model
    closuretimes, closurestatistics = closurelinear(A, D, dt, int(tend/dt), initialstats)
    
    # Compute the result using matching with Lx = 0 and Ly = 1
    N = initialmatchparticles.shape[0]
    p1, w1, t1, st1 = matching(A, D, a, b, c, d, 0.0, tend, dt, 0, 2, np.copy(initialmatchparticles), dt, dtmax, 1.2, 0.5)
    st1 = getMeanAndVariance(st1)
    realt1, realst1 = getRealPoints(t1, st1)
    analytictimes, analyticstats = meanAndVarianceExtrapolatedAnalytic(realt1, initialstats, A, D)
    
    # And plot the graphs for this experiment
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(closuretimes, closurestatistics[:, 0], label="Closure model")
    ax1.plot(t1, st1[:,1], label="Matching Lx=0, Ly=2")
    ax1.plot(realt1, realst1[:,1], 'X')
    ax1.plot(analytictimes, analyticstats[:,0], label="Analytical reference")
    plt.title("Mean of the fast variable with "+str(N)+" particles.")
    plt.legend()
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(closuretimes, closurestatistics[:, 2], label="Closure model")
    ax2.plot(t1, st1[:,3], label="Matching Lx=0, Ly=2")
    ax2.plot(realt1, realst1[:,3],'X')
    ax2.plot(analytictimes, analyticstats[:,2], label="Analytical reference")
    plt.title("Mean of the slow variable with "+str(N)+" particles.")
    plt.legend()
    
    # And plot the graphs for the variance of this experiment
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.plot(closuretimes, closurestatistics[:, 1], label="Closure model")
    ax3.plot(t1, st1[:,2], label="Matching Lx=0, Ly=2")
    ax3.plot(realt1, realst1[:,2], 'X')
    ax3.plot(analytictimes, analyticstats[:,1], label="Analytical reference")
    plt.title("Variance of the fast variable with "+str(N)+" particles.")
    plt.legend()
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111)
    ax4.plot(closuretimes, closurestatistics[:, 3], label="Closure model")
    ax4.plot(t1, st1[:,4], label="Matching Lx=0, Ly=2")
    ax4.plot(realt1, realst1[:,4],'X')
    ax4.plot(analytictimes, analyticstats[:,3], label="Analytical reference")
    plt.title("Variance of the slow variable with "+str(N)+" particles.")
    plt.legend()
    
    # And plot the graphs for this experiment
    fig5 = plt.figure()
    plt.plot(closuretimes, closurestatistics[:, 4], label="Closure model")
    plt.plot(t1, st1[:,5], label="Matching Lx=0, Ly=2")
    plt.plot(realt1, realst1[:,5], 'X')
    plt.plot(analytictimes, analyticstats[:,4], label="Analytical reference")
    plt.title("Covariance of the fast and slow variables with "+str(N)+" particles.")
    plt.legend()
    
    # And finally show the plots
    plt.figure(fig1.number)
    tikz_save('plots/Analytical_Reference/fastmeanmeanvarianceextrapolated.tex')
    plt.figure(fig2.number)
    tikz_save('plots/Analytical_Reference/slowmeanmeanvarianceextrapolated.tex')
    plt.figure(fig3.number)
    tikz_save('plots/Analytical_Reference/fastvariancemeanvarianceextrapolated.tex')
    plt.figure(fig4.number)
    tikz_save('plots/Analytical_Reference/slowvariancemeanvarianceextrapolated.tex')
    plt.show()
    
if __name__ == '__main__':
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -6.
    b = 6.
    c = -6.
    d = 6.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**5
    dt = 0.01
    K = 100
    tend = 1.
    dtmax = 0.1
    
    # Define the model and initial condition
    eps = 0.1
    A, D = slowfastmodel(eps)
    initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N)
    initialmatchparticles = np.copy(particles)
    initialstats = MCStatistics(particles)
    
    # Parse the inputs
    args = parseArguments()
    if args.Ly == "1":
        meanExtrapolatedPlot(A, D, a, b, c, d, tend, dt, dtmax, initialmatchparticles, initialstats)
    elif args.Ly == "2":
        meanAndVarianceExtrapolatedPlot(A, D, a, b, c, d, tend, dt, dtmax, initialmatchparticles, initialstats)