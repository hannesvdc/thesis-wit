from linear import *

import numpy as np
import scipy as sp
import scipy.stats
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

import argparse

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "closure", "micro" and "fp"')

    args = parser.parse_args()
    return args
    
# Convergence test for the closure model. We should see first order convergence in time  
def closureTest(A, D, times, tend, initial):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    Aode = np.array([[a11, 0, a12, 0, 0],
                  [0, 2.*a11, 0, 0, 2*a12], 
                  [1, 0, a22, 0, 0],
                  [0, 0, 0, 2*a22, 2*a21],
                  [0, a21, 0, a12, a11+a22]])
    bode = np.array([0, Dx**2, 0, Dy**2, 0])
    invariant = -np.linalg.solve(Aode, bode)
    xtend = invariant + sp.linalg.expm(Aode*tend).dot(initial-invariant)
    print("True invariant measure ", invariant)
    print("Solution at time ", tend, xtend)
    
    errors = []
    for k in range(len(times)):
        print("\n\n\nstep size ", times[k], k)
        steps = int(round(tend/times[k]))
        
        # Perform the simulation
        print("initial condition", initial)
        closuretimes, stats = closurelinear(A, D, times[k], steps, initial)
        stats = np.asarray(stats[-1,:])[0]
        print("Computed invariant ", stats)
        errors.append(np.abs(stats-xtend))
        print("Errors at ", times[k], np.abs(stats-xtend))
        
    errors = np.matrix(errors)

    # Make the order plots
    fig1 = plt.figure()
    plt.loglog(times, errors[:,0], "-x", label="E[X]")
    plt.loglog(times, errors[:,2], "-x", label="E[Y]")
    plt.loglog(times, times, label="First order")
    plt.title("Error for the mean of the slow and fast variables")
    plt.xlabel("Step Size")
    plt.ylabel("Absolute error")
    plt.legend()
    fig2=plt.figure()
    plt.loglog(times, errors[:,1], "-x", label="Var[X]")
    plt.loglog(times, errors[:,3], "-x", label="Var[Y]")
    plt.loglog(times, times, label="First order")
    plt.title("Error for the variance of the slow variable")
    plt.xlabel("Step Size")
    plt.ylabel("Absolute error")
    plt.legend()
    
    plt.figure(fig1.number)
    tikz_save('plots/cleconvergence.tex')
    plt.figure(fig2.number)
    tikz_save('plots/clvarconvergence.tex')
    plt.show()

# Convergence test for the Monte Carlo micro simulator in time. We should see first order here.
def microTestTime(A, D, times, tend, initial, a, b, c, d, particles):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    Aode = np.array([[a11, 0, a12, 0, 0],
                  [0, 2.*a11, 0, 0, 2*a12], 
                  [1, 0, a22, 0, 0],
                  [0, 0, 0, 2*a22, 2*a21],
                  [0, a21, 0, a12, a11+a22]])
    bode = np.array([0, Dx**2, 0, Dy**2, 0])
    invariant = -np.linalg.solve(Aode, bode)
    xtend = invariant + sp.linalg.expm(Aode*tend).dot(initial-invariant)
    print("True invariant measure ", invariant)
    print("Solution at time ", tend, xtend)
    
    errors = []
    for k in range(len(times)):
        print("\n\n\nstep size ", times[k], k)
        steps = int(round(tend/times[k]))

        # Perform the simulation
        newparticles, mctimes, callback = microsimulation(A, D, a, b, c, d, np.copy(particles), times[k], steps)
        stats = np.asarray(callback[-1,:])
        print("Computed statistics ", stats)
        errors.append(np.abs(stats-xtend))
        print("Errors at ", times[k], np.abs(stats-xtend))

    errors = np.matrix(errors)
    print(errors)
    print(times)
    tosave = np.vstack([times, errors])
    np.savetxt('data/MCtimeConvergence.data', tosave)
    #===========================================================================
    # print(errors[:,0])
    # plt.figure()
    # plt.loglog(times, errors[:,0], "-x", label="E[X]")
    # plt.loglog(times, errors[:,2], "-x", label="E[Y]")
    # plt.loglog(times, times, "-x", label="First order")
    # plt.title("Error for the mean of the slow and fast variables")
    # plt.xlabel("Step Size")
    # plt.ylabel("Absolute error")
    # plt.legend()
    # plt.figure()
    # plt.loglog(times, errors[:,1], "-x", label="Var[X]")
    # plt.loglog(times, errors[:,3], "-x", label="Var[Y]")
    # plt.loglog(times, times, "-x", label="First order")
    # plt.title("Error for the variance of the slow variable")
    # plt.xlabel("Step Size")
    # plt.ylabel("Absolute error")
    # plt.legend()
    # plt.show()
    #===========================================================================

# Make a convergence plot for the monte carlo simulation as a function of the number of particles
def microTestParticles(A, D, nparticles, tend, initial, a, b, c, d):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    Aode = np.array([[a11, 0, a12, 0, 0],
                  [0, 2.*a11, 0, 0, 2*a12], 
                  [1, 0, a22, 0, 0],
                  [0, 0, 0, 2*a22, 2*a21],
                  [0, a21, 0, a12, a11+a22]])
    bode = np.array([0, Dx**2, 0, Dy**2, 0])
    invariant = -np.linalg.solve(Aode, bode)
    xtend = invariant + sp.linalg.expm(Aode*tend).dot(initial-invariant)
    print("True invariant measure ", invariant)
    print("Solution at time ", tend, xtend)
    
    errors = []
    dt = 0.001
    steps = 500
    for k in range(len(nparticles)):
        print("\n\n\nparticles ", nparticles[k], k)
        N = nparticles[k]
        particles = np.zeros([N,2])
        particles[:,0] = rd.normal(1., 1., N)
        particles[:,1] = rd.normal(2., 1., N)
        
        # Perform the simulation
        newparticles, mctimes, callback = microsimulation(A, D, a, b, c, d, particles, dt, steps)
        stats = np.asarray(callback[-1,:])
        print("Computed statistics ", stats)
        errors.append(np.abs(stats-xtend))
        print("Errors at ", nparticles[k], np.abs(stats-xtend))

    errors = np.matrix(errors)
    open('data/MCparticlesConvergence.data','w').close()
    file = open('data/MCparticlesConvergence.data','ab')
    np.savetxt(file, np.asmatrix(np.reshape(nparticles, [1, len(nparticles)])))
    np.savetxt(file, np.transpose(errors))
    file.close()
    #===========================================================================
    # print(errors[:,0])
    # 
    # plt.figure()
    # plt.loglog(nparticles, errors[:,0], "-x", label="E[X]")
    # plt.loglog(nparticles, errors[:,2], "-x", label="E[Y]")
    # plt.loglog(nparticles, 1./np.sqrt(nparticles), "-x", label="Half order")
    # plt.title("Error for the mean of the slow and fast variables")
    # plt.xlabel("Particles")
    # plt.ylabel("Absolute error")
    # plt.legend()
    # plt.figure()
    # plt.loglog(nparticles, errors[:,1], "-x", label="Var[X]")
    # plt.loglog(nparticles, errors[:,3], "-x", label="Var[Y]")
    # plt.title("Error for the variance of the slow variable")
    # plt.loglog(nparticles, 1./np.sqrt(nparticles), "-x", label="Half order")
    # plt.xlabel("Particles")
    # plt.ylabel("Absolute error")
    # plt.legend()
    # plt.show()
    #===========================================================================

# Do a convergence test in space and time for the Fokker-Planck equation
def fpTest(A, D, gridsizes, a, b, c, d, initialstats, tend):
    a11 = A[0,0]
    a12 = A[0,1]
    a21 = A[1,0]
    a22 = A[1,1]
    Dx = D[0]
    Dy = D[1]
    Aode = np.array([[a11, 0, a12, 0, 0],
                  [0, 2.*a11, 0, 0, 2*a12], 
                  [1, 0, a22, 0, 0],
                  [0, 0, 0, 2*a22, 2*a21],
                  [0, a21, 0, a12, a11+a22]])
    bode = np.array([0, Dx**2, 0, Dy**2, 0])
    invariant = -np.linalg.solve(Aode, bode)
    xtend = invariant + sp.linalg.expm(Aode*tend).dot(initialstats-invariant)
    mu = np.array([xtend[0], xtend[2]])
    Sigma = np.matrix([[xtend[1], xtend[4]],[xtend[4], xtend[3]]])
    solution = scipy.stats.multivariate_normal(mu, Sigma)
    
    errors = []
    times = []
    space = []
    for k in range(len(gridsizes)):
        print("\n\n\nCreating grid with ", gridsizes[k], " elements in each direction.")
        N = gridsizes[k]
        initial, particles = sampleGaussianDistribution(a, b, c, d, N, N, 0)
        dx = (b-a)/N
        dt = dx**2/32
        K = int(round(tend/dt))
        print(dt, dx)
        # Perform the FP simulation
        fpsolution, fptimes, fpstats = FPLinear(A, D, a, b, c, d, dx, dx, dt, K, initial)
        
        # And compute the error relative to the analytical solution
        X = np.linspace(a+(b-a)/(2*N), b-(b-a)/(2*N), N)
        Y = np.linspace(c+(d-c)/(2*N), d-(d-c)/(2*N), N)
        X, Y = np.meshgrid(X,Y)
        pos = np.empty(X.shape + (2,))
        pos[:, :, 0] = X; pos[:, :, 1] = Y
        analyticsolution = np.transpose(solution.pdf(pos))
        print("Analytic solution ", analyticsolution)
        print("Numerical solution ", fpsolution)
        errors.append(np.linalg.norm(analyticsolution-fpsolution)*dx**2)
        times.append(dt)
        space.append(dx)
    
    times = np.array(times)
    errors = np.array(errors)
    plt.figure()
    plt.loglog(times, errors, "-x", label="Numerical Error")
    plt.loglog(times, times, "-x", label="First order")
    plt.loglog(times, np.power(times, 2), "-x", label="Second order")
    plt.title("Global error between the numerical and analytical solution")
    plt.xlabel("Temporal step size dt")
    plt.ylabel("Absolute error")
    plt.legend()
    tikz_save('plots/fptimeconvergence.tex')
    
    plt.figure()
    plt.loglog(space, errors, "-x", label="Numerical Error")
    plt.loglog(space, np.power(space, 2), "-x", label="Second order")
    plt.loglog(space, np.power(space, 3), "-x", label="Third order")
    plt.title("Global error between the numerical and analytical solution")
    plt.xlabel("Spacial step size dx")
    plt.ylabel("Absolute error")
    plt.legend()
    tikz_save('plots/fpspaceconvergence.tex')
    plt.show()
    
if __name__ == '__main__':
    # Define grid parameters
    Nx = 48.
    Ny = 48.
    a = -6.
    b = 6.
    c = -6.
    d = 6.
    dx = (b-a)/Nx
    dy = (d-c)/Ny
    X = np.linspace(a+(b-a)/(2*Nx), b-(b-a)/(2*Nx), Nx)
    Y = np.linspace(c+(d-c)/(2*Ny), d-(d-c)/(2*Ny), Ny)
    N = 10**6
    
    # Define the model and initial condition
    eps = 0.1
    A, D = slowfastmodel(eps)
    initial, particles = sampleGaussianDistribution(a, b, c, d, Nx, Ny, N)
    initialstats = np.asarray([1., 1., 2., 1., 0.])

    # Carry out the desired experiment
    args = parseArguments()
    if args.experiment == "closure":
        closureTest(A, D, [0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0.0005, 0.0002, 0.0001], 0.5, np.copy(initialstats))
    elif args.experiment == "microtime":
        microTestTime(A, D, [0.05, 0.02, 0.01, 0.005, 0.002, 0.001], 0.5, np.copy(initialstats), a, b, c, d, particles)
    elif args.experiment == "microparticles":
        microTestParticles(A, D, [10**2, 2*10**2, 5*10**2, 10**3, 2*10**3, 5*10**3, 10**4, 2*10**4, 5*10**4, 10**5], 0.5, np.copy(initialstats), a, b, c, d)
    elif args.experiment == "fp":
        fpTest(A, D, [12, 24, 48, 96, 192, 384], a, b, c, d, np.copy(initialstats), 0.5)

