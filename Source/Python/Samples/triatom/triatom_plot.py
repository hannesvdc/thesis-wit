import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

from triatom import *

import argparse
import fnmatch
import os
import bisect

def parseArguments():
    parser = argparse.ArgumentParser(description='Input parameters for plotting on the periodic model.')
    parser.add_argument('--plot', metavar='p', nargs='?', dest='plot',
                        help='Select which the type of plot.')
    parser.add_argument('--eps', metavar='e', nargs='?',dest='eps', 
                        help='Select the epsilon value to make the plots for.')
    parser.add_argument('--A', metavar='a', nargs='?',dest='A', 
                        help='Select the coarse noise level for the plots.')
    parser.add_argument('--files', metavar='c', nargs='+', dest='files',
                        help='Select what data to display in the plots.')
    parser.add_argument('--save', metavar='d', nargs='?', dest='save', default = '0',
                        help='Select what data to display in the plots.')
    parser.add_argument('--tend', metavar='tend', nargs='?', dest='tend', type=float, default = 6.,
                        help='Select the end time of the plots.')
    
    args = parser.parse_args()
    return args

##############################################################
#                   File helper functions                    #
##############################################################
def getParticleAndDataFile(filename):
    print(filename)
    if "model" in filename:
        return filename, filename.replace("model", "particles")
    else:
        return filename.replace("particles", "model"), filename

# Get the type of data file
def getType(file):
    if "micro" in file:
        return "micro"
    elif "macroangle" in file:
        return "macroangle"
    elif "macrodistance" in file:
        return "macrodistance"
    elif "mM" in file:
        return "mM"
    else:
        return None
    
# Get the type of extrapolation from the mM file name
def getExtrapolateFromFile(file):
    index = file.find('extrapolate') + 11
    endindex = file.find('.data')
    return file[index:endindex]

# Get the extrapolation factor from a mM file.
def getFactorFromFileName(file):
    index = file.find('factor')
    index += 6
    factorindex = file.find('tend', index)
    factor = float(file[index:factorindex])
    
    return factor

##############################################################
#                  Several plot functions                    #
##############################################################
def plotSolutions(files, tend, save=False):
    fig1 = plt.figure()
    anglehist = fig1.add_subplot(111)
    plt.title('Angle Histogram')
    plt.xlabel(r'$\theta$')
    fig2 = plt.figure()
    anglemean = fig2.add_subplot(111)
    plt.title('Angle Mean')
    plt.xlabel('Time [s]')
    fig3 = plt.figure()
    anglevar = fig3.add_subplot(111)
    plt.title('Angle Variance')
    plt.xlabel('Time [s]')
    fig4 = plt.figure()
    distancehist = fig4.add_subplot(111)
    plt.title('Distance Histogram')
    plt.xlabel(r'$(A-C)^2$')
    fig5 = plt.figure()
    distancemean = fig5.add_subplot(111)
    plt.title('Distance Mean')
    plt.xlabel('Time [s]')
    fig6 = plt.figure()
    distancevar = fig6.add_subplot(111)
    plt.title('Distance Variance')
    plt.xlabel('Time [s]')
    
    bins = 30
    
    for file in files:
        datafile, particlefile = getParticleAndDataFile(file)
        particles = np.loadtxt(particlefile)
        data = np.loadtxt(datafile)
        times = data[0,:]
        index = np.argmax(times > tend)
        if index == 0:
            index = times.shape[0]
            
        if getType(file) == 'micro': 
            # Cmopute the final theta and distance particles
            angleparticles = np.arccos(particles[1,:]/np.sqrt(particles[1,:]**2+particles[2,:]**2))
            angleparticles = angleparticles[np.logical_not(np.isnan(angleparticles))]
            distparticles = (particles[0,:]-particles[1,:])**2+particles[2,:]**2
            
            # And plot the obtained results
            anglemean.plot(times[0:index:10], data[5,0:index:10], label='Micro')
            anglevar.plot(times[0:index:10], data[6,0:index:10], label='Micro')
            anglehist.hist(angleparticles[0:index:10], bins=bins, normed=True, label='Micro', alpha=0.4)
            distancehist.hist(distparticles[0:index:10], bins=bins,  normed=True, label='Micro', alpha=0.4)
            distancemean.plot(times[0:index:10], data[7,0:index:10], label='Micro')
            distancevar.plot(times[0:index:10], data[8,0:index:10], label='Micro') 
            
        elif getType(file) == 'macroangle':
            anglemean.plot(times[0:index:10], data[1,0:index:10], label='Macro Theta')
            anglevar.plot(times[0:index:10], data[2,0:index:10], label='Macro Theta')
            anglehist.hist(particles[0:index:10], bins=bins, normed=True, label='Macro Theta', alpha=0.4)
        elif getType(file) == 'macrodistance':
            distancemean.plot(times[0:index:10], data[1,0:index:10], label='Macro Distance')
            distancevar.plot(times[0:index:10], data[2,0:index:10], label='Macro Distance')
            distancehist.hist(particles[0:index:10], bins=bins, normed=True, label='Macro Distance', alpha=0.4)
        elif getType(file) == 'mM':
            extrapolate = getExtrapolateFromFile(datafile)
            factor = getFactorFromFileName(datafile)
            mmweights = particles[3,:]
            angleparticles = np.arccos(particles[1,:]/np.sqrt(particles[1,:]**2+particles[2,:]**2))
            angleparticles = angleparticles[np.logical_not(np.isnan(angleparticles))]
            angleweights = mmweights[np.logical_not(np.isnan(angleparticles))]
            distparticles = (particles[0,:]-particles[1,:])**2+particles[2,:]**2
            
            anglehist.hist(angleparticles[0:index:10], weights=angleweights[0:index:10], bins=bins, normed=True, label='mM '+extrapolate, alpha=0.4)
            anglemean.plot(times[0:index:10], data[7,0:index:10], label='mM '+extrapolate+' factor ='+str(factor))
            anglevar.plot(times[0:index:10], data[8,0:index:10], label='mM '+extrapolate+' factor ='+str(factor))
            distancehist.hist(distparticles[0:index:10], weights=angleweights[0:index:10], bins=bins, normed=True, label='mM '+extrapolate+' factor ='+str(factor), alpha=0.4)
            distancemean.plot(times[0:index:10], data[9,0:index:10], label='mM '+extrapolate+' factor ='+str(factor))
            distancevar.plot(times[0:index:10], data[10,0:index:10], label='mM '+extrapolate+' factor ='+str(factor))
        else:
            print('This type of data file is not supported.')   
            
    anglehist.legend()
    anglemean.legend()
    anglevar.legend()
    distancehist.legend()
    distancemean.legend()
    distancevar.legend()
    
    if save:
        plt.figure(fig1.number)
        tikz_save('plots/thetahistogram.tex')
        plt.figure(fig2.number)
        tikz_save('plots/thetamean.tex')
        plt.figure(fig3.number)
        tikz_save('plots/thetavariance.tex')
        plt.figure(fig4.number)
        tikz_save('plots/distancehistogram.tex')
        plt.figure(fig5.number)
        tikz_save('plots/distancemean.tex')
        plt.figure(fig6.number)
        tikz_save('plots/distancevariance.tex')
        
    plt.show()

def getList(args):
    if type(args) is list:
        return args
    else:
        return [args] 
        
            
if __name__ == '__main__':
    args = parseArguments()
    tend = float(args.tend)
    if args.save == '0':
        save = False
    else:
        save = True
        
    if args.plot == 'plotSolutions':
        plotSolutions(getList(args.files), tend, save)