import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

import argparse
import os
import fnmatch

def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Choose which experiment to perform.')
    parser.add_argument('--run', metavar='r', nargs='?', dest='run', default=None,
                        help='Choose which experiment to perform.')
    parser.add_argument('--beta', metavar='b', nargs='?', dest='beta', type=float,
                        help='Choose the inverse temperature.')
    parser.add_argument('--k', metavar='k', nargs='?', dest='k', type=float,
                        help='Choose the stiffness of the angle between the two end atoms.')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float, default=None,
                        help='Choose the time scale separation. Default is 0.1.')
    
    args = parser.parse_args()
    return args

################################################################
#                      File functions                          #
################################################################
def getRunFiles(eps, beta, k):
    files = []
    for file in os.listdir('./data/distancemacro/'):
        if fnmatch.fnmatch(file, '*eps='+str(eps)+'beta='+str(beta)+'k='+str(k)+'*run*'):
            files.append('data/distancemacro/'+file)
            
    return files

################################################################
#                    Computation functions                     #
################################################################
# A monte carlo integrator for intractable 3D integrals
def integrateDistanceSurface(integrand, z, N):
    if z == 0.:
        return 0.
    
    print('Sampling')
    xas = np.random.uniform(-5., 5., N)
    angles = np.random.uniform(0., np.pi, N)
    xcs = xas + np.sqrt(z)*np.cos(angles)
    ycs = np.sqrt(z)*np.sin(angles)
    
    print('Computing integral')
    sum = 0.
    for i in range(N):
        sum += integrand(np.array([xas[i], xcs[i], ycs[i]]))
    sum = sum/N
    
    return sum

# Generate the array of data points for b(z) for the given parameter set
def generateMacroData(eps, beta, ktheta, run):
    leq = 1.
    tsaddle = np.pi/2.
    dtheta = tsaddle - 1.187
    theta = lambda x: np.arccos(x[1]/np.sqrt(x[1]**2+x[2]**2))
    dthetadxc = lambda x: -x[2]/(x[1]**2+x[2]**2)
    dthetadyc = lambda x: x[1]/(x[1]**2+x[2]**2)
    W = lambda x: ktheta/2*((theta(x)-tsaddle)**2 - dtheta**2)**2
    dW = lambda x: 2.*ktheta*((theta(x)-tsaddle)**2-dtheta**2)*(theta(x)-tsaddle)
    rc = lambda x: np.sqrt(x[1]**2+x[2]**2)
    drcdxc = lambda x: x[1]/np.sqrt(x[1]**2+x[2]**2)
    drcdyc = lambda x: x[2]/np.sqrt(x[1]**2+x[2]**2)
    dVxa = lambda x: (x[0]-leq)/eps
    dVxc = lambda x: (np.sqrt(x[1]**2+x[2]**2)-leq)/eps*drcdxc(x) + dW(x)*dthetadxc(x)
    dVyc = lambda x: (np.sqrt(x[1]**2+x[2]**2)-leq)/eps*drcdyc(x) + dW(x)*dthetadyc(x)    
    dV = lambda x: np.array([dVxa(x), dVxc(x), dVyc(x)])
    V = lambda x: (x[0]-leq)**2/(2.*eps) + (rc(x)-leq)**2/(2.*eps) + W(x)
    
    # Define the reaction coordinate
    xi = lambda x: (x[0]-x[1])**2 + x[2]**2
    gradxi = lambda x: np.array([2*(x[0]-x[1]), 2*(x[1]-x[0]), 2*x[2]])
    laplacian = lambda x: 6.
    
    rho = lambda x: np.exp(-V(x))
    bintegrand = lambda x: (-np.dot(gradxi(x), dV(x)) + laplacian(x)/beta)*rho(x)
    sigmaintegrand = lambda x: np.linalg.norm(gradxi(x))**2*rho(x)
    
    zvalues = np.linspace(0., 4., 100)
    bvalues = []
    sigmavalues = []
    N = 10**6
    for z in zvalues:
        print('z = ', z)
        Z = integrateDistanceSurface(rho, z, N)
        if Z == 0.:
            bvalues.append(0.)
            sigmavalues.append(0.)
            continue
        
        b = integrateDistanceSurface(bintegrand, z, N)
        s = integrateDistanceSurface(sigmaintegrand, z, N)    
        bvalues.append(b/Z)
        sigmavalues.append(np.sqrt(s/Z))
        
    np.savetxt('data/distancemacro/distancemacrodataeps='+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'N='+str(N), np.vstack((zvalues,bvalues,sigmavalues)))

# Average the results over all runs and store it into a new file
def averageRuns(eps, beta, k):
    files = getRunFiles(eps, beta, k)
    
    zvalues = None
    bvalues = None
    sigmavalues = None
    number = 0.
    for file in files:
        data = np.loadtxt(file)
        z = data[0,:]
        bval = data[1,:]
        sval = data[2,:]
        
        if zvalues is None:
            zvalues = np.copy(z)
            bvalues = np.copy(bval)
            sigmavalues = np.copy(sval)
            number = 1.
        else:
            bvalues = (number*bvalues + bval)/(number+1.)
            sigmavalues = (number*sigmavalues + sval)/(number+1.)
            number += 1.
    
    bvalues[0] = bvalues[1]
    sigmavalues[0] = sigmavalues[1]      
    np.savetxt('data/distancemacro/distancemacrodataeps='+str(eps)+'beta='+str(beta)+'k='+str(k)+'.data', np.vstack((zvalues,bvalues,sigmavalues)))
    
    fig1 = plt.figure()
    plt.plot(zvalues, bvalues, label='b(z)')
    plt.plot(zvalues, np.zeros_like(zvalues), color='k')
    plt.xlabel(r"$z$")
    plt.ylabel(r"$b(z)$")
    plt.legend()
    tikz_save('plots/b(z)macrodistance.tex')
    fig2 = plt.figure()
    plt.plot(zvalues, sigmavalues, label=r'$\sigma(z)$')
    plt.xlabel(r"$z$")
    plt.ylabel(r"$\sigma(z)$")
    plt.legend()
    tikz_save('plots/sigma(z)macrodistance.tex')
    plt.show()
    
    
if __name__ == '__main__':
    args = parseArguments()
    eps = float(args.epsilon)
    beta = float(args.beta)
    k = float(args.k)
    
    if args.experiment == 'generateMacroData':
        generateMacroData(eps, beta, k, int(args.run))
    elif args.experiment == 'averageRuns':
        averageRuns(eps, beta, k)