import sys
sys.path.append("../../")

from Samples.potential.potential_model import *
from interpolation_model import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.euler_maryama import *
from tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy.random as rd
import argparse

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Choose which experiment to perform.')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float, default=None,
                        help='Choose the time scale separation. Default is 0.1.')
    parser.add_argument('--dtmax', metavar='dtmax', nargs='?', dest='factor', type=float, default=None,
                        help='Choose the (maximal) time step. Exact meaning depends on the experiment.')
    parser.add_argument('--beta', metavar='b', nargs='?', dest='beta', type=float,
                        help='Choose the inverse temperature.')
    parser.add_argument('--k', metavar='k', nargs='?', dest='k', type=float,
                        help='Choose the stiffness of the angle between the two end atoms.')
    parser.add_argument('--Lx', metavar='lx', nargs='?', dest='Lx', type=float, default=2,
                        help='Choose the number of coarse moments.')
    parser.add_argument('--Ly', metavar='ly', nargs='?', dest='Ly', type=float, default=0,
                        help='Choose the number of microscopic moments')
    parser.add_argument('--dt', metavar='dt', nargs='?', dest='dt', type=float, default=None,
                        help='Choose the number of microscopic moments')
    parser.add_argument('--extrapolate', nargs='?', dest='extrapolate', default=None,
                        help='Choose which function to extrapolate during micro-macro acceleration.')
    parser.add_argument('--factor', nargs='?', dest='factor', type=float, default=1. ,
                        help='Choose the extrapolation factor during mM acceleration.')
    parser.add_argument('--storeresult', nargs='?', dest='store', type=bool, default=True ,
                        help='Choose whether to store the simulation results.')
    
    args = parser.parse_args()
    return args

# Define theYc moment functions
def momentFunctions(Lxa, Lxc, Lyc):
    RLxa = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lxa+1))))
    RLxc = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Lxc+1))))
    RLyc = lambda X: np.asarray(list(map(lambda p: X[2]**(p), range(1, Lyc+1))))
    R = lambda X: np.append(np.append(np.append(np.asarray([1.]), RLxa(X)), RLxc(X)), RLyc(X))
    
    return R

# Define some angle and distance callback functions
def callbackFunctions(x, weights):
    thetamean = np.average(np.arccos(x[:,1]/np.sqrt(x[:,1]**2+x[:,2]**2)), weights=weights)
    thetavar = np.average((np.arccos(x[:,1]/np.sqrt(x[:,1]**2+x[:,2]**2)) - thetamean)**2, weights=weights)
    distancemean = np.average((x[:,1]-x[:,0])**2+x[:,2]**2, weights=weights)
    distancevar = np.average(((x[:,1]-x[:,0])**2+x[:,2]**2 - distancemean)**2, weights=weights)

    return np.array([thetamean, thetavar, distancemean, distancevar])

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanXa = vector[:,1]
    moment2Xa = vector[:,2]
    vector[:,2] = moment2Xa - meanXa*meanXa
    meanXc = vector[:,3]
    moment2Xc = vector[:,4]
    vector[:,4] = moment2Xc - meanXc*meanXc
    meanYc = vector[:,5]
    moment2Yc = vector[:,6]
    vector[:,6] = moment2Yc - meanYc*meanYc
    
    return vector   

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

#####################################################################
#                Define several micro and macro models              #
#####################################################################
def getTriAtomModel(eps, beta, ktheta):
    leq = 1.
    tsaddle = np.pi/2.
    dtheta = tsaddle - 1.187
    theta = lambda x: np.arccos(x[1]/np.sqrt(x[1]**2+x[2]**2))
    dW = lambda x: 2.*ktheta*((theta(x)-tsaddle)**2-dtheta**2)*(theta(x)-tsaddle)
    drcdxc = lambda x: x[1]/np.sqrt(x[1]**2+x[2]**2)
    drcdyc = lambda x: x[2]/np.sqrt(x[1]**2+x[2]**2)
    dthetadxc = lambda x: -x[2]/(x[1]**2+x[2]**2)
    dthetadyc = lambda x: x[1]/(x[1]**2+x[2]**2)
    dVxa = lambda x: (x[0]-leq)/eps
    dVxc = lambda x: (np.sqrt(x[1]**2+x[2]**2)-leq)/eps*drcdxc(x) + dW(x)*dthetadxc(x)
    dVyc = lambda x: (np.sqrt(x[1]**2+x[2]**2)-leq)/eps*drcdyc(x) + dW(x)*dthetadyc(x)    
    dV = lambda x: np.array([dVxa(x), dVxc(x), dVyc(x)])

    # The real potential functions
    dV = lambda x: np.array([dVxa(x), dVxc(x), dVyc(x)])
    A = lambda x: np.sqrt(2./beta)*np.ones(3)
    
    return PotentialModel(dV, A)

# Return the macroscopic model based on reaction coordinate theta    
def getThetaTriAtomModel(eps, beta, ktheta):
    leq = 1.
    tsaddle = np.pi/2.
    dtheta = tsaddle - 1.187
    dW = lambda z: 2.*ktheta*((z-tsaddle)**2-dtheta**2)*(z-tsaddle)
    A = lambda z: np.sqrt(2./beta)
    
    return PotentialModel(dW, A)

# Return the macroscopic model based on reaction coordinate (A - C)**2
def getDistanceTriAtomModel(eps, beta, ktheta):
    # Define all model expressions
    data = np.loadtxt('data/distancemacro/distancemacrodataeps='+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'.data')
    zvalues = data[0,:]
    bvalues = data[1,:]
    sigmavalues = data[2,:]
        
    return InterpolationModel(zvalues, bvalues, sigmavalues)

#####################################################################
#                    Several simulation functions                   #
#####################################################################    
# Perform a full-scale microscopic simulation
def microscopicSimulation(model, dt, tend):
    cb = lambda x: computeMoments(x, momentFunctions(2,2,2))
    N = 10**4
    X = np.zeros([N,3])
    X[:,0] = np.random.uniform(0.4, 0.5, N)
    anglesamples = np.random.uniform(1.0, 1.3, N)
    X[:,1] = 2*np.cos(anglesamples)
    X[:,2] = 2*np.sin(anglesamples)
    X, times, values, particles = model.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb, storeParticles=True)
    
    return X, times, values, particles

# Perform a simulation of the macroscopic model
def thetaMacroscopicSimulation(avgmodel, dt, tend):
    cb = lambda x: np.array([np.mean(x), np.mean(x*x)-np.mean(x)**2])
    N = 10**4
    X = np.random.uniform(1.0, 1.3, N)
    X, times, values = avgmodel.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb)
    
    return X, times, values

# perform a simulation of the macroscopic distance model
def distanceMacroscopicSimulation(model, dt, tend):
    cb = lambda x: np.array([np.mean(x), np.mean(x*x)-np.mean(x)**2])
    N = 10**4
    thetas = np.random.uniform(1.0, 1.3, N)
    xas = np.random.uniform(0.4, 0.5, N)
    xcs = 2*np.cos(thetas)
    ycs = 2*np.sin(thetas)
    zs = (xas-xcs)**2 + ycs**2
    X, times, values = model.fullScaleMicroSimulation(zs, 0., tend, dt, callback=cb)
    
    return X, times, values

# Matching acceleration algorithm
def match(model, tend, dt, dtmax, extrapolate):
    # Initial condition
    N = 10**4
    X = np.zeros([N,3])
    X[:,0] = np.random.uniform(0.4, 0.5, N)
    anglesamples = np.random.uniform(1.0, 1.3, N)
    X[:,1] = 2*np.cos(anglesamples)
    X[:,2] = 2*np.sin(anglesamples)
    
    if extrapolate == 'angle':
        R = lambda x: np.array([1., np.arccos(x[1]/np.sqrt(x[1]**2+x[2]**2))])
    elif extrapolate == 'distance':
        R = lambda x: np.array([1., (x[1]-x[0])**2+x[2]**2])
    else:
        moments = extrapolate.split(',',2)
        R = momentFunctions(int(moments[0]), int(moments[1]), int(moments[2]))
    Rplot = momentFunctions(2, 2, 2)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(R)
    params = {'K': 1, 'Dtmin': dt, 'Dtmax': dtmax, 'Dt0':dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    
    callback = lambda X, w: np.append(computeMoments(points=X, R=Rplot, weights=w), callbackFunctions(X, w))
    cpiresult = cpi.simulate(X, np.ones(N), 0., tend, dt, callback, storeParticles=False)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues'] #, cpiresult['particleHistory'], cpiresult['weightHistory']

# Perform the experiments
def performExperiments(type, eps, beta, ktheta, dt, tend, factor=None, extrapolate=None, store=False):
    fig1 = plt.figure()
    ax1xa = fig1.add_subplot(311)
    plt.title('Histogram xa')
    ax1xc = fig1.add_subplot(312)
    plt.title('xc')
    ax1yc = fig1.add_subplot(313)
    plt.title('yc')
    fig2 = plt.figure()
    ax2xa = fig2.add_subplot(311)
    plt.title('Mean Eps = '+str(eps) + 'beta = '+str(beta)+'k='+str(ktheta)+' xa')
    ax2xc = fig2.add_subplot(312)
    plt.title('xc')
    ax2yc = fig2.add_subplot(313)
    plt.title('yc')
    fig3 = plt.figure()
    ax3xa = fig3.add_subplot(311)
    plt.title('Variance Eps = '+str(eps) + 'beta = '+str(beta)+'k='+str(ktheta)+' xa')
    ax3xc = fig3.add_subplot(312)
    plt.title('xc')
    ax3yc = fig3.add_subplot(313)
    plt.title('yc')
    fig4 = plt.figure()
    axthetahist = fig4.add_subplot(311)
    plt.title('Theta histogram')
    axthetamean = fig4.add_subplot(312)
    plt.title('Mean')
    axthetavar = fig4.add_subplot(313)
    plt.title('Variance')
    fig5 = plt.figure()
    axdisthist = fig5.add_subplot(311)
    plt.title('(A-C)^2 histogram')
    axdistmean = fig5.add_subplot(312)
    plt.title('Mean')
    axdistvar = fig5.add_subplot(313)
    plt.title('Variance')
    plt.xlabel('Time [s]')
    store = False
    
    if type == 'micro':
        print('Micro Simulation dt = '+str(dt))
        model = getTriAtomModel(eps, beta, ktheta)
        particles, times, values, particlehistory = microscopicSimulation(model, dt, tend)
        values = getMeanAndVariance(values)

        # Precomputations to compute theta(x_c, y_c)
        angleparticles = np.arccos(particles[:,1]/np.sqrt(particles[:,1]**2+particles[:,2]**2))
        angleparticles = angleparticles[np.logical_not(np.isnan(angleparticles))]
        distparticles = (particles[:,0]-particles[:,1])**2+particles[:,2]**2
        thetameans = []
        thetavariances = []
        distmeans = []
        distvariances = []
        for particlelist in particlehistory:
            thetaparticles = np.arccos(particlelist[:,1]/(particlelist[:,1]**2+particlelist[:,2]**2))
            thetaparticles = thetaparticles[np.logical_not(np.isnan(thetaparticles))]
            thetameans.append(np.mean(thetaparticles, axis=0))
            thetavariances.append(np.var(thetaparticles, axis=0))
            dparticles = (particlelist[:,0]-particlelist[:,1])**2+particlelist[:,2]**2
            distmeans.append(np.mean(dparticles, axis=0))
            distvariances.append(np.var(dparticles, axis=0))
            
        # Store the simulated data
        if store:
            np.savetxt('data/micromodeleps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', np.vstack((times, values[:,1], values[:,2], values[:,3], values[:,4], np.array(thetameans), np.array(thetavariances), np.array(distmeans), np.array(distvariances))))
            np.savetxt('data/microparticleseps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', np.vstack((particles[:,0], particles[:,1], particles[:,2])))

        # Make a few compelling plots
        ax1xa.hist(particles[:,0], bins=100, normed=True, label='Microscopic slow dt='+str(dt))
        ax1xc.hist(particles[:,1], bins=100, normed=True, label='Microscopic fast dt='+str(dt))
        ax1yc.hist(particles[:,2], bins=100, normed=True, label='Microscopic fast dt='+str(dt))
        ax2xa.plot(times, values[:,1], label='Microscopic slow dt='+str(dt))
        ax2xc.plot(times, values[:,3], label='Microscopic slow dt='+str(dt))
        ax2yc.plot(times, values[:,5], label='Microscopic slow dt='+str(dt))
        ax3xa.plot(times, values[:,2], label='Microscopic slow dt='+str(dt))
        ax3xc.plot(times, values[:,4], label='Microscopic slow dt='+str(dt))
        ax3yc.plot(times, values[:,6], label='Microscopic slow dt='+str(dt))
        axthetamean.plot(times, thetameans, label='Microscopic slow dt='+str(dt))
        axthetavar.plot(times, thetavariances, label='Microscopic slow dt='+str(dt))
        axthetahist.hist(angleparticles, bins=100, normed=True, label='Microscopic slow dt='+str(dt))
        axdisthist.hist(distparticles, bins=100, normed=True, label='Microscopic slow dt='+str(dt))
        axdistmean.plot(times, distmeans, label='Microscopic slow dt='+str(dt))
        axdistvar.plot(times, distvariances, label='Microscopic slow dt='+str(dt))
    elif type == 'macrotheta':
        thetamodel = getThetaTriAtomModel(eps, beta, ktheta)
        avgparticles, avgtimes, avgvalues= thetaMacroscopicSimulation(thetamodel, dt, tend)

        # Store the data in a permanent file
        if store:
            np.savetxt('data/macroanglemodeleps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', np.vstack((avgtimes, avgvalues[:,0], avgvalues[:,1])))
            np.savetxt('data/macroangleparticleseps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', avgparticles)
        
        # Make compelling plots based on the macro model
        axthetahist.hist(avgparticles, bins=100, normed=True, label='Macroscopic Angle')
        axthetamean.plot(avgtimes, avgvalues[:,0], label='Averaged dt='+str(dt))
        axthetavar.plot(avgtimes, avgvalues[:,1], label='Averaged dt='+str(dt))
    elif type == 'macrodistance':
        print('macrodistance')
        distancemodel = getDistanceTriAtomModel(eps, beta, ktheta)
        avgparticles, avgtimes, avgvalues = distanceMacroscopicSimulation(distancemodel, dt, tend)
        
        # Store the data in a permanent file
        if store:
            np.savetxt('data/macrodistancemodeleps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', np.vstack((avgtimes, avgvalues[:,0], avgvalues[:,1])))
            np.savetxt('data/macrodistanceparticleseps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'tend'+str(tend)+'initialfareq.data', avgparticles)
        
        # Make mean and variance plots of the reaction coordinate
        axdisthist.hist(avgparticles, bins=100, normed=True, label='Macroscopic Distance')
        axdistmean.plot(avgtimes, avgvalues[:,0], label='Averaged dt='+str(dt))
        axdistvar.plot(avgtimes, avgvalues[:,1], label='Averaged dt='+str(dt))
    elif type == 'mM':
        dtmax = factor*dt
        model = getTriAtomModel(eps, beta, ktheta)
        mmparticles, mmweights, cbtimes, cbvalues = match(model, tend, dt, dtmax, extrapolate)
        cbvalues = getMeanAndVariance(cbvalues)
        cbtimes, cbvalues = getRealPoints(cbtimes, cbvalues)

        # Post-simulation computations to compute the macro variables
        angleparticles = np.arccos(mmparticles[:,1]/np.sqrt(mmparticles[:,1]**2+mmparticles[:,2]**2))
        indices = np.logical_not(np.isnan(angleparticles))
        angleparticles = angleparticles[indices]
        angleweights = mmweights[indices]
        distparticles = (mmparticles[:,0]-mmparticles[:,1])**2 + mmparticles[:,2]**2
            
        # Store the mM data for further use
        if store:
            np.savetxt('data/mMmodeleps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'extrapolate'+extrapolate+'initialfareq.data', np.vstack((cbtimes, cbvalues[:,1], cbvalues[:,2], cbvalues[:,3], cbvalues[:,4], cbvalues[:,5], cbvalues[:,6], cbvalues[:,7], cbvalues[:,8], cbvalues[:,9], cbvalues[:,10])))
            np.savetxt('data/mMparticleseps'+str(eps)+'beta='+str(beta)+'k='+str(ktheta)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'extrapolate'+extrapolate+'initialfareq.data', np.vstack((mmparticles[:,0], mmparticles[:,1], mmparticles[:,2], mmweights)))
        
        # And make compelling plots again
        ax1xa.hist(mmparticles[:,0], weights=mmweights, bins=100, normed=True, label='mM')
        ax1xc.hist(mmparticles[:,1], weights=mmweights, bins=100, normed=True, label='mM')
        ax1yc.hist(mmparticles[:,2], weights=mmweights, bins=100, normed=True, label='mM')
        ax2xa.plot(cbtimes, cbvalues[:,1], label='mM '+extrapolate+'  dtmax={0:.4f}'.format(factor))
        ax2xc.plot(cbtimes, cbvalues[:,3], label='mM '+extrapolate+' dtmax={0:.4f}'.format(factor))
        ax2yc.plot(cbtimes, cbvalues[:,5], label='mM '+extrapolate+' dtmax={0:.4f}'.format(factor))
        ax3xa.plot(cbtimes, cbvalues[:,2], label='mM '+extrapolate+' dtmax={0:.4f}'.format(factor))
        ax3xc.plot(cbtimes, cbvalues[:,4], label='mM '+extrapolate+' dtmax={0:.4f}'.format(factor))
        ax3yc.plot(cbtimes, cbvalues[:,6], label='mM '+extrapolate+' dtmax={0:.4f}'.format(factor))
        axthetahist.hist(angleparticles, weights=angleweights, bins=100, normed=True, label='mM')
        axthetamean.plot(cbtimes, cbvalues[:,7], label='mM '+extrapolate)
        axthetavar.plot(cbtimes, cbvalues[:,8], label='mM '+extrapolate)
        axdisthist.hist(distparticles, weights=mmweights, bins=100, normed=True, label='mM '+extrapolate)
        axdistmean.plot(cbtimes, cbvalues[:,9], label='mM '+extrapolate)
        axdistvar.plot(cbtimes, cbvalues[:,10], label='mM '+extrapolate)
    else:
        print('This experiment is not supported.')
            
    ax1xa.legend()
    ax1xc.legend()
    ax1yc.legend()
    ax2xa.legend()
    ax2xc.legend()
    ax2yc.legend()
    ax3xa.legend()
    ax3xc.legend()
    ax3yc.legend()
    axthetahist.legend()
    axthetamean.legend()
    axthetavar.legend()
    axdisthist.legend()
    axdistmean.legend()
    axdistvar.legend()
    
    plt.show()
    

if __name__ == '__main__':
    args = parseArguments()
    if args.dt is not None:
        dt = float(args.dt)
    else:
        dt = float(args.epsilon)
    tend = 2.
    
    performExperiments(args.experiment, float(args.epsilon), float(args.beta), float(args.k), dt, tend, float(args.factor), args.extrapolate, bool(args.store))
