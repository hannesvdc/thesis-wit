import numpy as np

from timeintegrators.euler_maryama import *

class InterpolationModel:
    def __init__(self, zvalues, bvalues, sigmavalues):
        self.zvalues = zvalues
        self.bvalues = bvalues
        self.sigmavalues = sigmavalues
        
    def a(self, t, z):
        index = self.zvalues.searchsorted(z)
        
        # Interpolate the b-value
        if index < self.zvalues.size - 1:
            bleft = self.bvalues[index]
            bright = self.bvalues[index+1]
            zleft = self.zvalues[index]
            zright = self.zvalues[index+1]
            return bleft + (bright-bleft)*(z-zleft)/(zright-zleft)
        # Linear extrapolation
        else:
            bleft = self.bvalues[index-1]
            bright = self.bvalues[index]
            zleft = self.zvalues[index-1]
            zright = self.zvalues[index]
            
            return bleft + (bright-bleft)*(z-zleft)/(zright-zleft)
        
    def b(self, t, z):
        index = self.zvalues.searchsorted(z)
        
        # Interpolate the b-value
        if index < self.zvalues.size - 1:
            sigmaleft = self.sigmavalues[index]
            sigmaright = self.sigmavalues[index+1]
            zleft = self.zvalues[index]
            zright = self.zvalues[index+1]
            return sigmaleft + (sigmaright-sigmaleft)*(z-zleft)/(zright-zleft)
        # Linear extrapolation
        else:
            sigmaleft = self.sigmavalues[index-1]
            sigmaright = self.sigmavalues[index]
            zleft = self.zvalues[index-1]
            zright = self.zvalues[index]
            
            return sigmaleft + (sigmaright-sigmaleft)*(z-zleft)/(zright-zleft)
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None, storeParticles=False):
        timestepper = EulerMaryamaIntegrator(self.a, self.b)
            
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        particles = [np.copy(X)]
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt
            
            if storeParticles:
                particles.append(np.copy(X))

        if storeParticles:
            return X, np.asarray(cbtimes), np.asarray(cbvalues), particles
        else:
            return X, np.asarray(cbtimes), np.array(cbvalues)