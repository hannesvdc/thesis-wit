import sys

import numpy as np

import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save


def getM(file):
    index = file.find('M=')
    index += 2
    endindex = file.find('runs', index)
    M = int(file[index:endindex])
    
    return M

def plotMeanAndVariance(mMfile, exactfile):
    # Get metadata of the files
    M = getM(mMfile)
    
    # Load the data
    mMdata = np.loadtxt(mMfile)
    exactdata = np.loadtxt(exactfile)
    mMtimes = mMdata[0,:]
    mMaveragedMeans = mMdata[1,:]
    mMvarianceMeans = mMdata[2,:]
    exactTimes = exactdata[0,:]
    exactMeans = exactdata[1,:]
    exactVariances = exactdata[2,:]
    analyticVariances = exactdata[3,:]
    print(mMdata.shape)
    # Make the plot
#     plt.figure()
#     plt.hist(mMmeans[:,-1], weights=np.ones_like(mMmeans[:,0]), bins=50, normed=True, label="mM", color='r',alpha = 0.5)
#     plt.hist(Y, weights=np.ones_like(Y), bins=50, normed=True, label="Macro", color='b',alpha = 0.5)
#     plt.title("Histogram M = "+str(M))
#     plt.legend()
    plt.figure()
    plt.plot(mMtimes, mMaveragedMeans, label="mM")
    plt.plot(exactTimes, exactMeans, label="Macro")
    plt.legend()
    plt.title("Average of means M = "+str(M))
    plt.figure()
    plt.plot(mMtimes, mMvarianceMeans, label="mM")
    plt.plot(exactTimes, exactVariances, label="macro")
    plt.plot(exactTimes, analyticVariances, label="Analytical macro variance")
    plt.plot(mMtimes[0::3], mMvarianceMeans[0::3], label="mM after matching")
    plt.legend()
    plt.title("Variance of means M = "+str(M))
    plt.show()
    
    
if __name__ == "__main__":
    files = sys.argv[1:]
    plotMeanAndVariance(files[0], files[1])