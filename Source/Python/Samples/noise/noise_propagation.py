import sys
sys.path.append("../../")

import numpy as np
import numpy.random as rd
import time
import argparse

import matplotlib.pyplot as plt

from library.timeintegrators.micromacro_cpi import *
from library.timeintegrators.euler_maryama import *
from library.matching.kld import *
from library.models.linear_model import *

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "micro", "mean" and "meanvariance".')
    parser.add_argument('--M', metavar='M', nargs='?', dest='M', type=int,
                        help='Choose the time number of particles.')
    parser.add_argument('--N', metavar='N', nargs='?', dest='N', type=int,
                        help='Choose the number of independent realizations of the mean process')
    parser.add_argument('--run', metavar='r', nargs='?', dest='run', type=int, default=0,
                        help='Choose the number of different runs.')
                        
    args = parser.parse_args()
    return args

# Run the possible experiments
def linearSDENoisePropagation(lam, sigma, M, N, number):
    verbose = False
    mean_init = 2.
    std_init = 1.
    mMmodel = LinearModel(np.array([[lam]]), np.array([[sigma]]))
    
    dt = 0.02
    Dt = 0.1
    ratio = Dt/dt
    tend = 2.
    
    R = lambda X: np.asarray(list(map(lambda p: X**p, range(0,2))))
    stepper = EulerMaruyamaIntegrator(mMmodel.a, mMmodel.b, verbose=verbose)
    kld = KLDMatchingStrategy(R, iterations=10, verbose=verbose)
    params = {'K': 1, 'Dtmin': Dt, 'Dtmax': Dt, 'alfadecrease': 1., 'alfaincrease': 1.}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    callback = lambda X, w: computeMoments(points=X, R=R, weights=w) 
    
    # Execute for 'runs' times
    mMmeans = []
    cbtimes = []
    current_milli_time = lambda: int(round(time.time() * 1000))

#    initial = rd.normal(mean_init, std_init, M)
    for i in range(0, N):
        print('Iteration ',i)
        initial = rd.normal(mean_init, std_init, M)
        mMresult = cpi.simulate(np.copy(initial), np.ones(M), 0., tend, dt, callback,verbose=verbose)
        cbtimes = mMresult['cbtimes']
        mMmeans.append(mMresult['cbvalues'][:,1])
    mMmeans = np.array(mMmeans)
    mMaveragedMeans = np.average(mMmeans,axis=0)
    mMvarianceMeans = np.var(mMmeans, axis=0)

    # Now run the approximate model for the mean
    model = LinearModel(np.array([[lam]]), np.array([[sigma*np.sqrt(ratio/M)]]))
    macroR = lambda X: np.asarray(list(map(lambda p: X**p, range(0,3))))
    rd.seed(current_milli_time()%(2**32))
    initial = rd.normal(mean_init, std_init/np.sqrt(M), N)
    macroCallback = lambda X: computeMoments(points=X, R=macroR, weights=np.ones_like(X))
    Y, times, callbackResult = model.fullScaleMicroSimulation(np.copy((initial-mean_init)/np.sqrt(M)+mean_init), 0., tend, Dt, callback=macroCallback)
    
    # Compute the exact variance of the macroscopic model of the mean
    current_var = np.var(initial)/M
    t = 0.
    exact_vars = []
    exact_vars.append(current_var)
    exact_times = []
    exact_times.append(t)
    while t + Dt < tend+0.01:
        t = t + Dt
        current_var = (1+lam*Dt)**2*current_var + sigma**2*(ratio/M)*Dt
        exact_vars.append(current_var)
        exact_times.append(t)
        
    print(times, callbackResult, np.square(callbackResult[:,1]))
    print(times, exact_vars)
    
    if number == 0:
        mMfilename = 'data/mMnoise_lam='+str(lam)+"sig="+str(sigma)+"M="+str(M)+"N="+str(N)+".txt"
    else:
        mMfilename = 'data/mMnoise_lam='+str(lam)+"sig="+str(sigma)+"M="+str(M)+"N="+str(N)+"number="+str(number)+".txt"
    np.savetxt(mMfilename, np.vstack((cbtimes, mMaveragedMeans, mMvarianceMeans)))
    np.savetxt('data/mMnoise_lam='+str(lam)+"sig="+str(sigma)+"M="+str(M)+"N="+str(N)+".txt", np.vstack((times, np.array(callbackResult[:,1]), np.array(callbackResult[:,2])-np.square(callbackResult[:,1]), np.array(exact_vars))))

#     plt.figure()
#     plt.hist(mMmeans[:,-1], weights=np.ones_like(mMmeans[:,-1]), bins=50, normed=True, label="mM", color='r',alpha = 0.5)
#     plt.hist(Y, weights=np.ones_like(Y), bins=50, normed=True, label="Macro", color='b',alpha = 0.5)
#     plt.title("Histogram M = "+str(M))
#     plt.legend()
#     plt.figure()
#     plt.plot(cbtimes, mMaveragedMeans, label="mM")
#     plt.plot(times, callbackResult[:,1], label="Macro")
#     plt.legend()
#     plt.title("Average of means M = "+str(M))
#     plt.figure()
#     plt.plot(cbtimes, mMvarianceMeans, label="mM")
#     plt.plot(times, np.array(callbackResult[:,2])-np.square(callbackResult[:,1]), label="macro")
#     plt.plot(exact_times, exact_vars, label="Analytical macro variance")
#     plt.plot(cbtimes[0::3], mMvarianceMeans[0::3], label="mM after matching")
#     plt.legend()
#     plt.title("Variance of means M = "+str(M))
#     plt.show()


def matchingWithFiniteSamples(lam, sigma, M):
    verbose = True
    mean_init = 2.
    std_init = 1.
    mMmodel = LinearModel(np.array([[lam]]), np.array([[sigma]]))
    
    dt = 0.02
    Dt = 0.1
    ratio = Dt/dt
    tend = 2.
    
    R = lambda X: np.asarray(list(map(lambda p: X**p, range(0,2))))
    callbackR = lambda X: np.asarray(list(map(lambda p: X**p, range(0,3))))
    stepper = EulerMaruyamaIntegrator(mMmodel.a, mMmodel.b, verbose=verbose)
    kld = KLDMatchingStrategy(R, verbose=verbose)
    params = {'K': 1, 'Dtmin': Dt, 'Dtmax': Dt, 'alfadecrease': 1., 'alfaincrease': 1.}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    callback = lambda X, w: computeMoments(points=X, R=callbackR, weights=w)
    
    current_milli_time = lambda: int(round(time.time() * 1000))
    rd.seed(current_milli_time()%(2**32))
    initial = rd.normal(mean_init, std_init, M)
    mMresult = cpi.simulate(np.copy(initial), np.ones(M), 0., tend, dt, callback,verbose=verbose)
    cbtimes = mMresult['cbtimes']
    mMmeans = np.array(mMresult['cbvalues'][:,1])
    mMseconds = np.array(mMresult['cbvalues'][:,2])
    
    newtimes = np.delete(cbtimes, np.s_[2::3])
    newMeans = np.delete(mMmeans, np.s_[2::3], 0)
    newSeconds = np.delete(mMseconds, np.s_[2::3], 0)
    
    plt.plot(cbtimes, mMmeans, label="mM mean")
    plt.plot(cbtimes, mMseconds - mMmeans**2, label="mM variance")
    plt.plot(newtimes, newSeconds - newMeans**2, 'X')
    plt.legend()
    plt.show()
    
# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result
        
if __name__ == "__main__":
    args = parseArguments()
    
    if args.experiment == 'linearSDENoisePropagation':
        linearSDENoisePropagation(-4., 5., int(args.M), int(args.N), int(args.run))    