import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib2tikz import save as tikz_save

from periodic import *

import argparse
import fnmatch
import os
import bisect

def parseArguments():
    parser = argparse.ArgumentParser(description='Input parameters for plotting on the periodic model.')
    parser.add_argument('--plot', metavar='p', nargs='?', dest='plot',
                        help='Select which the type of plot.')
    parser.add_argument('--eps', metavar='e', nargs='?',dest='eps', 
                        help='Select the epsilon value to make the plots for.')
    parser.add_argument('--epses', metavar='es', nargs='+', dest='epses',
                        help='Select the epsilons to make the crossover curves.')
    parser.add_argument('--factors', metavar='fs', nargs='+', dest='factors',
                        help='Select the extrapolation factors to make the crossover curves.')
    parser.add_argument('--curves', metavar='c', nargs='+', dest='curves',
                        help='Select what data to display in the plots.')
    parser.add_argument('--p', metavar='p', nargs='?', dest='period', default=1.,
                        help='Select the period of the sinusoidal.')
    
    args = parser.parse_args()
    return args

##############################################################
#              Several file helper functions                 #
##############################################################
def getMMFiles(period):
    files = []
    for file in os.listdir('./data/'):
        if fnmatch.fnmatch(file, 'mMmodel*') and fnmatch.fnmatch(file,'*p='+str(period)+'*'):
            print(file)
            files.append('data/'+file)
            
    return files

def getMMFilesWithEps(eps, period, averaged):
    files = []
    for file in os.listdir('./data/'):
        if averaged:
            if fnmatch.fnmatch(file, 'mMmodeleps='+str(eps)+'*p='+str(period)+'*run*'):
                files.append('data/'+file)
        
        else:
            if fnmatch.fnmatch(file, 'mM*eps='+str(eps)+'*p='+str(period)+'*') and 'run' not in file:
                files.append('data/'+file)
                
    return files

def getMMFilesEpsFactor(eps, factor, period, averaged):
    files = []
    for file in os.listdir('./data/'):
        if averaged:
            if fnmatch.fnmatch(file, 'mM*eps='+str(eps)+'*factor='+str(factor)+'p='+str(period)+'*run*'):
                files.append('data/'+file)
        else:
            if fnmatch.fnmatch(file, 'mM*eps='+str(eps)+'*factor='+str(factor)+'*p='+str(period)+'*'):
                files.append('data/'+file)
    
    return files

def getMacroFile():
    for file in os.listdir('./data/'):
        if fnmatch.fnmatch(file, 'macromodel*'):
            return 'data/'+file
    
    return None
        
def getEpsilonFromFileName(file):
    index = file.find('eps')
    index += 4
    endindex = file.find('lam', index)
    eps = float(file[index:endindex])
    
    return eps

def getFactorFromFileName(file):
    index = file.find('factor')
    index += 7
    factorindex = file.find('p=', index)
    factor = float(file[index:factorindex])
    
    return factor

def getLambdaFromFileName(file):
    index = file.find('lam=')
    index += 4
    lamindex = file.find('c=', index)
    lam = float(file[index:lamindex])
    
    return lam

def getCFromFileName(file):
    index = file.find('c=')
    index += 2
    cindex = file.find('dt=', index)
    c = float(file[index:cindex])
    
    return c

def getDtFromFileName(file):
    index = file.find('dt')
    index += 3
    dtindex = file.find('tend', index)
    dt = float(file[index:dtindex])
    
    return dt

# Make an appealing label from the given file name.
def getLabel(filename):
    filename = filename.replace("data/", "")
    filename = filename.replace(".data", "")
    filename = filename.replace("model", " ")
    return filename

# Get the type of data file
def getType(file):
    if "micro" in file:
        return "microscopic"
    elif "macro" in file:
        return "macroscopic"
    elif "mM" in file:
        return "micro-macro"
    else:
        return None


##############################################################
#              Several data helper functions                 #
##############################################################    
def getAnalytic(initial, eps, period,fast=False):
    a = 2.*pi/period
    lam = 2.
    c = 10.
    A = np.matrix([[0, -1./eps, -a, 1./eps],[-1./eps,0,1./eps,a],[-a,lam,0,lam],[lam,a,lam,0]])
    B = np.array([0., 0., c, 0.])
    modelmatrix = np.matrix([[-1./eps, 1./eps],[-lam, -lam]])
    coef = np.linalg.solve(A, B)
    
    slowanalytic = lambda t: (scipy.linalg.expm(t*modelmatrix).dot(initial-np.array([coef[2], coef[0]])))[1]+ coef[0]*cos(a*t) + coef[1]*sin(a*t)
    if not fast:
        return slowanalytic
    else:
        fastanalytic = lambda t: (scipy.linalg.expm(t*modelmatrix).dot(initial-np.array([coef[2], coef[0]])))[0]+ coef[2]*cos(a*t) + coef[3]*sin(a*t)
        return slowanalytic, fastanalytic
    
def getMacroAnalytic(initial, period):
    a = 2.*pi/period
    lam = 2.
    c = 10.
    A = c/(a**2/(2.*lam)+2.*lam)
    B = -a*c/(a**2. + 4.*lam**2)
    analytic = lambda t: (initial[1]-B)*np.exp(-2.*lam*t) + A*sin(a*t) + B*cos(a*t)
    
    return analytic

def getMacroError(times, values, initial, eps, period=1.):
    analytic = getAnalytic(initial, eps, period)
    l2error = L2error(analytic, times, values, period)
    maxerror = Maxerror(analytic, times, values, period)
    
    return l2error, maxerror

def getInitial(eps, lam, c, p):
    tbegin = 0.
    a = 2.*np.pi/p
    A = np.matrix([[0, -1./eps, -a, 1./eps],[-1./eps,0,1./eps,a],[-a,lam,0,lam],[lam,a,lam,0]])
    B = np.array([0., 0., c, 0.])
    coef = np.linalg.solve(A, B)    
    Z0 = np.array([coef[2]*cos(a*tbegin) + coef[3]*sin(a*tbegin), coef[0]*cos(a*tbegin) + coef[1]*sin(a*tbegin)])
    
    return Z0

# Compute L2 error norm between analytical and numerical solution
def L2error(analytic, times, values, period):
    finaltime = times[-1]
    firsttime = finaltime - period
    error = 0.
    for tindex in range(len(times)-1):
        if times[tindex] < firsttime:
            continue
        err1 = (values[tindex] - analytic(times[tindex]))**2
        err2 = (values[tindex+1] - analytic(times[tindex+1]))**2
        error += 0.5*(times[tindex+1]-times[tindex])*(err1 + err2)
     
    return np.sqrt(error)

# Compute L_inf error between the analytical and numerical solution
def Maxerror(analytic, times, values, period):
    finaltime = times[-1]
    firsttime = finaltime - period
    error = 0.
    for tindex in range(len(times)):
        if times[tindex] < firsttime:
            continue
        err = np.abs(values[tindex] - analytic(times[tindex]))
        
        if err > error:
            error = err
     
    return error

###################################################################################    
###                           Different plot functions                          ###
###################################################################################
def plotSolutions(files):
    meanfig = plt.figure()
    meanax1 = meanfig.add_subplot(111)
#    meanax2 = meanfig.add_subplot(212)
    plt.xlabel('Time [s]')
#    plt.title('Mean Fast')
    
    for file in files:
        data = np.loadtxt(file)
        label = getType(file)
        if label == 'micro-macro':
            label = label + ' M = '+str(getFactorFromFileName(file)) 
            
        times = data[0,:]
        if getType(file) == 'macroscopic':
            meanax1.plot(times, data[1,:], label=label)
        else:
            meanax1.plot(times, data[2,:], label=label)
        
#        if getType(file) == 'micro' or getType(file) == 'mM':
#            meanax2.plot(times, data[1,:], label=label)
            
    meanax1.legend()
#    meanax2.legend()
    tikz_save('plots/means.tex')
    plt.show()

def plotErrors(files):
    meanfig = plt.figure()
    meanax1 = meanfig.add_subplot(211)
    plt.title('Mean Slow')
    meanax2 = meanfig.add_subplot(212)
    plt.xlabel('Time [s]')
    plt.title('Mean Fast')
    
    for file in files:
        data = np.loadtxt(file)
        label = getLabel(file)
        period = getPeriodFromFileName(file)
                    
        times = data[0,:]
        if getType(file) is "micro" or getType(file) is "mM":
            eps = getEpsilonFromFileName(file)
            slowanalytic, fastanalytic = getAnalytic(initial, eps, period, fast=True)
            slowerror = []
            fasterror = []
            for index in range(len(times)):
                slowerror.append(slowanalytic(times[index])-data[2,index])
                fasterror.append(fastanalytic(times[index])-data[1,index])
            
            meanax1.plot(times, slowerror, label=label)
            meanax2.plot(times, fasterror, label=label)
        else:
            slowanalytic = getMacroAnalytic(initial, period, fast=False)
            slowerror = []
            for index in range(len(times)):
                slowerror.append(slowanalytic(times[index])-data[1,index])
            meanax1.plot(times, slowerror, label=label)
            
    meanax1.legend()
    meanax2.legend()
    
    plt.show()

# 
# Plot the macroscopic error as a function of epsilon over one interval.
#
def plotMacroErrorEps(period):
    fig = plt.figure()
    plt.xlabel('Epsilon')
    plt.ylabel('L2 Error')
    
    epses = np.linspace(10**-7, 1., 1000)
    l2errors = []
    for eps in epses:
        print(eps)
        initial = getInitial(eps, 2, 10, period)
        slowanalytic = getAnalytic(initial, eps, period)
        macroanalytic = getMacroAnalytic(initial, period)
        errorfunction = lambda t: (slowanalytic(t) - macroanalytic(t))**2
        l2macroerror, quaderror = scipy.integrate.quad(errorfunction, 5.,6.)
        l2errors.append(np.sqrt(l2macroerror))
        
    plt.loglog(epses, l2errors, label='Averaged model')
    plt.loglog(epses, 6.*epses, label='First order')
    plt.legend()
    
    tikz_save('plots/macroerror.tex')
    plt.show()

#
# Make a plot of the mM error for a fixed eps and dt(eps) as a function of dtmax and factor.
# The parameter 'averaged' denotes whether the results need to be averged over the number
# of available independent runs of the experiment or not.
#
def plotMMErrorDtmax(eps, period, averaged=True):
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    plt.title('Epsilon = '+str(eps))  
    plt.xlabel('Dtmax')
    plt.ylabel('Error')       
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    plt.xlabel('Extrapolation Factor')
    plt.ylabel('Error')
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    
    filenames = getMMFilesWithEps(eps, period, averaged)
    
    dtmaxs = []
    factors = []
    l2errors = []
    numbers = []
    print(filenames)
    for file in filenames:
        data = np.loadtxt(file)
        times = data[0,:]
        valuesslow = data[2,:]
        valuesfast = data[1,:]

        lam = getLambdaFromFileName(file)
        c = getCFromFileName(file)
        initial = getInitial(eps, lam, c, period)
        l2error, maxerror = getMacroError(times, valuesslow, initial, eps, period)
        dt = getDtFromFileName(file)
        factor = getFactorFromFileName(file)
        
        try:
            index = factors.index(factor)
            l2errors[index] += l2error
            numbers[index] += 1.
        except ValueError:
            factors.append(factor)
            dtmaxs.append(factor*dt)
            l2errors.append(l2error)
            numbers.append(1.)
            
        print(dt, factor, l2error)
    
    for index in range(len(factors)):
        l2errors[index] = l2errors[index]/numbers[index]
        
    dtmaxs, l2errors = zip(*sorted(zip(dtmaxs, l2errors))) 
    ax1.loglog(dtmaxs, l2errors, label='L2', marker = 'x')
    ax1.loglog(np.array(dtmaxs), 5.*np.array(dtmaxs), label='First order')
#    ax1.loglog(np.array(dtmaxs), 3000.*np.array(dtmaxs)**2, label='Second order')
    ax2.loglog(factors, l2errors, label='L2', marker = 'x')
    
    ax1.legend()
    ax2.legend()
    
    plt.figure(fig1.number)
    tikz_save('plots/mmerroreps='+str(eps)+'.tex')
    plt.show()

def plotCrossoverCurves(epses, factors, period, averaged=True):
    mpl.rc('text', usetex=True)
    errors = np.zeros([len(epses), len(factors)])
    numbers = np.zeros([len(epses), len(factors)])
    alphas = np.zeros(len(epses))
    dts = np.zeros(len(epses))
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'tab:olive', 'tab:orange', 'tab:gray']
    
    for epsindex in range(len(epses)):
        for factorindex in range(len(factors)):
            files = getMMFilesEpsFactor(epses[epsindex], factors[factorindex], period, averaged)
            for file in files:
                print(file)
                dts[epsindex] = getDtFromFileName(file)
                lam = getLambdaFromFileName(file)
                c = getCFromFileName(file)
                factor = getFactorFromFileName(file)
                initial = getInitial(epses[epsindex], lam, c, period)
                data = np.loadtxt(file)
                times = data[0,:]
                valuesslow = data[2,:]
                l2error, maxerror = getMacroError(times, valuesslow, initial, epses[epsindex], period)
                
                errors[epsindex][factorindex] += l2error
                numbers[epsindex][factorindex] += 1.0
                print(errors, numbers)
                
    print(epses, factors, errors)  
    for epsindex in range(len(epses)):
        for factorindex in range(len(factors)):
            errors[epsindex][factorindex] /= numbers[epsindex][factorindex]
        alphas[epsindex] = (errors[epsindex][-1] - errors[epsindex][0])/((factors[-1]*dts[epsindex])**2 - (factors[0]*dts[epsindex])**2)
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    plt.xlabel('$M$')
    plt.ylabel(r'$L_2 \ \ \text{Error}$')
    crossoverFactors = []
    for index in range(len(epses)):
        initial = getInitial(epses[index], lam, c, period)
        ax1.semilogy(factors, errors[index,:], label=r"$\varepsilon="+str(epses[index])+'$', color=colors[index])
        slowanalytic = getAnalytic(initial, epses[index], period)
        macroanalytic = getMacroAnalytic(initial, period)
        errorfunction = lambda t: (slowanalytic(t) - macroanalytic(t))**2
        l2macroerror, quaderror = scipy.integrate.quad(errorfunction, 5.,6.)
        l2macroerror = np.sqrt(l2macroerror)
        
        # Determine the crossover point between mM and the macro model.
        Factorcross = factors[-1]
        Errcross = errors[index][-1]
        for k in range(len(errors[index])):
            if errors[index][k] < l2macroerror:
                continue
             
            if k == 0:
                Factorcross = factors[k]
                Errcross = errors[index][k]
                break
            else:
                # Linear interpolation
                Factorcross = factors[k-1] + (factors[k]-factors[k-1])*(l2macroerror-errors[index][k-1])/(errors[index][k]-errors[index][k-1])
                Errcross = l2macroerror
                break
        crossoverFactors.append(Factorcross)
        
        xaxis = np.linspace(1., 5., 10)
        ax1.semilogy(xaxis, l2macroerror*np.ones_like(xaxis), color=colors[index], Linestyle='--')
        ax1.semilogy(Factorcross, Errcross, marker='x', color=colors[index])
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.loglog(epses, crossoverFactors, color='b', marker='x', label='Max extrapolation factor')
#    ax2.loglog(np.array(epses), np.array(epses)**-0.5, color='g', label=r'$\frac{1}{\sqrt{\varepsilon}}}$')
    ax2.loglog(np.array(epses), np.array(epses)**-0.25, color='g', label=r'$\frac{1}{\varepsilon^{1/4}}$')
    plt.ylim(0.,5.)
    
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.loglog(epses, np.array(crossoverFactors)*np.array(epses)/20., color='b', marker='x', label='Max extrapolation step')  
#    ax3.loglog(epses, np.sqrt(np.array(epses))/100., color='g', label=r'$\sqrt{\varepsilon}$')
    ax3.loglog(epses, np.array(epses)**0.75/20., color='g', label=r'$\varepsilon^{0.75}$')
    plt.xlabel(r'$\varepsilon$')
    plt.ylabel(r'$\Delta t$')
      
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111)
    ax4.loglog(epses, alphas, color='b', marker='x', label='alpha(eps)')
    ax4.loglog(epses, 500.*np.array(epses)**-0.5, label='1/sqrt(eps)')
    
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    
    plt.figure(fig1.number)
    tikz_save('plots/crossovercurves.tex')
    plt.figure(fig2.number)
    tikz_save('plots/maximalM.tex')
    plt.figure(fig3.number)
    tikz_save('plots/maximalDt.tex')
    plt.show()


def getFloatList(args):
    if type(args) is list:
        retlist = []
        for l in args:
            retlist.append(float(l))
        return retlist
    else:
        return [float(args)] 
                            
def getList(args):
    if type(args) is list:
        return args
    else:
        return [args] 
        
if __name__ == '__main__':
    args = parseArguments()

    if args.plot == 'plotSolutions':
        plotSolutions(getList(args.curves))
    elif args.plot == 'plotErrors':
        plotErrors(getList(args.curves))
    elif args.plot == 'plotMacroErrorEps':
        plotMacroErrorEps(float(args.period))
    elif args.plot == 'plotMMErrorDtmax':
        plotMMErrorDtmax(float(args.eps), float(args.period))
    elif args.plot == "plotCrossoverCurves":
        plotCrossoverCurves(getFloatList(args.epses), getFloatList(args.factors), float(args.period))
    else:
        print("This plot type is not supported.")