import sys

sys.path.append("../../")

from periodic_model import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from tools.sample import *

import numpy.random as rd
import argparse

# Define the moment functions
def momentFunctions(Lx, Ly, cov=False):
    RLX = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    
    if cov:
        RXY = lambda X: np.asarray([X[0]*X[1]])
        Rcov = lambda X: np.append(R(X), RXY(X))
        return Rcov
        
    return R

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,3]
    moment2Y = vector[:,4]
    vector[:,4] = moment2Y - meanY*meanY
    vector[:,5] = vector[:,5] - meanX*meanY
    
    return vector

def find_nearest(array, value):
    idx = (np.abs(array-value)).argmin()
    return idx

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='type',
                        help='Choose which experiment to perform.')
    parser.add_argument('--store', metavar='s', nargs='?', dest='store', type=bool, default=True,
                        help='Store all the intermediate results or not.')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float, default=None,
                        help='Choose the time scale separation. Default is 0.1.')
    parser.add_argument('--dtmax', metavar='dtmax', nargs='?', dest='factor', type=float,
                        help='Choose the (maximal) time step. Exact meaning depends on the experiment.')
    parser.add_argument('--p', metavar='p', nargs='?', dest='period', type=float, default=1.,
                        help='Choose the period of the invariant solution.')
    parser.add_argument('--tend', metavar='end', nargs='?', dest='tend', type=float, default=6.,
                        help='Choose the duration of the simulation.') 
    parser.add_argument('--run', metavar='run', nargs='?', dest='run', type=float,
                        help='Choose the how many times the experiment should run.')

    args = parser.parse_args()
    return args

def slowfastmodel(eps, lam, c, a):
    return PeriodicModel(np.matrix([[-1./eps, 1./eps],[-lam, -lam]]), np.array([1./np.sqrt(eps), 1.]), np.array([0., c]), a=a), np.matrix([[-1./eps, 1./eps],[-lam, -lam]])

def averagedmodel(lam, c, a):
    return PeriodicModel(np.matrix([[-2.*lam]]), np.array([1.]), np.array([c]), a=a)

def match(model, dt, factor, tend, Lx, Ly, Z0):
    N = 10**4
    K = 1
    dtmax = factor*dt
    Rcb = lambda x, w: computeMoments(points=x, R=momentFunctions(2, 2, cov=True), weights=w)

    # And perform matching
    X0 = np.ones([N,2])
    X0[:,0] = Z0[0]*np.ones(N)
    X0[:,1] = Z0[1]*np.ones(N)
    R = momentFunctions(Lx, Ly)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(X0, R, computeMoments(X0, R), 'NR', 10)
    params = {'K': K, 'Dtmax': dtmax, 'Dt0': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(X0, np.ones(N), 0., tend, dt, [0,1], adaptive=True, callback = Rcb)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues'], cpiresult['matchfails'], cpiresult['matchfailtimes']

def simulateMicroModel(micromodel, dt, tend, Z0):
    # Simulation parameters
    N = 10**5
    
    # microscopic solution
    cb = lambda X: np.mean(X, axis=0)
    X0 = np.ones([N,2])
    X0[:,0] = Z0[0]*np.ones(N)
    X0[:,1] = Z0[1]*np.ones(N)
    X, microcbtimes, microcbvalues = micromodel.fullScaleMicroSimulation(X0, 0., tend, dt, callback=cb)
    
    return microcbtimes, microcbvalues

def simulateAveragedModel(avgmodel, dt, tend, Z0):
    # Simulation parameters
    N = 10**5

    # macroscopic simulation
    cb = lambda X: np.mean(X, axis=0)
    X0 = Z0[1]*np.ones(N)
    X, avgtimes, avgvalues = avgmodel.fullScaleMicroSimulation(X0, 0., tend, dt, callback=cb)

    return avgtimes, avgvalues

def performExperiment(type, eps, lam, c, p, tend, factor=None, run=None):
    # Simulation models and parameters
    a = 2.*np.pi/p
    micromodel, modelmatrix = slowfastmodel(eps, lam, c, a)
    avgmodel = averagedmodel(lam, c, a)
    dt = eps/20.
    tbegin = 0.
    
    # Initial condition
    A = np.matrix([[0, -1./eps, -a, 1./eps],[-1./eps,0,1./eps,a],[-a,lam,0,lam],[lam,a,lam,0]])
    B = np.array([0., 0., c, 0.])
    coef = np.linalg.solve(A, B)    
    Z0 = np.array([coef[2]*cos(a*tbegin) + coef[3]*sin(a*tbegin), coef[0]*cos(a*tbegin) + coef[1]*sin(a*tbegin)])
    print(Z0)
    if run is None:
        runstr = ""
    else:
        runstr = "run"+str(run)
        np.random.seed(1000*run) 
           
    if type == 'micro':
        times, values = simulateMicroModel(micromodel, dt, tend, Z0)
        np.savetxt('data/micromodeleps='+str(eps)+'lam='+str(lam)+'c='+str(c)+'dt='+str(dt)+'tend='+str(tend)+'p='+str(p)+'initialperiodic'+runstr+'.data', np.vstack((times, values[:,0], values[:,1]))) 
    elif type == 'macro':
        times, values = simulateAveragedModel(avgmodel, dt, tend, Z0)
        np.savetxt('data/macromodeleps='+str(eps)+'lam='+str(lam)+'c='+str(c)+'dt='+str(dt)+'tend='+str(tend)+'p='+str(p)+'initialperiodic'+runstr+'.data', np.vstack((times, values[:,0], values[:,1])))
    elif type == 'mM':
        points, weights, cbtimes, cbvalues, matchfails, failtimes = match(micromodel, dt, factor, tend, 0, 1, Z0)
        np.savetxt('data/mMmodeleps='+str(eps)+'lam='+str(lam)+'c='+str(c)+'dt='+str(dt)+'tend='+str(tend)+'factor='+str(factor)+'p='+str(p)+'initialperiodic'+runstr+'.data', np.vstack((cbtimes, cbvalues[:,1], cbvalues[:,3])))
    else:
        print('This simulation type is not supported!')
        
if __name__ == '__main__':
    args = parseArguments()
    lam = 2.
    c = 10.
    
    performExperiment(args.type, args.epsilon, lam, c, float(args.period), float(args.tend), args.factor, args.run)
