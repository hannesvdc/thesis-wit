import matplotlib.pyplot as plt

from potential import *

import argparse
import fnmatch
import os
import bisect

def parseArguments():
    parser = argparse.ArgumentParser(description='Input parameters for plotting on the periodic model.')
    parser.add_argument('--plot', metavar='p', nargs='?', dest='plot',
                        help='Select which the type of plot.')
    parser.add_argument('--eps', metavar='e', nargs='?',dest='eps', 
                        help='Select the epsilon value to make the plots for.')
    parser.add_argument('--A', metavar='a', nargs='?',dest='A', 
                        help='Select the coarse noise level for the plots.')
    parser.add_argument('--curves', metavar='c', nargs='+', dest='curves',
                        help='Select what data to display in the plots.')
    
    args = parser.parse_args()
    return args

##############################################################
#              Several file helper functions                 #
##############################################################
# Get the type of data file
def getType(file):
    if "micro" in file:
        return "micro"
    elif "macro" in file:
        return "macro"
    elif "mM" in file:
        return "mM"
    else:
        return None
    
# return the histogram and transient data file accompanied with this file.
def getPotentialFiles(filename):
    print(filename)
    if "model" in filename:
        return filename, filename.replace("model", "particles")
    else:
        return filename.replace("particles", "model"), filename
 
# Make an appealing label from the given file name.
def getLabel(filename):
    filename = filename.replace("data/", "")
    filename = filename.replace(".data", "")
    filename = filename.replace("model", " ")
    return filename
    
# Get the epsilon value from a file name.
def getEpsilonFromFileName(file):
    index = file.find('eps')
    index += 4
    endindex = file.find('A', index)
    eps = float(file[index:endindex])
    
    return eps

# Get the extrapolation factor from a mM file.
def getFactorFromFileName(file):
    index = file.find('factor')
    index += 6
    factorindex = file.find('tend', index)
    factor = float(file[index:factorindex])
    
    return factor

# Get the noise level from a mM file.
def getBetaFromFileName(file):
    index = file.find(' beta')
    index += 2
    aindex = file.find('dt', index)
    beta = float(file[index:aindex])
    
    return beta


##############################################################
#                  Several plot functions                    #
##############################################################
def plotSolutions(files):
    meanfig = plt.figure()
    meanax1 = meanfig.add_subplot(211)
    plt.title('Mean Slow')
    meanax2 = meanfig.add_subplot(212)
    plt.xlabel('Time [s]')
    plt.title('Mean Fast')
    varfig = plt.figure()
    varax1 = varfig.add_subplot(211)
    plt.title('Variance Slow')
    varax2 = varfig.add_subplot(212)
    plt.xlabel('Time [s]')
    plt.title('Variance Fast')
    histfig = plt.figure()
    histax1 = histfig.add_subplot(211)
    plt.title('Histogram Slow')
    histax2 = histfig.add_subplot(212)
    plt.xlabel('y')
    plt.title('Histogram Fast')
    
    for file in files:
        datafile, histfile = getPotentialFiles(file)
        data = np.loadtxt(datafile)
        histdata = np.loadtxt(histfile)
        label = getLabel(datafile)

        times = data[0,:]
        meanax1.plot(times, data[1,:], label=label)
        varax1.plot(times, data[2,:], label=label)
        
        if getType(file) == 'macro':
            histax1.hist(histdata, bins=100,label=label, normed=True, histtype='step')
        else:
            histax1.hist(histdata[0,:], bins=100,label=label, normed=True, histtype='step')
        
        if getType(file) == 'micro' or getType(file) == 'mM':
            meanax2.plot(times, data[3,:], label=label)
            varax2.plot(times, data[4,:], label=label)
            histax2.hist(histdata[1,:], bins=100, label=label, normed=True, histtype='step')
            
    meanax1.legend()
    meanax2.legend()
    varax1.legend()
    varax2.legend()
    histax1.legend()
    histax2.legend()
    
    plt.show()
    
def getList(args):
    if type(args) is list:
        return args
    else:
        return [args] 
        
if __name__ == '__main__':
    args = parseArguments()

    if args.plot == 'plotSolutions':
        plotSolutions(getList(args.curves))
    elif args.plot == 'plotSteadyStates':
        plotSteadyStates(float(args.A))
    else:
        print("This plot type is not supported.")