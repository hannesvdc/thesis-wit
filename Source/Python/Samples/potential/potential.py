import sys
sys.path.append("../../")

from potential_model import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.euler_maryama import *
from tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy.random as rd
import argparse

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Choose which experiment to perform.')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float, default=None,
                        help='Choose the time scale separation. Default is 0.1.')
    parser.add_argument('--dtmax', metavar='dtmax', nargs='?', dest='factor', type=float, default=None,
                        help='Choose the (maximal) time step. Exact meaning depends on the experiment.')
    parser.add_argument('--T', metavar='T', nargs='?', dest='T', type=float,
                        help='Choose the how many times the experiment should run.')
    parser.add_argument('--Lx', metavar='lx', nargs='?', dest='Lx', type=float, default=2,
                        help='Choose the number of coarse moments.')
    parser.add_argument('--Ly', metavar='ly', nargs='?', dest='Ly', type=float, default=0,
                        help='Choose the number of microscopic moments')
    parser.add_argument('--dt', metavar='dt', nargs='?', dest='dt', type=float, default=None,
                        help='Choose the number of microscopic moments')
    args = parser.parse_args()
    return args

# Define the moment functions
def momentFunctions(Lx, Ly, cov=False):
    RLX = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    
    if cov:
        RXY = lambda X: np.asarray([X[0]*X[1]])
        Rcov = lambda X: np.append(R(X), RXY(X))
        return Rcov
        
    return R

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,3]
    moment2Y = vector[:,4]
    vector[:,4] = moment2Y - meanY*meanY
    vector[:,5] = vector[:,5] - meanX*meanY
    
    return vector   

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Perform a full-scale microscopic simulation
def microscopicSimulation(model, dt, tend):
    cb = lambda x: computeMoments(x, momentFunctions(2,2,True))
    N = 10**4
    X = np.zeros([N,2])
    X[:,0] = np.random.normal(1., 0.5, N)
    X[:,1] = np.random.normal(0., 0.01, N)
    X, times, values = model.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb)
    
    return X, times, values

# Perform a simulation of the macroscopic model
def macroscopicSimulation(avgmodel, dt, tend):
    cb = lambda x: np.array([np.mean(x), np.mean(x*x)-np.mean(x)**2])
    N = 10**4
    X = np.random.normal(1., 0.5, N)
    X, times, values = avgmodel.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb)
    
    return X, times, values

# Matching acceleration algorithm
def match(model, tend, dt, Lx, Ly, dtmax):
    # Initial condition
    N = 10**4
    X = np.zeros([N,2])
    X[:,0] = np.random.normal(1., 0.5, N)
    X[:,1] = np.random.normal(0., 0.01, N)
    
    R = momentFunctions(Lx, Ly)
    Rplot = momentFunctions(2, 2, cov=True)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(X, R, computeMoments(X, R))
    params = {'K': 1, 'Dtmin': dt, 'Dtmax': dtmax, 'Dt0':dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    
    callback = lambda X, w: computeMoments(points=X, R=Rplot, weights=w)
    cpiresult = cpi.simulate(X, np.ones(N), 0., tend, dt, [0,1], callback)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues']

def performExperiments(type, eps, dt, T, factor=None, Lx=None, Ly=None):
    beta = 1./T
    tend = 0.2
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    ax2 = fig1.add_subplot(212)
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title('Mean Eps = '+str(eps) + 'beta = '+str(beta))
    ax4 = fig2.add_subplot(212)
    fig3 = plt.figure()
    ax5 = fig3.add_subplot(211)
    plt.title('Variance Eps = '+str(eps) + 'beta = '+str(beta))
    ax6 = fig3.add_subplot(212)
    
    V = lambda x: (x[0]**2 - 1.)**2 + (x[0]**2 + 1.)**2.*x[1]**2/eps
    Vx = lambda x: 4.*x[0]*(x[0]**2 - 1.) + 4./eps*x[0]*(x[0]**2 + 1.) *x[1]**2
    Vy = lambda x: 2.*x[1]*(x[0]**2 + 1.)**2/eps
    dV = lambda x: np.array([Vx(x), Vy(x)])
    A = np.array([np.sqrt(2./beta), np.sqrt(2./beta)])
    avgdV = lambda x: 4.*x*(x**2 - 1.) + 2.*x/(beta*(x**2+1.))
    avgA = np.sqrt(2./beta)
    model = PotentialModel(dV, A)
    avgmodel = PotentialModel(avgdV, avgA)
    
    if type == 'micro':
        print('Micro Simulation dt = '+str(dt))
        particles, times, values = microscopicSimulation(model, dt, tend)
        values = getMeanAndVariance(values)
        np.savetxt('data/micromodeleps'+str(eps)+'beta='+str(beta)+'dt='+str(dt)+'tend'+str(tend)+'.data', np.vstack((times, values[:,1], values[:,2], values[:,3], values[:,4])))
        np.savetxt('data/microparticleseps'+str(eps)+'beta='+str(beta)+'dt='+str(dt)+'tend'+str(tend)+'.data', np.vstack((particles[:,0], particles[:,1])))
        
        ax1.hist(particles[:,0], bins=100, normed=True, label='Microscopic slow dt='+str(dt))
        ax2.hist(particles[:,1], bins=100, normed=True, label='Microscopic fast dt='+str(dt))
        ax3.plot(times, values[:,1], label='Microscopic slow dt='+str(dt))
        ax4.plot(times, values[:,3], label='Microscopic fast dt='+str(dt))
        ax5.plot(times, values[:,2], label='Microscopic slow dt='+str(dt))
        ax6.plot(times, values[:,4], label='Microscopic fast dt='+str(dt))
    elif type == 'macro':
        avgparticles, avgtimes, avgvalues= macroscopicSimulation(avgmodel, dt, tend)
        np.savetxt('data/macromodelbeta='+str(beta)+'dt='+str(dt)+'tend'+str(tend)+'.data', np.vstack((avgtimes, avgvalues[:,0], avgvalues[:,1])))
        np.savetxt('data/macroparticlesbeta='+str(beta)+'dt='+str(dt)+'tend'+str(tend)+'.data', avgparticles)
        
        ax1.hist(avgparticles, bins=100, normed=True, label='Macroscopic')
        ax3.plot(avgtimes, avgvalues[:,0], label='Averaged dt='+str(dt))
        ax5.plot(avgtimes, avgvalues[:,1], label='Averaged dt='+str(dt))     
    elif type == 'mM':
        dtmax = factor*dt
        mmparticles, mmweights, cbtimes, cbvalues = match(model, tend, dt, Lx, Ly, dtmax)
        cbvalues = getMeanAndVariance(cbvalues)
        cbtimes, cbvalues = getRealPoints(cbtimes, cbvalues)
        np.savetxt('data/mMmodeleps'+str(eps)+'beta='+str(beta)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'Lx'+str(Lx)+'Ly'+str(Ly)+'.data', np.vstack((cbtimes, cbvalues[:,1], cbvalues[:,2], cbvalues[:,3], cbvalues[:,4])))
        np.savetxt('data/mMparticleseps'+str(eps)+'beta='+str(beta)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'Lx'+str(Lx)+'Ly'+str(Ly)+'.data', np.vstack((mmparticles[:,0], mmparticles[:,1])))
        
        ax3.plot(cbtimes, cbvalues[:,1], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax4.plot(cbtimes, cbvalues[:,3], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax5.plot(cbtimes, cbvalues[:,2], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax6.plot(cbtimes, cbvalues[:,4], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
    else:
        print('This experiment is not supported.')
            
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    
    plt.show()
    
if __name__ == '__main__':
    args = parseArguments()
    eps = float(args.epsilon)
    T = float(args.T)
    experiment = args.experiment
    if args.dt is None:
        dt = eps/20.
    else:
        dt = float(args.dt)
    
    performExperiments(experiment, eps, dt, T, args.factor, args.Lx, args.Ly)