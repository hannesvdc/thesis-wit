import sys
sys.path.append("../../")

from functools import partial
import argparse

import numpy as np
from scipy import stats
import math

import matplotlib.pyplot as plt

from Solvers.newton_raphson import *
from matching.kld import *
from matching.l2d import *
from matching.l2n import *
from tools.plots.plots import *
from timeintegrators.micromacro_cpi import *

from fene_model import *

def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the fene model')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "simulate", "match" and "momentplot"')
    parser.add_argument('--match', dest='matchtype', nargs=1, default='none',
                        help='The type of matching to use')
    parser.add_argument('--times', dest='times', nargs=1, default='none',
                        help='Specify the times to return the microscale with the simulate option')
    parser.add_argument('--moments', dest='moments', nargs=1, default='none',
                        help='Specify the number of moments used for the moment plot.')
    parser.add_argument('--plot', dest='plot', action='store_true',help="Make intermediate plots in an interactive session in the simulate mode.")
    
    parser.set_defaults(plot=False)
    
    args = parser.parse_args()
    return args

# Perform a full scale microscopic simulation and store the results for
# further experiments.
def simulate(times, J, makeplot):
    We = 1
    gamma = 7
    k=lambda t: 2
    fene = Fene(We, gamma, k)
    points = fene.sampleInitialDistribution(J)
    
    ntimes = times.size
    prevtime = 0.0
    currentTime = 0
    
    stresstensor = np.asarray([])
    while currentTime < ntimes:
        print("Integrating towards ", times[currentTime])
        points = fene.fullScaleMicroSimulation(points, prevtime, times[currentTime], dt)
        stress = fene.computeStress(points)
        stresstensor = np.append(stresstensor, stress)
        print("computing stress at time ", times[currentTime], " with stress value ", stress)

        print("KDE at t = ", times[currentTime])
        if makeplot:
            plotpoints = np.linspace(0, 8, 1000)
            pdf = kde(points, plotpoints, 0.01*gamma)
            plt.xlabel("Dumbbell length")
            plt.plot(plotpoints, pdf, '.')
            plt.show()
            plt.plot(stresstensor)
            plt.show()
        np.savetxt('data/Fenedatareport'+str(times[currentTime])+'s.txt', points)
        print("Timestepping for next plot")
        prevtime = times[currentTime]
        currentTime = currentTime + 1
    
    return points 

# Make a plot with the prior density at 1 second, posterior density at 1.1 second
# and the matched density of the given type and number of moments.
def match(type, L, makeplot):
    gamma = 7.
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    
    prior = np.loadtxt("data/Fenedatareport1.0s.txt", delimiter=',')
    target = np.loadtxt("data/Fenedatareport1.1s.txt", delimiter=',')
    
    plotpoints = np.linspace(5.4, 6.8, 1000)
    if makeplot:
        plt.plot(plotpoints, kde(prior, plotpoints, gamma*0.01), label='Prior')
        plt.plot(plotpoints, kde(target, plotpoints, gamma*0.01), label='Target')
    targetmoments = computeMoments(target, RL)
    print("Target moments ", targetmoments)
    
    weights = np.empty(0)
    if type == 'kld':
        kld = KLDMatchingStrategy(RL)
        converged, lambdas = kld.match(prior, targetmoments)
        weights = kld.weights(lambdas, prior)
    elif type == 'l2d':
        l2d = L2DMatchingStrategy(RL)
        lambdas = l2d.match(prior, targetmoments)
        weights = l2d.weights(lambdas, prior)
    elif type == 'l2n':
        l2n = L2NMatchingStrategy(RL, gamma, np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (L,L), dtype=int))
        lambdas = l2n.match(prior, targetmoments)
        weights = l2n.weights(lambdas)
    else:
        print("This matching strategy is not supported.")
        return
        
    matched = resample(prior, weights, 5000)
    if makeplot:
        plt.plot(plotpoints, kde(matched, plotpoints, gamma*0.01), label='Moments: '+str(L))
        plt.show()
    print("Target moments: ", targetmoments)
    print("matched moments: ", computeMoments(prior, RL, weights))
    np.savetxt("data/match"+type+str(L), matched)
    
    
def computeMoments(points, R, weights=None):
    if weights is None: weights = np.ones(points.size)/points.size
    
    weightsum = np.sum(weights)
    R0 = R(0)
    result = np.zeros(R0.size)
    
    for i in range(points.size):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

# Generate a plot for the relative error between the matched and exact moments as a
# function of the number of extrapolated moments L.
def generateMomentPlot(type, L):
    M = 14
    dt = 2.e-4
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    testmoments = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, M))))
    
    prior = np.loadtxt("data/Fenedatareport1.0s.txt", delimiter=',')
    
    gamma = 7.
    runs = 50
    relativemoments = np.zeros(M)
    for k in range(runs):
        print("IID run ", k)
        fene = Fene(1., 7., lambda t: 2.)
        target = fene.fullScaleMicroSimulation(np.copy(prior), 1.0, 1.1, dt)
        targetmoments = computeMoments(target, RL)
        
        if type == 'kld':
            kld = KLDMatchingStrategy(RL)
            conv, lambdas = kld.match(prior, targetmoments)
            weights = kld.weights(lambdas, prior)
            matchedmoments = computeMoments(prior, testmoments, weights)
        elif type == 'l2d':
            l2d = L2DMatchingStrategy(RL)
            lambdas = l2d.match(prior, targetmoments)
            weights = l2d.weights(lambdas, prior)
            matchedmoments = computeMoments(prior, testmoments, weights)
        elif type == 'l2n':
            l2n = L2NMatchingStrategy(RL, gamma, np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (L,L), dtype=int))
            lambdas = l2n.match(prior, targetmoments)
            H = np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (M,L), dtype=int)
            matchedmoments = H.dot(lambdas) + computeMoments(prior, testmoments)
        else:
            print("This matching strategy is not supported.")
            return
        
        realmoments = computeMoments(target, testmoments)
        print("Real moments ", realmoments)
        print("Matched moments ", matchedmoments)
        relerrors = np.absolute(realmoments - matchedmoments)/np.absolute(realmoments)
        relativemoments = relativemoments + relerrors
        
    relativemoments = relativemoments/runs
    np.savetxt('data/momentconvergence'+type+str(L)+'_'+str(runs)+'.data', relativemoments)
    plt.semilogy(relativemoments)
    plt.show()

# Generate a convergence plot of the relative stress error
# as a function of \Delta t
def generateTimeplot(type, L, dt):
    fene = Fene(1., 7., lambda t: 2.)
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    prior = np.loadtxt("data/Fenedatareport1.0s.txt", delimiter=',')
    times = np.asarray([0., 5*dt, 20*dt, 50*dt, 100*dt, 150*dt, 200*dt, 250*dt, 300*dt, 350*dt, 400*dt, 450*dt, 500*dt])
    
    runs = 50
    relativestresses = np.zeros(times.size-1)
    for k in range(runs):
        print("IID run ", k)
        
        X = np.copy(prior)
         
        data = []
        realstress = []
        targetmoments = []
        # Generate target data
        for i in range(times.size-1):
            X = fene.fullScaleMicroSimulation(X, 1.0 + times[i], 1.0 + times[i+1], dt)
            realstress.append(fene.computeStress(X))
            targetmoments.append(computeMoments(X, RL))
        
        # perform matcing
        plottimes = np.asarray(times[1:])
        matchedstress = []
        if type == 'kld':
            for i in range(len(plottimes)):
                kld = KLDMatchingStrategy(RL)
                converged, lambdas = kld.match(prior, targetmoments[i])
                weights = kld.weights(lambdas, prior)
                matchedstress.append(fene.computeStress(prior, weights))
        elif type == 'l2d':
            for i in range(len(plottimes)):
                l2d = L2DMatchingStrategy(RL)
                lambdas = l2d.match(prior,targetmoments[i])
                weights = l2d.weights(lambdas,prior)
                matchedstress.append(fene.computeStress(prior, weights))
        elif type == 'l2n':
            for i in range(len(plottimes)):
                l2n = L2NMatchingStrategy(RL, gamma, np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (L,L), dtype=int))
                lambdas = l2n.match(prior, targetmoments[i])
                weights = l2n.weights(lambdas, prior)
                matchedstress.append(fene.computeStress(prior, weights))
        else:
            print("This matching strategy is not supported.")
            return
            
        realstress = np.asarray(realstress)
        matchedstress = np.asarray(matchedstress)
        print("Real stresses", realstress)
        print("Matched stresses ", matchedstress)
        relativestresses += (np.abs(matchedstress-realstress)/np.abs(realstress))
    
    np.savetxt('data/relativestresserror'+type+str(L)+'.data', relativestresses/runs)    
    plt.loglog(times[1:], relativestresses/runs)
    plt.show()

# Plot the stresses obtained by a force-driven FENE model and the micro-macro
# acceleration for a given dtmax and number of moments.
def matchStress(dt, L):
    We = 1.
    gamma = 7.
    fene = Fene(We, gamma, lambda t: 2*(1.1+sin(pi*t)))
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L+1))))
    tend = 6.
    
    runs = 50
    avgstresstimes = np.asarray([0.])
    avgstressvalues = np.asarray([0.])
    avgstressfreq = np.asarray([0.])
    for k in range(runs):
        print("IID run ", k)
        X = fene.sampleInitialDistribution(1.e4)
        weights = np.ones(X.size)
        macromoments = computeMoments(X, RL, weights)
        
        kld = KLDMatchingStrategy(RL)
        stepper = AcceptRejectEulerMaryama(fene.a, fene.b, lambda x: abs(x) < (1.-sqrt(dt))*gamma)
        params = {}
        params['K'] = 1
        params['Dtmax'] = 5.*dt
        params['alfadecrease'] = 0.5
        params['alfaincrease'] = 1.2
        cpi = MicroMacroCPI(params, kld, stepper, RL)
        
        cpiresult = cpi.simulate(X, weights, 0., tend, dt, [0,1], lambda X, W: fene.computeStress(X, W))
        stresstimes = cpiresult['cbtimes']
        stresses = cpiresult['cbvalues']
        stresstimes = np.delete(stresstimes, np.s_[2::3])
        stresses = np.delete(stresses, np.s_[2::3])
        
        i = 0
        while i < stresstimes.shape[0] - 1:
            if stresstimes[i] == stresstimes[i+1]:
                stresses[i] = 0.5*(stresses[i]+stresses[i+1])
                stresses = np.delete(stresses, i+1)
                stresstimes = np.delete(stresstimes, i+1)
            i = i+1
            
        # Merge the averaged stress values with the new stress values.
        print("Merging stress values.")
        avgindex = 0
        newindex = 0
        
        while(newindex < stresstimes.shape[0]):
            print(newindex, avgindex)
            print("new time ", stresstimes[newindex], avgstresstimes[avgindex])
            if np.isclose(avgstresstimes[avgindex], stresstimes[newindex], atol=10**-8):
                print('equal times', avgstresstimes[avgindex], stresstimes[newindex])
                N = avgstressfreq[avgindex]
                avgstressvalues[avgindex] = (N*avgstressvalues[avgindex] + stresses[newindex])/(N+1.)
                avgstressfreq[avgindex] += 1
                newindex = newindex+1
            elif avgstresstimes[avgindex] > stresstimes[newindex]:
                print('intermediate times', avgstresstimes[avgindex], stresstimes[newindex])
                avgstressvalues = np.insert(avgstressvalues, avgindex, stresses[newindex])
                avgstresstimes = np.insert(avgstresstimes, avgindex, stresstimes[newindex])
                avgstressfreq = np.insert(avgstressfreq, avgindex, 1)
                avgindex += 1
                newindex += 1
            elif avgstresstimes[avgindex] < stresstimes[newindex]:
                # This case should only happen at the end of the stress values.
                avgstressvalues = np.append(avgstressvalues, stresses[newindex:])
                avgstresstimes = np.append(avgstresstimes, stresstimes[newindex:])
                avgstressfreq = np.append(avgstressfreq, np.ones_like(stresstimes[newindex:]))
                newindex = stresstimes.shape[0]
                
            if newindex >= stresstimes.shape[0]:
                break
            
            while avgstresstimes[avgindex] < stresstimes[newindex] and not np.isclose(avgstresstimes[avgindex], stresstimes[newindex]):
                avgindex += 1
                if avgindex >= avgstresstimes.shape[0]:
                    avgindex = avgstresstimes.shape[0] - 1
                    break
                    
    np.savetxt('data/Stresstimes_kld_'+str(L)+'.txt', avgstresstimes)
    np.savetxt('data/stressesmatched_kld_'+str(L)+'.txt', avgstressvalues)
    plt.plot(avgstresstimes, avgstressvalues, label="L = "+str(L))
    plt.xlabel('Time [s]')
    plt.ylabel('Polymer Stress')
    plt.show()
    
# Generate a plot of the stress as a function of time for the Euler-Maruyama
# scheme with given time step.
def computeStressData(dt):
    We = 1.
    gamma = 7.
    k = lambda t: 2*(1.1 + sin(pi*t))
    fene = Fene(We, gamma, k)
    X = fene.sampleInitialDistribution(1e4)
    
    stresses = []
    t = 0.0
    tend = 6.0
    em = AcceptRejectEulerMaryama(fene.a, fene.b, lambda x: abs(x)<(1-sqrt(dt))*gamma)
    weights = np.ones(X.size)
    while t + dt <= tend:
        print(t)
        X = em.step(X, t, dt)
        stresses.append(fene.computeStress(X, weights))
        t += dt
        
    np.savetxt('data/FeneReferenceStress.data', np.asarray(stresses))
    plt.plot(np.linspace(0., tend, len(stresses)), np.asarray(stresses))
    plt.show()
    
def resample(sample, weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = len(sample)
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros(nb_sam)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample

if __name__ == '__main__':
    J = 1e5
    dt = 2e-4
    Dt = 1
    We = 1
    gamma = 7
    gammasq = gamma*gamma
    k = 2
    args = parseArguments()
    
    if args.experiment == 'simulate':
        simulate(np.asarray(list(map(float, args.times[0].split(',')))), J, args.plot)
    elif args.experiment == 'match':
        match(args.matchtype[0], int(args.moments[0]), args.plot)
    elif args.experiment == 'momentplot':
        generateMomentPlot(args.matchtype[0], int(args.moments[0]))
    elif args.experiment == 'convergenceplot':
        generateTimeplot(args.matchtype[0], int(args.moments[0]), dt)
    elif args.experiment == 'generatestress':
        computeStressData(dt)
    elif args.experiment == 'matchstress':
        matchStress(dt, int(args.moments[0]))
    elif args.experiment == 'fenetest':
        fenetest(dt, int(args.moments[0]))
