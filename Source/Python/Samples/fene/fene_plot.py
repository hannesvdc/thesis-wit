import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl
from matplotlib import rc
from matplotlib2tikz import save as tikz_save
import numpy as np

import argparse
import fnmatch
import os
import bisect

def parseArguments():
    parser = argparse.ArgumentParser(description='Input parameters for plotting on the periodic model.')
    parser.add_argument('--plot', metavar='p', nargs='?', dest='plot',
                        help='Select which the type of plot.')
    parser.add_argument('--strategy', metavar='c', nargs='?', dest='strategy',
                        help='Select what data to display in the plots.')
    parser.add_argument('--factor', metavar='f', nargs='?', dest='factor',
                        help='Select what data to display in the plots.')
    parser.add_argument('--save', metavar='s', nargs='?', dest='save',
                        help='Select what data to display in the plots.')
    
    args = parser.parse_args()
    return args

##############################################################
#              Several file helper functions                 #
##############################################################
dir = 'data/lessnewton/'

def getStrategyFiles(strategy, factor):
    files = []
    for file in os.listdir('./'+dir):
        if fnmatch.fnmatch(file, 'micromodel*'):
            files.append(dir+file)
            
    for file in os.listdir('./'+dir):
        if fnmatch.fnmatch(file, 'mMmodel*strategy'+strategy+'*factor='+factor+'*'):
            files.append(dir+file)
            
    return files
 
def getLFromFileName(file):
    index = file.find('L=')
    index += 2
    dtindex = file.find('tend', index)
    if dtindex == -1:
        dtindex = file.find('factor', index)
    L = int(file[index:dtindex])
    
    return L     

def getType(file):
    if "micro" in file:
        return "microscopic"
    elif "macro" in file:
        return "macroscopic"
    elif "mM" in file:
        return "micro-macro"
    else:
        return None

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3])
    
    return newtimes, newstats

###################################################################################    
###                           Different plot functions                          ###
###################################################################################
def plotHysteresis(strategy, factor, save):
    rc('font',**{'family':'serif','serif':['lmodern']})   
    rc('text', usetex=True)
    mpl.rcParams.update({'font.size': 20})
    fig = plt.figure(figsize=(10, 8))

    outer = gridspec.GridSpec(1, 2, wspace=0.2, hspace=0.2)
    
    inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                    subplot_spec=outer[1], wspace=0.1, hspace=0.1)
    leftinner = gridspec.GridSpecFromSubplotSpec(1, 1,
                    subplot_spec=outer[0], wspace=0.1, hspace=0.1)

    ax3 = plt.Subplot(fig, leftinner[0])
    ax3.set_ylabel(r'$\tau$')
    ax3.set_xlabel(r'$M_1$')
    fig.add_subplot(ax3)
    
    ax2 = plt.Subplot(fig, inner[0])
    ax2.set_ylabel(r'$M_1$')
    fig.add_subplot(ax2)
    
    ax1 = plt.Subplot(fig, inner[1])
    ax1.set_xlabel('Time [s]')
    ax1.set_ylabel(r'$\tau$')
    fig.add_subplot(ax1)

    files = getStrategyFiles(strategy, factor)
    files.sort()
    files = files[-1:] + files[:-1]
    tend = 2.
    for file in files:
        if getType(file) == 'micro-macro':
            label = r'Micro-macro $L = '+str(getLFromFileName(file))+'$'
            linestyle = '-'
        elif getType(file) == 'microscopic':
            label = r'Microscopic'
            linestyle = ':'
            
        data = np.loadtxt(file)
        times = data[0,:]
        stress = data[1,:]
        M1 = data[2,:]
        index = np.argmax(times > tend)
        if index == 0:
            index = times.shape[0]
        times = times[0:index]
        stress = stress[0:index]
        M1 = M1[0:index]
        ax1.plot(times[::10], stress[::10], label=label,linestyle=linestyle,linewidth=2)
        ax2.plot(times[::10], M1[::10], label=label,linestyle=linestyle,linewidth=2)
        ax3.plot(M1[::10], stress[::10], label=label,linestyle=linestyle,linewidth=2)
        
        if getType(file) == 'micro-macro':
            realtimes, realdata = getRealPoints(times, stress)
            extrapolatetimes = []
            extrapolatesteps = []
            index = 2
            dt=2*10**-4
            while index < realtimes.shape[0]:
                macro = realtimes[index] - realtimes[index-2]
                extrapolatetimes.append(realtimes[index-2])
                extrapolatesteps.append(macro)
                index = index+2
            extrapolatetimes = np.array(extrapolatetimes)
            extrapolatesteps = np.array(extrapolatesteps)
#            ax4.plot(extrapolatetimes, extrapolatesteps/dt, label=label)  
            
            index1 = np.argmax(extrapolatetimes > 0.57)
            print('index',index1)
            print('averaged extrapolation factor before the peek', np.average(extrapolatesteps[:index1])/dt)
            print('Overall averaged extrapolation factor', np.average(extrapolatesteps)/dt)
        
    ax3.legend(fontsize=14)
#    ax4.legend()
    
    if save:
        plt.figure(fig1.number)
        tikz_save('plots/momentselection/stresses.tex')
        plt.figure(fig2.number)
        tikz_save('plots/momentselection/M1s.tex')
        plt.figure(fig3.number)
        tikz_save('plots/momentselection/phasediagram.tex')
        
    plt.show()
    
if __name__ == '__main__':
    args = parseArguments()
    
    if args.save == '1':
        save=True
    else:
        save=False
        
    if args.plot == 'plotHysteresis':
        plotHysteresis(args.strategy, args.factor, save)
    else:
        print('This plot type is not supported')