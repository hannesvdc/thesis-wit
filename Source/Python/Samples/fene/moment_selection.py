import sys
from tkinter.test.test_ttk.test_widgets import StandardTtkOptionsTests
sys.path.append("../../")

from functools import partial
import argparse
import tables

import numpy as np
from scipy import stats
import math

import matplotlib.pyplot as plt

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from fene_model import *

def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the fene model')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "micro", "match" and "momentplot"')
    parser.add_argument('--strategy', dest='strategy', nargs='?', default='none',
                        help='The the strategy for moment selection.')
    parser.add_argument('--L', dest='L', nargs='?', default=2, type=int,
                        help='Specify the number of moments used for the moment plot.')
    
    parser.set_defaults(plot=False)
    
    args = parser.parse_args()
    return args

def computeMoments(points, R, weights=None):
    if weights is None: weights = np.ones(points.size)/points.size
    
    weightsum = np.sum(weights)
    R0 = R(0)
    result = np.zeros(R0.size)
    
    for i in range(points.size):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

 ####################### Define several moment hierarchies ############################
def strategy1Moments(L):
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L+1))))
    return RL

def stratey2Moments(L, eps, We, gamma):
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    Rs = lambda x: eps/We*(x**2/(1-(x/gamma)**2) - 1)
    R = lambda x: np.append(RL(x),Rs(x))
    return R

def strategy3Moments(L, b):
    M1 = lambda x: x**2
    M2 = lambda x: x**2/(1-x**2/b) - 1
    M3 = lambda x: x**2/(1-x**2/b)**2
    M4 = lambda x: x**4/(1-x**2/b)**3
    
    if L == 1:
        return lambda x: np.array([1., M1(x)])
    elif L == 2:
        return lambda x: np.array([1., M1(x), M2(x)])
    elif L == 3:
        return lambda x: np.array([1., M1(x), M2(x), M3(x)])
    elif L == 4:
        return lambda x: np.array([1., M1(x), M2(x), M3(x), M4(x)])
    else:
        print('This number of moments for strategy 3 is not supported.')

def computeM1(X, weights=None):
    if weights is None:
        weights = np.ones_like(X)
   
    return np.average(X**2, weights=weights)

####################### Define the simulation functions ##############################
# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result
 
def projective_adaptive(model, strategy, L, dt, tend):
    filename = 'data/projective-adaptive-nblin2-tau0-nconstrained100.h5'
    h5 = tables.open_file(filename,mode="r")
    table = h5.root.variance.realizations
    row = table[0]
    times = row['time']
    taus = row['tau']
    h5.close()
    
    J = 5*10**4
    if strategy == '1':
        RL = strategy1Moments(L)
    elif strategy == '2':
        RL = stratey2Moments(L, 1., model.We, model.gamma)
    elif strategy == '3':
        RL = strategy3Moments(L, model.gamma**2)
        
    X = fene.sampleInitialDistribution(J)
    weights = np.ones(X.size)
    callback = lambda X, W: np.array([model.computeStress(X, W), computeM1(X,W)])
    cbmoments = []
    cbtimes = []
    t = 0.
    macromoments = computeMoments(X, RL, weights)
    tindex = 0
    K = 1
    max_entropy = np.log10(weights.size)
    while tindex < times.shape[0]:
        t = times[tindex]
        print('\n\n t = ', t)
        print('extrapolation step', times[tindex+1]-times[tindex])
        if t > 2.:
            break
        cbmoments.append(callback(X, weights))
        cbtimes.append(times[tindex])
        
        Y = model.fullScaleMicroSimulation(X, t, t+dt, dt)
        micromoments = computeMoments(Y, RL, weights)
        print('Microscopic moments', micromoments)
        Dt = times[tindex+1] - times[tindex]
        
        if math.isclose(Dt, dt, abs_tol=10**-6):
            macromoments = np.copy(micromoments)
            tindex += 1
            continue
        
        newmacromoments = (1.-Dt/(K*dt))*macromoments + Dt/(K*dt)*micromoments
        print("Extrapolated moments ", newmacromoments)
        
        matcher = KLDMatchingStrategy(X, RL, newmacromoments, optimizer='NR', iterations=6)
        conv, lambdas = matcher.match(weights, np.zeros_like(newmacromoments))
        
        if not conv:
            print('!!! Not converged !!!')
            times = np.insert(times, tindex+1, (times[tindex]+times[tindex+1])/2.)
            continue
        
        newweights = matcher.weights(lambdas, weights)
        
        cbtimes.append(times[tindex+1])
        cbmoments.append(callback(X, newweights))
        print('Callback at time ',times[tindex+1], cbmoments[-1])
        weights = np.copy(newweights)
        macromoments = np.copy(newmacromoments)
        
        # Resampling if the entropy of the weights is too large for accurate computations
        entropy = matcher.entropy(weights)
        print("Entropy of weights", entropy)
        if entropy > max_entropy/10:
            print("Resampling")
            indices = resample(weights, 5000)
            if len(X.shape) > 1:
                X = np.copy(X[indices, :])
                weights = np.ones(X.shape[0])
            else:
                X = X[indices]
                weights = np.ones(X.size)
                
        tindex += 1
        
    
    cbtimes = np.array(cbtimes)
    cbmoments = np.array(cbmoments)
    M1s = cbmoments[:,1]    
    stresses = cbmoments[:,0]
    
    np.savetxt('data/newmomentselection/mMmodelstrategy'+strategy+'L='+str(L)+'factor=fixed.data', np.vstack((cbtimes, cbmoments[:,0], cbmoments[:,1])))
    

def resample(weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = weigths.size
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros_like(weigths, dtype=int)  # resampling

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = int(shelf)  # duplication of samples
            count += 1
            ind += 1
        shelf += 1
    print("indices ", rsample)
    return rsample

    
def matchWithPredefinedTimes():
    # List all groups
    filename = 'data/projective-adaptive-nblin2-tau0-nconstrained100.h5'
    h5 = tables.open_file(filename,mode="r")
    table = h5.root.variance.realizations
    row = table[0]
    time = row['time']
    tau = row['tau']
    for t in time:
        print(t)
    
def microSimulation(model, dt, tend):
    J = 10**4
    t = 0.
    points = model.sampleInitialDistribution(J)
    stresses = [model.computeStress(points)]
    M1s = [computeM1(points)]
    times = [t]
    
    while t+dt < tend:
        points = model.fullScaleMicroSimulation(points, t, t+dt, dt)
        stresses.append(model.computeStress(points))
        M1s.append(computeM1(points))
        times.append(t+dt)
        t = t + dt
    
    times = np.array(times)
    stresses = np.array(stresses)
    M1s = np.array(M1s)
    
    np.savetxt('data/momentselection/micromodeltend='+str(tend)+'.data', np.vstack((times,stresses,M1s)))

def match(model, strategy, L, dt, tend):
    J = 10**4
    if strategy == '1':
        RL = strategy1Moments(L)
    elif strategy == '2':
        RL = stratey2Moments(L, 1., model.We, model.gamma)
    elif strategy == '3':
        RL = strategy3Moments(L, model.gamma**2)
    
    X = fene.sampleInitialDistribution(J)
    weights = np.ones(X.size)
    macromoments = computeMoments(X, RL, weights)  
    kld = KLDMatchingStrategy(X, RL, macromoments, optimizer='NR',iterations=4)
    stepper = AcceptRejectEulerMaryama(fene.a, fene.b, lambda x: abs(x) < (1.-sqrt(dt))*gamma)
    params = {}
    params['K'] = 1
    params['Dtmax'] = 0.01
    params['alfadecrease'] = 0.5
    params['alfaincrease'] = 1.2
    cpi = MicroMacroCPI(params, kld, stepper, RL)
    
    cpiresult = cpi.simulate(X, weights, 0., tend, dt, lambda X, W: np.array([model.computeStress(X, W), computeM1(X,W)]))
    X = cpiresult['points']
    weights = cpiresult['weights']
    cbtimes = cpiresult['cbtimes']
    cbvalues = cpiresult['cbvalues']
    print(cbvalues)   
    np.savetxt('data/momentselection/mMmparticlesstrategy'+strategy+'L='+str(L)+'tend='+str(tend)+'factor=25'+'.data', np.vstack((X, weights)))
    np.savetxt('data/momentselection/mMmodelstrategy'+strategy+'L='+str(L)+'tend='+str(tend)+'factor=25'+'.data', np.vstack((cbtimes,cbvalues[:,0],cbvalues[:,1])))

        
if __name__ == '__main__':
    args = parseArguments()
    gamma = 7
    We = 1.
    k = lambda t: 100.*t*(1-t)*exp(-4*t)
    fene = Fene(We, gamma, k)
    dt = 0.0001
    tend = 4.
    
    if args.experiment == 'micro':
        microSimulation(fene, dt, tend)
    elif args.experiment == 'match':
        match(fene, args.strategy, int(args.L), dt, tend)
    elif args.experiment == 'projective_adaptive':
        projective_adaptive(fene, args.strategy, int(args.L), dt, tend)
    elif args.experiment == 'matchWithPredefinedTimes':
        matchWithPredefinedTimes()
    else:
        print('This operation is not supported')