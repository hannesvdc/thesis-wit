delta = 0.01;
eps = 0.025;
tauy = 25.;
k2 = 1;
k3 = 0.48*delta;
k4 = 0.0666*delta*delta;
k5 = 0.833;
k6 = 1;

X = 550;
Y = 20;
J = [[(-k2+2*X*k3-k4*(3*X*X-6*X+2))/eps, 0];[k5/tauy, -k6/tauy]];
dt = 2./max(abs(eig(J)));
disp(dt);