import sys
sys.path.append("../../")

from bruna_model import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.noflux_euler_maruyama import *
from tools.sample import *

import matplotlib.pyplot as plt
import numpy.random as rd
from scipy.integrate import *

from tabulate import tabulate
import argparse

# Parse the command line arguments to select the experiment to perform.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the fene model')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='experiment',
                        help='Select which experiment you want to perform. Options are "simulate", "match" and "momentplot"')
    parser.add_argument('--regime', dest='regime', nargs=1, default='none',
                        help='The regime for the experiment')
    parser.add_argument('--match', dest='matchtype', nargs=1, default='none',
                        help='The type of matching to use')
    parser.add_argument('--times', dest='times', nargs=1, default='none',
                        help='Specify the times to return the microscale with the simulate option')
    parser.add_argument('--moments', dest='moments', nargs=1, default='none',
                        help='Specify the number of moments used for the moment plot.')
    parser.add_argument('--plot', dest='plot', action='store_true',help="Make intermediate plots in an interactive session in the simulate mode.")
    
    parser.set_defaults(plot=False)
    
    args = parser.parse_args()
    return args

# Return different sets of model parameters to perform different experiments.
# The output format is k1, k2, k3, k4, k5, k6, eps, tauy.
def brunaRegime(regime):
    delta = 0.01
    if regime == 'gaussian':
        return 0.8/delta, 1, 0.48*delta, 0.0666*delta**2, 0.833, 1, 0.025, 25
    elif regime == '1':
        return 0.6/delta, 1, 0.48*delta, 0.0666*delta**2, 0.833, 1, 0.025, 250
    elif regime == '2':
        return 0.6/delta, 1, 0.48*delta, 0.0666*delta**2, 0.833, 1, 0.025, 25
    else:
        print("This regime is not supported in the Bruna model")

# Helper function that defines the invariant distribution for a given
# set of parameter values.
def getInvariantDistribution(k1, k2, k3, k4, k5, k6):
    v = lambda x: k1 - k2*x + k3*x*(x-1) - k4*x*(x-1)*(x-2)
    d = lambda x: 0.5*(k1 + k2*x + k3*x*(x-1) + k4*x*(x-1)*(x-2))
    rho = lambda x: exp(quad(lambda s: v(s)/d(s), 0, x)[0])/d(x)
    A = quad(rho, 0, 600)[0]
    rhox = lambda x: rho(x)/A
    
    return rhox

# Define the moment functions
def momentFunctions(Lx, Ly, scalex=1., scaley=1.):
    RLX = lambda X: np.asarray(list(map(lambda p: (X[0]/scalex)**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: (X[1]/scaley)**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    return R

# Generate a plot for the relative error between the matched and exact moments as a
# function of the number of extrapolated moments L.
def generateMomentPlot(bruna, type, L):
    N = 10**5
    dt = bruna.eps/2.
    
    M = 8
    RL = momentFunctions(0, L)
    testmoments = momentFunctions(0, M)
    
    prior = np.loadtxt('data/brunamicrosimulation.data')
    print(prior.shape)
    runs = 1
    relativemoments = np.zeros(M+1)
    for k in range(runs):
        print("IID run ", k)
        target = bruna.fullScaleMicroSimulation(np.copy(prior), 1.0, 1.1, dt)
        targetmoments = computeMoments(target, RL)
        
        if type == 'kld':
            kld = KLDMatchingStrategy(prior, RL, targetmoments, iterations=50)
            conv, lambdas = kld.match()
            weights = kld.weights(lambdas)
            matchedmoments = computeMoments(prior, testmoments, weights)
        elif type == 'l2d':
            l2d = L2DMatchingStrategy(prior, RL, targetmoments)
            lambdas = l2d.match()
            weights = l2d.weights(lambdas)
            matchedmoments = computeMoments(prior, testmoments, weights)
        elif type == 'l2n':
            l2n = L2NMatchingStrategy(prior, RL, targetmoments, gamma, np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (L,L), dtype=int))
            lambdas = l2n.match()
            H = np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (M,L), dtype=int)
            matchedmoments = H.dot(lambdas) + computeMoments(prior, testmoments)
        else:
            print("This matching strategy is not supported.")
            return
        
        realmoments = computeMoments(target, testmoments)
        print("Real moments ", realmoments)
        print("Matched moments ", matchedmoments)
        relerrors = np.absolute(realmoments - matchedmoments)/np.absolute(realmoments)
        relativemoments = relativemoments + relerrors
        
    relativemoments = relativemoments/runs
    np.savetxt('data/momentconvergence'+type+str(L)+'_'+str(runs)+'.data', relativemoments)
    plt.semilogy(relativemoments)
    plt.show()
    
# Simulate the Schlogl model using the SSA algorithm
def ssa(X, Y, reactions, alfa, t, tend):
    while t <= tend:
        print(t)
        props = alfa(X, Y)
        totalprop = np.sum(props)
        tau = -1./totalprop*log(rd.uniform())
        
        y = rd.uniform()
        sum = 0.
        for i in range(props.size):
            sum += props[i]/totalprop
            if y < sum:
                reaction = i
                break
        
        X, Y = reactions[i](X, Y)
        t += tau
    
    return t, X, Y
    
def plotMatching(bruna, eps, tend, N, K, rhox, Lx, Ly, cols):
    # Simulate using SSA
#    alfa = lambda x, y: np.asarray([k1/eps, k2*x/eps, k3*x*(x-1)/eps, k4*x*(x-1)*(x-2)/eps, k5*x/tauy, k6*y/tauy])
#    reactions = [lambda X, Y: (X+1, Y), lambda X, Y: (X-1, Y), lambda X,Y: (X+1,Y), lambda X,Y:(X-1,Y), lambda X,Y:(X,Y+1), lambda X,Y:(X,Y-1)]
#    t = 0.0
#    X = 0
#    Y = 0
#    Xvalues = []
#    Yvalues = []
#    tend = 10**3
#    while t <= tend:
#        t, X, Y = ssa(X, Y, reactions, alfa, t, t+5)
#        Xvalues.append(X)
#        Yvalues.append(Y)
#
#    plt.hist(np.asarray(Yvalues), bins=100, normed=True, label="Fast SSA algorithm")

    # Simulate using the accelerated RM1
#    k5bar = quad(lambda x: k5*x*rhox(x), 0., 600.)[0]
#    alfa = lambda x, y: np.asarray([k5bar/tauy, k6*y/tauy])
#    reactions = [lambda X,Y:(X,Y+1), lambda X,Y:(X,Y-1)]
#    t = 0.0
#    X = 0
#    Y = 0
#    Xvalues = []
#    Yvalues = []
#    tend = 10**6
#    while t <= tend:
#        t, X, Y = ssa(X, Y, reactions, alfa, t, t+5)
#        Xvalues.append(X)
#        Yvalues.append(Y)
#
#    plt.subplot(212)
#    plt.hist(np.asarray(Yvalues), bins=100, normed=True, histtype='step', label="Model Reduction 1")

    # Simulate using micro-macro acceleration and relative entropy matching with KLD
    print("KLD Matching with "+str(K)+" internal steps")
    Xsamples = acceptrejectuniform(rhox, 0., 600., 0.01, N)
    X = np.ones([N, 2])
    X[:, 0] = Xsamples
    #X[:, 1] = rd.uniform(0,10, N)
    
    R = momentFunctions(Lx, Ly, scalex=600, scaley=30)
    Rplot = momentFunctions(3,3)
    callback = lambda X, w: computeMoments(points=X, R=Rplot, weights=w)
    params = {'K': K, 'Dtmin': eps, 'Dtmax': 0.5, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    stepper = NoFluxEulerMaryama(bruna.a, bruna.b)
    print(computeMoments(X, R))
    kld = KLDMatchingStrategy(X, R, computeMoments(X, R))
    cpi = MicroMacroCPI(params, kld, stepper, R)
    cpiresult = cpi.simulate(X, np.ones(N), 0., tend, eps, cols, callback)
    X = cpiresult['points']
    weights = cpiresult['weights']
    cbtimes = cpiresult['cbtimes']
    cbvalues = cpiresult['cbvalues']
    
    return X, weights, cbtimes, cbvalues

# Perform a microscopic simulation of the given bruna model.
def microscaleSimulation(bruna, dt, tend, N, rhox):
    print("\n\nFull microscale simulation.")
    
    Xsamples = acceptrejectuniform(rhox, 0., 600., 0.01, N)
    X = np.ones([N, 2])
    X[:,0] = np.copy(Xsamples)
    t = 0.0
    
    R = momentFunctions(Lx=3, Ly=3)
    callback = lambda X: computeMoments(X, R)
    X, cbtimes, cbvalues = bruna.fullScaleMicroSimulation(X, 0., tend, dt, callback)
    
    np.savetxt('data/brunamicrosimulation.data', X)
    return X, cbtimes, cbvalues


def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i,:])*weights[i]/weightsum
    
    return result

def resample(weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = weigths.size
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros_like(weigths, dtype=int)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = int(shelf)  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample

def plot_density(figure, X, w, label):
    plt.figure(figure.number)
    plt.subplot(211)
    plt.hist(X[:,0], weights=w, bins=40, histtype='step', normed=True, label=label)
    plt.xlabel("x")
    plt.legend()
    plt.subplot(212)
    plt.hist(X[:,1], weights=w, bins=40, histtype='step', normed=True, label=label)
    plt.xlabel("y")
    plt.legend()

def plot_moments(figure, times, xvalues, yvalues, labelx, labely):
    plt.figure(figure.number)
    plt.subplot(211)
    plt.plot(times, xvalues, label=labelx)
    plt.xlabel("Time [s]")
    plt.legend()
    plt.subplot(212)
    plt.plot(times, yvalues, label=labely)
    plt.xlabel("Time [s]")
    plt.legend()

# Compute the five statistics form an MC distribution   
def MCStatistics(particles, weights=None):
    if weights is None:
        weights = np.ones(particles.shape[0])
        
    X = particles[:,0]
    Y = particles[:,1]
    N = particles.shape[0]
    
    meanX = np.average(X, weights=weights)
    meanY = np.average(Y, weights=weights)
    varX = N/(N-1)*np.average((X-meanX)**2, weights=weights)
    varY = N/(N-1)*np.average((Y-meanY)**2, weights=weights)
    
    normw = weights/np.sum(weights)
    V2 = np.sum(normw*normw)
    covXY = np.average((X-meanX)*(Y-meanY), weights=normw)/(1-V2)
    
    return np.array([meanX, varX, meanY, varY, covXY])

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,4]
    moment2Y = vector[:,5]
    vector[:,5] = moment2Y - meanY*meanY
    vector[:,6] = vector[:,6] - meanX*meanY
    
    return vector
    
def main():
    # Select the experiment to perform.
    args = parseArguments()

    # Define general model variables
    k1, k2, k3, k4, k5, k6, eps, tauy = brunaRegime(args.regime[0])
    bruna = Bruna([k1, k2, k3, k4, k5, k6], eps, tauy)
    rhox = getInvariantDistribution(k1, k2, k3, k4, k5, k6)
    
    # Perform the chosen experiment
    if args.experiment == 'simulate':
        microscaleSimulation(bruna, eps/2, 1., 10**5, rhox)
        return
    elif args.experiment == 'momentplot':
        generateMomentPlot(bruna, args.matchtype[0], int(args.moments[0]))
        return
    
    # Setup the Bruna model
    N = 10**5
    tend = 100.
    R = momentFunctions(2,2)
    
    # setup the experiments
    configs = [[0,2,1]]
                     
    # Create a bunch of figures
    densityfig = plt.figure()
    meanfig = plt.figure()
    varfig = plt.figure()
    
    # Setup variables to print a table
    header = ["Experiment", "E[X]", "Var[X]", "E[Y]", "Var[Y]", "E[X^2]", "E[Y^2]"]
    experiments = []
    
    # Perform the numerical experiments and plot the results
    X, cbtimes, cbvalues = microscaleSimulation(bruna, eps/2, tend, N, rhox)
    cbvalues = getMeanAndVariance(cbvalues)
    plot_density(densityfig, X, np.ones(X.shape[0]), "Micro simulation")
    plot_moments(meanfig, cbtimes, cbvalues[:,1], cbvalues[:, 4], "Micro mean X", "Micro mean Y")
    plot_moments(varfig, cbtimes, cbvalues[:,2], cbvalues[:, 5], "Micro var X", "Micro var Y")
    microstats =  MCStatistics(X)
    micromoments = computeMoments(X, R)
    microexperiment = ["Micro "]
    microexperiment.extend(microstats[0:4])
    microexperiment.extend([micromoments[2], micromoments[4]])
    experiments.append(microexperiment)
    
    for config in configs:
        print("New config ", config)
        Lx = config[0]
        Ly = config[1]
        X, w, cbtimes, cbvalues = plotMatching(bruna, eps/2, tend, N, config[2], rhox, Lx=Lx, Ly=Ly, cols=[0,1])
        
        cbvalues = getMeanAndVariance(cbvalues)
        plot_density(densityfig, X, w, "Matching Lx="+str(Lx)+" Ly="+str(Ly))
        plot_moments(meanfig, cbtimes, cbvalues[:,1], cbvalues[:, 4], "Matching E[X] Lx="+str(Lx)+"Ly="+str(Ly), "Matching E[Y] Lx="+str(Lx)+"Ly="+str(Ly))
        plot_moments(varfig, cbtimes, cbvalues[:,2], cbvalues[:, 5], "Matching Var[X] Lx="+str(Lx)+"Ly="+str(Ly), "Matcing Var[y] Lx="+str(Lx)+"Ly="+str(Ly))
        
        matchstats = MCStatistics(X, w)
        matchmoments = computeMoments(X, R, w)
        matchexperiment = ["Lx="+str(Lx)+", Ly="+str(Ly)]
        matchexperiment.extend(matchstats[0:4])
        matchexperiment.extend([matchmoments[2], matchmoments[4]])
        experiments.append(matchexperiment)
    
    meanfig.show()
    varfig.show()
    
    # Add extra figures to the plot
    plt.figure(densityfig.number)
    plt.subplot(211)
    s = np.linspace(0, 600, 1000)
    rhovalues = []
    for k in s:
        rhovalues.append(rhox(k))
    plt.plot(s, np.asarray(rhovalues), label="Steady State X")
    
    # Print the table with statistics
    print("\n\n\n", tabulate(experiments, headers=header))
    
    # Finally show all the plots
    plt.show()

if __name__ == '__main__':
    main()
    
