from bruna import *

import sys
import argparse
from tabulate import tabulate

# Input parser for the different order tests.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('-regime', metavar='r', nargs='?', dest='regime',
                        help='Select in which regime you want to carry out convergence experiments')
    parser.add_argument('-experiment', metavar='r', nargs='?', dest='experiment',
                        help='Select the experiment you want to carry out. Options are "moments", "Dt", "dt" and "N".')
    parser.add_argument('-L', metavar='l', nargs='?', dest='L',
                        help='Number of macroscopic moments, if applicable.')
    parser.add_argument('-dt', metavar='t', nargs='?', dest='dt',
                        help='Microscopic step size, if applicable.')
    parser.add_argument('-Dt', metavar='T', nargs='?', dest='Dt',
                        help='Macroscopic step size, if applicable.')
    args = parser.parse_args()
    return args

# Convergence test for the number of moments
def momentsTest(bruna, dt, Dtmax, tend, N, Lrange, rhox):
    # Setup variables to print a table
    header = ["Experiment", "E[X]", "Var[X]", "E[Y]", "Var[Y]", "E[X^2]", "E[Y^2]"]
    experiments = []
    
    # Perform a microscale simulation to compute errors.
    X, microTimes, microValues = microscaleSimulation(bruna, dt, tend, N, rhox)
    microValues = getMeanAndVariance(microValues)
    Rtab = momentFunctions(2,2)
    microstats =  MCStatistics(X)
    micromoments = computeMoments(X, Rtab)
    microexperiment = ["Micro "]
    microexperiment.extend(microstats[0:4])
    microexperiment.extend([micromoments[2], micromoments[4]])
    experiments.append(microexperiment)
    
    # Carry out the experiments
    errors = []
    K = 1
    for k in range(len(Lrange)):
        print("\n\n\n moments ", Lrange[k], k)
        Xsamples = acceptrejectuniform(rhox, 0., 600., 0.01, N)
        particles = np.ones([N, 2])
        particles[:,0] = np.copy(Xsamples)
        
        # Perform the simulation
        R = momentFunctions(0, Lrange[k], scalex=600, scaley=30)
        params = {'K': K, 'Dtmin': dt, 'Dtmax': Dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
        stepper = NoFluxEulerMaryama(bruna.a, bruna.b)
        print(computeMoments(X, R))
        kld = KLDMatchingStrategy(X, R, computeMoments(X, R), iterations=Lrange[k]+5)
        cpi = MicroMacroCPI(params, kld, stepper, R)
        X, weights = cpi.simulate(particles, np.ones(N), 0., tend, dt, [0,1])
        
        stats = MCStatistics(X, weights)
        print("Computed statistics ", stats)
        errors.append(np.abs(stats-microstats))
        print("Errors at ", Lrange[k], np.abs(stats-microstats))
        
        matchmoments = computeMoments(X, Rtab, weights)
        matchexperiment = ["Lx=0, Ly="+str(Lrange[k])]
        matchexperiment.extend(stats[0:4])
        matchexperiment.extend([matchmoments[2], matchmoments[4]])
        experiments.append(matchexperiment)

    errors = np.matrix(errors)
    open('data/LrangeConvergence.data','w').close()
    file = open('data/LrangeConvergence.data','ab')
    np.savetxt(file, np.asmatrix(np.reshape(Lrange, [1, len(Lrange)])))
    np.savetxt(file, np.transpose(errors))
    file.close()
    
    # Print the table with statistics
    print("\n\n\n", tabulate(experiments, headers=header))
    
if __name__ == "__main__":
    args = parseArguments()
    
    # Select the desired Bruna regime.
    regime = args.regime[0]
    if regime == '1':
        k1, k2, k3, k4, k5, k6, eps, tauy = gaussian()
    elif regime == '2':
        k1, k2, k3, k4, k5, k6, eps, tauy = regime1()
    elif regime == '3':
        k1, k2, k3, k4, k5, k6, eps, tauy = regime2()
    else:
        print('This regime is not supported in the Bruna paper.')
        sys.exit(0)
    
    # Build the Bruna model
    k = [k1, k2, k3, k4, k5, k6]
    bruna = Bruna(k, eps, tauy)
    N = 10**5
    tend = 1.
    v = lambda x: k1 - k2*x + k3*x*(x-1) - k4*x*(x-1)*(x-2)
    d = lambda x: 0.5*(k1 + k2*x + k3*x*(x-1) + k4*x*(x-1)*(x-2))
    rho = lambda x: exp(quad(lambda s: v(s)/d(s), 0, x)[0])/d(x)
    A = quad(rho, 0, 600)[0]
    rhox = lambda x: rho(x)/A
    
    # Perform a convergence test for the number of moments
    momentsTest(bruna, eps/2, 0.5, tend, N, [0,1,2,3], rhox)
    