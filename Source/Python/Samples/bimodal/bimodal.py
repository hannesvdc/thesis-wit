import sys
sys.path.append("../../")

from bimodal_model import *
from Samples.linear.linear_model import *

from matching.kld import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.euler_maryama import *
from tools.sample import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy.random as rd
import argparse

# Input parser for the different experiments.
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments for the different order tests.')
    parser.add_argument('--experiment', metavar='e', nargs='?', dest='type',
                        help='Choose which experiment to perform.')
    parser.add_argument('--epsilon', metavar='eps', nargs='?', dest='epsilon', type=float, default=None,
                        help='Choose the time scale separation. Default is 0.1.')
    parser.add_argument('--dtmax', metavar='dtmax', nargs='?', dest='factor', type=float, default=None,
                        help='Choose the (maximal) time step. Exact meaning depends on the experiment.')
    parser.add_argument('--Lx', metavar='lx', nargs='?', dest='Lx', type=float, default=2,
                        help='Choose the number of coarse moments.')
    parser.add_argument('--Ly', metavar='ly', nargs='?', dest='Ly', type=float, default=0,
                        help='Choose the number of microscopic moments')
    parser.add_argument('--A', metavar='A', nargs='?', dest='A', type=float,
                        help='Choose coarse noise level.')
    parser.add_argument('--run', metavar='r', nargs='?', dest='run', type=int, default=None,
                        help='Select the run for this experiment, if applicable.')
                        
    args = parser.parse_args()
    return args

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,3]
    moment2Y = vector[:,4]
    vector[:,4] = moment2Y - meanY*meanY
    vector[:,5] = vector[:,5] - meanX*meanY
    
    return vector   

# Define the moment functions
def momentFunctions(Lx, Ly, cov=False):
    RLX = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    
    if cov:
        RXY = lambda X: np.asarray([X[0]*X[1]])
        Rcov = lambda X: np.append(R(X), RXY(X))
        return Rcov
        
    return R

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result
    
# Compute the five statistics form an MC distribution   
def MCStatistics(particles, weights=None):
    if weights is None:
        weights = np.ones(particles.shape[0])
        
    X = particles[:,0]
    Y = particles[:,1]
    N = particles.shape[0]
    
    meanX = np.average(X, weights=weights)
    meanY = np.average(Y, weights=weights)
    varX = N/(N-1)*np.average((X-meanX)**2, weights=weights)
    varY = N/(N-1)*np.average((Y-meanY)**2, weights=weights)
    
    normw = weights/np.sum(weights)
    V2 = np.sum(normw*normw)
    covXY = np.average((X-meanX)*(Y-meanY), weights=normw)/(1-V2)
    
    return np.array([meanX, varX, meanY, varY, covXY])

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Compute the statistics from a density given in a grid
def computeStatistics(data, X, Y, dx, dy):
    # Compute marginal densities
    margX = dy*np.sum(data, axis=0)
    margY = dx*np.sum(data, axis=1)

    # Compute statistics
    meanX = dx*margX.dot(X)
    meanY = dy*margY.dot(Y)
    varX = dx*margX.dot((X-meanX)**2)
    varY = dy*margY.dot((Y-meanY)**2)
    X, Y = np.meshgrid(X, Y)
    covXY = dx*dy*np.sum((X-meanX)*(Y-meanY)*data)
    
    # Return the results as a vector
    return [meanX, varX, meanY, varY, covXY]

# Perform a full-scale microscopic simulation
def microscopicSimulation(model, dt, tend):
    cb = lambda x: computeMoments(x, momentFunctions(2,2,True))
    N = 10**4
    X = np.zeros([N,2])
    X[:,0] = np.random.normal(-np.sqrt(2.)/2., np.sqrt(1./8.), N)
    X[:,1] = np.random.normal(np.sqrt(2.), np.sqrt(0.5), N)
    X, times, values = model.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb)
    
    return X, times, values

# Perform a simulation of the macroscopic model
def macroscopicSimulation(avgmodel, dt, tend):
    cb = lambda x: np.array([np.mean(x), np.mean(x*x)-np.mean(x)**2])
    N = 10**4
    X = np.random.normal(-np.sqrt(2.)/2., np.sqrt(1./8.), N)
    X, times, values = avgmodel.fullScaleMicroSimulation(X, 0., tend, dt, callback=cb)
    
    return X, times, values

# Matching acceleration algorithm
def match(model, tend, dt, Lx, Ly, dtmax):
    # Initial condition
    N = 10**4
    X = np.zeros([N,2])
    X[:,0] = np.random.normal(-np.sqrt(2.)/2., np.sqrt(1./8.), N)
    X[:,1] = np.random.normal(np.sqrt(2.), np.sqrt(0.5), N)
    
    R = momentFunctions(Lx, Ly)
    Rplot = momentFunctions(2, 2, cov=True)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(R)
    params = {'K': 1, 'Dtmin': dt, 'Dtmax': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    
    callback = lambda X, w: computeMoments(points=X, R=Rplot, weights=w)
    cpiresult = cpi.simulate(X, np.ones(N), 0., tend, dt, callback=callback)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues']

def performExperiments(type, eps, dt, A, factor=None, Lx=None, Ly=None, run=None):
    tend = 2.

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    plt.xlabel('X')
    ax2 = fig1.add_subplot(212)
    plt.xlabel('Y')
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title('Mean Eps = '+str(eps) + 'A = '+str(A))
    ax4 = fig2.add_subplot(212)
    fig3 = plt.figure()
    ax5 = fig3.add_subplot(211)
    plt.title('Variance Eps = '+str(eps) + 'A = '+str(A))
    ax6 = fig3.add_subplot(212)
    
    model = BimodalModel(eps, A)
    avgmodel = LinearModel(np.matrix([-2.]), np.array([A]))
    
    if run is None:
        endstring = '.data'
    else:
        endstring = 'run='+str(run)+'.data'
        
    if type == 'micro':
        print('Micro Simulation dt = '+str(dt))
        particles, times, values = microscopicSimulation(model, dt, tend)
        values = getMeanAndVariance(values)
        np.savetxt('data/micromodeleps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'tend'+str(tend)+endstring, np.vstack((times, values[:,1], values[:,2], values[:,3], values[:,4])))
        np.savetxt('data/microparticleseps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'tend'+str(tend)+endstring, np.vstack((particles[:,0], particles[:,1])))
        
        ax1.hist(particles[:,0], bins=50, normed=True, label='Slow distribution')
        ax2.hist(particles[:,1], bins=50, normed=True, label='Fast distribution')
        ax3.plot(times, values[:,1], label='Microscopic slow dt='+str(dt))
        ax4.plot(times, values[:,3], label='Microscopic fast dt='+str(dt))
        ax5.plot(times, values[:,2], label='Microscopic slow dt='+str(dt))
        ax6.plot(times, values[:,4], label='Microscopic fast dt='+str(dt))
    elif type == 'macro':
        avgparticles, avgtimes, avgvalues= macroscopicSimulation(avgmodel, dt, tend)
        np.savetxt('data/macromodeleps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'tend'+str(tend)+endstring, np.vstack((avgtimes, avgvalues[:,0], avgvalues[:,1])))
        np.savetxt('data/macroparticleseps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'tend'+str(tend)+endstring, avgparticles)
        
        ax3.plot(avgtimes, avgvalues[:,0], label='Averaged dt='+str(dt))
        ax5.plot(avgtimes, avgvalues[:,1], label='Averaged dt='+str(dt))     
    elif type == 'mM':
        dtmax = factor*dt
        mmparticles, mmweights, cbtimes, cbvalues = match(model, tend, dt, Lx, Ly, dtmax)
        cbvalues = getMeanAndVariance(cbvalues)
        cbtimes, cbvalues = getRealPoints(cbtimes, cbvalues)
#        np.savetxt('data/mMmodeleps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'Lx'+str(Lx)+'Ly'+str(Ly)+endstring, np.vstack((cbtimes, cbvalues[:,1], cbvalues[:,2], cbvalues[:,3], cbvalues[:,4])))
#        np.savetxt('data/mMparticleseps'+str(eps)+'A='+str(A)+'dt='+str(dt)+'factor'+str(factor)+'tend'+str(tend)+'Lx'+str(Lx)+'Ly'+str(Ly)+endstring, np.vstack((mmparticles[:,0], mmparticles[:,1], mmweights)))
        
        ax3.plot(cbtimes, cbvalues[:,1], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax4.plot(cbtimes, cbvalues[:,3], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax5.plot(cbtimes, cbvalues[:,2], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
        ax6.plot(cbtimes, cbvalues[:,4], label='mM Lx = '+str(Lx)+'mM Ly = '+str(Ly)+' dtmax={0:.4f}'.format(factor))
    else:
        print('This experiment is not supported.')
            
    ax1.legend()
    ax2.legend()
    ax3.legend()
    ax4.legend()
    ax5.legend()
    ax6.legend()
    
    plt.show()
    
    
if __name__ == '__main__':
    args = parseArguments()
    eps = float(args.epsilon)
    A = float(args.A)
    experiment = args.type
    dt = eps/20.
    
    performExperiments(experiment, eps, dt, A, args.factor, int(args.Lx), int(args.Ly))

