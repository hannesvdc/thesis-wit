import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save
import matplotlib as mpl

from bimodal import *

import argparse
import fnmatch
import os
import bisect

def parseArguments():
    parser = argparse.ArgumentParser(description='Input parameters for plotting on the periodic model.')
    parser.add_argument('--plot', metavar='p', nargs='?', dest='plot',
                        help='Select which the type of plot.')
    parser.add_argument('--epsilon', metavar='e', nargs='?',dest='eps', 
                        help='Select the epsilon value to make the plots for.')
    parser.add_argument('--A', metavar='a', nargs='?',dest='A', 
                        help='Select the coarse noise level for the plots.')
    parser.add_argument('--tend', metavar='tend', nargs='?',dest='tend', 
                        help='Select the coarse noise level for the plots.')
    parser.add_argument('--files', metavar='c', nargs='+', dest='files',
                        help='Select what data to display in the plots.')
    parser.add_argument('--densify', metavar='d', nargs='?', dest='densify', 
                        help='Select what data to display in the plots.')
    
    args = parser.parse_args()
    return args

##############################################################
#              Several file helper functions                 #
##############################################################
# Get the type of data file
def getType(file):
    if "micro" in file:
        return "microscopic"
    elif "macro" in file:
        return "macroscopic"
    elif "mM" in file:
        return "micro-macro"
    else:
        return None
    
# return the histogram and transient data file accompanied with this file.
def getBimodalFiles(filename):
    print(filename)
    if "model" in filename:
        return filename, filename.replace("model", "particles")
    else:
        return filename.replace("particles", "model"), filename

# Make an appealing label from the given file name.
def getLabel(filename):
    filename = filename.replace("data/", "")
    filename = filename.replace(".data", "")
    filename = filename.replace("model", " ")
    return filename

# Get the epsilon value from a file name.
def getEpsilonFromFileName(file):
    index = file.find('eps')
    index += 4
    endindex = file.find('A', index)
    eps = float(file[index:endindex])
    
    return eps

# Get the extrapolation factor from a mM file.
def getFactorFromFileName(file):
    index = file.find('factor')
    index += 6
    factorindex = file.find('tend', index)
    factor = float(file[index:factorindex])
    
    return factor

# Get the noise level from a mM file.
def getNoiseLevelFromFileName(file):
    index = file.find('A')
    index += 2
    aindex = file.find('dt', index)
    factor = float(file[index:aindex])
    
    return factor

# Return the macro file.
def getMacroFile():
    for file in os.listdir('./data/'):
        if fnmatch.fnmatch(file, 'macromodel*'):
            return 'data/'+file
    
    return None

# Return all mM data files.
def getMMFiles(A):
    print(A)
    files = []
    for file in os.listdir('./data/'):
        print(getNoiseLevelFromFileName(file), file[0:6])
        if fnmatch.fnmatch(file, 'mMmodel*') and getNoiseLevelFromFileName(file) == A:
            files.append('data/'+file)
            
    return files



##############################################################
#              Several data helper functions                 #
##############################################################
# Compute the steady state value of the simulation
def getSteadyState(series):
    return np.average(series[-100:])
        
##############################################################
#                  Several plot functions                    #
##############################################################
def plotSolutions(files, tend, densify=False):
    meanfig = plt.figure()
    meanax1 = meanfig.add_subplot(111)
    plt.title('Slow Mean')
#    meanax2 = meanfig.add_subplot(212)
    plt.xlabel('Time [s]')
#    plt.title('Fast Mean')
    varfig = plt.figure()
    varax1 = varfig.add_subplot(111)
    plt.title('Slow Variance')
#    varax2 = varfig.add_subplot(212)
    plt.xlabel('Time [s]')
#    plt.title('Fast Variance')
    histfig = plt.figure()
    histax1 = histfig.add_subplot(111)
    plt.title('Slow Histogram')
#    histax2 = histfig.add_subplot(212)
    plt.xlabel(r'$X$')
#    plt.title('Fast Histogram')
    
    binwidth = 0.025
    for file in files:
        datafile, histfile = getBimodalFiles(file)
        data = np.loadtxt(datafile)
        histdata = np.loadtxt(histfile)
        label = getType(datafile)
        type = getType(file)
        times = data[0,:]
        if label == 'microscopic':
            label = label + ' model'
        elif label == 'macroscopic':
            label = label + ' model'
        elif label == 'micro-macro':
            label = label+r' $\Delta t = '+str(getFactorFromFileName(datafile))+r"\delta t$"
        
        index = np.argmax(times > tend)
        if index == 0:
            index = times.shape[0]

        if densify:
            meanax1.plot(times[0:index:10], data[1,0:index:10], label=label)
            varax1.plot(times[0:index:10], data[2,0:index:10], label=label)
        else:
            print(type)
            print(data.shape)
            meanax1.plot(times[:index], data[1,:index], label=label)
            varax1.plot(times[:index], data[2,:index], label=label)
            
        bins = min(int((np.max(histdata) - np.min(histdata))/binwidth),40)
        print('bins, ',bins)
        if getType(file) == 'macroscopic':
            histax1.hist(histdata, bins=bins,label=label, normed=True)
        else:
            histax1.hist(histdata[0,:], bins=bins,label=label, normed=True, alpha = 0.5)
        
        if getType(file) == 'microscopic' or getType(file) == 'micro-macro':
            print('bla')
#             meanax2.plot(times[:index], data[3,:index], label=type+'scopic model')
#             varax2.plot(times[:index], data[4,:index], label=type+'scopic model')
#             histax2.hist(histdata[1,:], bins=100, label=type+'scopic model', normed=True, histtype='step')
            
    meanax1.legend()
#    meanax2.legend()
    plt.figure(meanfig.number)
    tikz_save('plots/meanfigure.tex')
    varax1.legend()
#    varax2.legend()
    plt.figure(varfig.number)
    tikz_save('plots/varfigure.tex')
    histax1.legend()
#    histax2.legend()
    plt.figure(histfig.number)
    tikz_save('plots/histogramfigure.tex')
    
    plt.show()
    
# Plot the steady state means and variances for the macro and mM
# models as a function of epsilon
def plotSteadyStates(A):
    varfig = plt.figure()
    varax1 = varfig.add_subplot(211)
    plt.title("Slow steady state variance")
    varax2 = varfig.add_subplot(212)
    plt.title("Fast steady state variance")
    plt.xlabel('Extrapolation Factor')
    
    macrofile = getMacroFile()
    macrodata = np.loadtxt(macrofile)
    macrovariance = getSteadyState(macrodata[2,:])
    
    files = getMMFiles(A)
    epses = []
    factors = []
    slowsteadystates = []
    faststeadystates = []
    for file in files:
        data = np.loadtxt(file)
        eps = getEpsilonFromFileName(file)
        factor = getFactorFromFileName(file)
        print('new factor ', factor)
        if eps not in epses:
            print('eps', eps, 'not found')
            inserted = False
            for index in range(len(epses)):
                if eps < epses[index]:
                    epses.insert(index, eps)
                    slowsteadystates.insert(index, np.zeros(len(factors)))
                    faststeadystates.insert(index, np.zeros(len(factors)))
                    inserted = True
                    break
            if not inserted:
                epses.append(eps)
                slowsteadystates.append(np.zeros(len(factors)))
                faststeadystates.append(np.zeros(len(factors)))
        if len(epses) == 0:
            epses.append(eps)
            slowsteadystates.append(np.zeros(len(factors)))
            faststeadystates.append(np.zeros(len(factors)))
            
        if factor not in factors:
            print('factor', factor, 'not found')
            inserted = False
            for index in range(len(factors)):
                if factor < factors[index]:
                    factors.insert(index, factor)
                    for rowid in range(len(epses)):
                        slowsteadystates[rowid] = np.insert(slowsteadystates[rowid], index, 0.)
                        faststeadystates[rowid] = np.insert(faststeadystates[rowid], index, 0.)
                    inserted = True
                    break
                
            if not inserted:
                factors.append(factor)
                for rowid in range(len(epses)):
                    slowsteadystates[rowid] = np.append(slowsteadystates[rowid], 0.)
                    faststeadystates[rowid] = np.append(faststeadystates[rowid], 0.)
        if len(factors) == 0:
            factors.append(factor)
            for rowid in range(len(epses)):
                slowsteadystates[rowid] = np.append(slowsteadystates[rowid], 0.)
                faststeadystates[rowid] = np.append(faststeadystates[rowid], 0.)
        
        epsindex = epses.index(eps)
        factorindex = factors.index(factor)
        print('new steady states',getSteadyState(data[2,:]),getSteadyState(data[4,:]))        
        slowsteadystates[epsindex][factorindex] = getSteadyState(data[2,:])
        faststeadystates[epsindex][factorindex] = getSteadyState(data[4,:])
        print(slowsteadystates)
    
    print('----------------')
    print(epses)
    print(factors)
    print(slowsteadystates)
    for index in range(len(epses)):
        varax1.plot(factors, slowsteadystates[index], label='Eps = '+str(epses[index]))
        varax2.plot(factors, faststeadystates[index], label='Eps = '+str(epses[index]))
        
    varax1.legend()
    varax2.legend()
    
    plt.show()

# Plot the fast double-well potential and the fast autonomous density
def plotFastPotential(eps):
    V = lambda y: y**4/(4.*eps) - y**2/(2.*eps)
    dV = lambda y: y**3/eps - y/eps
    density = lambda y: np.exp(-V(y))
    Z, Zerror = scipy.integrate.quad(density, -100., 100.)
    normalizeddensity = lambda y: density(y)/Z
    print(Z)
    Z1, Z1error = scipy.integrate.quad(normalizeddensity, -100., 100.)
    print(Z1)
    
    plt.rc('text', usetex=True)
    font = {'family' : 'normal', 'size'   : 14}
    plt.rc('font', **font)

    y = np.linspace(-1.5,1.5,1000)
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(y, V(y), color='blue', label='Epsilon = '+str(eps))
    ax1.yaxis.set_label_coords(-0.12, 0.55)
    plt.xlabel(r'y')
    plt.ylabel(r'V(y)', rotation=0.)
    tikz_save('plots/potential.tex')
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(y, normalizeddensity(y)/Z, color='blue', label='Epsilon = '+str(eps))
    ax2.yaxis.set_label_coords(-0.1, 0.55)
    plt.xlabel(r'y')
    plt.ylabel(r'$\rho^{\infty}(y)$', rotation=0.)
    tikz_save('plots/density.tex')
    
    plt.show()

# Plot one stochastic path through the bimodal potential with given epsilon
def plotParticleMovement(eps, A, filter=True):
    filter=True
    dt = 0.0001
    A = 0.
    model = BimodalModel(eps, A)
    Y = np.zeros([1,2])
    Y[:,0] = 0.55
    Y[:,1] = -1.
    cb = lambda x: computeMoments(x, momentFunctions(1,1,False))
    yfig = plt.figure()
    tend = 2.
    for k in range(1,5):
        Z, times, values, particles = model.fullScaleMicroSimulation(np.copy(Y), 0., tend, dt, callback=cb, storeparticles=True)
        xvalues = particles[:,0]
        yvalues = particles[:,1]
        print(xvalues.shape)
        if filter:
            times = times[0::100]
            xvalues = xvalues[0::100]
            yvalues = yvalues[0::100]
        print(xvalues.shape)    
        plt.plot(times, yvalues, label='Fast Component')
#        plt.plot(times, xvalues, color='r', label='Slow Component')

    plt.xlabel('Time[s]')
    plt.ylabel('X, Y')
#    plt.legend()
    tikz_save('plots/particlemovement.tex')
    plt.show()

# Plot the slow histograms on top of each other
def plotSlowHistograms(files):
    mpl.rc('font', size=18)
    for file in files:
        data = np.loadtxt(file)
        if getType(file) is 'micro':
            plt.hist(data[0,:], bins=50, normed=True, label='Micro')
        elif getType(file) is 'macro':
            plt.hist(data, bins=50, normed=True, label='Macro')
        elif getType(file) is 'mM':
            particles = data[0,:]
            weights = data[2,:]
            plt.hist(particles, weights=weights, bins=50, normed=True, label='mM')
    
    print('Showing plots')
    plt.legend()
    plt.show()
    
def getList(args):
    if type(args) is list:
        return args
    else:
        return [args]   
    
if __name__ == '__main__':
    args = parseArguments()

    if args.densify == '0':
        densify = False
    else:
        densify=True
        
    if args.plot == 'plotSolutions':
        plotSolutions(getList(args.files), float(args.tend), densify)
    elif args.plot == 'plotSteadyStates':
        plotSteadyStates(float(args.A))
    elif args.plot == 'plotFastPotential':
        plotFastPotential(float(args.eps))
    elif args.plot == 'plotParticleMovement':
        plotParticleMovement(float(args.eps), float(args.A), densify)
    elif args.plot == 'plotSlowHistograms':
        plotSlowHistograms(getList(args.files))
    else:
        print("This plot type is not supported.")