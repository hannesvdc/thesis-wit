import sys
sys.path.append("../../")

from momentfunction import *

# Standard normalised polynomial moment functions in one dimension.
class PolynomialMoments(MomentFunction):
    
    # Construct the moment objects with the given powers and 
    # normalisation constant.
    def __init__(self, degrees, scale=1.):
        self.scale = scale
        self.moments = lambda X: np.asarray(list(map(lambda p: (X/scale)**(p), degrees)))
    
    # Compute the moments for a given weighted set of samples. Not yet
    # parallelised which is due for further releases.
    def evaluateMoments(self, X, weights=None):
        N = self.X.shape[0]
        if weights is None:
            weights = np.ones(N)
            
        sum = 0.
        for i in range(N):
            sum = sum + self.moments(X[i])*weights[i]
            
        return sum/np.sum(weights)
    