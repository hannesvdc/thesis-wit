import sys
sys.path.append("../../")

# Base class for moment functions that can be used in CPI.
class MomentFunction:
    
    def __init__(self):
        pass
    
    def evaluateMoments(self, X, weights=None):
        print("Function is not implemented. Choose a proper instantiation")
        pass