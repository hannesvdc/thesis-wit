import sys
sys.path.append("../../")

from momentfunctions.moment_function import *

# Standard normalised polynomial moment functions in one dimension.
class NormalisedPolynomialMoments(MomentFunction):
    
    # Construct the moment objects with the given powers and 
    # normalisation constant.
    def __init__(self, degree, scale=1.):
        self.degree = degree
        self.scale = scale
        self.moments = lambda X: np.asarray(list(map(lambda p: (X/scale)**(p), range(2, degree+1))))
    
    # Compute the moments for a given weighted set of samples. Not yet
    # parallelised which is due for further releases.
    def evaluateMoments(self, X, weights=None):
        N = self.X.shape[0]
        if weights is None:
            weights = np.ones(N)
        
        mu = 0.
        std = 1.
        weightsum = np.sum(weights)
        result = [1.]
        if self.degree > 0: 
            mu = np.dot(X, weights)/weightsum
            result.append(mu)
        if self.degree > 1:
            std = np.sqrt(np.dot((X-mu)**2, weights)/weightsum)
            result.append(std**2)
            
        if self.degree > 2:
            sum = 0.
            for i in range(N):
                sum = sum + self.moments((X[i]-mu)/std)*weights[i]
            result.append(sum/weightsum)
            
        return result