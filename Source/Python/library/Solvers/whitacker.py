import scipy.linalg

from Solvers import iterative_solver

class WhitackerSolver(iterative_solver.Solver):
    def __init__(self, function, jacobian, param=None):
        super().__init__(param)
        self.function = function
        self.jacobian = jacobian
        self.residual = function
    
    def solve(self, x0):
        # Initialize the iterative solver
        x = x0
        k = 0
        f = self.function(x)
        jac = self.jacobian(x)
        
        # Iterate until the stop condition is met
        while scipy.linalg.norm(f) > self.param['abs_tol'] and k <= self.param['nb_iter']:
            print("\n\nNew solver step")
            try:
                dx = scipy.linalg.solve(jac, f)
            except: # singular jacobian
                print("Exception while solving linear system in NR iteration")
                return None
            
            x = x - dx
            f = self.function(x)
            
            print("Lambdas ", x)
            print("gradient ", f)
            k+=1
        
        if scipy.linalg.norm(f) > self.param['abs_tol']:
            return None
        
        return x

