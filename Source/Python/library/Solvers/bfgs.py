import scipy.linalg
import numpy as np

from library.Solvers import iterative_solver

class BFGSSolver(iterative_solver.Solver):
    def __init__(self, function, jacobian, param=None):
        super().__init__(param)
        self.function = function
        self.jacobian = jacobian
        
    def solve(self, x0):
        # Initialize the iterative solver
        x = x0
        k = 0
        f = self.function(x)
        B = self.jacobian(x)
        
        while scipy.linalg.norm(f) > self.param['abs_tol'] and k <= self.param['nb_iter']:
            print("\n\nNew BFGS step")
            
            try:
                dx = scipy.linalg.solve(B, f)
            except:
                print("Exception while solving linear system in BFGS iteration.")
                return False, x
                
            xnew = x - dx
            try:
                fnew = self.function(xnew)
            except:
                print("Exception while computing function value in BFGS solver.")
                return False, x
                
            # Perform the well known BFGS update
            yk = fnew - f
            sk = xnew - x
            Bsk = B.dot(sk)
            B = B + np.outer(yk, yk)/np.dot(yk, sk) - np.outer(Bsk, Bsk)/np.dot(sk, Bsk)
            f = fnew
            x = xnew
            
            print("Lambdas ", x)
            print("gradient ", f)
            k += 1
            
            
        if scipy.linalg.norm(f) > self.param['abs_tol']:
            return False, x
            
        return True, x