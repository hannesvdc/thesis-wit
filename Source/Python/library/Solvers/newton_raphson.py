import scipy.linalg
import numpy as np

from library.Solvers import iterative_solver

from library.tools.verboseprint import *

class NRSolver(iterative_solver.Solver):
    def __init__(self, function, jacobian, param=None, verbose=True):
        super().__init__(param)
        self.function = function
        self.jacobian = jacobian
        self.residual = function
        self.verbose = verbose
    
    def step(self,x):
        dx = scipy.linalg.solve(self.jacobian(x), self.function(x))
        verboseprint(self.verbose, "Lambdas ", x-dx)
        verboseprint(self.verbose, "gradient ", self.function(x))
        return x - dx
        
    def solve(self, x0):
        np.seterr(all='warn')
        
        # Initialize the iterative solver       
        x = x0      
        k = 0
        verboseprint(self.verbose,"\n\nInitial")
        verboseprint(self.verbose,"Lambdas ", x)
        f = self.function(x)
        verboseprint(self.verbose,"gradient ", f)
        
        # Iterate until the stop condition is met
        while scipy.linalg.norm(f) > self.param['abs_tol'] and k <= self.param['nb_iter']:
            verboseprint(self.verbose,"\n\nNew solver step")
            try:
                jac = self.jacobian(x)
            except:
                verboseprint(self.verbose,"Exception while computing the jacobian")
                return False, x
            
            try:
                dx = scipy.linalg.solve(jac, f)
            except: # singular jacobian
                verboseprint(self.verbose,"Exception while solving linear system in NR iteration.")
                return False, x
            
            x = x - dx
            
            try:
                f = self.function(x)
            except:
                verboseprint(self.verbose,"Exception while computing function value in NR solver.")
                return False, x
            
            verboseprint(self.verbose,"Lambdas ", x)
            verboseprint(self.verbose,"gradient ", f)
            k+=1
        
        if scipy.linalg.norm(f) > self.param['abs_tol']:
            return False, x
            
        return True, x
