import scipy

CONVERGENCE = 1
FIXED_NB_ITERATIONS = 2

class Solver(object):
    def __init__(self, param=None):
        if param == None:
            param = Solver.getDefaultParameters()
        if param['abs_tol']!=None:
            self.stop_criterion = CONVERGENCE
        elif param['nb_iter']!=None:
            self.stop_criterion = FIXED_NB_ITERATIONS
        else:
            raise ValueError("Bad combination of input parameters")
        self.param = param

    def getDefaultParameters():
        param = {}
        param['abs_tol'] = 1e-8
        param['nb_iter'] = None
        return param
    getDefaultParameters = staticmethod(getDefaultParameters)
    
    def step(self, x0):
        pass
        
    def stop(self, x, k):
        if k > self.param['nb_iter']:
            return True

        return (scipy.linalg.norm(self.residual(x)) < self.param['abs_tol'])
    
    def solve(self, x0):
        # Initialize the iterative solver       
        x = x0      
        k = 0
        
        # Iterate until the stop condition is met
        while not self.stop(x,k):
            print("\n\nNew solver step")
            x = self.step(x)
            k+=1
            
        if k > self.param['nb_iter']:
            return None
            
        return x
        