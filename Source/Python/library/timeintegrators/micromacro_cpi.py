import numpy as np
import numbers

from library.timeintegrators import cpi

from library.tools.verboseprint import *

class MicroMacroCPI(cpi.CPI):
    def __init__(self, params, matcher, microstepper, R):
        super().__init__(params, matcher, microstepper)
        self.R = R
        
    def simulate(self, X, weights, tbegin, tend, dt, callback=None, adaptive=True, storeParticles=False, verbose=True):
        # Define meta-variables to return extra information
        matchfails = 0
        sumDt = 0.
        sumsqDt = 0.
        nsteps = 0
        prevDt = 0.

        # Get simulation parameters
        alfadown = self.params['alfadecrease']
        alfaup = self.params['alfaincrease']
        K = self.params.get('K', 1)
        dtmax = self.params['Dtmax']
        dtmin = self.params.get('Dtmin', K*dt)
        max_entropy = np.log10(weights.size)
        
        # Start the simulation
        microstepper = self.microstepper
        macromoments = self.computeMoments(X, weights)
        times = []
        results = []
        matchtimes = []
        matchfailtimes = []
        particleHistory = []
        weightHistory = []
        if callback is not None:
            cbmoments = callback(X, weights)
            times.append(tbegin)
            results.append(cbmoments)
        if storeParticles:
            particleHistory.append(np.copy(X))
            weightHistory.append(np.copy(weights))
        t = tbegin;
        Dt = self.params.get('Dt0', dtmin)
        while t+dtmin <= tend:
            verboseprint(verbose, "\n\n\n")
            # Perform K microsteps
            Y = microstepper.integrate(X, t, t+K*dt, dt)
            micromoments = self.computeMoments(Y, weights)
            verboseprint(verbose, "Micromoments ", micromoments)

            if callback is not None:
                times.append(t+K*dt)
                microcbmoments = callback(Y, weights)
                results.append(microcbmoments)
            if storeParticles:
                particleHistory.append(np.copy(Y))
                weightHistory.append(np.copy(weights))    
                    
            # Match until convergence
            while True:
                verboseprint(verbose,"Step size ", Dt)
                 
                # Extrapolate
                newmacromoments = (1.-Dt/(K*dt))*macromoments + Dt/(K*dt)*micromoments
                verboseprint(verbose,"Extrapolated moments ", newmacromoments)
                
                # Match
                if Dt > dt:
                    converged, newlambdas = self.matcher.match(Y, newmacromoments, weights)
                else:
                    converged = True
                    newlambdas = np.zeros_like(newmacromoments)
                    
                if not converged:
                    verboseprint(verbose,'Matching failure!')
                    matchfails = matchfails + 1
                    matchfailtimes.append(t+K*dt)
                        
                if adaptive == False or converged == True:
                    verboseprint(verbose,"Solver converged.")
                    verboseprint(verbose,"extrapolated moments ", newmacromoments)
                    
                    # Store extrapolated moments if wanted by user
                    if callback is not None:
                        newcbmoments = callback(Y, weights)
                        extrapolatedcbmoments = (1.-Dt/(K*dt))*cbmoments + Dt/(K*dt)*(microcbmoments)
                        verboseprint(verbose,"Extrapolated callback moments ", extrapolatedcbmoments)
                        times.append(t+Dt)
                        results.append(extrapolatedcbmoments)
                    
                    # Update CPI statistics
                    sumDt = sumDt + Dt
                    sumsqDt = sumsqDt + Dt*Dt
                    nsteps = nsteps + 1
                    prevDt = Dt

                    # And put everything in place for the next iterations
                    newweights = self.matcher.weights(newlambdas, Y, weights)
                    if storeParticles:
                        particleHistory.append(np.copy(Y))
                        weightHistory.append(newweights)
                    weights = newweights
                    macromoments = self.computeMoments(Y, weights)
                    t = t + Dt
                    Dt = max(min(min(alfaup*Dt, dtmax), tend-t), dtmin)
                    X = Y
                    verboseprint(verbose,"Posterior moments ", macromoments)
                    
                    # And finally store the callback moments after matching too
                    if callback is not None:
                        times.append(t)
                        cbmoments = callback(X, weights)
                        results.append(cbmoments)
                        verboseprint(verbose,"Callback after matching at time ", t, cbmoments)
                        
                    break
                
                verboseprint(verbose,"Iterative solver did not converge, lowering extrapolation step.")
                Dt = max(alfadown*Dt, dtmin)
                verboseprint(verbose,"New step size ", Dt)
            
            # Resampling if the entropy of the weights is too large for accurate computations
            entropy = self.matcher.entropy(weights)
            verboseprint(verbose,"Entropy of weights", entropy)
            if entropy > max_entropy/10:
                verboseprint(verbose,"Resampling")
                indices = self.resample(weights, 5000)
                if len(X.shape) > 1:
                    X = np.copy(X[indices, :])
                    weights = np.ones(X.shape[0])
                else:
                    X = X[indices]
                    weights = np.ones(X.size)
        
	    # For statistical reasons, remove the last Dt because this is usually very small and corrupts the mean and variance of Dt
        sumDt = sumDt-prevDt
        sumsqDt = sumsqDt - prevDt*prevDt
        nsteps = max(nsteps - 1, 1)
        verboseprint(verbose,nsteps)
        verboseprint(verbose,sumsqDt - sumDt*sumDt)
        verboseprint(verbose,'variance',(sumsqDt - sumDt*sumDt)/(nsteps-1))
        verboseprint(verbose,'Matchfails ', matchfails)
        
        # Build the result dictionary
        result = {'points': X, 'weights': weights, 'matchfails': matchfails, 'matchfailtimes': matchfailtimes,'meanDt': sumDt/nsteps, 'varDt': (sumsqDt - sumDt*sumDt)/(nsteps-1)}
        if callback is not None:
            result.update({'cbtimes': np.asarray(times), 'cbvalues': np.asarray(results)})
        if storeParticles:
            result.update({'particleHistory': particleHistory, 'weightHistory': weightHistory})
    
        return result
        
    def computeMoments(self, points, weights=None):
        N = points.shape[0]
        if weights is None: weights = np.ones(N)/N
        
        weightsum = np.sum(weights)
        result = 0.
        for i in range(N):
            result = result + self.R(points[i])*weights[i]/weightsum
        
        return result
        
    def resample(self, weigths, seed):
        # resampling (stratified)

        # seed setting
        np.random.seed(seed)
        np.random.randn(10**3)  # warm up of RNG

        nb_sam = weigths.size
        # stratified random numbers in [0,1)
        strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

        # normalized cumulative sum of weights
        ncs = np.cumsum(weigths) / np.sum(weigths)

        rsample = np.zeros_like(weigths, dtype=int)  # resampling

        shelf = 0
        ind = 0
        while max(shelf, ind) < nb_sam:
            count = 0
            while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
                rsample[ind] = int(shelf)  # duplication of samples
                count += 1
                ind += 1
            shelf += 1
        return rsample
