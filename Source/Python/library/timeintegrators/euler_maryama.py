from library.timeintegrators import monte_carlo_integrator

from math import *
import time

import numpy as np
import numpy.random as rd

class EulerMaruyamaIntegrator(monte_carlo_integrator.MonteCarloIntegrator):
    def __init__(self, a, b, verbose=True):
        self.randomGenerator = np.random.RandomState(int(round(time.time() * 1000))%(2**32))
        super().__init__(a, b, verbose=verbose)
        
    def step(self, X, t, dt):
        return X + self.a(t, X)*dt + self.b(t, X, self.randomGenerator.normal(0, 1, X.shape))*sqrt(dt)
        