import numpy as np

from library.tools.verboseprint import *

class MonteCarloIntegrator:
    def __init__(self, a, b, verbose=True):
        self.a = a
        self.b = b
        self.verbose = verbose
    
    def step(self, X, t, dt):
        pass
        
    def integrate(self, X, tbegin, tend, dt):
        t = tbegin
        N = X.shape[0]
        while t +dt <=  tend:
            verboseprint(self.verbose, t)

            for i in range(N):
                X[i] = self.step(X[i], t, dt)
            t = t + dt

        return X
        
    def transitionProbability(self, x, xprime, dt):
        pass
