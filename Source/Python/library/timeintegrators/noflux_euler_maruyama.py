from timeintegrators import euler_maryama

import numpy.random as rd
import numpy as np

class NoFluxEulerMaryama(euler_maryama.EulerMaryamaIntegrator):
    def __init__(self, a, b):
        super().__init__(a, b)
        
    def step(self, X, t, dt):
        X = super().step(X, t, dt)
        X = np.absolute(X)
            
        return X