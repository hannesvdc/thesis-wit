from library.timeintegrators import euler_maryama

import numpy as np

class GridNoFluxEulerMaruyama(euler_maryama.EulerMaruyamaIntegrator):
    # Grid in the form [a, b, c, d] for square [a,b]x[c,d]
    def __init__(self, a, b, grid):
        super().__init__(a, b)
        self.grid = grid
        
    def step(self, X, t, dt):
        X = np.squeeze(np.asarray(super().step(X, t, dt)))
        if X[0] < self.grid[0]:
            X[0] = self.grid[0] + (self.grid[0]-X[0])
        if X[0] > self.grid[1]:
            X[0] = self.grid[1] - (X[0]-self.grid[1])
        if X[1] < self.grid[2]:
            X[1] = self.grid[2] + (self.grid[2]-X[1])
        if X[1] > self.grid[3]:
            X[1] = self.grid[3] - (X[1]-self.grid[3])
        
        return X
