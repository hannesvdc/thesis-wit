from timeintegrators import euler_maryama
import numpy.random as rd

class AcceptRejectEulerMaryama(euler_maryama.EulerMaryamaIntegrator):
    def __init__(self, a, b, acceptcriterion):
        super().__init__(a, b)
        self.accept = acceptcriterion
        
    def step(self, X, t, dt):
        x = super().step(X, t, dt)
        while not self.accept(x):
            x = super().step(X, t, dt)
        
        return x

