class CPI:
    def __init__(self, params, matcher, microstepper):
        if params['K'] is None:
            params['K'] = 1
        if params['Dtmax'] is None:
            params['Dtmax'] = 1.
        if params['alfadecrease'] is None:
            params['alfadecrease'] = 0.5
        if params['alfaincrease'] is None:
            params['alfaincrease'] = 1.2
        
        self.params = params
        self.matcher = matcher
        self.microstepper = microstepper
        
    def simulate(self, X, weights, tbegin, tend, dt, callback=None):
        raise NotImplementedError("Coarse projective integrator not implemented.")