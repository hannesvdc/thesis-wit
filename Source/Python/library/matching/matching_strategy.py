class MatchingStrategy:
    def match(self, weights=None, initial=None, entropy=False):
        raise NotImplementedError("Matching not implemented not implemented.")
    
    def weights(self, lamdas, weights=None):
        raise NotImplementedError("Reweighing not implemented.")
    