import numpy as np
from scipy.stats import *
import scipy as sp

from math import *

from matching.matching_strategy import *

class L2NMatchingStrategy(MatchingStrategy):
    def __init__(self, R, gamma, matrix=None):
        self.R = R
        self.gamma = gamma
        self.H = matrix
    
    def computeMoments(self, weights=None):
        if weights is None: weights = np.ones(self.X.size)/self.X.size
        
        weightsum = np.sum(weights)
        R0 = self.R(0)
        result = np.zeros(R0.size)
        
        for i in range(self.X.size):
            result = result + self.R(self.X[i])*weights[i]/weightsum
        
        return result
    
    def match(self, X, moments):
        self.X = np.copy(X)
        self.moments = np.copy(moments)
        
        if self.H is None:
            print("Left hand side for L2N matching not supplied.")
            return
        else:
            H = self.H
        m = self.moments - self.computeMoments()
        
        lambdas = sp.linalg.solve(H, m)
        print("Lambdas ", lambdas)
        return lambdas
        
    def weights(self, lambdas, X, weights=None):
        import sklearn as skl
        from sklearn.neighbors.kde import KernelDensity
        from sklearn.grid_search import GridSearchCV
        
        if weights is None:
            weights = np.ones(X.size)/X.size
            
        # denisty computation
        kde = KernelDensity(kernel='gaussian', bandwidth=0.01*self.gamma, rtol=1E-5).fit(X[:, np.newaxis])
        pdf = kde.score_samples(X[:, np.newaxis])
        pdf = np.exp(pdf)
        w = np.zeros(X.size)
        
        for j in range(X.size):
            Rj = self.R(X[j])
            sum = np.dot(lambdas, Rj)
        
            w[j] = weights[j]*(sum/pdf[j] + 1)
            
        return w
        