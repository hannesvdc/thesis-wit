import numpy as np

from math import *

from matching.matching_strategy import *

class L2DMatchingStrategy(MatchingStrategy):
    def __init__(self, R):
        self.R = R
    
    def match(self, X, moments, weights=None):
        self.X = np.copy(X)
        self.moments = np.copy(moments)
        
        tolerance = 1.e-9
        lambdas = np.zeros(self.R(0).size)
        steps = 0
        
        initialmoments = self.computeMoments(self.X)
        # Simplified Newton-Raphson iteration
        while True:
            if steps > 10:
                print("Newton solver did not converge for L2D matching.")
                return None
                
            hess = self.hessian(lambdas)
            grad =  self.moments + hess.dot(lambdas) - initialmoments
            
            if np.linalg.norm(grad) < tolerance:
                return lambdas #self.weights(lambdas, np.ones(self.X.size))
                
            lambdas = lambdas - np.linalg.solve(hess, grad)
            steps += 1
        
    def hessian(self, lambdas):
        print("Computing hessian with lambdas = ", lambdas)
        n = self.R(0).size
        hess = np.empty([n, n])
        
        for k in range(n):
            for l in range(k+1):
                sum = 0.0
                
                for j in range(self.X.size):
                    Rj = self.R(self.X[j])
                    lambdasum = np.dot(lambdas, Rj) + 1
                        
                    if lambdasum > 0:
                        sum = sum + Rj[k]*Rj[l]/self.X.size
                    
                hess[k, l] = -sum
                hess[l, k] = -sum
        print(hess)        
        return hess
        
    def weights(self, lambdas, X, weights=None):
        if weights is None:
            weights = np.ones(X.size)
            
        w = np.zeros(X.size)
        for j in range(X.size):
            w[j] = weights[j]*max(0., 1. + np.dot(lambdas, self.R(X[j])))
        print("Weights")
        print(w)
        return w
        
    def computeMoments(self, points, weights=None):
        if weights is None: weights = np.ones(points.size)/points.size
        
        weightsum = np.sum(weights)
        R0 = self.R(0)
        result = np.zeros(R0.size)
        
        for i in range(points.size):
            result = result + self.R(points[i])*weights[i]/weightsum
        
        return result