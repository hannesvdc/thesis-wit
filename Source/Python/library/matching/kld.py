import numpy as np 

from functools import partial

from math import *
from scipy.stats import multivariate_normal

from library.Solvers.newton_raphson import *
from library.Solvers.bfgs import *
from library.matching.matching_strategy import *

class KLDMatchingStrategy(MatchingStrategy):
        
    def __init__(self, R, optimizer='NR', iterations = 6, verbose=True):
        self.R = R
        self.optimizer = optimizer
        self.iterations = iterations
        self.verbose = verbose
    
    @staticmethod
    def computeDistance(prior, points, priorweights=None, weights=None):
        if priorweights is None:
            priorweights = np.ones(prior.shape[0])
        if weights is None:
            weights = np.ones(points.shape[0])
        N = prior.shape[0]
        if len(prior.shape)>1:
            d = prior.shape[1]
        else:
            d=1
            
        priorMean = np.average(prior, weights=priorweights, axis=0)
        priorVar = 0.
        for i in range(prior.shape[0]):
            priorVar += np.outer(prior[i]-priorMean, prior[i]-priorMean)*priorweights[i]
        priorVar = priorVar/np.sum(priorweights)
                                             
        pointsMean = np.average(points, weights=weights, axis=0)
        pointsVar = 0.
        for i in range(points.shape[0]):
            pointsVar += np.outer(points[i]-pointsMean, points[i]-pointsMean)*weights[i]
        pointsVar = pointsVar/np.sum(weights)

        density = multivariate_normal.pdf(points, pointsMean, pointsVar)
        priordensity = multivariate_normal.pdf(points, priorMean, priorVar)

        sum = 0.
        for i in range(points.shape[0]):
            sum += np.log(density[i]/priordensity[i]) * weights[i]
        sum = sum / np.sum(weights)
        
        return sum
        
    def hessian(self, lambdas, weights):
        weightsum = np.sum(weights)
        sum = 0.
        
        for j in range(self.X.shape[0]):
            Rj = self.R(self.X[j]);
            prod = np.dot(lambdas, Rj)
            sum = sum + np.outer(Rj, Rj)*exp(prod)*weights[j]/weightsum
            
        return -sum;
        
    def gradient(self, lambdas, weights):
        weightsum = np.sum(weights)
        sum = 0.
        
        for i in range(self.X.shape[0]):
            Ri = self.R(self.X[i])
            prod = np.dot(lambdas, Ri)
            sum = sum + Ri*exp(prod)*weights[i]
        sum = sum/weightsum
   
        return self.moments - sum
        
    def weights(self, lambdas, X, weights=None):
        N = X.shape[0]
        if weights is None:
            weights = np.ones(N)
          
        w = np.zeros(N)
        verboseprint(self.verbose, lambdas)
        for j in range(N):
            try:
                w[j] = weights[j]*exp(np.dot(lambdas, self.R(X[j])))
            except (BaseException, BaseException) as e:
                print(e)
                w[j] = weights[j]
            except BaseException as e:
                w[j] = weights[j]
        return w
        
    def match(self, X, moments, weights=None, initial=None, entropy=False):
        self.X = np.copy(X)
        self.moments = np.copy(moments)
        
        if weights is None:
            weights = np.ones(self.X.shape[0])
        
        if initial is None:
            initial = np.zeros(self.moments.size)
        
        gradient = partial(self.gradient, weights=weights)
        hessian = partial(self.hessian, weights=weights)
        if self.optimizer == 'NR':
            params = {'abs_tol': 1e-9, 'nb_iter': self.iterations}
            
            self.solver = NRSolver(gradient, hessian, params, verbose=self.verbose)
        elif self.optimizer == 'BFGS':
            params = {'abs_tol': 1e-9, 'nb_iter': self.iterations}
            
            self.solver = BFGSSolver(gradient, hessian, params)
        conv, lambdas = self.solver.solve(initial)
        
        if entropy is False:
            return conv, lambdas
        else:
            return conv, lambdas, self.moments.dot(lambdas) - np.average(np.asarray(list(map(lambda i: np.exp(lambdas.dot(self.R(self.X[i])))-1., range(weights.size)))), axis=0,weights=weights)
    
        
    def entropy(self, weights):
        nweights = weights / np.sum(weights)
        return np.sum(nweights * np.log(nweights.size*nweights))
        
    def computeMoments(self, points, weights=None):
        N = points.shape[0]
        if weights is None: weights = np.ones(N)/N
        
        weightsum = np.sum(weights)
        result = 0.
        
        for i in range(N):
            result = result + self.R(points[i])*weights[i]/weightsum
        
        return result
