import numpy as np

from timeintegrators.gridnoflux_eulermaruyama import *
from timeintegrators.euler_maryama import *

class PeriodicModel:
    def __init__(self, A, D, P, a):
        self.a = lambda t, x: A.dot(x) + P*sin(a*t)
        self.b = lambda t, x, dw: D.dot(dw)
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None):
        timestepper = EulerMaryamaIntegrator(self.a, self.b)
            
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt

        return X, np.asarray(cbtimes), np.asarray(cbvalues)