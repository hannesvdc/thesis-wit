import numpy as np

from timeintegrators.euler_maryama import *

class PotentialModel:
    def __init__(self, dV, A):
        self.a = lambda t, x: -dV(x)
        self.b = lambda t, x, dw: A(x)*dw
        
    def fullScaleMicroSimulation(self, X, tbegin, tend, dt, callback=None, storeParticles=False):
        timestepper = EulerMaryamaIntegrator(self.a, self.b)
            
        if callback is None:
            return timestepper.integrate(X, tbegin, tend, dt)

        t = tbegin
        cbtimes = [t]
        cbvalues = [callback(X)]
        particles = [np.copy(X)]
        while t < tend:
            X = timestepper.integrate(X, t, t+dt, dt)
            cbtimes.append(t+dt)
            cbvalues.append(callback(X))
            t += dt
            
            if storeParticles:
                particles.append(np.copy(X))

        if storeParticles:
            return X, np.asarray(cbtimes), np.asarray(cbvalues), particles
        else:
            return X, np.asarray(cbtimes), np.array(cbvalues)