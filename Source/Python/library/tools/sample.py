import numpy as np
import numpy.random as rd

def acceptrejectuniform(dist, a, b, h, N):
    samples = []
    while len(samples) < N:
        x = rd.uniform(a, b)
        y = rd.uniform(0, 1)
        if y < dist(x)/h:
            samples.append(x)
            
    return np.asarray(samples)