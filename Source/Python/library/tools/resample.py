import numpy as np

def resample(sample, weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = len(sample)
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros(nb_sam)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample