import sys
sys.path.append("../../")

import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

import json
import argparse

from tools.plots.plots import *

# Parse the input arguments
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments to make a convergence plot')
    parser.add_argument('--config', metavar='c', nargs=1, dest='config',
                        help='Select the location of the plot config file')
    
    args = parser.parse_args()
    return args

# Plot all numerical data in loglog scale
def plotDensities(lines, interval, plottype):
    for j in range(len(lines)):
        format = lines[j]
        X = np.loadtxt(format.get('datafile', ''))
        bandwidth = format.get('bandwidth', 1.)
        npoints = format.get('plotpoints', 1000)
        plotpoints = np.linspace(interval[0], interval[1], npoints)
        density = kde(X, plotpoints, bandwidth)
        
        if plottype == "linear":
            plt.plot(plotpoints, density, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'semilogy':
            plt.semilogy(plotpoints, density, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'loglog':
            plt.loglog(plotpoints, density, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
            
# Finish the plot
def makePlot(config, interval):
    plt.title(config.get('title', ''))
    plt.xlabel(config.get('xlabel', ''))
    plt.xlim(interval[0], interval[1])
    if config.get('legend', False):
        plt.legend()
        
if __name__ == "__main__":
    args = parseArguments()
    
    config = json.load(open(args.config[0]))
    plottype = config.get('plottype', 'linear')
    interval = config.get('interval', [0,0])

    plotDensities(config.get('lines', {}), interval, plottype)
    makePlot(config, interval)
    
    tikz_save(config.get('savelocation', None))
    plt.show()