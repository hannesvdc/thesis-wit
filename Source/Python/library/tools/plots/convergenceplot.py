import numpy as np
import matplotlib.pyplot as plt
from matplotlib2tikz import save as tikz_save

import json
import argparse

# Parse the input arguments
def parseArguments():
    parser = argparse.ArgumentParser(description='Input arguments to make a convergence plot')
    parser.add_argument('--config', metavar='c', nargs=1, dest='config',
                        help='Select the location of the plot config file')
    parser.add_argument('--data', dest='data', nargs=1, help='Location of the data file for this plot')

    args = parser.parse_args()
    return args

# Preprocess the data file and write everything to a temporary file
def getData(file):
    input = open(file, 'r')
    data = np.empty([1,1])
    
    for i, line in enumerate(input):
        if not line.startswith('#'):
            ar = np.fromstring(line, sep=' ')

            if data.size== 1:
                data = ar
            else:
                data = np.vstack((data, ar))
    
    print(data)        
    return data
    
# Plot all numerical data in loglog scale
def plotData(data, lines, plottype):
    xdata = data[0,:]
    
    for j in range(len(lines)):
        format = lines[j]
        dataline = format.get('dataline', 1)
        ydata = data[dataline,:]
        
        if plottype == "linear":
            plt.plot(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'semilogy':
            plt.semilogy(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'loglog':
            plt.loglog(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
    
# Plot possible reference lines
def plotReferences(data, references, plottype):
    xdata = data[0,:]
    
    for j in range(len(references)):
        format = references[j]
        order = format.get('order', 0)
        ydata = format.get('offset', 1.0)*np.power(xdata, -order)
        
        if plottype == 'linear':
            plt.plot(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'semilogy':
            plt.semilogy(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
        elif plottype == 'loglog':
            plt.loglog(xdata, ydata, linestyle=format.get('style','-'), color=format.get('color', 'b'), label=format.get('label',''))
    
# Finish the plot
def makePlot(config):
    plt.title(config.get('title', ''))
    plt.xlabel(config.get('xlabel', ''))
    plt.ylabel(config.get('ylabel', ''))
    if config.get('legend', False):
        plt.legend()
        
if __name__ == "__main__":
    args = parseArguments()
    
    data = getData(args.data[0])
    config = json.load(open(args.config[0]))
    plottype = config.get('plottype', 'linear')
    
    plotData(data, config.get('lines', {}), plottype)
    plotReferences(data, config.get('references',{}), plottype)
    makePlot(config)
    
    tikz_save(config.get('savelocation', None))
    plt.show()
    