import numpy as np

# kernel density estimation
def kde(sample, grid, bw):
    import sklearn as skl
    from sklearn.neighbors.kde import KernelDensity
    from sklearn.grid_search import GridSearchCV
    
    # denisty computation
    kde = KernelDensity(kernel='gaussian', bandwidth=bw, rtol=1E-5).fit(sample[:, np.newaxis])
    log_pdf = kde.score_samples(grid[:, np.newaxis])
    
    #dens = sm.nonparametric.KDEUnivariate(self.traj[:,step_nr])
    #dens.fit(bw = "silverman", adjust = g*10**(-2))#kernel = "triw", 
    #bw = "silverman", fft = False, cut = self.model.param['gamma'])
    return np.exp(log_pdf)