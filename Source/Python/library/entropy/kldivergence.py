import sys
sys.path.append('../')

from tools.resample import *

import numpy as np
from sklearn.neighbors.kde import KernelDensity
    
def getridofnullvaluesinfront(bins, vals):
    nnul = 0
    startindex = 0
    for index in range(len(bins)-1):
        if vals[index] == 0.:
            if nnul == 0:
                startindex = index
            nnul = nnul +1
            continue
        
        if nnul == 0:
            continue
        
        mass = vals[index]
        dx = bins[index+1] - bins[startindex]
        
        for indexprime in range(startindex, index+1):
            vals[indexprime] = (bins[indexprime+1]-bins[indexprime])/dx*mass

        nnul = 0
    
    return bins, vals

def kldivergence_particles(X1, w1, X2, w2, bins1=20, bins2=20):
    vals1, bins1 = np.histogram(X1, weights=w1, bins=bins1, normed=True)
    vals2, bins2 = np.histogram(X2, weights=w2, bins=bins2, normed=True)
    
    # Make the histogram edges compatible for computing the KLD.
    if bins1[0] < bins2[0]:
        mass = (bins2[1] - bins2[0])*vals2[0]
        bins2[0] = bins1[0]
        vals2[0] = mass/(bins2[1] - bins2[0])
    else:
        mass = (bins1[1] - bins1[0])*vals1[0]
        bins1[0] = bins2[0]
        vals1[0] = mass/(bins1[1] - bins1[0])
        
    if bins1[-1] < bins2[-1]:
        mass = (bins1[-1] - bins1[-2])*vals1[-1]
        bins1[-1] = bins2[-1]
        vals1[-1] = mass/(bins1[-1] - bins1[-2])
    else:
        mass = (bins2[-1] - bins2[-2])*vals2[-1]
        bins2[-1] = bins2[-1]
        vals2[-1] = mass/(bins2[-1] - bins2[-2])
    
    # Get rid of all null values by redistributing the mass
    bins1, vals1 = getridofnullvaluesinfront(bins1, vals1)
    bins2, vals2 = getridofnullvaluesinfront(bins2, vals2)
    bins1, vals1 = getridofnullvaluesinfront(np.flip(bins1,0), np.flip(vals1,0))
    bins2, vals2 = getridofnullvaluesinfront(np.flip(bins2,0), np.flip(vals2,0))
    bins1 = np.flip(bins1,0)
    bins2 = np.flip(bins2,0)
    vals1 = np.flip(vals1,0)
    vals2 = np.flip(vals2,0)
    
    # Now compute the KLD based on the modified histograms with equal support
    kld = 0.
    index1 = 0
    index2 = 0
    currentx = bins1[0]
    
    while True:
        dx = 0.
        val1 = 0.
        val2 = 0.
        if bins1[index1+1] <= bins2[index2+1]:
            dx = bins1[index1+1] - currentx
            val1 = vals1[index1]
            val2 = vals2[index2]
            index1 = index1 + 1
            currentx = currentx + dx
        else:
            dx = bins2[index2+1] - currentx
            val1 = vals1[index1]
            val2 = vals2[index2]
            index2 = index2 + 1
            currentx = currentx + dx 
        kld += np.log(val1/val2)*val1*dx
        
        if index1 == bins1.size - 2 or index2 == bins2.size - 2:
            break
        
    # Finally return the result
    return kld

def kldivergence_kde(X1, w1, X2, w2, a, b, npoints, bw1=1., bw2=1.):
    X1 = resample(X1, w1, 5000)
    X2 = resample(X2, w2, 5000)
    
    kde1 = KernelDensity(kernel='gaussian', bandwidth=bw1, rtol=1E-5).fit(X1[:,np.newaxis])
    kde2 = KernelDensity(kernel='gaussian', bandwidth=bw2, rtol=1E-5).fit(X2[:,np.newaxis])
    
    t = np.linspace(a, b, npoints)
    dx = t[1] - t[0]
    density1 = np.exp(kde1.score_samples(t[:,np.newaxis]))
    density2 = np.exp(kde2.score_samples(t[:,np.newaxis]))

    kld = 0.
    for index in range(len(density1)):
        kld += np.log(density1[index]/density2[index])*density1[index]*dx
        
    return kld

if __name__ == '__main__':
    X1 = np.random.normal(0., 1., 10**5)
    w1 = np.ones_like(X1)
    X2 = np.random.normal(1., 1., 10**5)
    w2 = np.ones_like(X2)
    
    print(kldivergence_particles(X1, w1, X2, w2, bins1=500, bins2=500))
    print(kldivergence_kde(X1, w1, X2, w2, -4., 4., 100000, bw1=0.01, bw2=0.01))
    