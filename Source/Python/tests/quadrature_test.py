import numpy as np

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from math import *

if __name__ == '__main__':
    analytic = lambda t: 1.675*sin(2*pi*t)
    times = np.linspace(0., 1., 100)
    values = 1.7*np.sin(2*np.pi*times)
    
    firsttime = 0.
    error = 0.
    for tindex in range(len(times)-1):
        if times[tindex] < firsttime:
            continue
        err1 = (values[tindex] - analytic(times[tindex]))**2
        #print(err1)
        err2 = (values[tindex+1] - analytic(times[tindex+1]))**2
        error += 0.5*(times[tindex+1]-times[tindex])*(err1 + err2)
        
    print(error)