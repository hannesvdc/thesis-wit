import sys
sys.path.append("../")

import numpy as np
import numpy.random as rd
from math import *

import matplotlib.pyplot as plt

from tools.sample import *

#
# Resampling methods
#
def resample_indices(weigths, seed):
    # resampling (stratified)
    
    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG
    
    nb_sam = weigths.size
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam
    
    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)
    
    rsample = np.zeros_like(weigths, dtype=int)  # resampling
    # DC = np.zeros(nb_sam) # duplication count
    
    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = int(shelf)  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample

def resample_values(sample, weigths, seed):
    # resampling (stratified)
    
    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG
    
    nb_sam = sample.shape[0]
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam
    
    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)
    
    rsample = np.zeros_like(sample)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1
    
    return rsample


#
# Test functions
#
def test_resample_values_1D():
    X = np.linspace(0, 100, 1000)
    weights = np.append(np.linspace(0.1, 100., 500), np.linspace(100., 0.1, 500))

    indices = resample_indices(weights, 5000)
    X_indices = X[indices]
    
    X_values = resample_values(X, weights, 6000)
    
    plt.hist(X, bins=100, weights=weights, normed=True, histtype='step', label='original')
    plt.hist(X_indices, bins=100, normed=True, histtype='step', label='index resampling')
    plt.hist(X_values, bins=100, normed=True, histtype='step', label='value resampling')
    
    plt.title('1D resampling test')
    plt.legend()
    plt.show()

def test_resample_values_2D():
    X1 = np.linspace(0, 100, 1000)
    X2 = acceptrejectuniform(lambda x: exp(-x**2 / 2.)/sqrt(2.*pi), -10, 10, 1./sqrt(2.*pi), 1000)
    X = np.column_stack((X1, X2));
    weights = np.append(np.linspace(0.1, 100., 500), np.linspace(100., 0.1, 500))
    
    indices = resample_indices(weights, 5000)
    X_indices = X[indices, :]
    
    X_values = resample_values(X, weights, 6000)
    
    plt.subplot(211)
    plt.hist(X[:,0], bins=100, weights=weights, normed=True, histtype='step', label='original')
    plt.hist(X_indices[:,0], bins=100, normed=True, histtype='step', label='index resampling')
    plt.hist(X_values[:,0], bins=100, normed=True, histtype='step', label='value resampling')
    plt.legend()
    plt.subplot(212)
    plt.hist(X[:,1], bins=100, weights=weights, normed=True, histtype='step', label='original')
    plt.hist(X_indices[:,1], bins=100, normed=True, histtype='step', label='index resampling')
    plt.hist(X_values[:,1], bins=100, normed=True, histtype='step', label='value resampling')
    plt.legend()
    
    plt.title('2D resampling test')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    test_resample_values_1D()
    test_resample_values_2D()
