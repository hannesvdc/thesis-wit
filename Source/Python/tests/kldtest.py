import sys
sys.path.append("../")

from matching.kld import *
from scipy.stats import multivariate_normal
from momentfunctions.normalised_polynomial_moments import *

import numpy as np
import numpy.linalg as lin

import matplotlib.pyplot as plt

def linearEquationEntropyTest():
    eps = 0.1
    mean = np.array([10., 1.])
    var = np.matrix([[100., 0.],[0., 1.]])
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    B = np.asmatrix(np.diag(np.asarray([1./np.sqrt(eps), 1.])))
    tend = 1.
    Dt = 0.05
    t = 0.
    slowvars = []
    integral = lambda u, i, j: (np.asmatrix(scipy.linalg.expm(-u*A))*(B*np.transpose(B))*np.asmatrix(scipy.linalg.expm(-u*np.transpose(A))))[i,j]
    entropies = []
    times = []
    while t < tend:
        newmean = np.squeeze(np.asarray(np.asmatrix(scipy.linalg.expm(Dt*A)).dot(mean)))
        newfastvar = scipy.integrate.quad(integral, 0, Dt, args=(0,0))[0]
        newcovar = scipy.integrate.quad(integral, 0, Dt, args=(1,0))[0]
        newslowvar = scipy.integrate.quad(integral, 0, Dt, args=(1,1))[0]
        print('var ', var)
        newvar = np.asmatrix(scipy.linalg.expm(Dt*A))*(var + np.matrix([[newfastvar, newcovar],[newcovar,newslowvar]]))*np.asmatrix(scipy.linalg.expm(Dt*np.transpose(A)))
        slowvars.append(newvar[1,1])
        print('new mean', newmean)
        print('new var', newvar)
        divergence = 0.5*(log(np.abs(lin.det(var)/lin.det(newvar)))-1.+np.trace(lin.inv(var)*newvar)+np.dot(mean-newmean, np.squeeze(np.asarray((lin.inv(var)).dot(mean-newmean)))))
        entropies.append(divergence)
        times.append(t+Dt)
        
        mean = newmean
        var = newvar
        t += Dt
    
    plt.semilogy(times, entropies, label="KLD on a linear SDE")
    plt.figure()
    plt.plot(times, slowvars, label="Slow variance")
    plt.legend()
    plt.show()
    
def kldCorrectnessTest():
    var1 = 4.
    var2 = var1 #np.matrix([[3.,1.5,],[1.5,1.]])
    mean1 = 2.
    mean2 = 2. #np.array([1., 0.])
    prior = np.random.normal(mean1, 2., size=10**5)
    posterior = np.random.normal(mean2, 2., size=10**5)
    R = lambda x: np.array([1., x, x**2])
#    print('mean ', np.mean(prior))
#    print('var ', np.mean(prior**2) - np.mean(prior)**2)
#    print(np.mean(np.array(list(map(lambda x: (R(x))[2], prior)))))

    print(mean1-mean2)
#    print((lin.inv(var1)).dot(mean1-mean2))
    div = KLDMatchingStrategy.computeDistance(prior, posterior)
    kld = KLDMatchingStrategy(prior, R, np.array([1., np.mean(prior, axis=0), np.mean(prior**2, axis=0)]), iterations=15)
    conv, lambdas, ent = kld.match(weights=np.ones(10**5), entropy=True)
    print(lambdas)
    print('lambdas', np.sum(lambdas))
    print('num', np.average(np.asarray(list(map(lambda i: np.exp(lambdas.dot(R(prior[i])))-1, range(10**5)))), axis=0,weights=np.ones(10**5)))
    divExact = 0.5*(log(np.abs(lin.det(var1)/lin.det(var2)))-2.+np.trace(np.asmatrix(lin.inv(var1))*var2)+np.dot(mean1-mean2, np.squeeze(np.asarray((np.asmatrix(lin.inv(var1))).dot(mean1-mean2)))))
    print('Numerical: ', div)
    print('Exact: ', divExact)
    print('Multipliers: ', ent)
    
if __name__ == '__main__':
    kldCorrectnessTest()
