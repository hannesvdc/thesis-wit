import sys
sys.path.append("../")

from Samples.linear.linear_model import *

from matching.kld import *
from timeintegrators.adaptive_mm_cpi import *
from timeintegrators.micromacro_cpi import *
from timeintegrators.euler_maryama import *

import matplotlib.pyplot as plt

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    meanY = vector[:,3]
    moment2Y = vector[:,4]
    vector[:,4] = moment2Y - meanY*meanY
    vector[:,5] = vector[:,5] - meanX*meanY
    
    return vector

# Filter the computed statistics from the extrapolated statistics
def getRealPoints(times, stats):
    newtimes = np.delete(times, np.s_[2::3])
    newstats = np.delete(stats, np.s_[2::3], 0)
    
    return newtimes, newstats

# Define the moment functions
def momentFunctions(Lx, Ly, cov=False):
    RLX = lambda X: np.asarray(list(map(lambda p: X[0]**(p), range(1, Lx+1))))
    RLY = lambda X: np.asarray(list(map(lambda p: X[1]**(p), range(1, Ly+1))))
    R = lambda X: np.append(np.append(np.asarray([1.]), RLX(X)), RLY(X))
    
    if cov:
        RXY = lambda X: np.asarray([X[0]*X[1]])
        Rcov = lambda X: np.append(R(X), RXY(X))
        return Rcov
        
    return R

# Compute the moment functions from a given distribution
def computeMoments(points, R, weights=None):
    N = points.shape[0]
    if weights is None: weights = np.ones(N)/N

    weightsum = np.sum(weights)
    result = 0.
    
    for i in range(N):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

def match(eps, X, Y, tend, dt, dtmax, Lx, Ly, K, kld=False):
    N = X.shape[0]
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    D = np.asarray([1./np.sqrt(eps), 1.])
    model = LinearModel(A, D, grid=None)
    muinv, varinv = model.invariantMeasure(dt, 10.**-4)
    Yinv = np.random.normal(muinv[1], np.sqrt(varinv[1,1]), N)
    print("Invariant slow variance ", varinv)
    priorY = np.copy(Y)
    Rcbpartial = lambda x, w: computeMoments(points=x, R=momentFunctions(2, 2, cov=True), weights=w)
    if kld is True:
        Rcb = lambda x, w: np.append(Rcbpartial(x, w), np.array([KLDMatchingStrategy.computeDistance(x[:,1], priorY, weights=w), KLDMatchingStrategy.computeDistance(x[:,1], Yinv, weights=w)]))
    else:
        Rcb = Rcbpartial
    
    Rmatchcb = lambda x, w, neww: np.array([KLDMatchingStrategy.computeDistance(x, x, priorweights=w, weights=neww),KLDMatchingStrategy.computeDistance(x[:,1], x[:,1], priorweights=w, weights=neww)])
    # And perform matching
    particles = np.zeros([N, 2])
    particles[:,0] = X
    particles[:,1] = Y
    R = momentFunctions(Lx, Ly)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(particles, R, computeMoments(particles, R), 'NR', 7)
    params = {'K': K, 'Dtmax': dtmax, 'Dt0': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, [0,1], adaptive=True, callback = Rcb, matchcallback=Rmatchcb)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues'], cpiresult['matchcbtimes'], cpiresult['matchcbvalues']

# Perform matching with the simple linear model with given epsilon and endtime.
def match_adaptive(eps, X, Y, tend, dt, dtmax, Lx, Ly, K, kld=False):
    N = X.shape[0]
    A = np.matrix([[-1./eps, 1./eps],[-1., -1.]])
    D = np.asarray([1./np.sqrt(eps), 1.])
    model = LinearModel(A, D, grid=None)
    muinv, varinv = model.invariantMeasure(dt, 10.**-4)
    Yinv = np.random.normal(muinv[1], np.sqrt(varinv[1,1]), N)
    print("Invariant slow variance ", varinv)
    priorY = np.copy(Y)
    Rcbpartial = lambda x, w: computeMoments(points=x, R=momentFunctions(2, 2, cov=True), weights=w)
    if kld is True:
        Rcb = lambda x, w: np.append(Rcbpartial(x, w), np.array([KLDMatchingStrategy.computeDistance(x[:,1], priorY, weights=w), KLDMatchingStrategy.computeDistance(x[:,1], Yinv, weights=w)]))
    else:
        Rcb = Rcbpartial
    
    Rmatchcb = lambda x, w, neww: np.array([KLDMatchingStrategy.computeDistance(x, x, priorweights=w, weights=neww),KLDMatchingStrategy.computeDistance(x[:,1], x[:,1], priorweights=w, weights=neww)])
    # And perform matching
    particles = np.zeros([N, 2])
    particles[:,0] = X
    particles[:,1] = Y
    R = momentFunctions(Lx, Ly)
    stepper = EulerMaryamaIntegrator(model.a, model.b)
    kld = KLDMatchingStrategy(particles, R, computeMoments(particles, R), 'NR', 7)
    params = {'K': K, 'Dtmax': dtmax, 'Dt0': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = AdaptiveMicroMacroCPI(params, kld, stepper, R) 
    cpiresult = cpi.simulate(particles, np.ones(N), 0., tend, dt, [0,1], adaptive=True, maxrelentropy=0.005, callback = Rcb, matchcallback=Rmatchcb)
    
    return cpiresult['points'], cpiresult['weights'], cpiresult['cbtimes'], cpiresult['cbvalues'], cpiresult['matchcbtimes'], cpiresult['matchcbvalues']

if __name__ == '__main__':
    eps = 0.1
    tend = 1.
    dt = eps/10.
    dtmax = 3*dt
    Lx = 0
    Ly = 2
    K = 1
    kld = True
    N = 10**4
    X = np.random.normal(10., 10., N)
    Y = np.random.normal(1., 1., N)
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(211)
    plt.title("Mean Fast Variables, epsilon = "+str(eps))
    ax2 = fig1.add_subplot(212)
    plt.title("Mean Slow Variables")
    plt.xlabel("Time[s]")
    fig2 = plt.figure()
    ax3 = fig2.add_subplot(211)
    plt.title("Variance Fast Variables, epsilon = "+str(eps))
    ax4 = fig2.add_subplot(212)
    plt.title("Variance Slow Variables")
    plt.xlabel("Time[s]")
    fig5 = plt.figure()
    ax5 = fig5.add_subplot(111)
    plt.title('Relative Entropy')
    plt.xlabel('Time [s]')
    
    apoints, aweights, acbtimes, acbvalues, amatchtimes, aentropy = match_adaptive(eps, np.copy(X), np.copy(Y), tend, dt, dtmax, Lx, Ly, K, kld)
    points, weights, cbtimes, cbvalues, matchtimes, entropy = match(eps, np.copy(X), np.copy(Y), tend, dt, dtmax, Lx, Ly, K, kld)
    
    acbtimes, acbvalues = getRealPoints(acbtimes, acbvalues)
    acbvalues = getMeanAndVariance(acbvalues)
    ax1.plot(acbtimes, acbvalues[:,1], label="mM Acceleration {0:.4f}".format(dtmax))
    ax2.plot(acbtimes, acbvalues[:,3], label="mM Acceleration {0:.4f}".format(dtmax))
    ax3.plot(acbtimes, acbvalues[:,2], label="mM Acceleration {0:.4f}".format(dtmax))
    ax4.plot(acbtimes, acbvalues[:,4], label="mM Acceleration {0:.4f}".format(dtmax))
    ax5.semilogy(amatchtimes, aentropy, label="Full relative entropy, dtmax={0:.4f}".format(dtmax))
    
    cbtimes, cbvalues = getRealPoints(cbtimes, cbvalues)
    cbvalues = getMeanAndVariance(cbvalues)
    ax1.plot(cbtimes, cbvalues[:,1], label="mM Acceleration {0:.4f}".format(dtmax))
    ax2.plot(cbtimes, cbvalues[:,3], label="mM Acceleration {0:.4f}".format(dtmax))
    ax3.plot(cbtimes, cbvalues[:,2], label="mM Acceleration {0:.4f}".format(dtmax))
    ax4.plot(cbtimes, cbvalues[:,4], label="mM Acceleration {0:.4f}".format(dtmax))
    ax5.semilogy(matchtimes, entropy, label="Full relative entropy, dtmax={0:.4f}".format(dtmax))
    
    plt.show()