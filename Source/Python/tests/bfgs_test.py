import sys
sys.path.append("../")

from functools import partial
import argparse

import numpy as np
from scipy import stats
from math import *

import matplotlib.pyplot as plt

from Solvers.newton_raphson import *
from matching.kld import *
from tools.plots import *
from timeintegrators.micromacro_cpi import *
from Samples.fene.fene_model import *

def computeMoments(points, R, weights=None):
    if weights is None: weights = np.ones(points.size)/points.size
    
    weightsum = np.sum(weights)
    R0 = R(0)
    result = np.zeros(R0.size)
    
    for i in range(points.size):
        result = result + R(points[i])*weights[i]/weightsum
    
    return result

# Helper functions for plotting
def getMeanAndVariance(vector):
    meanX = vector[:,1]
    moment2X = vector[:,2]
    vector[:,2] = moment2X - meanX*meanX
    
    return vector
    
# Matching acceleration algorithm
def cpi_test(particles, L, tbegin, tend, dt, dtmax, optimizer):
    We = 1
    gamma = 7
    k=lambda t: 2
    fene = Fene(We, gamma, k)
    R = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    
    # And perform matching
    N = particles.shape[0]
    Rplot = lambda x: np.asarray(list(map(lambda p: (x)**(p), range(0, L))))
    stepper = AcceptRejectEulerMaryama(fene.a, fene.b, lambda x: abs(x) < (1.-sqrt(dt))*gamma)
    kld = KLDMatchingStrategy(particles, R, computeMoments(particles, R), optimizer, 20)
    params = {'K': 1, 'Dtmax': dtmax, 'alfadecrease': 0.5, 'alfaincrease': 1.2}
    cpi = MicroMacroCPI(params, kld, stepper, R)
    
    callback = lambda X, w: computeMoments(points=X, R=Rplot, weights=w) 
    particles, weights, cbtimes, cbvalues = cpi.simulate(particles, np.ones(N), 0., tend, dt, [0,1], callback)
    
    return particles, weights, cbtimes, cbvalues

# Test one matching step between different optimization routines.
def match_test(type, L, optimizer):
    We = 1
    gamma = 7
    k=lambda t: 2
    fene = Fene(We, gamma, k)
    RL = lambda x: np.asarray(list(map(lambda p: (x/gamma)**(2*p), range(0, L))))
    
    prior = np.loadtxt("Fenedata1.0s.txt", delimiter=',')
    target = np.loadtxt("Fenedata1.1s.txt", delimiter=',')
    
    plotpoints = np.linspace(5.4, 6.6, 1000)
    plt.plot(plotpoints, kde(prior, plotpoints, gamma*0.01))
    plt.plot(plotpoints, kde(target, plotpoints, gamma*0.01))
    targetmoments = computeMoments(target, RL)
    print("Target moments ", targetmoments)
    
    weights = np.empty(0)
    if type == 'kld':
        kld = KLDMatchingStrategy(prior, RL, targetmoments, optimizer)
        conv, lambdas = kld.match()
        weights = kld.weights(lambdas)
    elif type == 'l2d':
        l2d = L2DMatchingStrategy(prior, RL, targetmoments)
        lambdas = l2d.match()
        weights = l2d.weights(lambdas)
    elif type == 'l2n':
        l2n = L2NMatchingStrategy(prior, RL, targetmoments, gamma, np.fromfunction(lambda k,l: 2*gamma/(2*(k+l)+1), (L,L), dtype=int))
        lambdas = l2n.match()
        weights = l2n.weights(lambdas)
    else:
        print("This matching strategy is not supported.")
        return
        
    matched = resample(prior, weights, 5000)
    plt.plot(plotpoints, kde(matched, plotpoints, gamma*0.01), label="Optimizer "+optimizer)
    print("Target moments: ", targetmoments)
    print("matched moments: ", computeMoments(prior, RL, weights))
    plt.legend()    
    
def resample(sample, weigths, seed):
    # resampling (stratified)

    # seed setting
    np.random.seed(seed)
    np.random.randn(10**3)  # warm up of RNG

    nb_sam = len(sample)
    # stratified random numbers in [0,1)
    strat_rn = (np.arange(nb_sam) + np.random.random(nb_sam))/nb_sam

    # normalized cumulative sum of weights
    ncs = np.cumsum(weigths) / np.sum(weigths)

    rsample = np.zeros(nb_sam)  # resampling
    # DC = np.zeros(nb_sam) # duplication count

    shelf = 0
    ind = 0
    while max(shelf, ind) < nb_sam:
        count = 0
        while ind < nb_sam and strat_rn[ind] < ncs[shelf]:
            rsample[ind] = sample[shelf]  # duplication of samples
            count += 1
            ind += 1
        # DC[shelf] = count # duplication count update
        shelf += 1

    return rsample
    
if __name__ == '__main__':
    J = 1e5
    dt = 2e-4
    Dt = 1
    We = 1
    gamma = 7
    gammasq = gamma*gamma
    k = 2
    We = 1
    gamma = 7
    k=lambda t: 2
    fene = Fene(We, gamma, k)
    iparticles = fene.sampleInitialDistribution(J)
    
    ## Test the CPI algorithm
    particles, weights, cbtimesnr, cbvaluesnr = cpi_test(np.copy(iparticles), 3, 0, 10., 2e-4, 0.1, 'NR')
    particles, weights, cbtimesbfgs, cbvaluesbfgs = cpi_test(np.copy(iparticles), 3, 0, 10., 2e-4, 0.1, 'BFGS') 
    cbvaluesnr = getMeanAndVariance(cbvaluesnr)
    cbvaluesbfgs = getMeanAndVariance(cbvaluesbfgs)
    
    # Jumps in variance can be attributed to jumps in mean as variance = second moment - mean^2
    plt.plot(cbtimesnr, cbvaluesnr[:,2],label="Newton Raphson")
    plt.plot(cbtimesbfgs, cbvaluesbfgs[:,2],label="BFGS")
    plt.title("Variance")
    plt.xlabel("Time [s]")
    plt.legend()
    plt.show()
    # match_test('kld', 3, 'NR')
    # match_test('kld', 3, 'BFGS')
    # plt.show()
    