\chapter{Stability of micro-macro acceleration}
Chapters one to three mainly discuss the convergence of the micro-macro acceleration algorithm meaning that the error between the numerical end exact solution converges to zero as $\Delta t \to 0, \delta t \to 0$ and $C_L \to \infty$. Convergence is a necessary property to have in a numerical method but in practice we want to use larger steps that make the simulation faster but also less accurate. Stability is concerned with the largest time steps $\delta t$ and $\Delta t$ that are feasible such that the numerical approximation does not explode to infinity as time goes to infinity if the exact solution stays finite. This chapter discusses the stability boundaries for the inner microscopic integrator and extrapolation and next chapter is about accuracy.

\section{A linear test equation}
In the context of ordinary differential equations (ODE's) a linear test equation of the form [cite ODE course]
\begin{equation}
\frac{dX(t)}{dt} = \lambda X(t), \ \ X(0) \in \mathbb{R}, \ \ \mathcal{R}e(\lambda) < 0
\end{equation}
so that $X(t)$ converges to zero. A numerical scheme is then stable when the discretized solution does not blow up to infinity. Because the eigenvalue $\lambda$ almost always appears in conjunction with the discrete time step $\delta t$ such a criterion constitutes a region in the complex plane where the numerical scheme is stable.

Another reason why the linear test equation is important is that for a general ODE
\begin{equation}
\frac{dX(t)}{dt} = f(X(t), t)
\end{equation}
we can linearise the right hand side around the numerical approximation $X^n$ at time $t^n$ and then the eigenvalues of the Jacobian determine the stability locally. The time step $\delta t$ should then be chosen such that every $\lambda_i \delta t$ is inside the stability domain. In practice people don't use this criterion to chose the time step since computing and diagonalising the jacobian matrix is an expensive procedure but it is of theoretical value.

\section{Stability in stochastic sense}
In the context of stochastic differential equations there is however no thorough connection between nonlinear SDE's and linearised SDE's to study stability [cite the reference in Przemeks paper]. Nevertheless we can study a stochastic linear test equation independently and derive stability bounds on the time step. 

\paragraph{Multiplicative noise}
A frequently chosen test equation is a linear equation with \textit{multiplicative} noise of the form
\begin{equation}
dX(t) = \lambda X(t) dt + \mu X(t) dW(t).
\end{equation}
An important property is that whenever $X(t)=0$ then $X$ remains zero forever. A numerical scheme is then stable when the discretized paths also converge to zero as time goes to infinity. There are two ways of expressing this convergence of paths in a stochastic embedding. The first is so called \textit{absolute} stability and requires
\begin{equation}
\lim_{t \to \infty} X(t) = 0
\end{equation}
for all paths. The second is \textit{mean squared} stability and requires that the variance of the paths converges to zero
\begin{equation}
\lim_{t \to \infty} \text{Var}[X(t)] =  0.
\end{equation}
It is possible to prove that absolute stability implies mean-squared stability but no the other way around [citation needed]. An intuitive reasoning is that mean-squared stability does not require paths to converge but only the variance. It can be sufficient for most paths to converge but some may diverge as long as the total variance tends to zero.
\paragraph{Additive Noise} A different test system equation is a linear equation with additive noise of the form
\begin{equation}
dX(t)= \lambda X(t) dt + \sigma dW(t).
\end{equation}
This equation defines an Orhnstein-Ohlenbeck process and it has a normal invariant distribution with mean 0 and variance $\frac{\sigma^2}{|\lambda|}$. A numerical scheme is stable in this context when the intermediate distributions $\mathcal{P}^n$ at times $t^n = n\delta t$ also converge to some invariant distribution $\mathcal{P}^{\infty}$, not necessarily equal to the invariant distribution of the continuous SDE. For example it is possible to show [citation] that the Euler-Maruyama scheme converges to a normal distribution with mean 0 and variance $\frac{\sigma^2}{\lambda-\delta t}$ if it has an invariant distribution. As $\delta t \to 0$ this distribution also converges to the invariant distribution of the continuous process.

\vspace{2mm}
Another way of deriving a stability bound is by looking at a general Ohrnstein-Uhlenbeck process 
\begin{equation} \label{eq:emtestequation}
dX(t) = AXdt + B dW(t), \ \ A \in \mathbb{R}^{n \times n}, \ \ B \in \mathbb{R}^{n\times n}.
\end{equation}
The Euler-Maruyama scheme then reads 
\begin{equation}
X^{n+1} = (I_n+\delta tA)X^n + \sqrt{\delta t}B \xi^n, \ \ \xi^n \sim \mathcal{N}(0,1)
\end{equation}
such that the by the martingale property the mean and variance yield
\begin{align}\label{eq:eminvariant}
\mathbb{E}[X^n] &= (I_n + \delta tA)^n \mathbb{E}[X^0] \\
\text{Var}[X^n] &= (I_n + \delta tA)^n \text{Var}[X^0](I_n + \delta tA^T)^n + \sum_{k=1}^n (I_n + \delta tA)^n BB^T(I_n + \delta tA^T)^n
\end{align}
The Euler-Maruyama scheme is stable when these quantities remain finite as $n \to \infty$. This is the case when $\text{spec}(I_n+\delta tA) \in \mathcal{B}(0,1)$.

For example consider the scale-separated linear test equation from previous chapter with $\varepsilon=0.1$. The smallest eigenvalue of $A$ is $-1/\varepsilon$ so the stability boundary is $\delta t \leq 0.2$. Figure \ref{fig:emstability} shows the coarse distribution after $T=30$ for different microscopic steps $\delta t$. For $\delta t < 0.2$ the numerical distribution approximates the exact invariant distribution, obtained by computing \ref{eq:eminvariant} up to a tolerance of $10^{-6}$, while for $\delta t < 0.2$ the variance diverges to infinity so the numerical scheme is unstable. 

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\linewidth}
		\input{../../Source/Python/Samples/linear/plots/stability/dt019.tex}
		\label{fig:dt019}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\linewidth}
		\input{../../Source/Python/Samples/linear/plots/stability/dt0197.tex}
		\label{fig:dt0197}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\linewidth}
		\input{../../Source/Python/Samples/linear/plots/stability/dt0203.tex}
		\label{fig:dt0203}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\linewidth}
		\input{../../Source/Python/Samples/linear/plots/stability/dt021.tex}
		\label{fig:dt021}
	\end{subfigure}
	\caption{Invariant distributions of the Euler-Maruyama scheme \ref{eq:eminvariant} for linear SDE \ref{eq:ch5linearteststde} for different step sizes $\delta t$ with end time $T=30$. The stability of the method clearly breaks if $\delta t > 0.2$.}
	\label{fig:emstability}
\end{figure}

\section{Micro-macro stability with mean extrapolation}
Stability of the micro-macro acceleration algorithm works in the same setting as \ref{eq:emtestequation} with additive noise and a Gaussian initial condition since then there exist analytical formulas for matching in this context. Stability again means that the intermediate distributions computed by the micro-macro acceleration algorithm converge to an invariant distribution, or put mathematically $\mathcal{P}_t \to \mathcal{P}_{\infty}$ as $t \to \infty$. The goal of acceleration is that $\Delta t$ can be much larger than the microscopic time step $\delta t$ while still having a stable numerical scheme. A very attractive property of micro-macro stability is that the stability bound for $\Delta t$ only relies on the coarse eigenvalues in the case of mean-only extrapolation. A large spectral gap is hence favourable stable extrapolation. The derivation here demonstrates stability for only mean extrapolation while next section discusses mean-variance extrapolation. The derivations were taken from [citation] but adapted to the notation of this thesis.

\vspace{2mm}
\noindent
Suppose again we have a linear system \ref{eq:emtestequation} where the drift matrix $A$ can be decomposed as, possibly after block diagonalisation
\[
A = \begin{bmatrix}
D^f & 0 \\ 0 & D^s
\end{bmatrix}.
\]
Fix the current time $t_n = n\Delta t$. After one step of the Euler-Maruyama scheme the mean and variance read
\begin{align}
\mu_{n,1} &= (I+\delta t A)\mu_n \\
\Sigma_{n,1} &= (I+\delta t A)\Sigma_n(I+\delta tA)^T + \delta t BB^T.
\end{align}
Extrapolating the coarse mean then gives 
\begin{equation}
\mu_{n+1}^s = \mu_{n,1}^s+\frac{\Delta t}{\delta t}(\mu_{n,1}^s-\mu_n^s) = (I+\Delta t D^s)\mu_n^s.
\end{equation}
By theorem [reference] we know that the other density parameters after matching are
\begin{align}
\Sigma_{n+1} &= \Sigma_n \\ 
\mu_{n+1}^f &= (I_f + \delta tD^f )\mu_n^f + (\Delta t - \delta t)C_{n,1}^T(\Sigma_{n,1})^{-1}D^s\mu_n^s.
\end{align}
As the variance stays constant during matching, its stability is determined by the Euler-Maruyama scheme and this is not affected by extrapolation. The full mean at each time step propagates as
\begin{equation} \label{eq:meanextrapolationstability}
\mu_{n+1} = \left(I + \begin{bmatrix} \delta t D^f & (\Delta t - \delta t)C_{n,1}^T(\Sigma_{n,1})^{-1}D^s \\ 0 & \Delta t D^s \end{bmatrix}\right)\mu_n = D_n \mu_n.
\end{equation}
The mean vector is thus stable when the time-variant matrix $D_n$ has all eigenvalues within the unit circle. As $D_n$ is upper block diagonal, its eigenvalues are union of the eigenvalues of $I^f+\delta tD^f$ and $I_s+\Delta t D^s$, meaning that the fast components are stable when the Euler-Maruyama scheme is stable and the extrapolation is stable when $\text{spec}(I^s+\Delta t D^s) \in \mathcal{B}(0,1)$. The fast and slow components are decomposed in the context of stability and the acceleration algorithm can take bigger extrapolation steps when there is a larger spectral gap between $D^f$ and $D^s$. Moreover, the variance converges to the invariant variance of the Euler-Maruyama scheme because the covariance matrix stays constant during matching.

\vspace{2mm}
\noindent
For example consider the slow-fast system given by
\begin{equation}\label{eq:ch5linearteststde}
dX(t) = \begin{bmatrix} -\frac{1}{\varepsilon} & 0 \\ 1 & -1\end{bmatrix}X(t)dt +  \begin{bmatrix} -\frac{1}{\sqrt{\varepsilon}} & 0 \\ 0 & 1\end{bmatrix}
\end{equation}
with a finite time-scale separation $\varepsilon$. The eigenvalues of $A$ are -1 and $-1/\varepsilon$ such that the Euler-Maruyama scheme is stable when $\delta t \leq 0.2$. By condition \ref{eq:meanextrapolationstability} the acceleration algorithm is stable when $\Delta t \leq 2$ which is a factor of 10 larger than the microscopic integrator. As $\varepsilon$ decreases this spectral gap will become larger but the micro-macro scheme will still be stable when $\Delta t \leq 2$.

\subsection{Numerical experiments}
A first experiment shows the influence of crossing the stability of extrapolation for the linear test equation \ref{eq:ch5linearteststde} with $\varepsilon = 0.1$. Figure \ref{fig:meanextrapolatedstability} displays the invariant distributions of the micro-macro method after $T=110$ seconds. On the figures we indeed see that the acceleration scheme reaches the invariant distribution of the Euler-Maruyama scheme for $\Delta t \leq 2$ while for $\Delta t > 2$ matching failures start to build up and the distribution of the acceleration scheme is biased. An important detail is that when a matching failure occurs, the algorithm does not change the extrapolation step $\Delta t$ adaptively, but keeps it constant and continues to work with the wrong Lagrange multipliers and wrong particle weights. The strong bias on the Lagrange Multipliers propagates to give the wrong invariant distribution on the figures. The maximum number of Newton-Raphson is 50 to make sure that matching failures are due to a non-convergence of the iterative solver.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt19_fails.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt195_fails.tex}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt205_fails.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.41\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt21_fails.tex}
	\end{subfigure}
	\caption{Invariant distributions of the micro-macro scheme with coarse mean-only extrapolation of the linear SDE with $\varepsilon = 0.1$, computed at $T = 110$ seconds.}
	\label{fig:meanextrapolatedstability}
\end{figure}

\vspace{2mm}
\noindent
We now turn on the adaptive extrapolation procedure based on matching failures and set the maximum number of Newton-Raphson iterations to 8. The adaptive strategy halves $\Delta t$ when a matching failure occurs and increases it with a factor of 1.2 otherwise. Figure \ref{fig:meanextrapolatedstabilityadaptive} depicts that the adaptivity only kicks in when $\Delta t_{\text{max}} \geq 2$ and the histograms lie closer to the exact invariant distribution than without adaptive stepping, but there still is a bias in the coarse mean. It also happens that the mean of the extrapolation step size is always below the stability boundary in these experiments although there is no theoretical ground for this behaviour [cite Przemeks results]. A deeper analysis of this property could by useful although the exact mean extrapolation step size heavily depends on problem parameters like the number of Newton-Raphson iterations and the exact adaptive procedure.  Such an analysis is not in the scope of this thesis.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt19_statistics.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt195_statistics.tex}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt205_statistics.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt21_statistics.tex}
	\end{subfigure}
	\caption{Invariant distributions of the acceleration scheme with adaptive time stepping at end time $T=300$ for several values $\Delta t_{\text{max}}$. For $\Delta t_{\text{max}} \geq 2$ the adaptivity sets in.}
	\label{fig:meanextrapolatedstabilityadaptive}
\end{figure}

\vspace{2mm}
\noindent
Finally, Figure \ref{fig:meanextrapolatedstabilityadaptiveloweps} shows that for a different value of $\varepsilon$, here $\varepsilon = 0.01$, the macroscopic stability bound is exactly the same and the adaptive procedure is only triggered when $\Delta t_{\text{max}} \geq 2$. The end time here is $T=300$ to make sure that the distributions have reached the invariant regime. Again for $\Delta t_{\text{max}} \leq 2$ the micro-macro acceleration algorithm attains the invariant distribution of the Euler-Maruyama scheme while for larger step sizes the acceleration algorithm is unstable, confirming the stability theorem with coarse mean extrapolation.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt19eps0.01_fails.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt195eps0.01_fails.tex}
	\end{subfigure}
	\begin{subfigure}[b]{0.39\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt205eps0.01_fails.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.39\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/Dt21eps0.01_fails.tex}
	\end{subfigure}
	\caption{Histograms of the acceleration scheme on the linear test system with a scale separation of $\varepsilon=0.01$ after $T=300$ seconds for several values of $\Delta t_{\text{max}}$.}
	\label{fig:meanextrapolatedstabilityadaptiveloweps}
\end{figure}

\section{Stability with extrapolated mean and variance}
There also exist analytical formulas for the stability bound when extrapolating mean and variance for a linear SDE with additive noise, proven in [cite Przemeks theory]. The only disadvantage is that the bound only takes the slow variables into account because the analytical formulas for matching with mean and variance are more involved. The stability bound here thus inherently is the stability of linear extrapolation for mean and variance. The derivation is more simplified from [Przemeks preprint] and one step is more detailed. The notation is also consistent with the rest of the thesis.

\subsection{Kronecker product and Kronecker sum}
The stability results in the next section makes use of well known concepts such as the Kronecker product and the Kronecker sum. These operations are well defined in [cite the matrix cookbook]. Given two matrices $A \in \mathbb{R}^{n \times m}, \ B \in \mathbb{R}^{k \times l}$ the Kronecker product is defined as 
\[
A \otimes B = \begin{bmatrix}
A_{11} B & A_{12}B & \dots & A_{1m} B \\
A_{21}B & A_{22}B & \dots & A_{2m} B \\
\vdots & & & \vdots \\
A_{n1} B & A_{n2} B & \dots & A_{nm} B\\
\end{bmatrix} \ \ \in \mathbb{R}^{nk \times ml}.
\]
Another important operation is vectorization of a matrix $A$, denoted as $\text{vec}(A)$ which consists of putting the columns of $A$ below each other in a vector of length $nm$. The next identity links the Kronecker product with the vectorization operator and is useful for the stability bound with extrapolated mean and variance. For compatible matrices $A, X, B$ it holds [cite matrix cookbook]
\begin{equation} \label{eq:kroneckerproductvectorization}
\text{vec}(AXB) = (B^T \otimes A)\text{vec}(X).
\end{equation}

\vspace{2mm}
\noindent
Finally, the Kronecker sum between two square matrices $A \in \mathbb{R}^{n \times n}, \ B \in \mathbb{R}^{m \times m}$ is defined in function of the Kronecker sum as 
\[
A \oplus B = A \otimes I_m + I_n \otimes B.
\]

\subsection{Derivation of the stability bound}
\vspace{2mm}
\noindent
From mean only extrapolation we know that the mean vector evolves through a time dependent linear system
\begin{equation} 
\mu_{n+1} = D_n\mu_n.
\end{equation}
and the former equation also holds in the case of mean-variance extrapolation. For the variance, one step of the Euler-Maruyama scheme reads
\[
\Sigma_{n,1} = (I+\delta t A)\Sigma_n(I+\delta tA)^T + \delta t BB^T.
\]
Linear extrapolation of the slow covariance matrix reads
\begin{equation} \label{eq:stabmeanvarderivation}
\Sigma_{n+1}^s = \Sigma_n^s + \frac{\Delta t}{\delta t}(\Sigma_{n,1}^s-\Sigma_n^s) = \Sigma_n^s + \Delta t (D^s \Sigma_n^s + \Sigma_n^s (D^s)^T + \delta t D^ s \Sigma_n^s (D^s)^T)  + \Delta t(BB^T)^s.
\end{equation}
The next step is to transform the previous identity into a linear operation on the covariance matrix $\Sigma_n^s$ to study stability of extrapolation through eigenvalues of the linear operator. Here the Kronecker product and Kronecker sum come in. The term $D^ s \Sigma_n^s (D^s)^T$ can be rewritten as $\text{vec}(D^ s \Sigma_n^s (D^s)^T) = (D^s \otimes D^s) \text{vec}(\Sigma_n^s)$. The other term $D^s \Sigma_n^s + \Sigma_n^s (D^s)^T$ is also equivalent to $(D^s \oplus D^s) \text{vec}(\Sigma_n^s)$ hence equation \ref{eq:stabmeanvarderivation} is equivalent to 
\begin{equation}
\Sigma_{n+1}^s = (I^{s^2} + \Delta t(D^s \oplus D^s + \delta t (D^s \otimes D^s))) . \Sigma_n^s + \delta t (B B^T)^s
\end{equation}
where the operator $A.B$ denotes the inverse vectorization of $A \text{vec}(B)$ if $B$ is a square matrix. Stability of the micro-macro scheme hence depends on the spectrum of the matrix $L_{\delta t}^s = D^s \oplus D^s + \delta t (D^s \otimes D^s)$ and micro-macro acceleration is stable when $\text{spec}(I^{s^2} + \Delta t(D^s \oplus D^s + \delta t (D^s \otimes D^s))) \subset B(0,1)$. 

\vspace{2mm}
\noindent
If $\delta t << \rho(D^s)$ the term $\delta t (D^s \otimes D^s)$ acts as a small perturbation on the matrix $I^{s^2} + \Delta t(D^s \oplus D^s)$ which describes the extrapolation of the covariance matrix. The perturbation may change the exact stability region of the acceleration algorithm slightly by making it larger or smaller than the macroscopic stability requirement. The authors of [cite Prezemks stability paper] investigate the eigenvalues of $L_{\delta t}^s$  further but this is out of scope for this thesis. We end this chapter with a numerical example showing the effect of the microscopic perturbation.

\subsection{Numerical experiment}
Take again the linear test system \ref{eq:ch5linearteststde} with $\varepsilon = 0.1$ so that $D^s = -1$. The stability bound for mean-variance extrapolation requires that $|1+\Delta t(-2+\delta t)| \leq 1$ which is equivalent to $\Delta t \leq \frac{2}{2-\delta t}$. For any $\delta t$ the perturbation ensure that the micro-macro stability bound is larger than the deterministic bound for the variance. Figure \ref{fig:ch5meanvariancestability} depicts the effect of the perturbation for $\delta t = \varepsilon = 0.1$ with stability bound $\Delta t \leq 1.053\dots$, after $T=210$ seconds. For $\Delta t \leq 1.053$ the numerical invariant distribution of the micro-macro scheme approximates the invariant distribution of the Euler-Maruyama scheme very well, but when $\Delta t$ crosses the stability boundary, the adaptive strategy kicks due to matching failures. The maximum number of Newton iterations in this experiment is 8.

\begin{figure}[!ht]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/meanvarianceDt10_statistics.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/meanvarianceDt1033_statistics.tex}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/meanvarianceDt1066_statistics.tex}
	\end{subfigure}%
	\begin{subfigure}[b]{0.4\textwidth}
		\input{../../Source/Python/Samples/linear/plots/stability/meanvarianceDt11_statistics.tex}
	\end{subfigure}
	\caption{Histograms of mean-variance extrapolation of the linear system with $\varepsilon = 0.1$ after 210 seconds for several values of $\Delta t_{\text{max}}$ below and above the stability bound.}
	\label{fig:ch5meanvariancestability}
\end{figure}