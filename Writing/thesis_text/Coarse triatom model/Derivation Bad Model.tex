\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\begin{document}
\noindent
Given a potential function $V(X)=V(x_a, x_c, y_c) = \frac{1}{2\varepsilon}(x_a-l_{eq})^2+\frac{1}{2\varepsilon}(r_{c}-l_{eq})^2 + W(\theta)$ with $r_c=\sqrt{x_c^2+y_c^2}$ and $\theta$ the angle between molecules $A = (x_a,0)$ and $C=(x_c,y_c)$, with potential $W(\theta) = \frac{k_{\theta}}{2}((\theta-\frac{\pi}{2})^2 - \delta \theta^2)^2$ and the molecule B fixed in the origin. The corresponding system of SDEs to the potential function is
\begin{equation*}
\begin{aligned}
dX_a &= -\frac{\partial V}{\partial x_a}dt + \sqrt{2\beta^{-1}}dW_x \\
dX_c &= -(\frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial x_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial x_c})dt + \sqrt{2\beta^{-1}}dW_x \\
dY_c &= -(\frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial y_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial y_c})dt + \sqrt{2\beta^{-1}}dW_x
\end{aligned}
\end{equation*}

\vspace{2mm}
\noindent
Now we take as reaction coordinate $\xi(x_a, x_c, y_c) = \norm{A-C}^2 = \norm{(x_a-x_c)^2+y_c^2}^2$, then the effective dynamics for $\xi(x_a,x_c,y_c,y)$ is given by $dz = b(z)dt+\sqrt{2\beta^{-1}}\sigma(z)dW$ where
\begin{equation}
\begin{aligned}
b(z) &= \int_{\xi(X)=z} (-\nabla V \cdot \nabla \xi + \Delta \xi) \rho^{\infty}(X) dX \\
\sigma(z)^2 &= \int_{\xi(X)=z} \norm{\nabla \xi(X)}^2 \rho^{\infty}(X) dX
\end{aligned}
\end{equation}
with $\rho^{\infty}(X) = \frac{1}{Z}\exp(V(X))$ the invariant distribution and $Z$ the normalization constant.

\vspace{2mm}
\noindent
In the case of $\xi$, the identities hold:
\begin{equation}
\begin{aligned}
\nabla \xi &= (4((x_a-x_c)^2+y_c^2)(x_a-x_c), -4((x_a-x_c)^2+y_c^)(x_a-x_c), 4((x_a-x_c)^2+y_c^)y_c \\
\nabla V &= (\frac{\partial V}{\partial x_a}, \frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial x_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial x_c}, \frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial y_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial y_c}) \\ 
\end{aligned}
\end{equation}
such that after some computation
\begin{equation}
\Delta \xi = 28(x_a-x_c)^2+20y_c^2
\end{equation}
and some more computations yield
\begin{equation}
\begin{aligned}
\nabla V \cdot \nabla \xi &= \frac{\partial V}{\partial x_a} ((x_a-x_c)^2+y_c^2)(x_a-x_c) \\
 &+ (\frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial x_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial x_c}) (-4)((x_a-x_c)^2+y_c^2)(x_a-x_c) \\
 &+ (\frac{\partial V}{\partial r_c}\frac{\partial r_c}{\partial y_c}+\frac{\partial V}{\partial \theta}\frac{\partial \theta}{\partial y_c})4y_c((x_a-x_c)^2+y_c^2) \\
 &=\frac{4}{\varepsilon}(x_a-l_{eq})((x_a-x_c)^2+y_c^2)(x_a-x_c) \\
 &-\left(\frac{1}{\varepsilon}(\sqrt{x_c^2+y_c^2}-l_{eq})\frac{x_c}{\sqrt{x_c^2+y_c^2}}-\frac{dW}{d\theta}\frac{y_c}{x_c^2+y_c^2}\right)((x_a-x_c)^2+y_c^2)(x_a-x_c) \\
 &+4(\frac{1}{\varepsilon}(\sqrt{x_c^2+y_c^2}-l_{eq})\frac{y_c}{\sqrt{x_c^2+y_c^2}}+\frac{dW}{d\theta}\frac{x_c}{x_c^2+y_c^2})y_c((x_a-x_c)^2+y_c^2)(x_a-x_c)
\end{aligned}
\end{equation}
where in the derivative of the cosine terms in $W(\theta)$ I assume that $y_c$ to avoid bistability in $y_c$ but only impose it on $x_c$ and hence on $\theta$. The problem now lies in computing the integrals of $\nabla V \cdot \nabla \xi $ and $\Delta \xi$ since we need to compute for every fixed value of $z$ the integral on the contour where $\xi(x_a.x_c,y_c)=z$. Substituting everything in terms of $z$ would be perfect but I'm afraid that is not really possible. For example we can write
\[
\Delta \xi = 20z+8(x_a-x_c)
\]
but I'm not sure if
\[
\int_{\xi(X)=z}  (28(x_a-x_c)^2 + 20y_c^2)\rho^{\infty}(X) dX = 20z+8\int_{\xi(X)=z}y_c^2\rho^{\infty}(X)dX
\]
or whether it yields the same dynamics as 
\[
-8\int_{\xi(X)=z}  (x_a-x_c)^2 \rho^{\infty}(X) dX +28z.
\]
And even if it is, the expressions for $\nabla V \cdot \nabla \xi $ are a lot more involved.
\end{document}