\documentclass{report}


\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry}
\geometry{
	left=10mm,
	right=10mm,
	bottom=20mm,
	top=20mm
}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}


\date{Academic year 2017-2018}
\title{Results of fene experiments}
\author{Hannes Vandecasteele \\
	2e master Wiskundige Ingenieurstechnieken}

\begin{document}
	\maketitle
	\tableofcontents

\section{Introduction}
This note contains an explanation to the numerical results for the fene bubbles experiments in reference \cite{micromacroacceleration}. The first section reproduces the results from section 7.1 and the second from section 7.2.
	
\section{Numerical results of matching}
We consider a simplified 1-d fene model  without coppeling to the navier-stokes equation. The SDE then becomes
\[
dX(t) = (\kappa(t)X(t)-\frac{1}{2W_e}F(X(t)))dt+ \frac{1}{\sqrt{W_e}}dW(t)
\]
with the Weisenberg number $W_e=1$ and the velocity field $\kappa(t)=2$. To discretise this equation we employ an accept-reject Euler-Maruyama scheme as $\tilde{X}^{k+1} = X^k +(\kappa(t^k)X^k-\frac{1}{2W_e}F(X^k))\Delta t+ \frac{1}{\sqrt{W_e}}\sqrt{\Delta t} \xi^k$ with $xi^k$ standard normal distributed and all iid. When $\norm{\tilde{X}^{k+1}} > \alpha \gamma$ then we reject and try a different gaussian sample. $\alpha$ is chosen to be $(1-\sqrt{\Delta t})$ because the lower the time step, the more we expect to accept. The initial distribution of the model is chosen to be the invariant distribution of the fene model under zero velocity gradient, being $\frac{1}{Z} \exp(-2W_eU(x))$ with $U(x) = -\frac{b}{4} \ln(1-\frac{x^2}{b})$ the force potential $F(x) = -\nabla U(x)$.

To perform matching, we use the first $L$ even moment functions $R_l(x) = \left(\frac{x}{\gamma}\right)^{2l}$ because the odd moment functions should be zero because the system is symmetric in $x$. Of course, numerically this will not exactly yield zero due to the finite number of samples in the discretised probability space. The zeroth moment (mass) is not included in this count $L$ but always in the experiments. For KLD and L2D matching we set the tolerance of the Newton-Rhapson solver to $10^{-9}$.

\subsection{Sampling from the initial distribution}
An initial problem is that the normalization constant in the initial distribution is not known. There are a few ways of getting around this. The first is the standard MCMC algorithm where you simulate the SDE with an accept-reject procedure. The downside is that the samples will be correlated and the results depend on the timestep you choose here. Another method is just numerically computing the constant $Z$ in scipy using 'quad' and then use a standard accept-reject method with a uniform prior distribution. We used the second approach.

\subsection{Full micro simulation}
Figure \ref{fig:fullmicroscale} shows the stress and pdf's of a full microscale simulation of the fene-model. We can see that the pdf goes from a peek in 0 at time 0 to almost flat at time 1s to a sharp peek around 6.5 for higher t. Also the stress increases rapidly in the beginning before it reaches an equilibrium around 140. These results are the same as in paper \cite{micromacro} and hence confirm the correctness of our micro Euler-Maruyama implementation. These results were obtained from one sample run and are not the average over multiple iid runs. 

\subsection{Empirical matching pdf's}
In this section we try out the different matching algorithms, KLD, L2D and L2N for number of moments $L = 3, 5, 7$. As a prior distribution, we take the microscopic ensemble after $t = 1.0$ and the target moments are the moments of the empirical distribution at $t=1.1$ obtained using a full-scale micro simulation. Figures \ref{fig:KLDEmpirical}, \ref{fig:L2DEmpirical} and \ref{fig:L2NEmpirical} show these empirical pdf's for KLD, L2D and L2N matching respectively for $L = 3, 5, 7$ moments. The zero'th moment, the mass of the pdf, is always added to the matched moments. For KLD, we see that the matched pdf's are very close to the exact pdf after 1.1 seconds, and that the are basically equal for different values of $L$. The same observation holds for L2D matching. For L2N, we see that increasing the number of moments yields a better approximation of the matched pdf's to the exact pdf, but they can however be negative, as can be seen slightly for $L=5$. This is because the conditions of Lemma 4.1 in \cite{micromacroacceleration} are not satisfied.

The empirical pdf's were obtained using a combination of the resampling algorithm (because the matched pdf has all diferent weights), and Gaussian kernel density estimation with a bandwith of $0.01 \gamma$.

\subsection{Errors in the computed moments}
The moments of a distribution in principle contain all the information about underlying distribution. Therefore it is useful to look at the errors between moments of the matched distribution and the exact distribution, for moments greater than the matched moments. For KLD, L2D and L2N these are in Figures \ref{fig:KLDmoments}, \ref{fig:L2Dmoments} and \ref{fig:L2Nmoments} for $L=3, 5, 7$. On the figures we obviously see that for $l \leq L$ the error is under the tolerance of the Newton-Rhapson solver, indicating convergence. The error for L2D also seems lower than KLD, and if $L$ increases, the error in a fixed moment $l$ also decreases. The results were obtained for 10 iid runs with prior distribution at time 1.0 seconds and target moments are the moments of the exact (simulated) distribution at time 1.1 seconds.

\subsection{Matching error as a function of the time step}
In this experiment, we perform matching with prior distribution at time $t=1$ second, and look at the relative error in the computed stress $|\tau(t) -\hat{\tau}(t)|/|\tau(t)|$ for matching timesteps $\Delta t = 0.001, 0.01 $ and $0.1$ and this for $L = 3, 5, 7$ moments and all the matching algorithms. The results are obtained using the average relative errors over 100 iid runs. These are plotted in Figures \ref{fig:KLDConvergence}, \ref{fig:L2DConvergence} and \ref{fig:L2NConvergence}. For all the matching techniques, we see that increasing $L$ decreases the relative error on the stress and for L2N we clearly have first order, as expected from theorem in section 5. For L2D and KLD the order is almost one but a little bit less. The reason for this may be that the error in the microsimulation or the constant $C_L$ may be taking over for small values of $\Delta t$. The results are however in line with paper \cite{micromacroacceleration}.

\subsection{On the number of NR steps}
The larger the matching step $\Delta t$, the more NR steps we may expect because the prior distribution will be further away from the target moments. It is interesting to have an idea on how the number of steps depends on the matching timestep. Figures \ref{fig:KLDNR} and \ref{fig:L2DNR} show this dependency for KLD and L2D matching for a range of time steps and for $L = 3, 5, 7$. For L2D matching, the number of Newton iterations always seems to be one while for KLD matching there are discrete jumps, but it remains under 5 mostly. The results were obtained over 100 iid runs.
\end{document}	