\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\begin{document}
	\title{Comparing Fokker-Planck with the closure relations and a micro simulation}
	\author{Hannes Vandecasteele}
	\maketitle

It is important for future use that the numerical simulation of the Fokker-Planck equation agrees with the results from the closure relations and is equivalent to a complete Monte-Carlo micro simulation of the system of linear stochastic differential equations. Here we will compare them for the simple linear slow-fast system of SDE's given by
\[
\begin{cases}
dX = -\frac{1}{\epsilon}Xdt + \frac{1}{\sqrt{\epsilon}}dW_x \\
dY = (X-Y)dt + dW_y
\end{cases}
\]
where $\epsilon$ indicates the scale separation.

We will write down the exact equations we are simulating for the Fokker-Planck simulation and the closure relations after which we will perform two experiments. In the first, we start with a symmetric uniform distribution on the rectangle $[-6,6] \times [-6,6]$, while for the second we start with a gaussian distribution on the rectangle $[-6,6]\times[-6,6]$ with means $[1,2]$ and variances both 1. The initial covariance is also chosen to be 0.

\section{Models used in this report}
The Fokker-Planck equation corresponding to the linear system above is given by
\[
\frac{dp(x,y,t)}{dt}+\partial_x(-\frac{1}{\epsilon}xp(x,y,t))+\partial_y((x-y)p(x,y,t)) = \frac{1}{2}(\frac{1}{\sqrt{\epsilon}}\partial_{xx}p(x,y,t)+\partial_{yy}p(x,y,t)).
\]
This equation can be discretized using a finite volume scheme to conserve the total mass numerically. For more details, we refer to section blabla.

The closure relations for the linear SDE above are given by
\[
\frac{d}{dt} \begin{pmatrix} \mu_X \\ \Sigma_X \\ \mu_Y \\ \Sigma_Y \\ \text{Cov}(X,Y) \end{pmatrix} = \begin{pmatrix} -\frac{1}{\epsilon} & 0 & 0 & 0 & 0 \\ 0 & -\frac{2}{\epsilon} & 0 & 0 & 0 \\ 1 & 0 & -1 & 0 & 0 \\  0 & 0 & 0 & -2 & 2 \\ 0 & 1 & 0 & 0 & -(\frac{1}{\epsilon}+1) \end{pmatrix} \begin{pmatrix} \mu_X \\ \Sigma_X \\ \mu_Y \\ \Sigma_Y \\ \text{Cov}(X,Y) \end{pmatrix} + \begin{pmatrix} 0 \\ \frac{1}{\epsilon} \\ 0 \\ 1 \\ 0 \end{pmatrix},
\]
for the derivation we refer to section...

For the micro simulation, we are using an Euler-Maruyama scheme with reflective boundary conditions. Because X and Y are confined to a rectangle, the particles should reflect at the boundaries to conserve the total mass of the density in the rectangle.


\section{A first experiment: Uniform initial distribution}
 The closure relations are simulated using a standard forward Euler method with step size 0.01 and initial condition the statistics for a uniform distribution on a grid $[-6,6] \times [-6,6]$. The Fokker-Planck equation is simulated on the same grid with $N_x = N_y = 48$ grid points in the X and Y direction and with the same initial condition and finally for the full micro simulation we are using the Euler-Maruyama method with step size 0.01 and the initial distribution is again the uniform distribution sampled with $10^5$ particles. 

 Figure \ref{fig:Agreement_FP_Closure_Microsimation_tend2} shows the simulated variance in the slow and fast variables agree nicely with each other. The fast variable relaxes quicly to a variance of 0.5, while the slow variable just reaches equilibrium after about 2.5 seconds. Possible small differences in variance can be attributed to the spatial discretization for FP(only twelve nodes in $x$ and $y$) and the temporal discritization for FP, the closure relations and the micro simulation.
 
 The mean of all simulations is zero as expected because the initial distribtuion has mean 0 and the invariant distribution too. These plots are not included in this report. Finally the covariance is plotted in Figure \ref{fig:Agreement_FP_Closure_Microsimation_covariance_tend2} and they also seem to agree nicely.

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Agreement_FP_experiment1_variance}
\caption{The simulated variance of the fast and slow variables using finite volumes for the Fokker-Planck equation, forward Euler for the closure relations and Euler-Maruyama for the SDE. They agree nicely and the errors can be attributed to discretizations in space and time.}
\label{fig:Agreement_FP_Closure_Microsimation_tend2}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Agreement_FP_Experiment2_covariance}
\caption{The covariance between the fast and slow variable for the first experiment. The three methods agree closely and the differences can be attributed to discretization errors in space and time.}
\label{fig:Agreement_FP_Closure_Microsimation_covariance_tend2}
\end{figure}

\section{A Second experiment: Mean not in the origin}
In the previous experiment, the mean of the initial condition was zero, and of the invariant distribution too. When the initial mean is not zero, the results may be different. In this experiment, the mean vector was chosen to be $[1,2]$ and the variance in $X$ and $Y$ equal to one. The initial covariance is zero. For the Fokker-Planck equation we are using a grid on $[-6,6]\times[-6,6]$ with $N_x = N_y = 24$ grid points in each direction. This is the same density of grid points as in the first experiment. The closure model is again simulated with the forward Euler method with step size 0.01 and the SDE is simulated using Euler-Maruyame with the same step size and $10^5$ independent particles. 

Figure \ref{fig:Agreement_FP_Experiment2_mean} shows the mean for three methods as  a function of time and Figure \ref{fig:Agreement_FP_Experiment2_variance} the variances. The means are almost exactly equal and also the variances seem to agree again nicely, although the steady state variance of the Euler-Maryama scheme for the fast variable overestimates the true variance a bit but this can be attributed to discretization errors in time and probability space. Finally, Figure \ref{fig:Agreement_FP_Experiment2_covariance} shows the covariance for these methods and again there are no surprises.

\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Agreement_FP_Experiment2_mean}
\caption{Mean of the slow and fast variable for the Fokker-Planck simualation, closure model and micro simulation for the second experiment. These three experiments produce almost exactly the same means in X and Y.}
\label{fig:Agreement_FP_Experiment2_mean}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Agreement_FP_Experiment2_variance}
\caption{The variances of the slow and fast variables computed with the three methods above. They seem to have the same behavior although the steady state values are a bit different.}
\label{fig:Agreement_FP_Experiment2_variance}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Agreement_FP_Experiment2_covariance}
\caption{Covariance between the fast and slow variables for the second experiment for the three numerical methods.}
\label{fig:Agreement_FP_Experiment2_covariance}
\end{figure}

These two experiments, together with the convergence plots for each method above give enough confidence to trust the numerical results.
\end{document}