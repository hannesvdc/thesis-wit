\select@language {english}
\contentsline {chapter}{\numberline {1}Matching for a linear test SDE}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Matching with only coarse moments}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Analytic results for matching with a Gaussian initial distribution}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Minimizing relative entropy}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Matching with extrapolated coarse mean}{5}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Matching with extrapolated coarse mean and variance}{6}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Numerical experiments}{7}{subsection.1.2.4}
\contentsline {subsubsection}{Propagation of mean and variance with the Euler-Maruyama scheme}{8}{section*.2}
\contentsline {subsubsection}{Experiment 1: extrapolating the coarse mean}{8}{section*.3}
\contentsline {subsubsection}{Experiment 2: extrapolating coarse mean and variance}{9}{section*.6}
\contentsline {section}{\numberline {1.3}Efficiency of acceleration for linear SDE's}{12}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Fokker-Planck, Closure relations and Euler-Maruyama}{12}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Optimal number of moments for linear SDE's}{12}{subsection.1.3.2}
