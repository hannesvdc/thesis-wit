\documentclass{kulakreport}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\theoremstyle{proposition}
\newtheorem*{proposition}{Proposition}
\theoremstyle{lemma}
\newtheorem*{lemma}{Lemma}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\theoremstyle{theorem}
\newtheorem*{theorem}{Theorem}

\begin{document}
\title{Matching for a linear test SDE}
\author{Hannes Vandecasteele}
\tableofcontents

\chapter{Matching for a linear test SDE}
This chapter discusses in detail how matching performs on a two dimensional linear test equation 
\begin{equation}\label{eq:lineartest}
dX(t) = AX(t)dt + BdW(t)
\end{equation}
with $A, B \in \mathbb{R}^{2 \times 2}$ and more specifically on a stiff slow-fast system of the form
\begin{equation}\label{eq:slowfastlinear}
\begin{aligned} 
dX(t) &= -\frac{1}{\epsilon}X(t)dt + \frac{1}{\sqrt{\epsilon}}dW_x(t) \\
dY(t) &= (X(t)-Y(t))dt + dW_y(t)
\end{aligned}
\end{equation}
with a time scale separation $\epsilon$. There are two fundamental questions concerning the micro-macro acceleration algorithm that can be studied with a linear test equation: the number of moments of the slow and fast variables that are necessary to approximate the solution as well as possible, and how large the extrapolation step can be before the simulation becomes unstable. The first question has not been studied thoroughly yet and is one step on the way of studying efficiency of the acceleration algorithm for more complicated problems. This chapter deals with this question while next chapter discusses stability.

\vspace{2mm}
\noindent
 The chapter starts with an important analytical decomposition of the joint density when the matching algorithm only uses coarse moments for extrapolation. In practice only these coarse moments are really of interest so a deeper understanding of this situation is useful. Then a section on analytical and numerical results follows for matching on the linear SDE when several coarse moments are used for extrapolation. It will turn out that if the prior distribution is Gaussian then every step during the micro-macro acceleration algorithm will be Gaussian too. Finally the chapter closes with a discussion on how many slow and fast moments are necessary to simulate the test equation as accurately as possible. 

\section{Matching with only coarse moments}
In practice we are usually only interested in the behaviour of the macroscopic stochastic variables so it would be useful to perform matching with only moments of the coarse variables. The reasons are two-fold. First, it is not unintuitive to assume that the fast variables will relax quickly to an equilibrium distribution after only a few microscopic steps. This allows for healing of the fast density after matching with only coarse moments and this happens on faster scales than the movement of the coarse variables. If the fast variables can relax quickly then there is no stringent reason to include these moments in the restriction operator. Second, adding more moments increases the computational burden of matching and will result in a slower Newton-Raphson procedure. This is often the computational bottleneck and minimizing this when possible will yield a faster total acceleration algorithm with the same accuracy of the slow values. 

\vspace{2mm}
For relative entropy matching there exists a decomposition formula that gives the full matched distribution as a function of the matched marginal distribution of only the coarse variables and a conditional distribution of the fast modes, given the slow. This effectively decouples fast from slow dynamics and will be an important result for further proofs on the linear test SDE's. The derivation that follows was taken from [] and reworked in the notation of this thesis.

\vspace{2mm}
Suppose the joint density between the slow stochastic variables $Y$ and fast variables $X$ is given by $\rho(z)$ with $z = (x,y)^T$. Another way of expressing this decomposition is by introducing projection operators $\Pi_m : \mathbb{R}^{m+d} \to \mathbb{R}^m$ and $\Pi_d: \mathbb{R}^{m+d} \to \mathbb{R}^d$ such that $x = \Pi_d(z)$ and $y = \Pi_m(z)$. Further assume a vector of coarse moments $m \in \mathbb{R}^L$ corresponds to the restriction operator $\mathcal{R}_L$ using only the coarse stochastic variables:
\[
\mathcal{R}_L(\mu) = (\mathbb{E}_{\mu}[R_1(Y)], \mathbb{E}_{\mu}[R_2(Y)], \dots \mathbb{E}_{\mu}[R_L(Y)])=m.
\]
Also denote for shortness of notation $\rho^*(z) = \mathcal{M}(m, \rho(z))$ as the result of applying the relative-entropy matching operator $\mathcal{M}$ to these moments $m$ with prior density $\rho(z)$. Then for relative entropy matching, formula {} states that the matched density can be written as
\[
\rho^*(z) = \frac{1}{Z} \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(z)
\]
where $Z$ is a normalization constant. This formula above is related to the reweighing of the particles after matching and the Lagrange multipliers $\lambda = (\lambda_1, \dots, \lambda_L)$ are the solution of the set of non-linear equations given by
\begin{equation} \label{eq:fullmoments}
\frac{\int_{\mathbb{R}^{m+d}} R_l(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(z) dz}{\int_{\mathbb{R}^{m+d}}\exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(z) dz} = m_l
\end{equation}
These formulas are exactly the same as for KLD-matching in chapter 2. Factorizing the joint density as  $\rho(z) = \rho(x|y) \rho(y)$ into a marginal density of the slow variables $\rho(y)$ and a conditional distributions of the fast variables given the slow $\rho(x|y)$ then gives a way of integrating out the fast variables for a general moment function $\phi(y)$
\begin{align*}
\int_{\mathbb{R}^{m+d}} \phi(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(z) dz &= \int_{\mathbb{R}^m} \int_{\mathbb{R}^d} \phi(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(x|y) \rho(y) dx dy \\
&= \int_{\mathbb{R}^m} \phi(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(y)  \int_{\mathbb{R}^d}  \rho(x|y) dx dy \\
&= \int_{\mathbb{R}^m} \phi(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(y) dy.
\end{align*}
Applying this identity to \ref{eq:fullmoments} with $\phi(y) = R_l(y)$ in the numerator and $\phi(y) = 1$ in the denominator results in 
\begin{equation} \label{eq:coarsemoments}
\frac{\int_{\mathbb{R}^m} R_l(y) \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(y) dy}{\int_{\mathbb{R}^m}\exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(y) dy} = m_l,
\end{equation}
or said differently: the Lagrange multipliers corresponding to the matching problem with fine and coarse variables are exactly the same as the multipliers for the coarse matching problem only. An interesting consequence of this result is that the matched microscopic density $\rho^*(z)$ is also be decomposable as the matched density of the marginal distribution of the slow variables $\rho^*(y)$ and the conditional density of the fast given  the slow variables $\rho(x|y)$:
\begin{equation} \label{eq:matchingdecomposition}
\rho^*(z) = \frac{1}{Z} \exp{\left(\lambda \cdot \mathcal{R}_L(y)\right)} \rho(x|y) \rho(y) = \rho^*(y) \rho(x|y)
\end{equation}
where the matched marginal distribution of the slow variables equals
\[
\rho^*(y) = \frac{1}{Z}  \exp{\left(\lambda * \mathcal{R}_L(y)\right)} \rho(y)
\]
and the normalization constant $Z$ for both matching problems is the same.

\vspace{2mm}
\noindent
Equation \ref{eq:matchingdecomposition} is an import result on its own but will also be useful to prove some properties of relative entropy matching in the context of linear equations with a Gaussian prior.




\section{Analytic results for matching with a Gaussian initial distribution}
If the initial distribution a linear system of SDE's \ref{eq:lineartest} is Gaussian, then all intermediate steps in the micro-macro acceleration algorithm are Gaussian too. This is a well-known result of the Euler-Maruyama method and thus for the $K$ internal microscopic steps but the same is true for relative entropy matching. If the prior distribution is Gaussian then the matched density turns out to be Gaussian too. This result is independent of whether only the coarse mean, variance or both are extrapolated and is proven in this section. The proof will rely heavily on decomposition \ref{eq:matchingdecomposition} and another important lemma stated first. The discussion ends with a numerical verification of these analytical results.

\subsection{Minimizing relative entropy}
The following lemma is a building block for the theorems concerning matching with a Gaussian prior. It concerns minimizing the relative entropy in the context of KLD-matching where the new distribution needs to have a predefined (extrapolated) mean and/or variance and the where the prior is normally distributed. The objective is thus to find the minimizing distribution $Q^*$ of the Kullback-Leibler divergence
\[
\mathcal{D}(Q||P) = \int{q(y)\ln\left(\frac{q(y)}{p(y)}\right)dy}
\]
where $P$ is a Gaussian distribution and $Q$ needs to have certain moments fixed. The following proof only holds when the matched distribution needs to have a certain mean but an extension of the proof to extrapolated mean and variance is straightforward and is discussed afterwards.

\begin{lemma} \label{lemma:entropyminimization}
	Given a prior Gaussian distribution $P = \mathcal{N}(\mu, \Sigma)$, and a new mean $\mu^*$ then the minimizer $Q^*$ of the constrained relative entropy minimization problem
	\begin{equation}
	\underset{Q}{\text{min}} \ \mathcal{D}(Q||P) = \int{q(y)\ln\left(\frac{q(y)}{p(y)}\right)dy} \ \ \ \text{subject to} \ \ \ \mathbb{E}[Q]=\mu^* \ \text{and} \ Q << P
	\end{equation}
	is again a Gaussian distribution with new mean $\mu^*$ and the prior variance $\Sigma$.
\end{lemma}
\begin{proof}
	For simplicity of notation, this proof only holds for one dimensional distributions and the dummy variable $y$ is used inside the integrals to suggest 'slow' variables. A generalization to more dimensions is straightforward. Using the fact that $p(y) \sim \mathcal{N}(\mu, \Sigma)$, we can expand the logarithm term to
	\begin{equation}
	\int{q(y)\ln(q(y))dy} - \int{q(y)\ln\left(\frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(y-\mu)}{2\sigma^2}\right)\right)dy}.
	\end{equation}
	The first term is the opposite of the Shannon entropy of a distribution $q(y)$ also denoted by $-\mathcal{H}(q)$. The second term can be further expanded using properties of the logarithm in moments of $q(y)$ to 
	\begin{equation}
	-\mathcal{H}(q) - \frac{1}{2}\ln(2\pi\sigma^2)\int{q(y)dy} + \frac{1}{2\sigma^2}\int{q(y)(y-\mu)^2dy}
	\end{equation}
	and then using that $q(y)$ is a probability density with mean $\mu^*$ we can finally write the relative entropy as 
	\begin{equation} \label{eq:entropytominimize}
	-\mathcal{H}(q) - \frac{1}{2}\ln(2\pi\sigma^2)+\frac{1}{2\sigma^2}(\text{Var}[q]-(\mu-\mu^*)^2).
	\end{equation}
	A basic result from information theory states that for a given variance $\sigma^{*2}$ the distribution that maximizes the Shannon entropy is exactly a Gaussian distribution with this variance, regardless the mean. The value of the entropy then is $\ln(\sigma^*\sqrt(2\pi e))$. The first term in equation \ref{eq:entropytominimize} is hence minimized by the Gaussian distribution with variance $\sigma^{*2}$. If we now this variance then and plug in $\mathcal{N}(\mu^*, \sigma^{*2})$ for $q(y)$ then a simple calculation shows that the relative entropy reads
	\begin{equation} \label{eq:functionentropytominimize}
	\mathcal{D}(Q||P) = \frac{1}{2} + \frac{1}{2}\ln\left(\frac{\sigma^2}{\sigma^{*2}}\right) + \frac{\sigma^{*2}}{2\sigma^2} + \frac{(\mu-\mu^*)^2}{2\sigma^2}.
	\end{equation}
	and it minimizes the value of \ref{eq:entropytominimize} over all distributions with variance $\sigma^{*2}$. The function \ref{eq:functionentropytominimize} is convex in $\sigma^*$ and attains its global minimum in $\sigma^*$ = $\sigma$  as it is the only feasible zero of the derivative of the previous expression in $\sigma^*$. This concludes the proof of this lemma.
\end{proof}

\noindent
\begin{remark} \label{remark:meanvariancekldminimizer}
	An similar lemma where the new distribution $Q^*$ needs to have a certain mean and  covariance matrix besides only an extrapolated mean is simpler to prove since it is not necessary to minimize over the variance of $Q$ since it is already fixed. Then the minimizer is also a Gaussian with the predefined mean and variance.
\end{remark}


\subsection{Matching with extrapolated coarse mean}
Now all elements are in place to prove that relative entropy matching with a Gaussian prior and coarse extrapolated moments results in a distribution that is also Gaussian. The proof here is specifically for the case where only the coarse mean is extrapolated but similar results also hold when only the variance or both are extrapolated. The formulas only become a bit more complicated. It will turn out that the matched variance exactly equals the prior variance. The proof of the theorem was taken from [] but with a slight change of notation.

\begin{theorem} \label{thm:linearmatchingmean}
	Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$ where the subscript $d$ again indicates the fast variables and $m$ the slow. Then the distribution $Q^*$ that minimizes the  Kullback-Leibler divergence, constrained with $\mathbb{E}[Q]_m = \mu_m^*$ is a normal distribution $\mathcal{N}(\mu^*, \Sigma)$ with $\mu^* = [\mu^*_d, \mu^*_m]$ and $\mu_d^* = \mu_d + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m)$.
\end{theorem}
\begin{proof}
Denote again $z=(x,y)^T$ and decompose the prior distribution $\mathcal{N}_{\mu, \Sigma}$ as the product of the marginal of the slow variables and a conditional probability of the fast variables given the slow
\[
\mathcal{N}_{\mu, \Sigma}(z) =  \mathcal{N}_{\mu_m, \Sigma_m}(y) \mathcal{N}_{\mu_{r|m}(y),\Sigma_{r|m}} (x).
\]
with
\begin{align} \label{eq:normalfactorizationdefinition}
\begin{cases}
\mu_{r|m}(y)&=\mu_r +C^T\Sigma_m^{-1}(y-\mu_m) \\
\Sigma_{r|m} &= \Sigma_r -C^T\Sigma_m^{-1}C.
\end{cases}
\end{align}
 This is a well known formula for normal densities. Formula \ref{eq:matchingdecomposition} states that only the matched density of the slow variables with prior $\mathcal{N}_{\mu_m, \Sigma_m}(y)$ is needed. From the multidimensional extension of lemma \ref{lemma:entropyminimization} we know that the minimizer of the Kullback-Leibler divergence with a prior Gaussian distribution and where the minimizer needs a certain mean $\mu_m^*$ is a new Gaussian with the same covariance as the prior. This is exactly the matched density with our Gaussian prior, so we can write 
\[
\mathcal{M}(\mu_m^*, \mathcal{N}_{\mu_m, \Sigma_m}) = \mathcal{N}_{\mu_m^*, \Sigma_m}.
\]
Hence by formula \ref{eq:matchingdecomposition} we have
\begin{equation} \label{eq:linearmeandecomposition}
\mathcal{M}(\mu_m^*, \mathcal{N}_{\mu, \Sigma})(z) =  \mathcal{N}_{\mu_m^*, \Sigma_m}(y) \mathcal{N}_{\mu_{r|m}(y),\Sigma_{r|m}} (x).
\end{equation}
Now we only need to rewrite this expression so that it forms a factorisation of another normal distribution. Adding and subtracting $C^T\Sigma_m^{-1}\mu_m^*$ from the conditional mean gives
\[
\mu_{r|m}(y) =\mu_r + C^T\Sigma_m^{-1}(\mu_m^*-\mu_m)+C^T\Sigma_m^{-1}(y-\mu_m^*) 
\]
and by defining $\mu_r^* =\mu_r + C^T\Sigma_m^{-1}(\mu_m^*-\mu_m)$, equation \ref{eq:linearmeandecomposition} becomes a decomposition for a joint normal density with mean $\mu^*=[\mu_r^*, \mu_m^*]$ and covariance matrix $\Sigma$. Written out this reads
\[
\mathcal{M}(\mu_m^*, \mathcal{N}_{\mu, \Sigma})  = \mathcal{N}_{\mu^*, \Sigma}.
\]
so the density obtained by KLD-matching with a Gaussian prior and extrapolated coarse mean is again a normal distribution with the same covariance matrix but a different expression for the mean. This concludes the proof.
\end{proof}

\subsection{Matching with extrapolated coarse mean and variance}
A very similar result holds when the restriction operator also contains the coarse variance instead of only the coarse mean. The proof is completely analogous but a new result and it also depends on decomposition \ref{eq:matchingdecomposition} but uses a variant of lemma \ref{lemma:entropyminimization} where the minimizer also needs a predefined variance instead of only the mean.

\begin{theorem} \label{thm:linearmatchingmeanvariance}
	Suppose $P$ is a prior Gaussian distribution with mean $[\mu_d, \mu_m]^T$ and covariance matrix $\Sigma = \begin{bmatrix} \Sigma_d & C \\ C^T & \Sigma_m \end{bmatrix}$. Also assume that the restriction operator $\mathcal{R}$ contains the coarse mean $\mu_m^*$ and variance $\Sigma_m^*$. Then the distribution $Q^*$ that minimizes the KLD-divergence, constrained to $\mathbb{E}[Q]_m = \mu_m^*$ and $Var[Q]_m=\Sigma_m^*$ is again a Gaussian $\mathcal{N}(\mu^*, \Sigma^*)$ with $\mu^*$ and $\Sigma^*$ defined at the end of the proof.
\end{theorem}
\begin{proof}
The also starts with decomposition
\[
\mathcal{N}_{\mu, \Sigma}(z) =  \mathcal{N}_{\mu_m, \Sigma_m}(y) \mathcal{N}_{\mu_{r|m}(y),\Sigma_{r|m}} (x).
\]
By remark \ref{remark:meanvariancekldminimizer} the minimizer of the Kullback-Leibler divergence with predefined mean $\mu^*$ and variance $\Sigma^*$ and prior a Gaussian is again a Gaussian with the given mean and variance, so
\[
\mathcal{M}((\mu_m^*, \Sigma_m^*), \mathcal{N}_{\mu_m, \Sigma_m}) = \mathcal{N}_{\mu_m^*, \Sigma_m^*},
\]
and as a consequence by \ref{eq:matchingdecomposition} the full matched density reads
\begin{equation} \label{eq:tofactorize}
\mathcal{M}((\mu_m^*, \Sigma_m^*), \mathcal{N}_{\mu, \Sigma})(z) =  \mathcal{N}_{\mu_m^*, \Sigma_m^*}(y) \mathcal{N}_{\mu_{r|m}(y),\Sigma_{r|m}} (x).
\end{equation}
We would like that this matched density is also a Gaussian distribution with mean $\mu^* = [\mu_m^*, \mu_r^*]^T$ and covariance $\Sigma^* = \begin{bmatrix} \Sigma_m^* & C^* \\ C^{*T} & \Sigma_r^* \end{bmatrix}$. Suppose there exists a similar factorization for the matched density as for the prior distribution:
\begin{equation} \label{eq:jointfactorization}
\mathcal{M}((\mu_m^*, \Sigma_m^*), \mathcal{N}_{\mu, \Sigma})(z) = \mathcal{N}_{\mu^*,\Sigma^*}(z) = \mathcal{N}_{\mu_m^*,\Sigma_m^*}(y)\mathcal{N}_{\mu_{r|m}^*(y), \Sigma_{r|m}^*}(x)
\end{equation}
Where the parameters $\mu_{r|m}^*(x)$ and $\Sigma_{r|m}^*$ are defined similarly as in equation \ref{eq:normalfactorizationdefinition}:
\begin{align} \label{eq:jointfactorizationparameters}
\begin{cases}
\mu_{r|m}^*(y) &= \mu_r^* + C^{*T} \Sigma_m^{* -1}(y-\mu_m^*) \\
\Sigma_{r|m}^* &= \Sigma_r^* - C^{*T}\Sigma_m^{* -1} C^*.
\end{cases}
\end{align}
The first factor in equation \ref{eq:jointfactorization} already equals the marginal normal density of the slow variables in \ref{eq:tofactorize}, hence the second conditional factors also need to be the same. Writing out the mean and covariance matrices of the two factors yields
\begin{equation} \label{eq:propositionsystem}
\begin{cases}
\mu_r+C^T\Sigma_m^{-1}(y-\mu_m) = \mu_r^* + C^{*T}\Sigma_m^{*-1}(y-\mu_m^*) \\
\Sigma_r - C^T\Sigma_m^{-1}C = \Sigma_r^* - C^{*T}\Sigma_m^{*-1}C^*.
\end{cases}
\end{equation}
The first equation is a linear polynomial in $y$, implying that $C^{*T}=C^T\Sigma_m^{-1}\Sigma_m^*$. Equality of the constant terms in the first equation then gives $\mu_r^* = \mu_r + C^T\Sigma_m^{-1}(\mu_m^*-\mu_m)$ and finally the second equation gives the correct value for $\Sigma_r^*= \Sigma_r - C^T\Sigma_m^{-1}(\Sigma_m-\Sigma_m^*)\Sigma_m^{-1}C$. If we use these values for the unknowns, then equation \ref{eq:tofactorize} defines a conditional factorization of a normal distribution, meaning that $\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu, \Sigma})(z)$ is also be normally distributed with mean $\mu^*$ and covariance $\Sigma^*$.

\end{proof}

\subsection{Numerical experiments}
The formulas for the mean and variance of the Gaussian distribution after matching with a Gaussian prior are straightforward to implement and make it possible to check against numerical experiments. Furthermore they give a way of verifying the Python code of the micro-macro acceleration algorithm for the simplified linear case. The goal of this section is to perform numerical experiments on matching with both extrapolated mean and extrapolated mean and variance and see if they agree with the theoretical mean and variance the slow and fast variables should have at each step of the acceleration algorithm. 

\subsubsection{Propagation of mean and variance with the Euler-Maruyama scheme}
To verify the correctness of the micro-macro implementation over larger time scales, it is necessary to know how the mean and variance propagate through the $K$ microscopic steps. This thesis always uses the Euler-Maruyama scheme to perform these microscopic steps and then the formulas for mean and variance become relatively easy. Remember that for a (possibly weighted) ensemble of particles $(X_j)_{j=1}^{J}$ the Euler-Maruyama scheme for a linear set of SDE's is
\begin{equation} \label{eq:lineareulermaruyama}
\begin{aligned}
X_j^{n+1} &= (1+a \delta t)X_j^n + b\delta t Y_j^n + D_x \delta W_{x,j}^n, \ \ \ \delta W_{x,j}^n \sim \mathcal{N}(0, \delta t)\\
Y_j^{n+1} &= c\delta t X_j^n + (1+d \delta t)Y_j^n + D_y \delta W_{y,j}^n, \ \ \ \delta W_{y,j}^n \sim \mathcal{N}(0, \delta t)
\end{aligned}
\end{equation}
Taking expectations from both sides and using the Martingale property for SDE's gives
\begin{equation} \label{eq:linearexpectationeulermaruyama}
\begin{aligned}
\mathbb{E}[X^{n+1}] &= (1+a \delta t)\mathbb{E}[X^n] + b\delta t \mathbb{E}[Y^n] \\
\mathbb{E}[Y^{n+1}] &= c\delta t \mathbb{E}[X^n] + (1+d \delta t)\mathbb{E}[Y^n]. 
\end{aligned}
\end{equation}
Subtracting \ref{eq:linearexpectationeulermaruyama} from \ref{eq:lineareulermaruyama}, squaring both sides and taking expectations finally gives, after some calculations, how the variances propagate
\begin{equation} \label{eq:linearvarianceeulermaruyama}
\begin{aligned}
\text{Var}[X^{n+1}] &= (1+a\delta t)^2\text{Var}[X^n] + b^2 \delta t^2 \text{Var}[Y^n] + D_x^2 \delta t + 2b\delta t(1+a\delta t) \text{Cov}[X^n, Y^n] \\
\text{Var}[Y^{n+1}] &= (1+d\delta t)^2\text{Var}[Y^n] + c^2 \delta t^2 \text{Var}[X^n] + D_y^2 \delta t + 2c\delta t(1+d\delta t) \text{Cov}[X^n, Y^n] \\
\text{Cov}[X^n, Y^n] &=c\delta t (1+a \delta t) \text{Var}[X^n] + b\delta t(1+d \delta t) \text{Var}[Y^n] + ((1+a\delta t)(1+d\delta t)+bc\delta t^2) \text{Cov}[X^n, Y^n].
\end{aligned}
\end{equation}
Equations \ref{eq:linearexpectationeulermaruyama} and \ref{eq:linearvarianceeulermaruyama} completely describe how a Gaussian ensemble of particles $(X_j^n, Y_j^n)_{j=1}^J$ propagates through one step of the Euler-Maruyama scheme with microscopic step size $\delta t$. Together with the formulas for matching with a Gaussian prior, we can simulate how the exact means and variances should propagate through the acceleration algorithm for linear SDE's. The following experiments test the numerics against these formulas.

\subsubsection{Experiment 1: extrapolating the coarse mean}
In a first experiment we visually check the acceleration algorithm by plotting the mean and variance for both the slow and fast variable given by the matching algorithm and the analytical expressions stated in \ref{eq:linearexpectationeulermaruyama} an \ref{eq:linearvarianceeulermaruyama}. This experiment only extrapolates the coarse mean before matching so theorem \ref{thm:linearmatchingmean} gives the exact expression of the matching operator with a Gaussian prior. The variance of both variables should remain constant during matching.

\vspace{2mm}
\noindent
The initial condition of the experiment is a Gaussian with mean $[1,2]^T$ and the identity for the covariance matrix. The inner time stepper is a standard Euler-Maruyama scheme on the domain $[-6,6] \times [-6,6]$ with reflective boundary conditions and $N=10^5$ independent particles sampled from this normal distribution. In practice very few particles reach these boundaries as the distribution should converge to the origin so this has no consequence but will be important in later experiments. The numerical results for the mean are shown in Figure \ref{fig:meanreferencely1tend1gaussianinitialn5} and the variance in Figure \ref{fig:variancereferencely1tend1gaussianinitial}.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Mean_reference_Ly1_tend1_gaussianinitial_N5}
	\caption{The analytical results of matching compared to the micro-macro acceleration algorithm for a linear test SDE with only the coarse mean extrapolated and a Gaussian initial condition.}
	\label{fig:meanreferencely1tend1gaussianinitialn5}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Variance_reference_Ly1_tend1_gaussianinitial}
	\caption{The analytical results of matching compared to the micro-macro acceleration algorithm for a linear test SDE with only the coarse mean extrapolated and a Gaussian initial condition.}
	\label{fig:variancereferencely1tend1gaussianinitial}
\end{subfigure}
\end{figure}
\vspace{2mm}
\noindent
The blue lines represent the exact mean and variance of the SDE \ref{eq:slowfastlinear} computed using a closure model. The red lines represent the analytical solutions of the micro-macro procedure by theorem \ref{thm:linearmatchingmean} and formulas \ref{eq:linearexpectationeulermaruyama}, \ref{eq:linearvarianceeulermaruyama} for Euler-Maruyama. The green points are the means and variances computed by the acceleration algorithm at the moments in time when the numerical solution is known, i.e. after matching and after each microscopic step. Furthermore, the orange line is part of the numerical scheme and represents what the extrapolated means and variances would look like if all of these were extrapolated. The analytical solution lies almost exactly on the numerical results of the micro-macro acceleration algorithm at the known times, given by the green crosses. The discontinuities arise when a certain quantity is not part of the extrapolation operator and the matching operator corrects these values at each step by theorems \ref{thm:linearmatchingmean} and \ref{thm:linearmatchingmeanvariance}. For example, the red lines in \ref{fig:variancereferencely1tend1gaussianinitial} are indeed flat during extrapolation as theorem \ref{thm:linearmatchingmean} predicts.

\vspace{2mm}
For completeness Figure \ref{fig:covariancereferencely1tend1gaussianinitialn5} shows the numerical covariance in orange and the analytical exact result in red. These do not seem to agree exactly but this is due to statistical noise. The same experiment with $N= 10^6$ particles in Figure \ref{fig:covariancereferencely1tend1gaussianinitialn6} shows that the numerical acceleration algorithm and the analytical expressions agree much better.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=1.2\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Covariance_reference_Ly1_tend1_gaussianinitial_N5}
	\caption{Comparison between the covariance of the acceleration algorithm and the analytical expressions for $N=10^5$ particles.}
	\label{fig:covariancereferencely1tend1gaussianinitialn5}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Covariance_reference_Ly1_tend1_gaussianinitial_N6}
	\caption{Comparison between the covariance of the acceleration algorithm and the analytical expressions for $N=10^6$ particles. Both agree much more than in Figure \ref{fig:covariancereferencely1tend1gaussianinitialn5} due to  better approximation in probability space.}
	\label{fig:covariancereferencely1tend1gaussianinitialn6}
\end{subfigure}
\end{figure}

\vspace{2mm}
The experiment confirms first that the analytic expressions by theorem \ref{thm:linearmatchingmean} and formulas \ref{eq:linearexpectationeulermaruyama} and \ref{eq:linearvarianceeulermaruyama} are indeed equal to the results by the acceleration algorithm up to statistical noise and this confirms a correct implementation. Furthermore notice in Figure \ref{fig:variancereferencely1tend1gaussianinitial} that extrapolating one moment of the slow variable gives a completely wrong value for the numerical variance of this variable, so more slow moments are necessary. This is what the next experiment does.

\subsubsection{Experiment 2: extrapolating coarse mean and variance}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Mean_reference_Ly2_tend1_gaussianinitial}
	\caption{The analytical results of matching compared to the micro-macro acceleration algorithm for a linear test SDE with coarse mean and variance extrapolated and a Gaussian initial condition.}
	\label{fig:meanreferencely2tend1gaussianinitial}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../../Source/Python/samples/linear/plots/Analytical_Reference/Variance_reference_Ly2_tend1_gaussianinitial_N5}
	\caption{Comparison between the variance of the numerical acceleration algorithm and the analytical results for the slow and fast variables. The slow variance lies now more closely to the exact variance in blue.}
	\label{fig:variancereferencely2tend1gaussianinitialn5}
\end{figure}

Figures \ref{fig:meanreferencely2tend1gaussianinitial} and \ref{fig:variancereferencely2tend1gaussianinitialn5} show similar plots as the previous experiment but with extrapolated coarse mean and variance. By theorem \ref{thm:linearmatchingmeanvariance} the coarse variance cannot remain constant during matching because it is also extrapolated. Both figures again show that the analytical results agree with the numerical output of the micro-macro acceleration algorithm. Moreover, the variance for $L_y=2$ seems to lie a lot closer to the true variance than for $L_y=1$. This is a logical result since every intermediate distribution in the algorithm is Gaussian and these are completely described by mean and (co)variance.


\section{Efficiency of acceleration for linear SDE's}
The previous theorems and experiments essentially show that for a linear SDE and Gaussian initial condition the numerical acceleration algorithm agrees exactly with the analytical results and that extrapolating coarse mean and variance gives a better fit on the exact variance of the slow variable than only extrapolating the coarse mean. A fundamental question however remains: how many moments of the fast and coarse variables are necessary to get an as accurate simulation result as possible, given other parameters as $\delta t$ and $\Delta t$? Previous theorems stated that all intermediate distributions of the acceleration algorithm are Gaussian when the initial condition is normal too. This implies that two moments should be enough to characterize the intermediate distributions completely. Even stronger, when the restriction and extrapolation operator contain more than two moments, than these moments can all be written as a function of the mean and variance, called \textit{slaving relations}. It is very unlikely that these relations would hold after extrapolation too, so this could introduce a modelling error and the matched distribution could numerically not be normal anymore, which contradicts the theorems above. More than two moments can hence introduce modelling errors and this is a situation to avoid. This section looks experimentally into the issue of optimal number of moments.

\subsection{Fokker-Planck, Closure relations and Euler-Maruyama}
\vspace{2mm}
\noindent
The exact intermediate distributions of the linear SDE are not readily available, so in order to see how accurate the acceleration algorithm is, it is necessary to compare the numerical results with different methods that simulate the same problem. This chapter considers a deterministic simulation of the Fokker-Planck PDE in two spatial dimensions, simulating a closure model for means, variances and covariance and a full microscopic simulation using the standard Euler-Maruyama method. If these three methods agree up to discretization errors then one of these can server as substitute of the exact solution to assess the accuracy of matching.

\vspace{2mm}
Appendix A contains the mathematical derivation of a finite volume scheme for the 2D Fokker-Planck method and a derivation of the closure relations for general linear SDE's together with convergence tests for these methods. Figure \ref{fig:agreementdeterministic} shows the means and variances of the slow and fast variables for a Fokker-Planck implementation on a $[-6, 6] \times [-6,6]$ grid with 48 cells in each direction and reflective boundary conditions, together with the closure relations and Euler-Maruyama with a step size of 0.1. The initial condition is a Gaussian with mean $[1,2]$ and unit covariance. All three methods agree up to discretization errors and this result indicates that we can substitute any of these as exact solution in the efficiency plots. The blue lines in all the plots in this chapter already contained the closure relations as substitute of the exact solution.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/Agreement_simulations/Agreement_FP_Experiment2_mean}
	\label{fig:agreementfpexperiment2mean}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/Agreement_simulations/Agreement_FP_Experiment2_variance}
	\label{fig:agreementfpexperiment2variance}
\end{subfigure}
\caption{Simulated mean and variance of the fast and slow variables using the deterministic Fokker-Planck equations, the closure relations and a standard microscopic Euler-Maruyama method.}
\label{fig:agreementdeterministic}
\end{figure}

\subsection{Optimal number of moments for linear SDE's}
Figure \ref{fig:variancereferencely2tend1gaussianinitialn5} already shows that two coarse moments yield a good approximation of the exact variance in blue. The question arises is if the acceleration algorithm can perform better with more coarse moments. Figure \ref{fig:variancelx0ly0123gaussianinitial} shows the influence on the variance of adding extra coarse moments but keeping zero fast moments. The approximation does not lie closer to the exact variance which can be attributed to the fact that a Gaussian distribution is completely characterized by two coarse moments. The remaining error is due to extrapolation. Even more, the figure on the right even depicts that adding extra moments of the fast variable also has no influence. This can be attributed to the fact that the fast variable reaches equilibrium very fast and keeping its moments only increases the computational cost.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/Variance_Lx0Ly0123_gaussianinitial}
	\caption{}
	\label{fig:variancelx0ly0123gaussianinitial}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/linear/plots/Variance_Lx0123_Ly2_gaussianinitial_tend1}
	\caption{}
	\label{fig:variancelx0123ly2gaussianinitialtend1}
\end{subfigure}
\end{figure}

\end{document}