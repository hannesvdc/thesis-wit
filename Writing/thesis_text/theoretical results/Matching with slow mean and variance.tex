\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\theoremstyle{proposition}
\newtheorem*{proposition}{Proposition}
\theoremstyle{lemma}
\newtheorem*{lemma}{Lemma}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}

\begin{document}
	\title{Matching with extrapolated slow mean and variance with a Gaussian prior}
	\author{Hannes Vandecasteele}
	\maketitle

\section{Minimizing relative entropy with Gaussian prior}
We will first pose an important lemma that will be used in the main result of this document:

\begin{lemma}
Given a prior Gaussian distribution $P \sim \mathcal{N}(\mu, \Sigma)$, and a new mean $\mu^*$ then the minimizer $Q$ of the relative entropy
\begin{equation}
\mathcal{D}(Q||P) = \int{q(x)\ln\left(\frac{q(x)}{p(x)}\right)dx} \ \ \ \text{subject to} \ \ \ \mathbb{E}[Q]=\mu^*
\end{equation}
is again a Gaussian distribution with new mean $\mu^*$ and the prior variance $\Sigma$.
\end{lemma}
\begin{proof}
We will only prove this for the one dimensional case for simplicity of notation. A generalization to more dimensions is very similar. Using the fact that $p(x)$ is Gaussian, we can expand the logarithm term to
\begin{equation}
\int{q(x)\ln(q(x))dx} - \int{q(x)\ln\left(\frac{1}{\sqrt{2\pi\sigma^2}}\exp\left(-\frac{(x-\mu)}{2\sigma^2}\right)\right)dx}.
\end{equation}
The second term can again be expanded using properties of the logarithm in moments of $q(x)$ to 
\begin{equation}
\int{q(x)\ln(q(x))dx} - \frac{1}{2}\ln(2\pi\sigma^2)\int{q(x)dx} + \frac{1}{2\sigma^2}\int{q(x)(x-\mu)^2dx}
\end{equation}
and then using the fact that $q(x)$ is a probability distribution with mean $\mu^*$ we can finally write the relative entropy as 
\begin{equation} \label{eq:entropytominimize}
\int{q(x)\ln(q(x))dx} - \frac{1}{2}\ln(2\pi\sigma^2)+\frac{1}{2\sigma^2}(\text{Var}[q]-(\mu-\mu^*)^2).
\end{equation}
A very well known result from information theory is that for a given variance $\sigma^{*2}$ the distribution that maximizes the Shannon entropy is exactly a Gaussian distribution with this variance. The mean does not matter. The first term in equation \ref{eq:entropytominimize} is exactly the opposite of the Shannon entropy of $q(x)$, hence this is minimized for the Gaussian distribution with variance $\sigma^{*2}$. If we now fix the variance of $q(x)$ to $\sigma^{*2}$ and plug in this Gaussian for $q(x)$ (with mean $\mu^*$), then a simple calculation shows that the relative entropy then equals
\begin{equation} \label{eq:functionentropytominimize}
\mathcal{D}(Q||P) = \frac{1}{2}\ln\left(\frac{\sigma^2}{\sigma^{*2}}\right) + \frac{\sigma^{*2}}{2\sigma^2} + \frac{(\mu-\mu^*)^2}{2\sigma^2}.
\end{equation}
This function in $\sigma^*$ is minimized for $\sigma^*=\sigma$ as can be shown by computing the only feasible zero of the derivative of the previous expression in $\sigma^*$. This concludes the proof of this lemma.
\end{proof}
\begin{remark}
The previous lemma states that the relative entropy with a Gaussian prior and fixed new mean is also an Gaussian distribution with this new mean and as variance the variance of the prior. If we require that the new density $q(x)$ is also constrained to have a fixed variance $\sigma^{*2}$ then this density will also be Gaussian. The proof is in fact easier than the previous lemma since we already have a fixed variance $\sigma^{*2}$ hence we only need the fact that a maximizer of the Shannon entropy is a Gaussian and no further minimization as in equation \ref{eq:functionentropytominimize} is needed. This result will be used in the next section.
\end{remark}
\section{Analytical expression for matching with slow mean and variance with Gaussian prior}
Denote a normal distribution in $d = m+r$ dimensions with mean 
\begin{equation} \label{eq:meandefinition}
\mu = \begin{bmatrix}
\mu_m \\ \mu_r
\end{bmatrix}, \mu_m \in \mathbb{R}^m , \mu_r \in \mathbb{R}^r
\end{equation}
and covariance matrix
\begin{equation} \label{eq:covariancedefinition}
\Sigma = \begin{bmatrix}
\Sigma_m & C \\
C^T & \Sigma_r
\end{bmatrix}
\end{equation}
and for the restriction operator $\mathcal{R}_m$, we consider the coarse mean and variance
\begin{equation} \label{eq:restrictiondefinition}
\mathcal{R}_m(P) = \begin{bmatrix}
\mathbb{E}_P[\Pi_m] \\
\text{Var}_P[\Pi_m]
\end{bmatrix}
\end{equation}
where $\Pi_m$ is defined as the orthogonal projection operator from $\mathbb{R}^{m+r}$ to $\mathbb{R}^m$. We can now state and prove the following proposition.

\begin{proposition}
Define $\mathcal{M}$ the minimum relative entropy matching relative to the restriction operator $\mathcal{R}_m$ on mean and variance. Then it holds that for a Gaussian prior distribution $\mathcal{N}_{\mu,\Sigma}$ and slow marginal mean $\mu_m^*$ and covariance $\Sigma_m^{*}$
\begin{equation} \label{eq:proposition}
\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu, \Sigma}) = \mathcal{N}_{\mu^*,\Sigma^*}
\end{equation}
where $\mu^*$ and $\Sigma^*$ are defined by
\begin{equation} \label{eq:propositionresult}
\mu^* = \begin{bmatrix} \mu_m^* \\ \mu_r^* \end{bmatrix} \\
\Sigma^* = \begin{bmatrix} \Sigma_m^* & C^* \\ C^{*T} & \Sigma_r^* \end{bmatrix}.
\end{equation}
The matched fast mean, covariance and cross covariance with the slow variables are defined by
\begin{align}
\mu_r^* &= \mu_r + C^T\Sigma_m^{-1}(\mu_m^*-\mu_m) \\
C^{*T} &= C^T \Sigma_m^{-1} \Sigma_m^{*} \\
\Sigma_r^* &= \Sigma_r - C^T\Sigma_m^{-1}(\Sigma_m-\Sigma_m^*)\Sigma_m^{-1}C
\end{align}
\end{proposition}
\begin{proof}
Define for simplicity $z = (x, y)$, then a well known results is that we can factorize a normal distribution as 
\begin{equation} \label{eq:normalfactorization}
\mathcal{N}_{\mu,\Sigma}(z)=\mathcal{N}_{\mu_m, \Sigma_m}\mathcal{N}_{\mu_{r|m}(x),\Sigma_{r|m}}(y)
\end{equation}
where the parameters $\mu_{r|m}(x)$ and $\Sigma_{r|m}$ are defined as
\begin{align} \label{eq:normalfactorizationparameters}
\mu_{r|m}(x) &= \mu_r + C^T \Sigma_m^{-1}(x-\mu_m) \\
\Sigma_{r|m} &= \Sigma_r - C^T \Sigma_m^{-1} C
\end{align}
We also know from a theorem above that matched joint density between the fast and slow variable can be written as the product of the matched distribution with only the slow variable and the conditional density defined in the equation above. So we only need to care about the slow variables. For this, we also know the matching a prior Gaussian density on a new mean $\mu_m^*$ and covariance matrix $\Sigma_m^*$ is also Gaussian distribution with this mean and covariance. Symbolically this yields
\begin{equation} \label{eq:matchingslowgaussian}
\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu_m, \Sigma_m}) = \mathcal{N}_{\mu_m^*,\Sigma_m^*}.
\end{equation}
Hence the joint matched density can be expressed as
\begin{equation} \label{eq:matchingfactorization}
\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu, \Sigma})(z) = \mathcal{N}_{\mu_m^*,\Sigma_m^*}(x)\mathcal{N}_{\mu_{r|m}(x),\Sigma_{r|m}}(y).
\end{equation}
We would like that this matched density is also a Gaussian distribution with mean $\mu^* = \begin{bmatrix} \mu_m^* \\ \mu_r^* \end{bmatrix}$ and covariance $\Sigma^* = \begin{bmatrix} \Sigma_m^* & C^* \\ C^{*T} & \Sigma_r^* \end{bmatrix}$. A similar factorization as for the prior distribution in \ref{eq:normalfactorization} gives
\begin{equation} \label{eq:jointfactorization}
\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu, \Sigma})(z) = \mathcal{N}_{\mu^*,\Sigma^*} = \mathcal{N}_{\mu_m^*,\Sigma_m^*}\mathcal{N}_{\mu_{r|m}^*(x), \Sigma_{r|m}^*}
\end{equation}
Where the parameters $\mu_{r|m}^*(x)$ and $\Sigma_{r|m}^*$ are defined similarly as in equation \ref{eq:normalfactorizationparameters}:
\begin{align} \label{eq:jointfactorizationparameters}
\mu_{r|m}^*(x) &= \mu_r^* + C^{*T} \Sigma_m^{* -1}(x-\mu_m^*) \\
\Sigma_{r|m} &= \Sigma_r^* - C^{*T}\Sigma_m^{* -1} C^*.
\end{align}
The first factor in equation \ref{eq:matchingfactorization} is already equal to the first factor in the previous equation, hence we also want conditional densities to be equal. This is equivalent to demanding that $\mu_{r|m}(x)=\mu_{r|m}^*$ and $\Sigma_{r|m}=\Sigma_{r|m}^*$. Expanding these terms then yields the following system with unknowns $\mu_r^*$, $\Sigma_r^*$ and $C^{*T}$:
\begin{equation} \label{eq:propositionsystem}
\begin{cases}
\mu_r+C^T\Sigma_m^{-1}(x-\mu_m) = \mu_r^* + C^{*T}\Sigma_m^{*-1}(x-\mu_m^*) \\
\Sigma_r - C^T\Sigma_m^{-1}C = \Sigma_r^* - C^{*T}\Sigma_m^{*-1}C^*
\end{cases}.
\end{equation}
The first equation is a linear polynomial in $x$, implying that $C^{*T}=C^T\Sigma_m^{-1}\Sigma_m^*$. Also equalling the constant term in the first equation then gives $\mu_r^* = \mu_r + C^T\Sigma_m^{-1}(\mu_m^*-\mu_m)$ and finally the second equation gives the correct value for $\Sigma_r^*$. If we use these values for the unknowns, then equation \ref{eq:jointfactorizationparameters} defines a conditional factorization of a normal distribution, meaning that $\mathcal{M}([\mu_m^*, \Sigma_m^*]^T, \mathcal{N}_{\mu, \Sigma})(z)$ will also be normally distributed with mean $\mu^*$ and covariance $\Sigma^*$.

For further use, the complete set of equations after one complete step of the micro-macro acceleration algorithm is
\begin{align}
\begin{cases}
\mu_r^* &= \mu_r + C^T \Sigma_m^{-1}(\mu_m^* - \mu_m) \\
C^{*T} &= C^T \Sigma_m^{-1}\Sigma_m^* \\
\Sigma_r^* &= \Sigma_r - C^T\Sigma_m^{-1}(\Sigma_m-\Sigma_m^*)\Sigma_m^{-1}C
\end{cases}
\end{align}
\end{proof}
\end{document}