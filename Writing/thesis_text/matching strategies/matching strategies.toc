\select@language {english}
\contentsline {chapter}{\numberline {1}Matching Strategies}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Mathematical Preliminaries}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Matching in $L_2$ norm (L2N)}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Derivation of a formula for L2N matching}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Non-negativity, continuity and consistency of L2N matching}{5}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Matching with f-divergence}{6}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Kullback-Leibler divergence}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}$L_2$ divergence}{8}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Numerical Illustration: FENE Dumbbels}{8}{section.1.4}
\contentsline {paragraph}{Euler-Maruyama Simulation}{9}{section*.2}
\contentsline {paragraph}{Comparison of matching strategies}{10}{section*.3}
\contentsline {paragraph}{Matching accuracy for a varying number of moments}{12}{section*.6}
\contentsline {paragraph}{Moment accuracy for a varying number of moments}{12}{section*.7}
\contentsline {paragraph}{Accuracy dependency on the extrapolation step}{12}{section*.9}
