\documentclass{kulakreport}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[makeroom]{cancel}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{geometry} 
\usepackage{amsthm}
\usepackage{cite}
\usepackage{algorithm}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newtheorem{definition}{Definition}
\theoremstyle{proposition}
\newtheorem*{proposition}{Proposition}
\theoremstyle{lemma}
\newtheorem*{lemma}{Lemma}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}
\theoremstyle{theorem}
\newtheorem*{theorem}{Theorem}

\begin{document}
	\title{Matching strategies}
	\author{Hannes Vandecasteele}
	\tableofcontents
\chapter{Matching Strategies}
The idea of a matching operator $\mathcal{P}_L(m, \pi)$ is to map a number of moments $m$ and a prior distribution $\pi$ to a new distribution $\varphi$ that is consistent with these moments, $\mathcal{R}_L(\varphi) = m$ and deviates the least from the prior. There are many ways of quantifying this deviation. One way is for example minimizing the $L_2$ norm of the difference $\varphi-\pi$, or another class of methods minimizes the so called f-divergence between two distributions. This is a functional defined on the two densities $\pi$ and $\varphi$. It is however also important that the matching operator is continuous and consistent with the given SDE. This chapter will study both matching strategies ($L_2$ norm and f-divergence) and will prove several properties of matching in 2-norm. Properties about f-divergence have not been proven yet. The chapter starts with terminology, after which $L_2$ matching is introduced with proofs of several properties. Then follows matching in f-divergence and the chapter closes with a practical problem on which these methods are tried.

\section{Mathematical Preliminaries}
Matching is inherently an optimization problem so it is necessary to precisely define what the optimization objective is and over what function space of density functions we are minimizing. Furthermore, the extrapolated moments $m$ serve as an equality constraint to the minimization problem. The notation used here is similar to the fourth section in \cite{debrabant2015micro}.

\vspace{2mm}
\noindent
Let $L^p(G,\mu)$ denote the standard Lebesgue space with Borel measure $\mu$ of integrable functions over the open and bounded set $G$ with standard norm $\norm{\cdot}_p$ and let $\mathcal{P}^p(G) \subset L^p(G)$ be the convex subspace of $p-$th power integrable density functions. Also define the cone of non-negative functions as $L_+^p(G)$. One key aspect of density functions is that they are non-negative everywhere and have mass (integral) one. For Lebesgue spaces $L^p(G)$ there exists a dual space isomorphic to $L^q(G)$ with $\frac{1}{p}+\frac{1}{q}=1$ but for $p=2$ as for matching in 2-norm this dual space is isomorphic to itself. Finally denote the so called \textit{dual pairing} between $\pi \in L^p(G)$ and $\phi \in L^q(G)$ by
\begin{equation}
\langle \pi, \varphi \rangle = \int_G \pi(x)\varphi(x)\mu(dx).
\end{equation}
When $p=2$ this coincides with the standard inner product in $L_2$ but this is in general not an inner product for $p \neq 2$.

\vspace{2mm}
\noindent
One problem with solving the functional minimization problem is that is does not necessarily impose that the obtained density needs to be a density function. Extra restrictions are needed to ensure the obtained function has mass one and is non-negative everywhere. We can readily deal with the first requirement by extending the restriction operator $\mathcal{R}_L(\varphi) = (\mathbb{E}_{\varphi}(R_1), \dots, \mathbb{E}_{\varphi}(R_L))$ with the zero-th moment $R_0 = 1$ to $\mathcal{\tilde{R}}_L: \mathcal{P}^p \to \mathbb{R}^{L+1}$. The extended restriction operator can then be written as 
\begin{equation} \label{eq:extendedrestrictionoperator}
(\mathcal{\tilde{R}}_L(\varphi))_l = \mathbb{E}_{\varphi}(R_l) = \int_G R_l(x) \varphi(x)dx, \ l = 0 \dots L
\end{equation}
This way, the solution to the minimization problem will have mass one. The second requirement of non-negativity is a lot harder to handle and the solution will be specific to the exact minimization objective. For example for matching in 2-norm it is not readily possible to incorporate this constraint in the objective but it is for some forms of matching in f-divergence. The next sections go deeper into this problem.

\section{Matching in $L_2$ norm (L2N)}
Matching in $L_2$-norm, also denoted by 'L2N' is possibly the easiest way of matching. Given a prior distribution $\pi \in \mathcal{P}^2$ and a (not extended ) vector of moments $m \in \mathbb{R}^L$ obtained by evaluating a sequence of linearly independent moment functions $(R_l)_{l=1}^{\infty}$ until a fixed value for $L \in \mathbb{N}$ for some density $\Psi$, i.e. $\mathcal{R}_L(\Psi) = m$. The optimization problem then reads
\begin{align} \label{eq:l2nobjective}
\begin{split}
\underset{\varphi \in L_2(G)}{\text{min}} \ \ &\norm{\varphi-\pi}_2 \\
\text{subject to} \ \ &\mathcal{\tilde{R}}_L(\varphi) = (1, m)
\end{split}
\end{align}
where we now used the extended restriction operator $\mathcal{\tilde{R}}_L$ for unit mass. To derive the exacted expression for the matched density $\varphi$ however, another formulation of the optimization problem is more useful with $\phi = \varphi-\Psi$
\begin{align} \label{eq:l2nchangedobjective}
\begin{split}
\underset{\phi \in L_2(G)}{\text{min}} \ \ &\norm{\phi - (\pi-\Psi)}_2 \\
\text{subject to} \ \ &\mathcal{\tilde{R}}_L(\phi) = 0 \in \mathbb{R}^{L+1}
\end{split}
\end{align}
\subsection{Derivation of a formula for L2N matching}
To derive an analytical expression for $\varphi$ we can safely assume that the moment functions $(R_l)_{l=0}^{\infty}$ form a basis for $L_2(G)$ since they are already linearly independent. Call the functions $(Q_l)_{l=0}^{\infty}$ the resulting functions by applying the Gram-Schmidt orthogonalization procedure on  $(R_l)_{l=0}^{\infty}$. More specifically we have
\begin{equation} \label{eq:gramschmidt}
Q_l = \sum_{j=0}^l \hat{Q}_{l,j} R_l
\end{equation}
where the matrix $\hat{Q}$ is lower triangular. 

\vspace{2mm}
\noindent
It is possible to show (see \cite{atkinson2005theoretical}) that the minimizer of \ref{eq:l2nchangedobjective} can be written as
\begin{equation} \label{eq:changedl2nobjectivesolution}
\phi(m, \pi) = \sum_{l=L+1}^{\infty} \langle \pi-\Psi, Q_l \rangle Q_l.
\end{equation}
and hence the solution $\varphi(m, \pi)$ of the first optimization problem \ref{eq:l2nobjective} can also be easily expressed in the orthogonal basis as
\begin{align} \label{eq:phidecomposition}
\begin{split}
\varphi(m, \pi) &= \Psi + \phi(m, \pi) \\
 &= \sum_{l=0}^{\infty}\langle \Psi, Q_l \rangle \ Q_l + \sum_{l=L+1}^{\infty} \langle \pi - \Psi, Q_l \rangle \ Q_l \\ 
 &= \sum_{l=0}^{L}\langle \Psi, Q_l \rangle \ Q_l + \sum_{l=L+1}^{\infty} \langle \pi , Q_l \rangle \ Q_l  \\ 
 &= \sum_{l=0}^L \langle \Psi-\pi, Q_l \rangle \ Q_l + \pi.
 \end{split}
\end{align}
This is in principle the exact solution to \ref{eq:l2nobjective} but is not practical since we need all the Gram-Schmidt coefficients $\hat{Q}_{l,j}$ and the density $\Psi$. It is better to transform the former expression back to the original basis. This can be achieved by using the basis transformation matrix $\hat{Q}_L$ from $(R_l)_{l=0}^{L}$ to $(Q_l)_{l=0}^{L}$ which is the upper left $(L+1) \times (L+1)$-matrix of $\hat{Q}$.  Writing $Q_l = \sum_{j=0}^l R_l$ yields by linearity of the inner product
\begin{align*} \label{eq:l2nbasistransform}
\varphi(m, \pi) &= \sum_{l=0}^L \sum_{i,j=0}^L \langle \Psi - \pi, R_l \rangle \hat{Q}_{l,j} \hat{Q}_{l, i} R_i \\
 &= \sum_{i=1}^L \left ( \sum_{j=0}^L(\hat{Q}_L^T \hat{Q}_L)_{i,j} \langle \Psi-\pi, R_j \rangle \right ) R_j + \pi \\
 &= (H_L^{-1} \langle \Psi-\pi, \boldsymbol{R} \rangle)^T \boldsymbol{R} + \pi
\end{align*}
where we defined $H_L^{-1} = \hat{Q}_L^T \hat{Q}_L$ and $\boldsymbol{R} = (R_0, \dots,  R_L)^T$. This finally leads to the very short expression for the matched density in $L_2$-norm
\begin{equation} \label{eq:l2nformula}
\varphi(m, \pi) = \sum_{l=0}^L \lambda_l R_l + \pi = \left(\frac{\sum_{l=0}^L \lambda_l R_l}{\pi} + 1\right) \pi
\end{equation}
where the values $\lambda_l$ can be interpreted as Lagrange multipliers of the optimization problem \ref{eq:l2nobjective}. Practically these values are the solution of the system
\begin{equation} \label{eq:l2nlagrangemultipliers}
H_L \lambda = \langle \Psi-\pi, \boldsymbol{R} \rangle = (0, m - \mathcal{R}_L(\pi))
\end{equation}
because the moments of $\Psi$ are exactly $m$ too by \ref{eq:l2nchangedobjective}. Finally the matrix $H_L$ is known too in practice by the Gram-Schmidt procedure:
\[
(H_L)_{j,k} = \int_G R_k(x) R_l(x) dx
\]
as $I = \int_G\boldsymbol{Q}\boldsymbol{Q}^Tdx = \int_G \hat{Q}_L \boldsymbol{R} \boldsymbol{R}^T \hat{Q}_L^Tdx = \hat{Q}_L \int_G \boldsymbol{R} \boldsymbol{R}^T dx \ \hat{Q}_L^T$.

\vspace{2mm}
\noindent
L2N matching is thus very easy to implement in practice as it only requires to solve a linear system of equations to obtain the Lagrange multipliers and the integral entries of the matrix can be integrated using a standard Monte-Carlo approach using the particles available from the discretization of the prior distribution.

\begin{remark} When the prior is discretized by an ensemble of particles $(X_j, w_j)_{j=1}^J$, then by formula \ref{eq:l2nformula} the weights of the matched distribution with Lagrange multipliers $\lambda$ are
	\begin{equation*}
	w_j(\lambda) = w_j\left(\frac{\sum_{l=0}^L \lambda_l R_l(X_j)}{\pi(X_j)} + 1\right) 
	\end{equation*}
	One big problem here is that we need to evaluate the prior distribution in a particle value $X_j$ and this requires some form of density estimation of the prior. Density estimation relies heavily on the value of certain parameters (like the bandwidth for kernel density estimation) and this can introduce significant errors that are higher than the discretization error of the SDE. This usually happens when the continuous density under or overfits the data points of the distribution which can occur with a bandwidth that is too high or low respectively. More details follow in the next chapter.
\end{remark}

\begin{remark}
	Note that it is important that $p=2$ in the formulas above because the derivation makes explicitly use of inner products and the orthogonal basis $(Q_l)_{l=0}^{\infty}$. When $p \neq 2$ the space $L^p(G)$ is no longer a Hilbert space and orthogonality is not defined. The $p-$norm of course remains to exist but the derivation does not hold anymore.
\end{remark}
\subsection{Non-negativity, continuity and consistency of L2N matching}
Adding an extra unit component to the vector of moments can ensure that the matched density has mass one but in principle it can still be negative in some parts of the domain $G$. It is not possible to incorporate this non-negativity requirement in the optimization problem itself, but it can be ensured when the prior distribution is bounded away from zero everywhere on its domain and when the extrapolated moments do not deviate too far from the moments of the prior. The following lemma puts this more mathematically accurate. This lemma was also taken from \cite{debrabant2015micro}.

\begin{lemma} Assume that $\pi \geq c$ almost everywhere on $G$ for a certain constant $c > 0$. Then there exists a constant $\delta = \delta(L, G, \boldsymbol{R})$ such that if $|m - \mathcal{R}_L(\pi)| < \delta$ then $\varphi(m, \pi) > 0$ almost everywhere.
\end{lemma}
\noindent
The problem in practice is that it is very difficult to ensure that the new moments $m$ don't deviate too far from the prior moments $\mathcal{R}_L(\pi)$ and the lemma practically says that the extrapolation step $\Delta t$ should not be too big. This is contrary to the fact that we would like to extrapolate as much as possible and is a big downside of L2N matching.

\vspace{2mm}
\noindent
The upside of L2N matching is that it is possible to prove the continuity and consistency which is a sufficient condition for the convergence theorem of last chapter. The following lemma states this property with proof, while there is no proof for the consistency property. The proof can however be found in \cite{debrabant2015micro} and is extremely technical in nature. The lemma and proof were also taken from \cite{debrabant2015micro}.
\begin{lemma}
	Let $\pi,\Psi,\Psi' \in \mathcal{P}^2(G)$, and let $m_L =\mathcal{R}_L(\Psi), m_L' =\mathcal{R}_L(\Psi')$ with $L \in \mathbb{N}$, then
	\[
	\norm{\varphi(m_L \pi) - \varphi(m_L', \pi)}_2 \leq \norm{\hat{Q}_L}_2 \norm{m_L - m_L'}_2
	\]
\end{lemma}
\begin{proof}
	Let $\pi_l, \Psi_l$ and $\Psi_l'$ denote the coefficients in the expansion of the respective densities in the basis $(Q_l)_{l=0}^{\infty}$. Then by noticing that only the first $L+1$ coefficients in third line of \ref{eq:phidecomposition} are different for $\varphi(m_L \pi)$ and  $\varphi(m_L', \pi)$, we can write
	\[
	\norm{\varphi(m_L \pi) - \varphi(m_L', \pi)}_2^2 = \norm{\sum_{l=0}^L (\Psi_l - \Psi_l')Q_l}_2^2
	\]
	and then by the well known Parsival's identity (which holds because the basis is orthogonal) the right hand side equals
	\[
	\norm{\sum_{l=0}^L (\Psi_l - \Psi_l')Q_l}_2^2 = \sum_{l=0}^L (\Psi_l - \Psi_l')^2.
	\]
	Notice again that by the Gram-Schmidt decomposition \ref{eq:gramschmidt} it is possible to write $(\Psi_0, \dots \Psi_L) = \hat{Q}_Lm_L$ and likewise for $\hat{Q}_L m_L'$ by taking inner products with $\Psi$ and $\Psi'$ respectively on the left and right hand side. This implies that
	\[
	\sum_{l=0}^L (\Psi_l - \Psi_l')^2 = \norm{\hat{Q}_L(m_L - m_L')}_2^2
	\]
	and the proof follows by taking the factor $\hat{Q}_L$ out of the right hand side which causes the inequality. L2N matching is by consequence continuous.
\end{proof}
\noindent
This concludes the part on L2N matching. A numerical illustration of the method is given at the end of the chapter.

\section{Matching with f-divergence}
Besides matching in $L_2$-norm which has some problems with non-negativity there is another approach called matching in \textit{f-divergence}. The objective here is not to minimize a norm on a function space but to minimize a certain functional $\mathcal{I}_f$:
\begin{align} \label{eq:fdivergenceobjective}
\mathcal{P}_L(m, \pi) = \underset{\varphi \in \mathcal{R}_L^{-1}(m)}{\text{argmin}} \mathcal{I}_f(\varphi | \pi)
\end{align}
where the functional $\mathcal{I}_f(\varphi|\pi)$ for a certain function $f: [0, +\infty) \to [0, +\infty)$ is defined as
\begin{equation} \label{eq:fdivergencefunctional}
\mathcal{I}_f(\varphi|\pi) = \begin{cases}
\int_G f \left( \frac{\varphi(x)}{\pi(x)}\right) \pi(x) dx \ \ \ \text{when supp} \ \varphi \subset \text{supp} \ \pi \\
\\
+\infty \ \ \text{otherwise.}
\end{cases}
\end{equation}
The condition $\text{when supp} \ \varphi \subset \text{supp} \ \pi$ states that the support of $\pi$ should include the support of $\varphi$ and implies absolute continuity of the latter with respect to the former density. It is important for the uniqueness of the solution of \ref{eq:fdivergenceobjective} that $f$ is not identically zero, convex and $f(0)=1$. 

\vspace{2mm}
\noindent
To solve the optimization problem, denote $f_+$ as the extension of $f$ to negative values such that $f_+(x) = +\infty$ whenever $x < 0$.  This incorporates the non-negativity of $\varphi$ in the minimization problem. The constraint that the mass of $\varphi$ should be one can again be handled by introducing the extended restriction operator $\mathcal{\tilde{R}}_L$. Both requirements make sure that the solution of \ref{eq:fdivergenceobjective} is a density function if it exists. The derivation of the general solution uses a lot of elements from functional analysis and is beyond the scope of this thesis. It can be found in much detail in Appendix A of \cite{debrabant2015micro}, but the main result states
\begin{equation} \label{eq:fdivergencesolution}
\mathcal{P}_L(m, \pi) = (f_+^*)' \left(\sum_{l=0}^L \lambda_l R_l \right) \pi
\end{equation}
where $f_+^*$ is the \textit{convex conjugate} of $f_+$. The values $\lambda_l, \ l = 0, \dots, L$ are again Lagrange multipliers and are the solution of the highly non-linear system
\begin{equation} \label{eq:fdivergencelagrange}
\mathcal{\tilde{R}}_L\left((f_+^*)' \left(\sum_{l=0}^L \lambda_l R_l \right) \right) = \tilde{m}
\end{equation}
with $\tilde{m} = (1,m)$ the extended vector of moments. This equation  follows from the KKT conditions and basically says that the matched distribution needs to have the predefined moments $m_l$Because the minimization problem is unique, system \ref{eq:fdivergencelagrange} also has a unique solution of Lagrange multipliers, which can in practice be found by using a Newton-Raphson method. The iterative scheme usually converges in a few iterations. The next sections go deeper in several instantiations of the function $f$ that are used in practice: Kullback-Leibler and $L_2$ divergence.

\begin{remark}
	The f-divergence functional does in general not constitute a distance metric between two densities $\varphi$ and $\pi$ because it even fails to have the symmetry property. One possible way of getting around this is by symmetrising the definition by adding $\mathcal{I}_f(\pi|\varphi)$ but this is not used in practice, although it may have applications in the case of Kullback-Leibler divergence of the next section. Having a distance metric is however no stringent requirement in the framework of matching and f-divergence will yield very good results later on.
\end{remark} 

\subsection{Kullback-Leibler divergence}
One popular instantiation of $f-$divergence is the so-called Kullback-Leibler divergence or KLD for short. It has many applications in statistics, information theory and machine learning and is a measure for how much information, or relative entropy, a density $\varphi$ adds to another distribution $\pi$. In this case is the function $f$ becomes
\begin{equation} \label{eq:kldfunction}
f(t) = \begin{cases} t \ln t - t + 1,  \ \ \ t > 0 \\
0, \ \ \ t = 0.
\end{cases}
\end{equation}
Note that $f$ is indeed convex and $f(1)=0$. The minimization hence becomes
\begin{equation} \label{eq:kldobjective}
\mathcal{P}_L(m, \pi) = \underset{\varphi \in \mathcal{R}_L^{-1}(m)}{\text{arg min}} \int_G \ln \left(\frac{\varphi(x)}{\pi(x)}\right) \pi(x)dx.
\end{equation}
The idea of relative entropy has already been exploited in \cite{samaey2011numerical} in the context of FENE dumbbels. There the authors perform a constraint simulation after lifting and they show that it maximizes the relative entropy. The convex conjugate of $f_+$ is $f_+^(s) = \exp(s)-1$ (see \cite{debrabant2015micro}, Appendix A) so by formula \ref{eq:fdivergencesolution} the matched distribution becomes
\begin{equation} \label{eq:kldsolution}
\mathcal{P}_L(m, \pi) = \exp \left(\sum_{l=0}^L \lambda_l R_l \right) \pi
\end{equation}
where the Lagrange multipliers $\lambda_l$ satisfy the non-linear system
\begin{equation} \label{eq:kldlagrange}
\int_G R_l(x) \exp \left(\sum_{l=0}^L \lambda_l R_l(x) \right) \pi(x) dx = m_l, \ \ l = 0,\dots,L.
\end{equation}
The integrals in this expression arise because the extend restriction operator from \ref{eq:fdivergencelagrange} are expectation and hence integrals of well chosen moment functions $R_l$. Also by convention $R_0(x)=1$ so it is possible to extract a factor $\exp(\lambda_0)$ out of the integral in \ref{eq:kldlagrange} but this has no further consequences. The above system is again highly non linear and needs to be solved by an iterative method such as Newton-Raphson.


\subsection{$L_2$ divergence}
Another possible instantiation of f-divergence is by choosing for $f$
\begin{equation} \label{eq:l2dfunction}
f(t) = \frac{1}{2}(t-1)^2
\end{equation}
such that for $\varphi, \ \pi \in \mathcal{P}^2(G)$
\begin{equation} \label{eq:l2ddivergence}
\mathcal{I}_f(\varphi|\pi) = \frac{1}{2} \int_G \left(\frac{\varphi(x)}{\pi{x}}-1\right)^2 \pi(x) dx.
\end{equation}
This matching strategy goes by the name of $L_2$-divergence matching (L2D for short)and by the same reasoning as KLD matching we now have $f_+^*(s) = \frac{1}{2}(\text{max}\{0, s+1\}^2-1)$. Again note that $f$ is convex due to the \texttt{max} term and $f(1)=0$ so it complies with all assumptions of f-divergence matching. Thus by formula \ref{eq:fdivergencesolution} the solution to L2D matching is
\begin{equation} \label{eq:l2dsolution}
\mathcal{P}_L(m, \pi) = \text{max} \left(0, \sum_{l=0}^L \lambda_l R_l \right) \pi
\end{equation}
with the Lagrange multipliers now given by
\begin{equation} \label{eq:l2dlagrange}
\int_G R_l(x) \text{max}\left( 0, \sum_{l=0}^L \lambda_l R_l(x) \right) \pi(x) dx = m_l.
\end{equation} 

\section{Numerical Illustration: FENE Dumbbels}
Stochastic multiscale processes occur frequently in nature and the next numerical experiment will try out pure matching on one such example: FENE dumbbels. The next chapter then contains experiments concerning the complete micro-macro acceleration algorithm. Paper \cite{debrabant2015micro} contains the same numerical example and the results here serve as a confirmation of this result and a verification of the implementation of this thesis at the same time.

\vspace{2mm}
\noindent
FENE stands for 'Finitely Extensible Non-linear Elastic' dumbbels, which are polymer chains moving through a solvent in which they are immersed. The stochastic variable $X(t)$ represents the length of the chain which is modelled as two beads connected by a non-linear spring. This spring force is modelled as
\begin{equation} \label{eq:fenespringforce}
F: B(\sqrt{b}) \to \mathbb{R}^d, x \mapsto F(x) = \frac{b}{b-\norm{x}_2^2}x
\end{equation}
with $\sqrt{b}$ the maximum length the dumbbels may have. The stiffness in this problem arises by the strong boundary conditions near $\sqrt{b}$ since the spring force becomes infinite there. Small time steps are then necessary to simulate the behaviour of the Monte Carlo particles accurately. Besides the spring force, the polymers also experience Stoke drag and Brownian motion. The complete SDE for this problem is
\begin{equation} \label{eq:fenesde}
dX(t) =  \left(\kappa(t)X(t)-\frac{1}{2W_e} F(X(t))\right) dt +\frac{1}{\sqrt{W_e}} dW(t), \ t \in [0, T ]
\end{equation}
where $\kappa(t)$ is the time-dependent velocity field due to Stokes drag and $W_e$ is the Weissenberg number. This equation is normally coupled to the Navier-Stokes equation describing the motion of the fluid but this is computationally very expensive and is not considered here. The coupling with the Navier-Stokes equation happens through the stress tensor, which is of most interest in practice
\begin{equation} \label{eq:fenestresstensor}
\tau = \frac{1}{W_e} \left( \mathbb{E}[X \otimes F(X)] - I_d \right)
\end{equation}
where the cross operator is the vector outer product and $I_d$ the $d \times d$ unit matrix.

\vspace{2mm}
\noindent
The following experiment consists of comparing the true density after 1.1 seconds with the three different matching strategies above with prior density the exact distribution after 1 second and a varying number of moments. The velocity field is kept constant $\kappa(t)=2$ and the Weissenberg number is 1. The exact densities are obtained by performing an Euler-Maruyama integration with initial condition the invariant distribution with zero velocity field and with time step $\delta t = 2 \ 10^{-4}$. This initial distribution is given by
\begin{equation} \label{eq:feneinitial}
X_0(r) = \frac{1}{Z} \exp\left(2W_eU(r)\right)
\end{equation}
where $U(r)$ is the potential energy function associated with the spring force $F(r) = \nabla U(r)$. A standard accept-reject sampling strategy suffices to sample the initial distribution with $N = 10^5$ independent particles.

\paragraph{Euler-Maruyama Simulation} A standard Euler-Maruyama simulation to obtain the densities after 1 and 1.1 seconds is not consistent with the stochastic model because the particles may slip out the domain $[-\gamma, \gamma]$, $\gamma = \sqrt{b}$. Therefore an accept-reject strategy is necessary to ensure the Monte Carlo particles remain in the feasible domain. First the method increments time for each particle by
\begin{equation} \label{eq:feneeulermaruyama}
X^{k+1} = X^k + (\kappa(t^k)X^k - \frac{1}{2}F(X^k))dt + \sqrt{\delta t} \xi^k, \ \ \xi^k \sim \mathcal{N}(0, 1)
\end{equation}
and then decides to accept or reject the new sample $X^{k+1}$ if it goes out of the domain $B(\sqrt{b})$. In practice this is achieved by rejecting a particle when $\norm{X^{k+1}} > \alpha \sqrt{b}$ with $\alpha <1$ to avoid very large spring forces that will result in rejections the next time step. The value of $\alpha$ should be close to 1 to get a consistent numerical scheme and the smaller the time step $\delta t$ the closer the particles may go to the boundaries since the Brownian increments are on average smaller too. In \cite{debrabant2015micro} they therefore propose to take $\alpha = 1 - \sqrt{\delta t}$. Upon rejection of a particle $\norm{X^{k+1}}$ the Euler-Maruyama step is repeated until the truncation accepts the particle.

\vspace{2mm}
\noindent
Figure .. shows the densities of the model after 0, 1 and 1.1 seconds. The distribution was obtained by kernel density estimation with a bandwidth of $0.01\gamma$. The plots are not so sensitive to the exact value of the bandwidth as long as it is not orders of magnitude higher or lower. As time increments there clearly occurs a peak close to the maximal polymer length so many particle are near the boundary. This equilibrium origins from the interaction between the spring force that keeps the particles inside the domain and the velocity fields that elongates the polymers. This is a visual argument that the step size should remain small.

\paragraph{Comparison of matching strategies}
Figure \ref{fig:fenedensities} compares the three different matching strategies from above, L2N, KLD and L2D. The experiment takes three moments into account. These serve as the extrapolated moments in the acceleration algorithm and are the moments of the exact distribution after 1.1 seconds, given by the  normalized moment functions 
\begin{equation} \label{eq:fenemomemtfunctions}
R_l(x) = \left({\frac{x}{\gamma}}\right)^{2l}, \ \ l = 1 \dots 3.
\end{equation}
 and the prior distribution for the experiment is the exact density after 1 second. Exact in this context means the density obtained with an Euler-Maruyama simulation from above. Odd moment functions should yield zero in expectation because every transient distribution of the FENE model is symmetric if the initial condition is symmetric around zero. The matching algorithm also takes the constant moment function $R_0(x) = 1$ into account to make the matched distribution have unit mass as discussed in the first section but this is always implicitly assumed.
 \begin{figure}
 	\centering
 	\includegraphics[width=0.6\linewidth]{../../Source/Python/Samples/fene/plots/fene_densities}
 	\caption{Probability densities for the FENE model \ref{eq:fenesde} with initial condition \ref{eq:feneinitial} after 0, 1 and 1.1 seconds. The plot was obtained my a kernel density estimation with $10^5$ particles and a bandwidth of $0.2$.}
 	\label{fig:fenedensities}
 \end{figure}
 
 \vspace{2mm}
 \noindent
 The experiment shows that L2N matching yields a density that lies far from the exact density especially where they peek. This is not the case matching in f-divergence which is a visual argument that KLD and L2D matching may perform better.
  \begin{figure}
 	\centering
 	\subcaptionbox{L2N\label{fig1:a}}{\includegraphics[width=0.5\textwidth]{../../Source/Python/Samples/fene/plots/l2n}}\hfill%
 	\subcaptionbox{KLD\label{fig1:b}}{\includegraphics[width=0.5\textwidth]{../../Source/Python/Samples/fene/plots/kld}}
 	\begin{subfigure}[b]{0.5\textwidth}
 		\includegraphics[width=\linewidth]{../../Source/Python/Samples/fene/plots/l2d}
 		\caption{L2D}
 		\label{fig:l2d}
 	\end{subfigure}
 	\caption{Matching results for the three different strategies and a varying number of moments.}
 	\label{fig:fenemoments}
 \end{figure}

 \paragraph{Matching accuracy for a varying number of moments} The next experiment has the same set-up as the previous one but now the number of (even) moment functions varies from 3 to 7. Figure \ref{fig:fenemoments} shows the experimental results for L2N, KLD and L2D matching. L2N seems to need many more moments than matching in f-divergence before it reaches an acceptable approximation of the exact density while f-divergence matching yields very good results even for relatively few extrapolated moments. This is one reason, together with the possible negativity of L2N matching that this thesis focusses on relative entropy matching.
  
 \paragraph{Moment accuracy for a varying number of moments} The third experiment again uses the exact distribution after 1.0 second as a prior and takes the exact normalized even moments at time 1.1 seconds as extrapolated moments.. We now look at the relative error 
 \begin{equation}
 \frac{|m_l - m_l^*|}{m_l^*}
 \end{equation}
 between the moments of the exact $m_l^*$ and matched distribution $m_l$ at 1.1 seconds. Figure \ref{fig:fenemomentconvergence} shows the experimental results averaged over 20 i.i.d. runs for the three matching strategies and a varying number of extrapolated even moments \ref{eq:fenemomemtfunctions}. The results show that for a moment function with $l \leq L$ the error between the matched and exact distribution is under the tolerance of the Newton-Raphson solver, which indicates convergence, but for higher order moments the error suddenly increases very rapidly. This is expected since these higher order moments are not taken into account while matching. The error for moments $l > L$ however decreases with increasing $L$ indicating that adding more moments while matching may have an impact on the accuracy of higher moments with $l >L$.
 
 \begin{figure}
 	\centering
 	\begin{subfigure}[b]{0.5\textwidth}
 	\includegraphics[width=\linewidth]{../../Source/Python/Samples/fene/plots/l2n_momentconvergence}
 	\caption{L2N matching.}
 	\label{fig:l2nmomentconvergence}
 \end{subfigure}%
 	\begin{subfigure}[b]{0.5\textwidth}
 	\includegraphics[width=\linewidth]{../../Source/Python/Samples/fene/plots/fdivergence_momentconvergence}
 	\caption{F-divergence matching.}
 	\label{fig:fdivergencemomentconvergence}
 	\end{subfigure}
 \caption{Relative error of even normalized moments for the different matching strategies as a function of the number of extrapolated moments $L$.}
 \label{fig:fenemomentconvergence}
 \end{figure}
 
 \paragraph{Accuracy dependency on the extrapolation step} Finally we look at how the matching error depends on the extrapolation time step $\Delta t$ for a fixed microscopic step $\delta t = 2 \ 10^{-4}$. By theorem ...  the error of a well behaved function $f$ should decrease linearly with $\Delta t$ if this is the dominant term in the error bound. This experiment considers the relative error of the stress tensor $|\tau(t) - \hat{\tau}(t)| / \tau(t)$ between the matched distribution $\hat{\tau}(t)$ and the exact density $\tau(t)$ with extrapolation step $\Delta t \in [5\delta t, 500 \delta t]$. Figure \ref{fig:fenetimeconvergence} shows the results for the three different matching strategies and $L=3,5,7$ normalized even moments, averaged over 10 i.i.d. simulations. The convergence for f-divergence is indeed roughly first order and the error decreases with increasing number of moments $L$. This is due to the constant $C_L$ from theorem that decreases uniformly with increasing $L$. The convergence for L2N is more complex but also attains almost first order for small $\Delta t$, confirming the theorem. Also note that the errors are almost an order of magnitude lower for KLD and L2D matching compared to matching in $L_2$-norm.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/fene/plots/l2n_convergence}
	\caption{L2N}
	\label{fig:l2nconvergence}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}
	\includegraphics[width=\linewidth]{../../Source/Python/Samples/fene/plots/fene_fdivergence_convergence}
	\caption{KLD and L2D}
	\label{fig:fenefdivergenceconvergence}
	\end{subfigure}
\caption{Relative error of the stress tensor as a function of the extrapolation time step $\Delta t$ for the three matching strategies and varying number of moment functions.}
\label{fig:fenetimeconvergence}
\end{figure}

 
\bibliography{december}{}
\bibliographystyle{plain}
\end{document}